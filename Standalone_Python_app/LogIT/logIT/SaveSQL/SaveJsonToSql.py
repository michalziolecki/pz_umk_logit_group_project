from logIT.Network.JsonNetwork import JsonConverter
from logIT.develop_log.dev_log import Logger
from logIT.db_models_entities.Pingi import PingiSQL
from logIT.db_models_entities.Info_Current_Process import CurrentProcessSQL
from logIT.db_models_entities.Info_User_Process import UserProcessSQL
from logIT.db_models_entities.InfoCard import InfoCardSQL
from logIT.db_models_entities.CapacityMonitor import CapcityMonitorSQL
from logIT.db_models_entities.TransferMonitor import TransferMonitorSQL
from logIT.Network.JsonNetwork import JsonObjectClass
from logIT.process.Process import ProcessJson


# Mati

class SavePingJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = PingiSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    ping = JsonObjectClass.Ping(ping=elements['ping'], address=elements['address'],
                                                data=elements['data'], host=elements['host'], user=elements['user'])
                    new_list.append(ping)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SavePingJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()


class SaveInfoCardJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = InfoCardSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    new_list.append(elements)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SaveInfoCardJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()


class SaveCapacityMonitorJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = CapcityMonitorSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    new_list.append(elements)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SaveCapacityMonitorJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()


class SaveTransferMonitorJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = TransferMonitorSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    new_list.append(elements)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SaveTransferMonitorJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()


class SaveCurrentProcessJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = CurrentProcessSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    process = ProcessJson(process_name=elements['process_name'], thread_count=elements['thread_count'],
                                          creation_date=elements['creation_date'],
                                          write_transfer_count=['write_transfer_count'],
                                          read_transfer_count=elements['read_transfer_count'], data=elements['data'],
                                          host=elements['host'], user=elements['user'])
                    new_list.append(process)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SaveCurrentProcessJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()


class SaveUserProcessJsonToSQL:
    def __init__(self, name):
        self.name = name

    def save(self):
        try:
            list = self._read_json()
            new_list = []
            for elements in list:
                save_sql = UserProcessSQL()
                save_sql.forward_the_dict_json(dict_json=elements)
                it_was_saved = save_sql.save_to_sql()
                if it_was_saved == False:
                    process = ProcessJson(process_name=elements['process_name'], thread_count=elements['thread_count'],
                                          creation_date=elements['creation_date'],
                                          write_transfer_count=['write_transfer_count'],
                                          read_transfer_count=elements['read_transfer_count'], data=elements['data'],
                                          host=elements['host'], user=elements['user'])
                    new_list.append(process)
            self._save_not_save(new_list)
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='SaveCurrentProcessJsonToSQL')
            logger.error(msg="Bład podcza save: {}".format(ex))

    def _read_json(self):
        js = JsonConverter.Read(self.name)
        list = js.get_list_object_class()
        return list

    def _save_not_save(self, new_list):
        js = JsonConverter.Save(self.name)
        if len(new_list) > 0:
            js.delete_and_(new_list)
        else:
            js.clear_file()
