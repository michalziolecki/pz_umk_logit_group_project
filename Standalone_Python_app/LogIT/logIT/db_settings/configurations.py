from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from logIT.application_parameters.Parameters import Parameters

# 'postgresql://<username>:<password>@localhost:5432/<database_name>'
# DB_URL = 'postgresql://postgres:admin@localhost:5432/pz_umk_logit'
DB_URL = 'postgresql://' + Parameters.DB_LOGIN + ':' + Parameters.DB_PASSWORD + '@' + Parameters.DB_ADDRESS + ':' + Parameters.DB_PORT + '/' + Parameters.DB_NAME
Engine = create_engine(DB_URL)
Base = declarative_base()
Session = sessionmaker(bind=Engine)
