import logging
from logging.handlers import RotatingFileHandler
from logging import FileHandler
import sys
import os
from logIT.application_parameters.Parameters import Parameters

_file_name = Parameters.LOG_PATH + '.' + Parameters.LOG_FILE_NAME + '.log'
_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
_formatter = logging.Formatter(_fmt)
_log_directory = Parameters.LOG_PATH
_one_mega_byte = 1024 * 1024
_log_backup_files = 3


class OnlyOneFileHandler:
    class __OnlyOne(FileHandler):
        def __init__(self, filename):
            super().__init__(filename)

        def __str__(self):
            return repr(self) + self.baseFilename

    instance = None

    def __init__(self, filename):
        if not OnlyOneFileHandler.instance:
            OnlyOneFileHandler.instance = OnlyOneFileHandler.__OnlyOne(filename)
            OnlyOneFileHandler.instance.setFormatter(_formatter)
        else:
            OnlyOneFileHandler.instance.val = filename

    def __getattr__(self, name):
        return getattr(self.instance, name)


class OnlyOneRotatingHandler:
    class __OnlyOne(RotatingFileHandler):
        def __init__(self, filename, max_bytes=_one_mega_byte, backup_count=_log_backup_files):
            super().__init__(filename, maxBytes=max_bytes, backupCount=backup_count)

        def __str__(self):
            return repr(self) + self.baseFilename

    instance = None

    def __init__(self, filename):
        if not OnlyOneRotatingHandler.instance:
            OnlyOneRotatingHandler.instance = OnlyOneRotatingHandler.__OnlyOne(filename)
            OnlyOneRotatingHandler.instance.setFormatter(_formatter)
        else:
            OnlyOneRotatingHandler.instance.val = filename

    def __getattr__(self, name):
        return getattr(self.instance, name)


class Logger:
    """
    Creates a logging object with level & name and returns it
    """

    # ='unnamed_class', logger_name
    @staticmethod
    def create_full_logger(logging_level=logging.DEBUG, logging_name='full_logIT'):

        if not os.path.isdir(_log_directory):
            os.makedirs(_log_directory)

        def create_linux_and_windows_logger():
            _logger = logging.getLogger(logging_name)
            _logger.setLevel(level=logging_level)
            # _handler: logging.FileHandler = OnlyOneFileHandler(_file_name)
            _handler: logging.FileHandler = OnlyOneRotatingHandler(_file_name)
            _logger.addHandler(_handler)
            _sh = logging.StreamHandler()
            _sh.setFormatter(_formatter)
            _logger.addHandler(_sh)
            return _logger

        def create_darwin_logger():
            print('OS X is not supported!' + sys.stderr)
            raise NotImplementedError

        platforms = {
            'LINUX': create_linux_and_windows_logger,
            'LINUX1': create_linux_and_windows_logger,
            'LINUX2': create_linux_and_windows_logger,
            'DARWIN': create_darwin_logger,
            'WIN32': create_linux_and_windows_logger
        }

        dev_log_platform = platforms[sys.platform.upper()]

        return dev_log_platform()

    @staticmethod
    def create_to_file_logger(logging_level=logging.DEBUG, logging_name='file_logIT'):

        if not os.path.isdir(_log_directory):
            os.makedirs(_log_directory)

        def create_linux_and_windows_logger():
            _logger = logging.getLogger(logging_name)
            _logger.setLevel(level=logging_level)
            #  _handler: logging.FileHandler = OnlyOneFileHandler(_file_name)
            _handler: logging.FileHandler = OnlyOneRotatingHandler(_file_name)
            _logger.addHandler(_handler)
            return _logger

        def create_darwin_logger():
            print('OS X is not supported!' + sys.stderr)
            raise NotImplementedError

        platforms = {
            'LINUX': create_linux_and_windows_logger,
            'LINUX1': create_linux_and_windows_logger,
            'LINUX2': create_linux_and_windows_logger,
            'DARWIN': create_darwin_logger,
            'WIN32': create_linux_and_windows_logger
        }

        dev_log_platform = platforms[sys.platform.upper()]

        return dev_log_platform()

    @staticmethod
    def create_to_console_logger(logging_level=logging.DEBUG, logging_name='stm_logIT'):

        def create_linux_and_windows_logger():
            _logger = logging.getLogger(logging_name)
            _logger.setLevel(level=logging_level)
            _sh = logging.StreamHandler()
            _sh.setFormatter(_formatter)
            _logger.addHandler(_sh)
            return _logger

        def create_darwin_logger():
            print('OS X is not supported!' + sys.stderr)
            raise NotImplementedError

        platforms = {
            'LINUX': create_linux_and_windows_logger,
            'LINUX1': create_linux_and_windows_logger,
            'LINUX2': create_linux_and_windows_logger,
            'DARWIN': create_darwin_logger,
            'WIN32': create_linux_and_windows_logger
        }

        dev_log_platform = platforms[sys.platform.upper()]

        return dev_log_platform()
