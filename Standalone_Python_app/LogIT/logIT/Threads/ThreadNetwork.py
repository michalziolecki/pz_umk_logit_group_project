import threading
import time
from logIT.Network.net import NetworkMonitoring
from logIT.Network.net import NetCard
from logIT.Network.net import Ping
from logIT.develop_log.dev_log import Logger
from logIT.Network.JsonNetwork import JsonConverter
from logIT.Network.JsonNetwork import JsonObjectClass
from logIT.SaveSQL.SaveJsonToSql import SavePingJsonToSQL
from logIT.SaveSQL.SaveJsonToSql import SaveInfoCardJsonToSQL
from logIT.SaveSQL.SaveJsonToSql import SaveCapacityMonitorJsonToSQL
from logIT.SaveSQL.SaveJsonToSql import SaveTransferMonitorJsonToSQL
from logIT.db_models_entities.SettingsNet import SettingsNetSQL
from logIT.db_models_entities.user_keylogger import UserKeylogger
from logIT.SaveSQL.TestSQLConnection import TestSqlConnection
from logIT.application_parameters.Parameters import Parameters
import os
import socket
import datetime
import getpass

#MATI

def _save_time(times):
    if times >= Parameters.TIME_:
        return times
    else:
        return Parameters.TIME_


def _save_to_file(wiadomosc, name):
    js = JsonConverter.Save(name)
    return_list = []
    for p in wiadomosc:
        or_save = js.save_to_file(p)
        if or_save == False:
            return_list.append(p)
    return return_list


class PingThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_PING, address=Parameters.PING_ADDRESS):
        threading.Thread.__init__(self)
        self.times = times
        self.address = address
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times=Parameters.TIME_PING, address=Parameters.PING_ADDRESS):
        self.address = address
        self.times = times

    def run(self):
        i = 0
        message = []

        while self.if_run:
            try:
                message.append(self._ping())
                if i / _save_time(self.times) >= 1:
                    message = _save_to_file(message, "Ping")
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='PingThread')
                logger.error(msg="Bład podcza _Ping: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _ping(self):
        p = Ping(self.address)
        message = JsonObjectClass.Ping(ping=p.ping(), address=p.hostname, data=str(datetime.datetime.now()))
        return message


class InfoNetCardThread(threading.Thread):
    def __init__(self, times=Parameters.INFO_CARD_TIME):
        threading.Thread.__init__(self)
        self.times = times
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times):
        self.times = times

    def run(self):
        i = 0
        message = []
        while self.if_run:
            try:
                message.append(self._info_net_card())
                if i / _save_time(self.times) >= 1:
                    message = _save_to_file(message, "InfoNetworkCard")
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='InfoNetCardThread')
                logger.error(msg="Bład podcza _InfoNetCard: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _info_net_card(self):
        network = NetCard()
        interface = network.get_default_interface()
        mac = network.get_default_mac(interface)
        ip = network.get_defaults_ip(interface)
        mask = network.get_defualt_mask(interface)
        broadcast = network.get_default_broadcast(interface)
        message = JsonObjectClass.InfoCard(interface, mac, ip, mask, broadcast)
        return message


class TransferMonitorThread(threading.Thread):
    def __init__(self, times=Parameters.TRANSFER_TIME):
        self.times = times
        threading.Thread.__init__(self)
        self.if_run = False

    def stop(self):
        self.if_run = False

    def set_settings(self, times):
        self.times = times


    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        i = 0
        message = []
        while self.if_run:
            try:
                message.append(self._monitor())
                if i / _save_time(self.times) >= 1:
                    message = _save_to_file(message, "TransferMonitor")
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='TransferMonitorThread')
                logger.error(msg="Bład podcza _monitor: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _monitor(self):
        monitor = NetworkMonitoring()
        transfer = monitor.get_all_card_transfer_upload_download_monitoring()
        message = JsonObjectClass.UploadDownload(transfer["upload"], transfer["download"])
        return message


class CapacityMonitorThread(threading.Thread):
    def __init__(self, times=Parameters.CAPACITY_TIME, upload=Parameters.CAPACITY_UPLOAD, download=Parameters.CAPACITY_DOWNLOAD):
        self.times = times
        self.upload = upload
        self.download = download
        threading.Thread.__init__(self)
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times, upload=Parameters.CAPACITY_UPLOAD, download=Parameters.CAPACITY_DOWNLOAD):
        self.times = times
        self.upload = upload
        self.download = download


    def run(self):
        i = 0
        message = []
        while self.if_run:
            try:
                message.append(self._capacity_monitor())
                if i / _save_time(self.times) >= 1:
                    message = _save_to_file(message, "CapacityMonitor")
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='CapacityMonitorThread')
                logger.error(msg="Bład podcza _InfoNetCard: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _capacity_monitor(self):
        monitor = NetworkMonitoring()
        transfer = monitor.get_upload_download_monitoring(self.upload, self.download)
        message = JsonObjectClass.UploadDownload(transfer["upload"], transfer["download"])
        return message

class SavePingToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="Ping"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='PingThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        # SavePing
        save_ping = SavePingJsonToSQL(name=self.file_name)
        save_ping.save()


class SaveInfoCardToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="InfoNetworkCard"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='PingThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        # Save Info Card
        save_info_card = SaveInfoCardJsonToSQL(name=self.file_name)
        save_info_card.save()
        # Save Capacity Monitor
        save_capacity_monitor = SaveCapacityMonitorJsonToSQL(name="CapacityMonitor")
        save_capacity_monitor.save()
        # Save Transfer Monitor
        save_transfer = SaveTransferMonitorJsonToSQL(name="TransferMonitor")
        save_transfer.save()

class SaveCapacityMonitorToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="CapacityMonitor"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='PingThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        # Save Capacity Monitor
        save_capacity_monitor = SaveCapacityMonitorJsonToSQL(name=self.file_name)
        save_capacity_monitor.save()
        # Save Transfer Monitor
        save_transfer = SaveTransferMonitorJsonToSQL(name="TransferMonitor")
        save_transfer.save()

class SaveTransferMonitorToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="TransferMonitor"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='PingThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        # Save Transfer Monitor
        save_transfer = SaveTransferMonitorJsonToSQL(name=self.file_name)
        save_transfer.save()


class NetworkMainThread(threading.Thread):
    def __init__(self, times=Parameters.SQL_CHECK_TIME):
        self.times = times
        threading.Thread.__init__(self)
        self.if_run = False
        self.ping_thread = PingThread(Parameters.TIME_PING, Parameters.PING_ADDRESS)
        self.save_ping_thread = SavePingToSQLThread()
        self.save_ic_thread = SaveInfoCardToSQLThread()
        self.info_nc_thread = InfoNetCardThread(Parameters.INFO_CARD_TIME)
        self.transfer_thread = TransferMonitorThread(Parameters.TRANSFER_TIME)
        self.save_transfer_thread = SaveTransferMonitorToSQLThread()
        self.capacity_thread = CapacityMonitorThread(Parameters.CAPACITY_TIME, Parameters.CAPACITY_UPLOAD, Parameters.CAPACITY_DOWNLOAD)
        self.save_cm_thread = SaveCapacityMonitorToSQLThread()

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times):
        self.times = times


    def run(self):
        i = 0
        self._run()
        while True:
            try:
                if i / _save_time(self.times) >= 1:
                    self._run()
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='NetworkMainThread')
                logger.error(msg="Bład podcza network main thread: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _run(self):
        sql_test = TestSqlConnection()
        if sql_test.ConnectionStatus() == True:
            try:
                # Tworzenie i Pobieranie danych o urzytkowniku
                user_test = UserKeylogger(host_name=socket.gethostname(), user_name=getpass.getuser())
                user_test.create_data_base_schema()
                id = user_test.get_id_or_create_user_in_db()
                # Settings
                setting = SettingsNetSQL(id_user_keylogger=id)
                setting_download = setting.get_all_setings_for_user_from_db()

                ## PING
                if (setting_download.what_a_lot_ping == None or setting_download.what_a_lot_ping <= 0):
                    what_a_lot_ping = Parameters.TIME_PING
                else:
                    what_a_lot_ping = setting_download.what_a_lot_ping
                address_ping = Parameters.PING_ADDRESS
                if (setting_download.address_to_ping != ""):
                    address_ping = setting_download.address_to_ping
                self.ping_thread.set_settings(times=what_a_lot_ping, address=address_ping)
                if (setting_download.or_ping == 1):
                    self.ping_thread.start()
                    self.save_ping_thread.start()
                else:
                    self.ping_thread.stop()
                    self.save_ping_thread.stop()

                ## Info Card
                if setting_download.what_a_lot_info_card == None or setting_download.what_a_lot_info_card <= 0:
                    what_a_lot_info_card = Parameters.INFO_CARD_TIME
                else:
                    what_a_lot_info_card = setting_download.what_a_lot_info_card
                self.info_nc_thread.set_settings(times=what_a_lot_info_card)
                if setting_download.or_info_card == 1:
                    self.info_nc_thread.start()
                    self.save_ic_thread.start()
                else:
                    self.info_nc_thread.stop()
                    self.save_ic_thread.stop()

                ## Transfer
                if setting_download.what_a_lot_monitor_transfer == None or setting_download.what_a_lot_monitor_transfer <= 0:
                    what_a_lot_transfer = Parameters.TRANSFER_TIME
                else:
                    what_a_lot_transfer = setting_download.what_a_lot_monitor_transfer
                self.transfer_thread.set_settings(times=what_a_lot_transfer)
                if setting_download.or_monitor_transfer == 1:
                    self.transfer_thread.start()
                    self.save_transfer_thread.start()
                else:
                    self.transfer_thread.stop()
                    self.save_transfer_thread.stop()

                ## Capacity Transfer
                if setting_download.what_a_lot_monitor_the_current_transfer == None or setting_download.what_a_lot_monitor_the_current_transfer <= 0:
                    what_a_lot_capacity = Parameters.CAPACITY_TIME
                else:
                    what_a_lot_capacity = setting_download.what_a_lot_monitor_the_current_transfer
                if setting_download.or_speed_download == 1:
                    download = True
                else:
                    download = False
                if setting_download.or_speed_upload == 1:
                    upload = True
                else:
                    upload = False
                self.capacity_thread.set_settings(times=what_a_lot_capacity, upload=upload, download=download)
                if setting_download.or_monitor_the_current_transfer == 1:
                    self.capacity_thread.start()
                    self.save_cm_thread.start()
                else:
                    self.capacity_thread.stop()
                    self.save_cm_thread.stop()


            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='NetworkMainThread')
                logger.error(msg="Bład podcza _run {}".format(ex))
        else:
            self.ping_thread.set_settings(times=Parameters.TIME_PING, address=Parameters.PING_ADDRESS)
            self.ping_thread.start()
            self.save_ping_thread.start()

            self.info_nc_thread.set_settings(times=Parameters.INFO_CARD_TIME)
            self.info_nc_thread.start()
            self.save_ic_thread.start()

            self.transfer_thread.set_settings(times=Parameters.TRANSFER_TIME)
            self.transfer_thread.start()
            self.save_transfer_thread.start()

            self.capacity_thread.set_settings(times=Parameters.CAPACITY_TIME, upload=Parameters.CAPACITY_UPLOAD, download=Parameters.CAPACITY_DOWNLOAD)
            self.capacity_thread.start()
            self.save_cm_thread.start()





