from logIT.db_settings.configurations import Base, Session, Engine
from sqlalchemy.exc import SQLAlchemyError
from logIT.develop_log.dev_log import Logger


def exception_decorator(function_to_wrap):
    def wrapper(*args, **kwargs):
        try:
            return function_to_wrap(*args, **kwargs)
        except SQLAlchemyError as exception:
            _logger = Logger.create_to_file_logger(logging_name='AbstractEntityDAO')
            _logger.warning(msg="Exception abstract dao method operations")

    return wrapper


class AbstractEntityDAO:

    def __init__(self):
        self.session = Session()

    @staticmethod
    @exception_decorator
    def create_data_base_schema():
        Base.metadata.create_all(Engine)

    def create_new_session(self) -> Session:
        self.session = Session()
        return self.session

    def do_commit_session(self):
        self.session.commit()

    def close_session(self):
        self.session.close()

    def save_entity(self, entity):
        self.session.add(entity)

    @exception_decorator
    def get_all_entieties_by_type(self, clazz_type) -> list:
        entities = self.session.query(clazz_type).all()
        return entities
