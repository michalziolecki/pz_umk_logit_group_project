from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime
from logIT.develop_log.dev_log import Logger
from logIT.db_models_entities.user_keylogger import UserKeylogger

##Mati

class Pingi(Base):
    __tablename__ = 'pingi'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name="id_uzytkownicy_keylogger")
    measurement_ping = Column(Integer, nullable=False, name="pomiar_pingu_w_ms")
    address = Column(String(50), nullable=True, name="adres")
    data = Column(DateTime, nullable=False, name="kiedy_rozpoczeto_pomiar")

    def __init__(self, id_user_keylogger=None, measurement_ping=None, data=None, address=None):
        self.id_user_keylogger = id_user_keylogger
        self.measurement_ping = measurement_ping
        self.address = address
        self.data = data

class PingiSQL(Pingi, AbstractEntityDAO):

    def __init__(self):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty

    def forward_the_object(self, ping):
        try:
            self.id_user_keylogger = ping.id_user_keylogger
            self.measurement_ping = ping.measurement_ping
            self.address = ping.address
            self.data = ping.data
        except Exception as ex:
            self._logger_save(ex)

    def forward_the_dict_json(self, dict_json):
        try:
            user_test = UserKeylogger(host_name=dict_json['host'], user_name=dict_json['user'])
            user_test.create_data_base_schema()
            id = user_test.get_id_or_create_user_in_db()
            if id > 0:
                self.id_user_keylogger = id
                self.measurement_ping = dict_json['ping']
                self.address = dict_json['address']
                self.data = dict_json['data']
        except Exception as ex:
            self._logger_save(ex)

    def _logger_save(self, str):
        log = Logger.create_to_file_logger(logging_name="PingiSQL")
        log.error(str)

    def save_to_sql(self) -> bool:
        try:
            if self.id_user_keylogger != None:
                self.create_new_session()
                self.save_entity(self)
                self.do_commit_session()
                self.close_session()
                return True
            else:
                #Brak danych
                return False
        except Exception as ex:
            self._logger_save(ex)
        return False

