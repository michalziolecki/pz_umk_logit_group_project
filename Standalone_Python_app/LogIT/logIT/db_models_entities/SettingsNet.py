from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, SmallInteger
from sqlalchemy.orm import relationship
from logIT.develop_log.dev_log import Logger

#Mati

class SettingsNet(Base):
    __tablename__ = 'ustawienia_internetowe'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name="id_uzytkownicy_keylogger")
    or_monitor_the_current_transfer = Column(SmallInteger,  nullable=False, name="czy_monitorowac_transfer_obecny")
    what_a_lot_monitor_the_current_transfer = Column(Integer, nullable=True, name="co_ile_monitorowac_transfer_obecny")
    or_speed_upload = Column(SmallInteger, nullable=False, name="czy_predkosc_wysylania")
    or_speed_download = Column(SmallInteger, nullable=False, name="czy_predkosc_pobierania")
    or_monitor_transfer = Column(SmallInteger, nullable=False, name="czy_monitorowac_transfer_od_wlaczenia_komputera")
    what_a_lot_monitor_transfer = Column(Integer, nullable=True, name="co_ile_monitorowac_transfer_od_wlaczenia_komputera")
    or_ping = Column(SmallInteger, nullable=False, name="czy_pingowac")
    address_to_ping = Column(String(50), nullable=True, name="adres_dla_pingu")
    what_a_lot_ping = Column(Integer, nullable=True, name="co_ile_pingowac")
    or_info_card = Column(SmallInteger, nullable=False, name="czy_zbierac_informacje_o_karcie_sieciowej")
    what_a_lot_info_card = Column(Integer, nullable= True, name="co_ile_zbierac_informacje_o_karcie_sieciowej")

class SettingsNetSQL(SettingsNet, AbstractEntityDAO):

    def __init__(self, id_user_keylogger):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty
        self.id_user_keylogger = id_user_keylogger

    def get_all_setings_for_user_from_db(self) -> SettingsNet:
        download_SettingsNet = SettingsNet()
        try:
            self.create_new_session()

            setting_download: list = self.session.query(SettingsNet) \
                .filter(SettingsNet.id_user_keylogger == self.id_user_keylogger).all()
            if len(setting_download) == 1:
                download_SettingsNet.id = setting_download[0].id
                download_SettingsNet.or_monitor_the_current_transfer = setting_download[0].or_monitor_the_current_transfer
                download_SettingsNet.what_a_lot_monitor_the_current_transfer = setting_download[0].what_a_lot_monitor_the_current_transfer
                download_SettingsNet.or_speed_upload = setting_download[0].or_speed_upload
                download_SettingsNet.or_speed_download = setting_download[0].or_speed_download
                download_SettingsNet.or_monitor_transfer = setting_download[0].or_monitor_transfer
                download_SettingsNet.what_a_lot_monitor_transfer = setting_download[0].what_a_lot_monitor_transfer
                download_SettingsNet.or_ping = setting_download[0].or_ping
                download_SettingsNet.address_to_ping = setting_download[0].address_to_ping
                download_SettingsNet.what_a_lot_ping = setting_download[0].what_a_lot_ping
                download_SettingsNet.or_info_card = setting_download[0].or_info_card
                download_SettingsNet.what_a_lot_info_card = setting_download[0].what_a_lot_info_card

            self.do_commit_session()
            self.close_session()
        except Exception as ex:
            _logger = Logger.create_full_logger(logging_name='SettingsNetSQL')
            _logger.error(msg=ex)
        return download_SettingsNet