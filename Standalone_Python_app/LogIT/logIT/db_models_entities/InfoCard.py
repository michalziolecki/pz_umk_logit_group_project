from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime
from logIT.develop_log.dev_log import Logger
from logIT.db_models_entities.user_keylogger import UserKeylogger


##Mati

class InfoCard(Base):
    __tablename__ = 'informacje_o_karcie_sieciowej'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name="id_uzytkownicy_keylogger")
    interface = Column(String(50), nullable=True, name="ineterface")
    mac = Column(String(20), nullable=True, name="mac")
    ip = Column(String(20), nullable=True, name="ip")
    mask = Column(String(20), nullable=True, name="maska")
    broadcast = Column(String(20), nullable=True, name="broadcast")
    date = Column(DateTime, nullable=False, name="kiedy_zebrano_informacje")

    def __init__(self, id_user_keylogger=None, interface=None, mac=None, ip=None, mask=None, broadcast=None, date=None):
        self.id_user_keylogger = id_user_keylogger
        self.interface = interface
        self.mac = mac
        self.ip = ip
        self.mask = mask
        self.broadcast = broadcast
        self.date = date

class InfoCardSQL(InfoCard, AbstractEntityDAO):

    def __init__(self):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty

    def forward_the_object(self, info_card):
        try:
            self.id_user_keylogger = info_card.id_user_keylogger
            self.interface = info_card.interface
            self.mac = info_card.mac
            self.ip = info_card.ip
            self.mask = info_card.mask
            self.broadcast = info_card.broadcast
            self.date = info_card.date
        except Exception as ex:
            self._logger_save(ex)

    def forward_the_dict_json(self, dict_json):
        try:
            user_test = UserKeylogger(host_name=dict_json['host'], user_name=dict_json['user'])
            user_test.create_data_base_schema()
            id = user_test.get_id_or_create_user_in_db()
            if id > 0:
                self.id_user_keylogger = id
                self.interface = dict_json['interface']
                self.mac = dict_json['mac']
                self.ip = dict_json['ip']
                self.mask = dict_json['mask']
                self.broadcast = dict_json['broadcast']
                self.date = dict_json['data']
        except Exception as ex:
            self._logger_save(ex)

    def _logger_save(self, str):
        log = Logger.create_to_file_logger(logging_name="InfoCardSQL")
        log.error(str)

    def save_to_sql(self) -> bool:
        try:
            if self.id_user_keylogger != None:
                self.create_new_session()
                self.save_entity(self)
                self.do_commit_session()
                self.close_session()
                return True
            else:
                return False
        except Exception as ex:
            self._logger_save(ex)
        return False
