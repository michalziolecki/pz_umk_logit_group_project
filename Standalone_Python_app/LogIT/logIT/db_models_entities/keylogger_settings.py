from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Boolean
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger
from sqlalchemy import update
from copy import copy


class KeyloggerSettings(Base, AbstractEntityDAO):
    __tablename__ = 'ustawienia_keylogger'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    tracking_status = Column(Boolean, nullable=False, name='czy_zbierac_wiadomosc')  # in DB this is smallint

    def __init__(self, id_user: int):
        super().__init__()
        self.id_user_keylogger = id_user
        self.tracking_status_in_db = None
        self.tracking_status = True
        self._logger = Logger.create_to_file_logger(logging_name='KeyloggerSettings')

    def set_tracking_status(self, tracking_status):
        self.tracking_status = tracking_status

    def save_tracking_status_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def update_tracking_status_in_db(self):
        self.create_new_session()
        self.session.query(KeyloggerSettings).filter(KeyloggerSettings.id_user_keylogger == self.id_user_keylogger)\
            .update({'tracking_status': self.tracking_status})

        self.do_commit_session()
        self.close_session()

    def get_tracking_status_from_db(self) -> bool:

        self.create_new_session()
        tracking_snapshot: list = self.session.query(KeyloggerSettings) \
            .filter(KeyloggerSettings.id_user_keylogger == self.id_user_keylogger).all()

        if len(tracking_snapshot) == 0:
            self.save_entity(self)
            new_tracking_snapshot: list = self.session.query(KeyloggerSettings) \
                .filter(KeyloggerSettings.id_user_keylogger == self.id_user_keylogger).all()

            if new_tracking_snapshot is not None and len(new_tracking_snapshot) > 0:
                self.tracking_status_in_db = copy(new_tracking_snapshot[0].tracking_status)

        elif len(tracking_snapshot) == 1:
            self.tracking_status_in_db = tracking_snapshot[0].tracking_status

        else:
            self._logger.warning(msg='Duplicated users, application use first but, fix this problem immediately!!! ')
            self.tracking_status_in_db = tracking_snapshot[0].tracking_status

        self.do_commit_session()
        self.close_session()

        if self.tracking_status_in_db is None:
            self._logger.error(msg='User does\'nt created in  DB by method: '
                                   + self.get_tracking_status_from_db.__name__ + ' method! ')
            raise NotImplementedError(
                'User does\'nt created in  DB by method: ' + self.get_tracking_status_from_db.__name__ + ' method! ')

        self.tracking_status_in_db = bool(self.tracking_status_in_db)
        return self.tracking_status_in_db
