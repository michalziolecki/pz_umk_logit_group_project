from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime
from logIT.develop_log.dev_log import Logger
from logIT.db_models_entities.user_keylogger import UserKeylogger

##MATI

class Capcity_Monitor(Base):
    __tablename__ = 'transfer_obecny'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name="id_uzytkownicy_keylogger")
    speed_upload = Column(Integer, nullable=True, name="predkosc_wysylania")
    speed_download = Column(Integer, nullable=True, name="predkosc_pobiernia")
    date = Column(DateTime, nullable=False, name="kiedy_rozpoczeto_pomiar")

    def __init__(self, id_user_keylogger=None, speed_upload=None, speed_download=None, date=None):
        self.id_user_keylogger = id_user_keylogger
        self.speed_upload = speed_upload
        self.speed_download = speed_download
        self.date = date

class CapcityMonitorSQL(Capcity_Monitor, AbstractEntityDAO):

    def __init__(self):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty

    def forward_the_object(self, capcity_monitor):
        try:
            self.id_user_keylogger = capcity_monitor.id_user_keylogger
            self.speed_upload = capcity_monitor.speed_upload
            self.speed_download = capcity_monitor.speed_download
            self.date = capcity_monitor.date
        except Exception as ex:
            self._logger_save(ex)

    def forward_the_dict_json(self, dict_json):
        try:
            user_test = UserKeylogger(host_name=dict_json['host'], user_name=dict_json['user'])
            user_test.create_data_base_schema()
            id = user_test.get_id_or_create_user_in_db()
            if id > 0:
                self.id_user_keylogger = id
                if dict_json['upload'] >= 0:
                    self.speed_upload = dict_json['upload']
                else:
                    self.speed_upload = None
                if dict_json['download'] >= 0:
                    self.speed_download = dict_json['download']
                else:
                    self.speed_download = None
                self.date = dict_json['data']
        except Exception as ex:
            self._logger_save(ex)

    def _logger_save(self, str):
        log = Logger.create_to_file_logger(logging_name="CapcityMonitorSQL")
        log.error(str)

    def save_to_sql(self) -> bool:
        try:
            if self.id_user_keylogger != None:
                self.create_new_session()
                self.save_entity(self)
                self.do_commit_session()
                self.close_session()
                return True
            else:
                return False
        except Exception as ex:
            self._logger_save(ex)
        return False
