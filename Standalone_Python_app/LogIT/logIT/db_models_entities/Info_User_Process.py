from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, DateTime
from logIT.develop_log.dev_log import Logger
from logIT.db_models_entities.user_keylogger import UserKeylogger


##Mati

class InfoUserProcess(Base):
    __tablename__ = 'informacje_o_procesach_usera'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name="id_uzytkownicy_keylogger")
    date = Column(DateTime, nullable=False, name="kiedy_zebrano_informacje")
    name_process = Column(String(100), nullable=True, name="nazwa_procesu")
    thread_count = Column(Integer, nullable=True, name="ilosc_watkow")
    date_start_process = Column(DateTime, nullable=True, name="od_kiedy_dziala_proces")
    write_kilobytes = Column(Integer, nullable=True, name="zapis_w_kilobytach")
    read_kilobytes = Column(Integer, nullable=True, name="odczyt_w_kilobytach")

    def __init__(self, id_user_keylogger=None, date=None, name_process=None, thread_count=None, date_start_process=None,
                 write_kilobytes=None, read_kilobytes=None):
        self.id_user_keylogger = id_user_keylogger
        self.date = date
        self.name_process = name_process
        self.thread_count = thread_count
        self.date_start_process = date_start_process
        self.write_kilobytes = write_kilobytes
        self.read_kilobytes = read_kilobytes


class UserProcessSQL(InfoUserProcess, AbstractEntityDAO):

    def __init__(self):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty

    def forward_the_object(self, curret_process):
        try:
            self.id_user_keylogger = curret_process.id_user_keylogger
            self.date = curret_process.date
            self.name_process = curret_process.name_process
            self.thread_count = curret_process.thread_count
            self.date_start_process = curret_process.date_start_process
            self.write_kilobytes = curret_process.write_kilobytes
            self.read_kilobytes = curret_process.read_kilobytes
        except Exception as ex:
            self._logger_save(ex)

    def forward_the_dict_json(self, dict_json):
        try:
            user_test = UserKeylogger(host_name=dict_json['host'], user_name=dict_json['user'])
            user_test.create_data_base_schema()
            id = user_test.get_id_or_create_user_in_db()
            if id > 0:
                self.id_user_keylogger = id
                self.date = dict_json['data']
                self.name_process = dict_json['process_name']
                self.thread_count = dict_json['thread_count']
                self.date_start_process = dict_json['creation_date']
                self.write_kilobytes = dict_json['write_transfer_count']
                self.read_kilobytes = dict_json['read_transfer_count']
        except Exception as ex:
            self._logger_save(ex)

    def _logger_save(self, str):
        log = Logger.create_to_file_logger(logging_name="UserProcessSQL")
        log.error(str)

    def save_to_sql(self) -> bool:
        try:
            if self.id_user_keylogger != None:
                self.create_new_session()
                self.save_entity(self)
                self.do_commit_session()
                self.close_session()
                return True
            else:
                # Brak danych
                return False
        except Exception as ex:
            self._logger_save(ex)
        return False
