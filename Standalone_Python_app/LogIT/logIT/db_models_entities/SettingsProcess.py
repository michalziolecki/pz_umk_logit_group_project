from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from sqlalchemy import Column, ForeignKey
from sqlalchemy import String, Integer, SmallInteger
from sqlalchemy.orm import relationship
from logIT.develop_log.dev_log import Logger

#Mati

class SettingsProcess(Base):
    __tablename__ = 'ustawienia_proces'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False, name="id_uzytkownicy_keylogger")
    or_current_process = Column(SmallInteger, nullable=False, name="czy_zbierac_aktywny_proces")
    or_user_process = Column(SmallInteger, nullable=False, name="czy_zbierac_procesy_usera")
    time_user_process = Column(Integer, nullable=True, name="co_ile_zbierac_procesy_usera")

class SettingsProcessSQL(SettingsProcess, AbstractEntityDAO):

    def __init__(self, id_user_keylogger):
        super().__init__()
        self.id_in_db = None  # this field is a bucket to id from database, because after commit field id is empty
        self.id_user_keylogger = id_user_keylogger

    def get_all_setings_for_user_from_db(self) -> SettingsProcess:
        download_SettingsProcess = SettingsProcess()
        try:
            self.create_new_session()

            setting_download: list = self.session.query(SettingsProcess) \
                .filter(SettingsProcess.id_user_keylogger == self.id_user_keylogger).all()
            if len(setting_download) == 1:
                download_SettingsProcess.id = setting_download[0].id
                download_SettingsProcess.or_current_process = setting_download[0].or_current_process
                download_SettingsProcess.or_user_process = setting_download[0].or_user_process
                download_SettingsProcess.time_user_process = setting_download[0].time_user_process

            self.do_commit_session()
            self.close_session()
        except Exception as ex:
            _logger = Logger.create_full_logger(logging_name='SettingsProcessSQL')
            _logger.error(msg=ex)
        return download_SettingsProcess