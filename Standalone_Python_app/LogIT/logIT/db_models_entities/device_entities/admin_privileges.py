from sqlalchemy import Column, ForeignKey
from sqlalchemy import Integer, Boolean, String
from logIT.db_settings.configurations import Base
from logIT.db_models_entities.abstract_dao import AbstractEntityDAO
from logIT.develop_log.dev_log import Logger
from sqlalchemy import update
from copy import copy


class Privileges(Base, AbstractEntityDAO):
    __tablename__ = 'uprawnienia_administracyjne'

    id = Column(Integer, primary_key=True, nullable=False, name='id')
    id_user_keylogger = Column(Integer, ForeignKey('uzytkownicy_keylogger.id'), nullable=False,
                               name='id_uzytkownicy_keylogger')
    login = Column(String(250), nullable=False, name='login_administratora')  # in DB this is smallint
    password = Column(String(250), nullable=False, name='haslo_administratora')  # in DB this is smallint

    def __init__(self, id_user: int):
        super().__init__()
        self.id_user_keylogger = id_user
        self.login_in_db = None
        self.login = "Admin"
        self.password_in_db = None
        self.password = "Admin"
        self._logger = Logger.create_to_file_logger(logging_name='Privileges')

    def set_login(self, login):
        self.login = login

    def set_password(self, password):
        self.password = password

    def save_login_and_password_in_db(self):
        self.create_new_session()
        self.save_entity(self)
        self.do_commit_session()
        self.close_session()

    def update_login_and_password_in_db(self):
        self.create_new_session()
        self.session.query(Privileges).filter(Privileges.id_user_keylogger == self.id_user_keylogger) \
            .update({'login': self.login,
                     'password': self.password
                     })

        self.do_commit_session()
        self.close_session()

    def get_login_and_password_from_db(self) -> tuple:

        self.create_new_session()
        privileges_snapshot: list = self.session.query(Privileges) \
            .filter(Privileges.id_user_keylogger == self.id_user_keylogger).all()

        if len(privileges_snapshot) == 0:
            self._logger.warning(
                msg='Admin privileges doesn\'t exist,'
                    ' application create p"Admin" & l"Admin" but, fix this problem immediately!!! ')
            self.save_entity(self)
            new_privileges_snapshot: list = self.session.query(Privileges) \
                .filter(Privileges.id_user_keylogger == self.id_user_keylogger).all()

            if new_privileges_snapshot is not None and len(new_privileges_snapshot) > 0:
                self.login_in_db = copy(new_privileges_snapshot[0].login)
                self.password_in_db = copy(new_privileges_snapshot[0].password)

        elif len(privileges_snapshot) == 1:
            self.login_in_db = copy(privileges_snapshot[0].login)
            self.password_in_db = copy(privileges_snapshot[0].password)

        else:
            self._logger.warning(
                msg='Duplicated admin privileges, application use first but, fix this problem immediately!!! ')
            self.login_in_db = copy(privileges_snapshot[0].login)
            self.password_in_db = copy(privileges_snapshot[0].password)

        self.do_commit_session()
        self.close_session()

        if self.login_in_db is None or self.password_in_db is None:
            self._logger.error(msg='User does\'nt created in  DB by method: '
                                   + self.get_login_and_password_from_db.__name__ + ' method! ')
            raise NotImplementedError(
                'User does\'nt created in  DB by method: ' + self.get_login_and_password_from_db.__name__ +
                ' method! ')

        return self.login_in_db, self.password_in_db
