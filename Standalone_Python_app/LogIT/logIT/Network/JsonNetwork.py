import json
import os
import datetime
import getpass
import portalocker
import socket
import time
from logIT.application_parameters.Parameters import Parameters
from logIT.develop_log.dev_log import Logger


# MATI


class JsonConverter:
    class File:
        def __init__(self, file="wynik", path=Parameters.KEYLOG_FILE_PATH):
            self.path = path
            self.file = file + ".json"
            self._logger = Logger.create_to_file_logger(logging_name='JsonConverter')

        def _path_and_file(self):
            return "{0}\{1}".format(self.path, self.file)

        def _if_exists_file(self):
            return os.path.exists(self._path_and_file())

    class Save(File):

        def save_to_file(self, list_object_class):
            contents = json.dumps(list_object_class)
            status = 1
            if self._if_exists_directory() == False:
                self._create_directory()
            if self._if_exists_file() == True:
                try_number = 1
                while status == 1 and try_number <= 5:
                    file = open(self._path_and_file(), 'r+')
                    try:
                        portalocker.lock(file, portalocker.LOCK_EX | portalocker.LOCK_NB)
                        if len(file.readlines()) > 0:
                            contents = "\n{0}".format(json.dumps(list_object_class))
                        status = 0
                    except portalocker.LockException as le:
                        self._logger.warning(
                            'Exception with locking file in method' + self.save_to_file.__name__ +
                            ', stacktrace: ' + str(le))
                        status = 1
                    file.close()
                    time.sleep(0.1)
                    try_number += 1
            else:
                status = 0
            if status == 0:
                status = 1
                try_number = 1
                while status == 1 and try_number <= 5:
                    file = open(self._path_and_file(), "a")
                    try:
                        portalocker.lock(file, portalocker.LOCK_EX | portalocker.LOCK_NB)
                        file.write(contents)
                        status = 0
                    except portalocker.LockException as le:
                        self._logger.warning(
                            'Exception with locking file in method' + self.save_to_file.__name__ +
                            ', stacktrace: ' + str(le))
                        status = 1
                    file.close()
                    time.sleep(0.1)
                    try_number += 1
                if status == 0:
                    return True
                else:
                    return False
            else:
                return False

        def delete_and_save_to_file(self, list_object_class):
            if self._if_exists_directory() == False:
                self._create_directory()
            if self._if_exists_file() == True:
                text = ""
                for obj in list_object_class:
                    if text == "":
                        text = json.dumps(obj)
                    else:
                        text = text + "\n{0}".format(json.dumps(obj))
                status = 1
                try_number = 1
                while status == 1 and try_number <= 5:
                    file = open(self._path_and_file(), "w")
                    try:
                        portalocker.lock(file, portalocker.LOCK_EX | portalocker.LOCK_NB)
                        file.write(text)
                        status = 0
                    except portalocker.LockException as le:
                        self._logger.warning(
                            'Exception with locking file in method' + self.delete_and_save_to_file.__name__ +
                            ', stacktrace: ' + str(le))
                        status = 1
                    file.close()
                    time.sleep(0.1)
                    try_number += 1

        def clear_file(self):
            if self._if_exists_directory() == False:
                self._create_directory()
            if self._if_exists_file() == True:
                status = 1
                try_number = 1
                while status == 1 and try_number <= 10:
                    file = open(self._path_and_file(), "w")
                    try:
                        portalocker.lock(file, portalocker.LOCK_EX | portalocker.LOCK_NB)
                        file.write("")
                        status = 0
                    except portalocker.LockException as le:
                        self._logger.warning(
                            'Exception with locking file in method' + self.clear_file.__name__ +
                            ', stacktrace: ' + str(le))
                        status = 1
                    file.close()
                    time.sleep(0.1)
                    try_number += 1

        def _if_exists_directory(self):
            return os.path.isdir(self.path)

        def _create_directory(self):
            try:
                os.makedirs(self.path)
            except:
                pass

    class Read(File):
        def get_list_object_class(self):
            listp = []
            if self._if_exists_file() == True:
                status = 1
                try_number = 1
                while status == 1 and try_number <= 5:
                    file = open(self._path_and_file(), 'r+')
                    try:
                        portalocker.lock(file, portalocker.LOCK_EX | portalocker.LOCK_NB)
                        text_file = file.readlines()

                        for line in text_file:
                            if line != '\n':
                                try:
                                    p = json.loads(line)
                                    listp.append(p)
                                except:
                                    self._logger.error(msg="Błąd podczas: {}, nie dodano linii: '{}'".format(
                                        self.get_list_object_class.__name__, line))
                        status = 0
                        file.close()
                    except portalocker.LockException as le:
                        self._logger.warning(
                            'Exception with locking file in method' + self.get_list_object_class.__name__ +
                            ', stacktrace: ' + str(le))
                        status = 1
                    file.close()
                    time.sleep(0.1)
                    try_number += 1
                return listp
            else:
                return listp


class JsonObjectClass:
    class Ping(dict):
        def __init__(self, ping, address, data=str(datetime.datetime.now()), host=socket.gethostname(),
                     user=getpass.getuser()):
            dict.__init__(self, data=data, host=host, user=user, ping=ping, address=address)

    class InfoCard(dict):
        def __init__(self, interface, mac, ip, mask, broadcast):
            dict.__init__(self, data=str(datetime.datetime.now()), host=socket.gethostname(),
                          user=getpass.getuser(), interface=interface, mac=mac, ip=ip, mask=mask, broadcast=broadcast)

    class UploadDownload(dict):
        def __init__(self, upload, download):
            dict.__init__(self, data=str(datetime.datetime.now()), host=socket.gethostname(),
                          user=getpass.getuser(), upload=upload, download=download)
