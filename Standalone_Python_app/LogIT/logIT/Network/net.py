import re
from subprocess import Popen, PIPE
import netifaces
import time
import psutil
import socket
from pythonping import ping

#Mati

class NetCard:
    def get_default_interface(self) -> str:
        """
        #Może się przydać

        list_gateways = []
        exist = False
        gateways=netifaces.gateways()
        for gw in gateways[netifaces.AF_INET]:
            for ip in list_gateways:
                if(ip == gw[0]):
                    exist = True
                    break
            if(exist == False):
                list_gateways.append(gw[0])
            else:
                exist = False
        for ip in list_gateways:
        """
        try:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            s.connect(('8.8.8.8', 53))
            ip_preferred = s.getsockname()[0]
        except:
            s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            ip_preferred = socket.gethostbyname(socket.gethostname())
        gws = netifaces.interfaces()
        for interface in gws:
            try:
                ip = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['addr']
                if ip == ip_preferred:
                    return interface
            except:
                pass

    def _get_default_interface2(self) -> str:
        gws = netifaces.gateways()
        return gws['default'][netifaces.AF_INET][1]

    def get_default_mac(self, interface) -> str:
        if interface:
            address = netifaces.ifaddresses(interface)
            return str(address[netifaces.AF_LINK][0]['addr']).upper()
        return ""

    def get_defaults_ip(self, interface) -> str:
        if interface:
            ip = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['addr']
            return ip
        return ""

    def get_defualt_mask(self, interface) -> str:
        if interface:
            ip = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['netmask']
            return ip
        return ""

    def get_default_broadcast(self, interface) -> str:
        if interface:
            ip = netifaces.ifaddresses(interface)[netifaces.AF_INET][0]['broadcast']
            return ip
        return ""

class Transfer(dict):
    def __init__(self, upload, download):
        dict.__init__(self, upload=upload, download=download)

class NetworkMonitoring():

    def _get_default_name(self) -> str:
        n = NetCard()
        list = psutil.net_if_addrs()
        for name, item in list.items():
            if n.get_default_mac(n.get_default_interface()) == str(item[0][1]).replace("-", ":"):
                return name
        return ""

    def _bytes_to_kilobytes(self, n):
        score = n / 1024
        return round(score)

    def get_upload_download_monitoring(self, upload=True, download=True) -> Transfer:
        pnic_before = psutil.net_io_counters(pernic=True)
        time.sleep(1)
        pnic_after = psutil.net_io_counters(pernic=True)

        nic_names = list(pnic_after.keys())
        nic_names.sort(key=lambda x: sum(pnic_after[x]), reverse=True)

        for name in nic_names:
            if self._get_default_name() == name:
                stats_before = pnic_before[name]
                stats_after = pnic_after[name]
                upload_T = self._bytes_to_kilobytes(stats_after.bytes_sent - stats_before.bytes_sent)
                download_T = self._bytes_to_kilobytes(stats_after.bytes_recv - stats_before.bytes_recv)
                if upload == True and download == True:
                    return Transfer(upload_T, download_T)
                elif upload == True and download == False:
                    return Transfer(upload_T, -1)
                elif upload == False and download == True:
                    return Transfer(-1, download_T)

                # return Upload,Download w KB/S
        return Transfer(-1, -1)

    def get_default_transfer_upload_download_monitoring(self) -> Transfer:

        pnic_after = psutil.net_io_counters(pernic=True)

        nic_names = list(pnic_after.keys())
        nic_names.sort(key=lambda x: sum(pnic_after[x]), reverse=True)

        for name in nic_names:
            if self._get_default_name() == name:
                stats_after = pnic_after[name]
                return Transfer(("%s" % self._bytes_to_kilobytes(stats_after.bytes_sent)), "%s" % self._bytes_to_kilobytes(stats_after.bytes_recv))
        return Transfer(-1, -1)

    def get_all_card_transfer_upload_download_monitoring(self) -> Transfer:

        pnic_after = psutil.net_io_counters(pernic=True)

        nic_names = list(pnic_after.keys())
        nic_names.sort(key=lambda x: sum(pnic_after[x]), reverse=True)

        sum_upload = 0
        upload = 0
        sum_download = 0
        download = 0
        for name in nic_names:
            stats_after = pnic_after[name]
            sum_upload = sum_upload + stats_after.bytes_sent
            sum_download = sum_download + stats_after.bytes_recv
            upload = self._bytes_to_kilobytes(sum_upload)
            download = self._bytes_to_kilobytes(sum_download)
        return Transfer(upload, download)



class Ping:

    def __init__(self, host="8.8.8.8"):
        self.hostname = host

    def ping(self) -> int:
        output = Popen('ping -n 1 ' + self.hostname, stdin=PIPE, stderr=PIPE, stdout=PIPE, shell=True)
        contents = str(output.stdout.read())
        m = re.search("Average = (.*)ms", contents)
        if str(m) == "None":
            return 0
        try:
            ms = int(m.group(1))
        except:
            return 0
        return ms
    """
    def ping(self) -> int:
        response_list = ping(self.hostname)
        return int(round(response_list.rtt_avg_ms, 0))
    """
