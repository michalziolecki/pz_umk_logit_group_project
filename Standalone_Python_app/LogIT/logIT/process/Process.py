from win32gui import GetWindowText, GetForegroundWindow
from win32process import GetProcessId, GetWindowThreadProcessId
import time
import psutil
import wmi
import getpass
from logIT.develop_log.dev_log import Logger
import datetime
import socket


# MATI


class ProcessJson(dict):
    def __init__(self, process_name, thread_count, creation_date, write_transfer_count, read_transfer_count,
                 data=str(datetime.datetime.now()), host=socket.gethostname(), user=getpass.getuser()):
        dict.__init__(self, data=data, host=host, user=user, process_name=process_name, thread_count=thread_count,
                      creation_date=str(creation_date), write_transfer_count=write_transfer_count,
                      read_transfer_count=read_transfer_count)


class Process():
    def __init__(self, name="Process"):
        self.name = name
        self._logger = Logger.create_to_file_logger(logging_name=self.name)

    def _get_datetime_form_CreationDate(self, creation_date) -> datetime:
        try:
            return datetime.datetime(int(creation_date[:4]), int(creation_date[4:6]), int(creation_date[6:8]),
                                     int(creation_date[8:10]), int(creation_date[10:12]), int(creation_date[12:14]),
                                     int(creation_date[15:21]))
        except Exception as ex:
            self._logger.error(
                msg="Bład podczas {}({}). Bład: {}".format("_get_datetime_form_CreationDate", creation_date, ex))
            return datetime.datetime.now()

    def _bytes_to_kilobytes(self, n):
        score = n / 1024
        return round(score)


class CurrnetProcess(Process):
    def __init__(self):
        Process.name = "CurrnetProcess"

    def get_process(self):
        # Jeśli pusty string to nic nie rób
        # Wystepuje on gdy minimalizuje okno on nie wie jaka aplikacja jest domyslna
        try:
            active_id_window = GetForegroundWindow()
            c = wmi.WMI()
            tmpid, process_id = GetWindowThreadProcessId(active_id_window)
            if (process_id > 0):
                for process in c.Win32_Process(ProcessId=process_id):
                    process_json = ProcessJson(process_name=process.Name, thread_count=process.ThreadCount,
                                               creation_date=self._get_datetime_form_CreationDate(process.CreationDate),
                                               write_transfer_count=self._bytes_to_kilobytes(
                                                   int(process.WriteTransferCount)),
                                               read_transfer_count=self._bytes_to_kilobytes(
                                                   int(process.ReadTransferCount)), data=str(datetime.datetime.now()))
                    return process_json
        except Exception as ex:
            logger = Logger.create_to_file_logger(logging_name='CurrnetProcess')
            logger.error(msg="Bład podcza get_process: {}".format(ex))
            return ""


class UserProcess(Process):
    def __init__(self):
        Process.name = "UserProcess"
        self.session_id = self._get_session_id()

    def _get_session_id(self) -> int:
        session_id = 0
        try:
            c = wmi.WMI()
            for process in c.Win32_Process():
                if process.GetOwner()[2] == getpass.getuser():
                    session_id = process.SessionId
                    break
            return session_id
        except Exception as ex:
            self._logger.error(msg="Bład podczas {}. Bład: {}".format("_get_session_id()", ex))
            return session_id

    def get_user_list_process(self) -> []:
        list = []
        c = wmi.WMI()
        try:
            date = str(datetime.datetime.now())
            for process in c.Win32_Process(SessionId=self.session_id):
                # if process.GetOwner()[2] == getpass.getuser():
                process_json = ProcessJson(process_name=process.Name, thread_count=process.ThreadCount,
                                           creation_date=self._get_datetime_form_CreationDate(process.CreationDate),
                                           write_transfer_count=self._bytes_to_kilobytes(
                                               int(process.WriteTransferCount)),
                                           read_transfer_count=self._bytes_to_kilobytes(int(process.ReadTransferCount)),
                                           data=date)
                list.append(process_json)
            return list
        except Exception as ex:
            self._logger.error(msg="Bład podczas {}. Bład: {}".format("get_user_list_process()", ex))
            return list


class AllProcess(Process):
    def __init__(self):
        Process.name = "AllProcess"

    def get_all_list_process(self) -> []:
        list = []
        c = wmi.WMI()
        try:
            date = str(datetime.datetime.now())
            for process in c.Win32_Process():
                process_json = ProcessJson(process_name=process.Name, thread_count=process.ThreadCount,
                                           creation_date=self._get_datetime_form_CreationDate(process.CreationDate),
                                           write_transfer_count=self._bytes_to_kilobytes(
                                               int(process.WriteTransferCount)),
                                           read_transfer_count=self._bytes_to_kilobytes(int(process.ReadTransferCount)),
                                           data=date)
                list.append(process_json)
            return list
        except Exception as ex:
            self._logger.error(msg="Bład podczas {}. Bład: {}".format("get_all_list_process()", ex))
            return list
