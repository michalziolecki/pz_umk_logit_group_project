from logIT.usb_module.tracking.device_tracker import DeviceTracker
from logIT.usb_module.tracking.remote_memory_tracker import RemoteMemoryTracker
from logIT.usb_module.tracking.memory_scaner import MemoryScanner
from logIT.usb_module.process.process_operation import *
from threading import Thread
import threading
from multiprocessing import Process
import os
import getpass
import socket
from _datetime import datetime


class USBThreadPeriod(Thread, Process):

    def __init__(self, tracker: DeviceTracker):
        Thread.__init__(self)
        self._tracker = tracker

    def run(self):
        self._tracker.track_usb_by_period_of_time()

    def stop_control_gently(self):
        self._tracker.stop_usb_by_flag(True)


class USBThreadEvent(Thread, Process):

    def __init__(self, tracker: DeviceTracker):
        Thread.__init__(self)
        self._tracker = tracker

    def run(self):
        self._tracker.track_usb_by_events()

    def stop_control_gently(self):
        self._tracker.stop_usb_by_flag(True)

    def stop_control_urgent(self):
        self._tracker.stop_usb_by_terminate_pid_process(pid=os.getpid())


class DiskDriversThread(Thread, Process):

    def __init__(self, tracker: RemoteMemoryTracker):
        Thread.__init__(self)
        self._tracker = tracker

    def run(self):
        self._tracker.track_disk_drivers_information()

    def stop_control_gently(self):
        self._tracker.stop_thread_tracking_disk(True)


mutex = threading.Lock()


class BlockPortThreadByExe(Thread):

    def __init__(self, tracker: DeviceTracker):
        Thread.__init__(self)
        self._tracker = tracker

    def run(self):
        mutex.acquire()
        try:
            self._tracker.block_usb_operation_by_exe()
        finally:
            mutex.release()

    def stop(self):
        pid = get_process_pid("PsExec.exe")
        kill_process_by_pid(pid)


class USBMemoryThread(Thread, Process):

    def __init__(self, tracker: RemoteMemoryTracker):
        Thread.__init__(self)
        self._tracker = tracker

    def run(self):
        finger_print = {
            'nick_name': getpass.getuser(),
            'host_name': socket.gethostname(),
            'date_time_info': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
        self._tracker.track_usb_remote_memory(finger_print)

    def stop_control_gently(self):
        pass
        # this thread working without loop
        # so stop automatically


class MemoryScannerThread(Thread, Process):

    def __init__(self, scanner: MemoryScanner):
        Thread.__init__(self)
        self._scanner = scanner

    def run(self):
        scanner_thread_counter = set()
        self._scanner.scan_memory(scanner_thread_counter)

    def stop_control_gently(self):
        pass
        # this thread working without loop
        # so stop automatically when finished scanning
        # or scanned disk disappear
