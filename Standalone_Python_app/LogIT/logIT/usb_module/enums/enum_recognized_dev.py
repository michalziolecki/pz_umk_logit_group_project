from enum import Enum


class Category(Enum):
    PRINTER = 'Printer'
    MOUSE = 'Mouse'
    PENDRIVE = 'USB remote memory'
    MASS_DISK = 'USB Mass Disk'
    KEYBOARD = 'Keyboard'
    PHONE = 'Phone'
    CAMERA = 'Camera'
    CARD = 'USB Card'
    HEADPHONE = 'Headphone'
    UNCATEGORIZED = 'Uncategorized USB'
