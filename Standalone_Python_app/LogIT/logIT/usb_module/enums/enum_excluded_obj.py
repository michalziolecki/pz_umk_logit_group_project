from enum import Enum


class ExcludedObject(Enum):

    concentrator_pl = 'koncentrator'
    concentrator_eng = 'concentrator'
    hub = 'hub'
    generic = 'generic'
    composite_dev_pl = 'kompozytowe'
    composite_dev_eng = 'composite'
    define_dev_pl = 'urządzenie zdefiniowane'
    define_dev_eng = 'device defined'
    dev_controller_hid_pl = 'sterujące'
    dev_controller_hid_eng = 'control'
    controller_dev_pl = 'kontroler'
    controller_dev_eng = 'controller'
    input_dev_pl = 'wejściowe'
    input_dev_eng = 'input'
    adapter_dev = 'transport'
    module_dev_pl = 'moduł'
    module_dev_eng = 'module'
    bluetooth_dev_pl = 'urządzenie bluetooth'
    bluetooth_dev_eng = 'bluetooth device'


