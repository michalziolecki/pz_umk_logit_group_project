from enum import Enum


class InsertStatus(Enum):
    set_in = 'IN'
    set_out = 'OUT'
