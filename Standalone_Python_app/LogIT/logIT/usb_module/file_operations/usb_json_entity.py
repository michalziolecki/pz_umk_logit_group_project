from logIT.usb_module.classes.usb_classes import AbstractDevice


class USBInsertInfoEntity:
    def __init__(self, usb_dev: AbstractDevice, finger_print: dict):
        self._nick_name = finger_print['nick_name']
        self._host_name = finger_print['host_name']
        self._date_time_info = finger_print['date_time_info']
        # self._usb_device = usb_dev
        self._dev_recognize_as = usb_dev.dev_recognize_as
        self._insert_status = usb_dev.insert_status
        self._dev_name = usb_dev.dev_name
        self._description = usb_dev.description
        self._manufacturer = usb_dev.manufacturer
        self._device_id = usb_dev.device_id
        self._guid = usb_dev.guid
        self._dev_type = usb_dev.dev_type

    @property
    def nick_name(self) -> str:
        return self._nick_name

    @property
    def host_name(self) -> str:
        return self._host_name

    @property
    def date_time_info(self) -> str:
        return self._date_time_info

    @property
    def dev_recognize_as(self) -> str:
        return self._dev_recognize_as

    @property
    def insert_status(self) -> str:
        return self._insert_status

    @property
    def dev_name(self) -> str:
        return self._dev_name

    @property
    def description(self) -> str:
        return self._description

    @property
    def manufacturer(self) -> str:
        return self._manufacturer

    @property
    def device_id(self) -> str:
        return self._device_id

    @property
    def guid(self) -> str:
        return self._guid

    @property
    def dev_type(self) -> str:
        return self._dev_type

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__
