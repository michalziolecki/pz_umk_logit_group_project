from logIT.usb_module.classes.memory_info_classes import AbstractRemoteMemoryInfo


class RemoteMemoryInsertInfoEntity:
    def __init__(self, remote_memory: AbstractRemoteMemoryInfo, finger_print: dict):
        self._nick_name = finger_print['nick_name']
        self._host_name = finger_print['host_name']
        self._date_time_info = finger_print['date_time_info']
        self._dev_recognize_as = remote_memory.dev_recognize_as
        self._insert_status = remote_memory.insert_status
        self._disk_name = remote_memory.disk_name
        self._description = remote_memory.description
        self._file_system = remote_memory.file_system
        self._free_space = remote_memory.free_space
        self._max_size = remote_memory.max_size
        self._volume_name = remote_memory.volume_name
        self._volume_serial_number = remote_memory.volume_serial_number

    @property
    def nick_name(self) -> str:
        return self._nick_name

    @property
    def host_name(self) -> str:
        return self._host_name

    @property
    def date_time_info(self) -> str:
        return self._date_time_info

    @property
    def dev_recognize_as(self) -> str:
        return self._dev_recognize_as

    @property
    def insert_status(self) -> str:
        return self._insert_status

    @property
    def disk_name(self) -> str:
        return self._disk_name

    @property
    def description(self) -> str:
        return self._description

    @property
    def file_system(self) -> str:
        return self._file_system

    @property
    def free_space(self) -> str:
        return self._free_space

    @property
    def max_size(self) -> str:
        return self._max_size

    @property
    def volume_name(self) -> str:
        return self._volume_name

    @property
    def volume_serial_number(self) -> str:
        return self._volume_serial_number

    def get_full_dict_of_entity(self) -> dict:
        return self.__dict__
