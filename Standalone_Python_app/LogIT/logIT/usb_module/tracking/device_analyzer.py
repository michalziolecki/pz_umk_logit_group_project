from logIT.usb_module.enums.enum_filter_object import FilterObject
from logIT.usb_module.enums.enum_excluded_obj import ExcludedObject
from logIT.usb_module.classes.usb_classes import *
from logIT.usb_module.file_operations.usb_json_entity import USBInsertInfoEntity
from logIT.usb_module.tracking.remote_memory_tracker import RemoteMemoryTracker
from logIT.develop_log.dev_log import Logger
from logIT.usb_module.enums.enum_recognized_dev import Category
import wmi
import copy
import socket
import getpass
from datetime import datetime


class DeviceAnalyzer:

    def __init__(self, remote_memory_tracker: RemoteMemoryTracker):
        self._remote_memory_tracker = remote_memory_tracker
        self.existed_devices_set = set()
        self.storage_container_to_write = []
        self._application_start = True
        self._logger = Logger.create_to_file_logger(logging_name='Device Analyzer')

    # param - list<wmi._wmi_object>
    def control_usb_devices(self, devices_list: list, period_scan=False):
        status = 1
        try_number = 1
        new_disposes_set = set()
        while status == 1 and try_number <= 5:
            try:
                self._filter_exclude_devices(devices_list)
                new_disposes_set = self._disposes_wmi_obj_into_classes(devices_list)
                status = 0
            except wmi.x_wmi as e:
                self._logger.info("USB suddenly took out when wmi parsing object, info: %s", e.info)
                status = 1
                new_disposes_set.clear()
            except Exception as e:
                self._logger.info("Something went wrong when wmi parsing object, info: %s", e.args)
                status = 1
                new_disposes_set.clear()
            try_number += 1

        if status == 0:
            finger_print = {
                'nick_name': getpass.getuser(),
                'host_name': socket.gethostname(),
                'date_time_info': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            }
            added_device_set = new_disposes_set.difference(self.existed_devices_set)
            removed_device_set = self.existed_devices_set.difference(new_disposes_set)
            self._write_usb_devices(added_device_set, InsertStatus.set_in, finger_print)
            self._write_usb_devices(removed_device_set, InsertStatus.set_out, finger_print)
            self.existed_devices_set = new_disposes_set
            if self.is_cached_new_memory_device(added_device_set):
                self._remote_memory_tracker.track_usb_remote_memory(finger_print, added_status=True)
            if len(removed_device_set) > 0:
                self._remote_memory_tracker.track_usb_remote_memory(finger_print, added_status=False)
            if self._application_start or period_scan is True:
                self._application_start = False
                self._remote_memory_tracker.track_usb_remote_memory(finger_print, added_status=True)

    def is_cached_new_memory_device(self, added_device_set):
        for device in added_device_set:
            device: AbstractDevice
            if device.dev_recognize_as == Category.PENDRIVE.value or \
                    device.dev_recognize_as == Category.MASS_DISK.value:
                return True

        return False

    def _write_usb_devices(self, devices_list: set, insert_status: InsertStatus, finger_print: dict):

        for device in devices_list:
            device: AbstractDevice
            dev_to_write = copy.copy(device)
            dev_to_write._insert_status = insert_status.value
            new_device = USBInsertInfoEntity(dev_to_write, finger_print)
            self.storage_container_to_write.append(new_device)

    # param - list < wmi._wmi_object >
    def _filter_exclude_devices(self, devices_list: list):
        devices_list_copy = copy.copy(devices_list)
        for device in devices_list_copy:
            check_obj = ' ' + str(device.Dependent.Caption).lower()
            exclude = self._check_exclude_status(check_obj)
            if exclude:
                devices_list.remove(device)

    def _check_exclude_status(self, check_obj: str) -> bool:

        if check_obj.find(ExcludedObject.concentrator_pl.value) > 0 \
                or check_obj.find(ExcludedObject.concentrator_eng.value) > 0 \
                or check_obj.find(ExcludedObject.hub.value) > 0 \
                or check_obj.find(ExcludedObject.generic.value) > 0 \
                or check_obj.find(ExcludedObject.composite_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.composite_dev_eng.value) > 0 \
                or check_obj.find(ExcludedObject.define_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.define_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.dev_controller_hid_pl.value) > 0 \
                or check_obj.find(ExcludedObject.dev_controller_hid_eng.value) > 0 \
                or check_obj.find(ExcludedObject.controller_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.controller_dev_eng.value) > 0 \
                or check_obj.find(ExcludedObject.input_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.input_dev_eng.value) > 0 \
                or check_obj.find(ExcludedObject.adapter_dev.value) > 0 \
                or check_obj.find(ExcludedObject.module_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.module_dev_eng.value) > 0 \
                or check_obj.find(ExcludedObject.bluetooth_dev_pl.value) > 0 \
                or check_obj.find(ExcludedObject.bluetooth_dev_eng.value) > 0:
            return True
        else:
            return False

    def _disposes_wmi_obj_into_classes(self, devices_list) -> set:

        disposes_set = set()

        for device in devices_list:
            # device = copy._copy_immutable(device_usb)
            dev_caption = ' ' + str(device.Dependent.Caption).lower()
            if dev_caption.find(FilterObject.mouse_pl.value) > 0 or dev_caption.find(
                    FilterObject.mouse_eng.value) > 0:
                converted_dev = Mouse(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.pendrive_pl.value) > 0 or dev_caption.find(
                    FilterObject.pendrive_eng.value) > 0:
                converted_dev = Pendrive(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.phone_pl.value) > 0 or dev_caption.find(
                    FilterObject.phone_eng.value) > 0:
                converted_dev = Phone(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.mass_usb_memory_pl.value) > 0 or dev_caption.find(
                    FilterObject.mass_usb_memory_eng.value) > 0:
                converted_dev = MassUSBDisk(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.camera_pl.value) > 0 or dev_caption.find(
                    FilterObject.camera_eng.value) > 0:
                converted_dev = Camera(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.printer_pl.value) > 0 or dev_caption.find(
                    FilterObject.printer_eng.value) > 0:
                converted_dev = Printer(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.keyboard_pl.value) > 0 or dev_caption.find(
                    FilterObject.keyboard_eng.value) > 0:
                converted_dev = Keyboard(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.card_pl.value) > 0 or dev_caption.find(
                    FilterObject.card_eng.value) > 0:
                converted_dev = Card(device)
                disposes_set.add(converted_dev)

            elif dev_caption.find(FilterObject.headphone_pl.value) > 0 or dev_caption.find(
                    FilterObject.headphone_eng.value) > 0:
                converted_dev = Headphone(device)
                disposes_set.add(converted_dev)

            else:
                converted_dev = UncategorizedDev(device)
                disposes_set.add(converted_dev)

        return disposes_set
