--Jeśli posiadasz Triggery i TRIGGER Function to usuń je przed ponownym wykonaniem


CREATE OR REPLACE FUNCTION add_Ustawienia_Internetowe()
  RETURNS trigger AS
$BODY$
BEGIN
 insert into Ustawienia_Internetowe (ID_Uzytkownicy_Keylogger, 
									Czy_Monitorowac_Transfer_Obecny, 
									Co_Ile_Monitorowac_Transfer_Obecny, 
									Czy_Predkosc_Wysylania, 
									Czy_Predkosc_Pobierania, 
									Czy_Monitorowac_Transfer_Od_Wlaczenia_Komputera, 
									Co_Ile_Monitorowac_Transfer_Od_Wlaczenia_Komputera, 
									Czy_Pingowac, 
									Adres_Dla_Pingu, 
									Co_Ile_Pingowac, 
									Czy_Zbierac_Informacje_O_Karcie_Sieciowej, 
									co_ile_zbierac_informacje_o_karcie_sieciowej)
values (new.ID,1,30,1,1,1,30,1,'8.8.8.8',5,1,30);

insert into Ustawienia_Keylogger (ID_Uzytkownicy_Keylogger,Czy_Zbierac_Wiadomosc)
values (new.ID,1);

insert into Ustawienia_Proces (ID_Uzytkownicy_Keylogger,Czy_Zbierac_Aktywny_Proces, Czy_Zbierac_Procesy_Usera, Co_Ile_Zbierac_Procesy_Usera)
values (new.ID,1,1,500);

insert into blokowanie_usb (ID_Uzytkownicy_Keylogger,blokuj_porty)
values (new.ID,0);

insert into sledzenie_usb (ID_Uzytkownicy_Keylogger,czy_sledzic, czy_skanowac)
values (new.ID,1,1);

insert into sledzenie_napedu_plyt (ID_Uzytkownicy_Keylogger,czy_sledzic, czy_skanowac)
values (new.ID,1,1);

insert into niesledzone_partycje (ID_Uzytkownicy_Keylogger, nazwa_partycji_dysku)
values (new.ID,'C:');
 
 RETURN NEW;
END;
$BODY$

LANGUAGE plpgsql VOLATILE
COST 100;


CREATE TRIGGER IF_Add_User
  after insert
  ON Uzytkownicy_Keylogger
  FOR EACH ROW
  EXECUTE PROCEDURE add_Ustawienia_Internetowe();
  
 ---------------------------------------------------------------------------------- 



  
  
  
 CREATE OR REPLACE FUNCTION delete_all()
  RETURNS trigger AS
$BODY$
BEGIN
 delete from Ustawienia_Internetowe where ID_Uzytkownicy_Keylogger = old.id;
 delete from Transfer_Obecny where ID_Uzytkownicy_Keylogger = old.id;
 delete from Transfer_Od_Wlaczenia_Komputera where ID_Uzytkownicy_Keylogger = old.id;
 delete from Pingi where ID_Uzytkownicy_Keylogger = old.id;
 delete from Informacje_O_Karcie_Sieciowej where ID_Uzytkownicy_Keylogger = old.id;
 delete from Ustawienia_Keylogger where ID_Uzytkownicy_Keylogger = old.id;
 delete from Keylogger_Filtr where ID_Uzytkownicy_Keylogger = old.id;
 delete from Keylogger_Wiadomosci where ID_Uzytkownicy_Keylogger = old.id;
 delete from Ustawienia_Proces where ID_Uzytkownicy_Keylogger = old.id;
 delete from Informacje_O_Aktywnym_Procesie where ID_Uzytkownicy_Keylogger = old.id;
 delete from Informacje_O_Procesach_Usera where ID_Uzytkownicy_Keylogger = old.id;
 delete from urzadzenia_sledzenie where ID_Uzytkownicy_Keylogger = old.id;
 delete from blokowanie_usb where ID_Uzytkownicy_Keylogger = old.id;
 delete from sledzenie_usb where ID_Uzytkownicy_Keylogger = old.id;
 delete from sledzenie_napedu_plyt where ID_Uzytkownicy_Keylogger = old.id;
 delete from uprawnienia_administracyjne where ID_Uzytkownicy_Keylogger = old.id;
 delete from skanowanie_dyskow_zdalnych where ID_Uzytkownicy_Keylogger = old.id;
 delete from informacje_dyski_zdalne where ID_Uzytkownicy_Keylogger = old.id;
 delete from niesledzone_partycje where ID_Uzytkownicy_Keylogger = old.id;

 
 RETURN OLD;
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


CREATE TRIGGER IF_delete_User
  before delete
  ON Uzytkownicy_Keylogger
  FOR EACH ROW
  EXECUTE PROCEDURE delete_all();
  
  
  --------------------------------------------------------------------------------------
  


create FUNCTION blocked_double_user()
  RETURNS trigger AS
$BODY$
BEGIN
   IF (SELECT id FROM Uzytkownicy_Keylogger WHERE nazwa_komputera = NEW.nazwa_komputera and Nazwa_Uzytkownika_Na_Komputerze = NEW.Nazwa_Uzytkownika_Na_Komputerze) is not null THEN
      RAISE EXCEPTION 'Uzytkownik juz istnieje w bazie danych';
   END IF;
   RETURN NEW;
 
END;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
  
create TRIGGER blocked_double_users
BEFORE INSERT ON uzytkownicy_keylogger
FOR EACH ROW EXECUTE PROCEDURE blocked_double_user();