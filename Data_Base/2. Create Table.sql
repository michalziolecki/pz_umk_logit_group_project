CREATE TABLE Uzytkownicy_Keylogger 
(
    ID INT Primary Key GENERATED ALWAYS AS IDENTITY,
    Nazwa_Komputera VARCHAR(50) NOT NULL,
	Nazwa_Uzytkownika_Na_Komputerze varchar(50) not null,
	Opis varchar(250)
);

create table Ustawienia_Internetowe	
(
	ID int Primary Key Generated Always as Identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Czy_Monitorowac_Transfer_Obecny smallint not null,
	Co_Ile_Monitorowac_Transfer_Obecny int,
	Czy_Predkosc_Wysylania smallint not null,
	Czy_Predkosc_Pobierania smallint not null,
	Czy_Monitorowac_Transfer_Od_Wlaczenia_Komputera smallint not null,
	Co_Ile_Monitorowac_Transfer_Od_Wlaczenia_Komputera int,
	Czy_Pingowac smallint not null,
	Adres_Dla_Pingu varchar(50),
	Co_Ile_Pingowac int,
	Czy_Zbierac_Informacje_O_Karcie_Sieciowej smallint not null,
	Co_Ile_Zbierac_Informacje_O_Karcie_Sieciowej int
);

create table Transfer_Obecny
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Predkosc_Wysylania int,
	Predkosc_Pobiernia int,
	kiedy_rozpoczeto_pomiar timestamp not null
);

create table Transfer_Od_Wlaczenia_Komputera
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	ilosc_wyslanych_kb bigint,
	ilosc_pobranych_kb bigint,
	kiedy_rozpoczeto_pomiar timestamp not null
);

create table Pingi
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Pomiar_Pingu_w_ms int not null,
	Adres varchar(50),
	kiedy_rozpoczeto_pomiar timestamp not null
);

create table Informacje_O_Karcie_Sieciowej 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Ineterface varchar(50),
	Mac varchar(20),
	Ip varchar(20),
	Maska varchar(20),
	Broadcast varchar(20),
	kiedy_Zebrano_Informacje timestamp not null
);

create table Ustawienia_Keylogger 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Czy_Zbierac_Wiadomosc smallint not null
);

create table Keylogger_Filtr 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Filtr varchar(50) not null
);

create table Keylogger_Wiadomosci 
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Wiadomosc varchar(2000) not null,
	Kiedy_Zebrano_Wiadomosc timestamp not null
);

create table Uzytkownicy_Webservice
(
	ID int primary key generated always as identity,
	Login varchar(50) not null,
	Haslo varchar(200) not null
);

create table Departamenty
(
	ID int primary key generated always as identity,
	Nazwa varchar(50) not null,
	Opis varchar(250) not null
);

create table Departamenty_Uzytkownicy_Keylogger
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	ID_Departamenty int references Departamenty(ID) not null
);

create table Role
(
	ID int primary key generated always as identity,
	Nazwa varchar(50) not null,
	Kod varchar(50) not null
);

create table Role_Przydzial
(
	ID int primary key generated always as identity,
	id_rola int references Role(ID) not null,
	id_uzytkownik_webservice int references Uzytkownicy_Webservice(ID) not null
);

create table Rola_Departamenty
(
	ID int primary key generated always as identity,
	id_role_przydzial int references Role_Przydzial(ID),
	id_departament int references Departamenty(ID) not null
);

create table Ustawienia_Proces
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Czy_Zbierac_Aktywny_Proces smallint not null,
	Czy_Zbierac_Procesy_Usera smallint not null,
	Co_Ile_Zbierac_Procesy_Usera int null
);

create table Informacje_O_Aktywnym_Procesie
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Kiedy_Zebrano_Informacje timestamp not null,
	Nazwa_Procesu varchar(100) null,
	Ilosc_Watkow int null,
	Od_Kiedy_Dziala_Proces timestamp null,
	Zapis_W_Kilobytach int null,
	Odczyt_W_Kilobytach int null
);

create table Informacje_O_Procesach_Usera
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	Kiedy_Zebrano_Informacje timestamp not null,
	Nazwa_Procesu varchar(100) null,
	Ilosc_Watkow int null,
	Od_Kiedy_Dziala_Proces timestamp null,
	Zapis_W_Kilobytach bigint null,
	Odczyt_W_Kilobytach bigint null
);

create table urzadzenia_sledzenie
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	data_operacji timestamp not null,
	rozpoznany_jako varchar(50) not null,
	status_operacji varchar(3) not null,
	nazwa_urzadzenia varchar(250) not null,
	opis varchar(250) not null,
	producent varchar(250) not null,
	id_urzadzenia varchar(250) not null,
	guid_urzadzenia varchar(250) not null,
	typ_urzadzenia varchar(100) not null
);

create table blokowanie_usb
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	blokuj_porty smallint not null
);

create table uprawnienia_administracyjne
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	login_administratora varchar(250) not null,
	haslo_administratora varchar(250) not null
);

create table informacje_dyski_zdalne
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	data_operacji timestamp not null,
	rozpoznany_jako varchar(50) not null,
	status_operacji varchar(3) not null,
	nazwa_dysku varchar(3) not null,
	opis varchar(100) not null,
	system_plikow varchar(10) not null,
	wolna_przestrzen bigint not null,
	calkowita_przestrzen bigint not null,
	nazwa_woluminu varchar(100) not null,
	identyfikator_woluminu varchar(100) not null
);

create table sledzenie_usb
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	czy_sledzic smallint not null,
	czy_skanowac smallint not null
);

create table sledzenie_napedu_plyt
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	czy_sledzic smallint not null,
	czy_skanowac smallint not null
);

create table skanowanie_dyskow_zdalnych			
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	id_dysku int references informacje_dyski_zdalne(ID) not null,
	nazwa_dysku varchar(3) not null,
	data_operacji timestamp not null,
	nazwa_pliku varchar(250) not null,
	sciezka_pliku varchar(512) not null,
	typ_pliku varchar(100) not null,
	jest_binarny smallint not null,
	rozszerzenie varchar(10) not null,
	rozmiar bigint not null
);

create table niesledzone_partycje
(
	ID int primary key generated always as identity,
	ID_Uzytkownicy_Keylogger int references Uzytkownicy_Keylogger(ID) not null,
	nazwa_partycji_dysku varchar(3) not null
);

