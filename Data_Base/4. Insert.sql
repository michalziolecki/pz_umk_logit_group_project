insert into Uzytkownicy_Keylogger (Nazwa_Komputera,Nazwa_Uzytkownika_Na_Komputerze)
values ('Zielony','Mateusz'),('PC-Laptop','Michal'),('Pc','Iwona');

insert into Uzytkownicy_Keylogger (Nazwa_Komputera,Nazwa_Uzytkownika_Na_Komputerze,opis)
values ('Kolega','Kamil',':)');


insert into Transfer_Obecny (ID_Uzytkownicy_Keylogger,Predkosc_Wysylania,Predkosc_Pobiernia,kiedy_rozpoczeto_pomiar)
values (1,5,50,NOW()),(2,0,10,NOW()),(3,null,150,NOW()),(4,null,180,NOW());

insert into Informacje_O_Aktywnym_Procesie (ID_Uzytkownicy_Keylogger,Kiedy_Zebrano_Informacje,Nazwa_Procesu,Ilosc_Watkow,Od_Kiedy_Dziala_Proces,Zapis_W_Kilobytach,Odczyt_W_Kilobytach)
values(1,NOW(),'chrome.exe',3,NOW(),423423,42534645),(1,NOW(),'pycharm64.exe',5,NOW(),345665,4567),(1,NOW(),'explorer.exe',3,NOW(),2345,4567);

insert into urzadzenia_sledzenie (ID_Uzytkownicy_Keylogger,data_operacji,rozpoznany_jako,status_operacji,nazwa_urzadzenia,opis,producent,id_urzadzenia,guid_urzadzenia,typ_urzadzenia)
values (3,NOW(),'pamiec SD','sta','Kingstone','opis','Kingstone','123','123asd1d23','Karta pamięci')

insert into blokowanie_usb	(ID_Uzytkownicy_Keylogger,blokuj_porty)
values (3,1)

insert into Transfer_Od_Wlaczenia_Komputera  (ID_Uzytkownicy_Keylogger,ilosc_wyslanych_kb,ilosc_pobranych_kb,kiedy_rozpoczeto_pomiar)
values (1,2550,8015,now()),(3,13556,144485,now());

insert into Informacje_O_Aktywnym_Procesie (ID_Uzytkownicy_Keylogger,Kiedy_Zebrano_Informacje,Nazwa_Procesu,Ilosc_Watkow,Od_Kiedy_Dziala_Proces,Zapis_W_Kilobytach,Odczyt_W_Kilobytach)
values(1,NOW(),'chrome.exe',3,NOW(),423423,42534645),(2,NOW(),'pycharm64.exe',3,NOW(),423423,42534645);

insert into uprawnienia_administracyjne	(ID_Uzytkownicy_Keylogger,login_administratora,haslo_administratora)
values (3,'SZYFR_LOGIN','SZYFR_HASLO')

insert into informacje_dyski_zdalne	(ID_Uzytkownicy_Keylogger,data_operacji,rozpoznany_jako,status_operacji,nazwa_dysku,opis ,system_plikow,wolna_przestrzen,calkowita_przestrzen,nazwa_woluminu,identyfikator_woluminu)
values (3,NOW(),'Pendrive','sta','D','Opis','NTFS',123432,250000000,'Nowy','identity')

insert into skanowanie_dyskow_zdalnych (ID_Uzytkownicy_Keylogger,id_dysku,nazwa_dysku,data_operacji,nazwa_pliku,sciezka_pliku,typ_pliku,jest_binarny,rozszerzenie,rozmiar)
values (3,1,'naz',Now(),'NazwaPliku','C:\\Tmp\\','tekstowy',0,'txt',1584)

insert into Pingi (ID_Uzytkownicy_Keylogger,Pomiar_Pingu_w_ms,Adres,kiedy_rozpoczeto_pomiar)
values (3,25,'8.8.8.8',now()),(3,48,'8.8.8.8',now()),(3,32,'8.8.8.8',now());


insert into Informacje_O_Karcie_Sieciowej (ID_Uzytkownicy_Keylogger,Ineterface,Mac,IP,Maska,Broadcast,kiedy_Zebrano_Informacje)
values (2,'Wi-fi1','00:00','192.168.1.25','255.255.255.0','255.255.255.254',now()),(3,'Wi-fi5','00:01','192.168.1.32','255.255.255.0','255.255.255.254',now());


insert into Keylogger_Filtr	 (ID_Uzytkownicy_Keylogger,Filtr)
values (1,'Praca'),(2,'Test'),(1,'LOL'),(3,'CS');


insert into Keylogger_Wiadomosci (ID_Uzytkownicy_Keylogger,Wiadomosc,kiedy_Zebrano_wiadomosc)
values(1,'Szukam pracy',now()),(2,'Test231',now()),(3,'Cs rozgrywki',now());

insert into Uzytkownicy_Webservice (login, haslo)
values ('admin','$2a$10$qXE5Z9xdCcyhgYeA1Tqqo.Jvq22yKZTNgdQ0ZK4z0SivWlw8eqWni');

insert into Uzytkownicy_Webservice (login, haslo)
values ('manager','$2a$10$2k/Hem5URfJReGd8Hhw3ae7fJEAWV8F/qmaOn7JzS5zKaKbUNW5a2');

insert into role (nazwa, kod)
values ('ADMIN',1);

insert into role (nazwa, kod) 
values ('MANAGER',2);

insert into departamenty (nazwa, opis)
values ('Marketing','opis');

insert into departamenty (nazwa, opis)
values ('Ksiegowosc','opis');

insert into role_przydzial (id_rola, id_uzytkownik_webservice)
values (1,1);

insert into role_przydzial (id_rola, id_uzytkownik_webservice)
values (2,2);

insert into rola_departamenty (id_role_przydzial, id_departament)
values (1,1);

insert into rola_departamenty (id_role_przydzial, id_departament)
values (2,1);