package pl.zespolowka.logit.service;

import pl.zespolowka.logit.configuration.security.exception.UserExistsException;
import pl.zespolowka.logit.domain.dto.WebserviceUserDTO;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.List;

public interface WebserviceUserService {

    WebserviceUser findById(Integer userId);

    WebserviceUser createWebserviceUserAccountIfDoesNotExists(String username);

    WebserviceUser getWebserviceUserByUsername(String username);

    WebserviceUserDTO save(WebserviceUserDTO webserviceUserDTO) throws UserExistsException;

    List<WebserviceUserDTO> findAll();
}
