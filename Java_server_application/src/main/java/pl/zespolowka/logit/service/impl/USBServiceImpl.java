package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.USBPortDTO;
import pl.zespolowka.logit.domain.mapper.USBPortMapper;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.USBPort;
import pl.zespolowka.logit.repository.KeyloggerUserRepository;
import pl.zespolowka.logit.repository.USBRepository;
import pl.zespolowka.logit.service.USBService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class USBServiceImpl implements USBService {

    @Autowired
    USBRepository usbRepository;
    @Autowired
    KeyloggerUserRepository keyloggerUserRepository;

    @Override
    public List<USBPortDTO> getUserUSBList(int userId) {
        Optional<KeyloggerUser> user = keyloggerUserRepository.findById(userId);
        return usbRepository
                .findAllByKeyloggerUser(user.get())
                .stream()
                .map(USBPortMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<USBPortDTO> changeStatus(int userId, int usbPortId) {
        Optional<KeyloggerUser> user = keyloggerUserRepository.findById(userId);
        Optional<USBPort> optUSBPort = usbRepository.findById(usbPortId);
        if (optUSBPort.isPresent()) {
            changeUSBPortStatus(optUSBPort.get());
        }
        return usbRepository.findAllByKeyloggerUser(user.get())
                .stream()
                .map(USBPortMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    private void changeUSBPortStatus(USBPort usbPort) {
        if (usbPort.getIsDisabled() == 1) {
            usbPort.setIsDisabled((short) 0);
        } else {
            usbPort.setIsDisabled((short) 1);
        }
        usbRepository.save(usbPort);
    }

}
