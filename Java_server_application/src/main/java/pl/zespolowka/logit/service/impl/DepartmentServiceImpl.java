package pl.zespolowka.logit.service.impl;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.DepartmentDTO;
import pl.zespolowka.logit.domain.mapper.DepartmentMapper;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.repository.DepartmentRepository;
import pl.zespolowka.logit.service.DepartmentService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    private DepartmentRepository departmentRepository;

    @Override
    public Department findById(int id) {
        return departmentRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Department not found"));
    }

    @Override
    public List<DepartmentDTO> findAll() {
        return departmentRepository.findAll().stream().map(DepartmentMapper.INSTANCE::map).collect(Collectors.toList());
    }

    @Override
    public Department create(DepartmentDTO departmentDTO) {
        Department department = DepartmentMapper.INSTANCE.map(departmentDTO);
        return departmentRepository.save(department);
    }
}
