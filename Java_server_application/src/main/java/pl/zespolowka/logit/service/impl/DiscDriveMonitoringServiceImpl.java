package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.DiscDriveMonitoringDTO;
import pl.zespolowka.logit.domain.mapper.DiscDriveMonitoringMapper;
import pl.zespolowka.logit.domain.model.DiscDriveMonitoring;
import pl.zespolowka.logit.repository.DiscDriveMonitoringRepository;
import pl.zespolowka.logit.service.DiscDriveMonitoringService;
import pl.zespolowka.logit.utils.PostgreBoolean;

import javax.persistence.EntityNotFoundException;

@Service
public class DiscDriveMonitoringServiceImpl implements DiscDriveMonitoringService {

    @Autowired
    private DiscDriveMonitoringRepository discDriveMonitoringRepository;

    @Override
    public DiscDriveMonitoringDTO getDiscDriveMonitoringInfo(int userId) {
        return discDriveMonitoringRepository.findByKeyloggerUserId(userId)
                .map(DiscDriveMonitoringMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Disc drive info not found"));
    }

    @Override
    public DiscDriveMonitoringDTO changeDiscDriveMonitoringStatus(int userId) {
        DiscDriveMonitoring discDriveMonitoring = getDiscDriveMonitoring(userId);
        discDriveMonitoring.setIsMonitored(PostgreBoolean.changeBoolean(discDriveMonitoring.getIsMonitored()));
        return saveFlushAndReturnDTO(discDriveMonitoring);
    }

    @Override
    public DiscDriveMonitoringDTO changeDiscDriveScanningStatus(int userId) {
        DiscDriveMonitoring discDriveMonitoring = getDiscDriveMonitoring(userId);
        discDriveMonitoring.setIsScaned(PostgreBoolean.changeBoolean(discDriveMonitoring.getIsScaned()));
        return saveFlushAndReturnDTO(discDriveMonitoring);
    }

    private DiscDriveMonitoringDTO saveFlushAndReturnDTO(DiscDriveMonitoring discDriveMonitoring) {
        discDriveMonitoringRepository.saveAndFlush(discDriveMonitoring);
        return DiscDriveMonitoringMapper.INSTANCE.map(discDriveMonitoring);
    }

    private DiscDriveMonitoring getDiscDriveMonitoring(int userId) {
        return discDriveMonitoringRepository.findByKeyloggerUserId(userId)
                .orElseThrow(() -> new EntityNotFoundException("Disc drive monitoring status not found"));
    }
}
