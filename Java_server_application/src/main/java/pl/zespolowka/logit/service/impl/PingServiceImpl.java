package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.PingDTO;
import pl.zespolowka.logit.domain.mapper.PingMapper;
import pl.zespolowka.logit.repository.PingRepository;
import pl.zespolowka.logit.service.PingService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PingServiceImpl implements PingService {

    @Autowired
    private PingRepository pingRepository;

    @Override
    public PingDTO getActualPing(int userId) {
        return pingRepository.findFirstByKeyloggerUserIdOrderByMonitorTimestampDesc(userId)
                .map(PingMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Ping for user id" + userId + "not found"));
    }

    @Override
    public List<PingDTO> getPingHistory(int userId) {
        return pingRepository.findAllByKeyloggerUserIdOrderByMonitorTimestampDesc(userId)
                .stream()
                .map(PingMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }


}
