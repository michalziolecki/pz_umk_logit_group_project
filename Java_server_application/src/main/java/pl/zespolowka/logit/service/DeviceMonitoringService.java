package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.DeviceMonitoringDTO;

import java.util.List;

public interface DeviceMonitoringService {

    List<DeviceMonitoringDTO> getAllUserConnectedDevices(int userId);

    List<DeviceMonitoringDTO> getAllUserDevices(int userId);
}
