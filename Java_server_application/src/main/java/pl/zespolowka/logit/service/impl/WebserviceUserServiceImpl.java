package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.security.exception.UserExistsException;
import pl.zespolowka.logit.configuration.security.service.RoleAssignmentService;
import pl.zespolowka.logit.domain.dto.WebserviceUserDTO;
import pl.zespolowka.logit.domain.mapper.WebserviceUserMapper;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.repository.RoleAssignmentRepository;
import pl.zespolowka.logit.repository.WebserviceUserRepository;
import pl.zespolowka.logit.service.WebserviceUserService;

import javax.persistence.EntityNotFoundException;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Service
public class WebserviceUserServiceImpl implements WebserviceUserService {

    @Autowired
    private WebserviceUserRepository webserviceUserRepository;

    @Autowired
    private RoleAssignmentRepository roleAssignmentRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Override
    public WebserviceUser findById(Integer userId) {
        return webserviceUserRepository.findById(userId).orElseThrow(() -> new EntityNotFoundException("User not found"));
    }

    @Override
    public WebserviceUser createWebserviceUserAccountIfDoesNotExists(String username) {
        WebserviceUser webserviceUser = webserviceUserRepository.findByUsernameIgnoreCase(username);
        if (nonNull(webserviceUser)) {
            webserviceUser.setRoleAssignments(new HashSet<>());
            return createUser(webserviceUser);
        }
        return webserviceUser;
    }

    @Override
    public WebserviceUser getWebserviceUserByUsername(String username) {
        return null;
    }

    @Override
    public WebserviceUserDTO save(WebserviceUserDTO webserviceUserDTO) throws UserExistsException {
        if(nonNull(webserviceUserRepository.findByUsernameIgnoreCase(webserviceUserDTO.getUsername()))){
            throw new UserExistsException("User " + webserviceUserDTO.getUsername() + " already exsists");
        }
        WebserviceUser webserviceUser = new WebserviceUser();
        webserviceUser.setUsername(webserviceUserDTO.getUsername());
        webserviceUser.setPassword(passwordEncoder.encode(webserviceUserDTO.getPassword()));
        WebserviceUser user = webserviceUserRepository.save(webserviceUser);
        return WebserviceUserMapper.INSTANCE.map(user);
    }

    @Override
    public List<WebserviceUserDTO> findAll() {
        return webserviceUserRepository.findAll().stream().map(WebserviceUserMapper.INSTANCE::map).collect(Collectors.toList());
    }

    private WebserviceUser createUser(WebserviceUser webserviceUser) {
        return webserviceUserRepository.save(webserviceUser);
    }
}
