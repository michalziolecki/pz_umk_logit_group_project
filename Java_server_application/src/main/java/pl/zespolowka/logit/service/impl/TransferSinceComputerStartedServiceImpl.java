package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.TransferSinceComputerStartedDTO;
import pl.zespolowka.logit.domain.mapper.TransferSinceComputerStartedMapper;
import pl.zespolowka.logit.repository.TransferSinceComputerStartedRepository;
import pl.zespolowka.logit.service.TransferSinceComputerStartedService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TransferSinceComputerStartedServiceImpl implements TransferSinceComputerStartedService {

    @Autowired
    private TransferSinceComputerStartedRepository transferSinceComputerStartedRepository;

    @Override
    public TransferSinceComputerStartedDTO getTransferSinceComputerStarted(int userId) {
        return transferSinceComputerStartedRepository.findFirstByKeyloggerUserIdOrderByMonitorTimestampDesc(userId)
                .map(TransferSinceComputerStartedMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Transfer status not found"));
    }

    @Override
    public List<TransferSinceComputerStartedDTO> getTransferSinceComputerStartedHistory(int userId) {
        return transferSinceComputerStartedRepository.findAllByKeyloggerUserIdOrderByMonitorTimestampDesc(userId)
                .stream()
                .map(TransferSinceComputerStartedMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
