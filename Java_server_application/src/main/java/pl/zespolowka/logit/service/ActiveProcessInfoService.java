package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.ActiveProcessInfoDTO;

import java.util.List;

public interface ActiveProcessInfoService {
    ActiveProcessInfoDTO getProcessByUserId(int userId);

    List<ActiveProcessInfoDTO> getProcessHistoryByUserId(int userId);
}
