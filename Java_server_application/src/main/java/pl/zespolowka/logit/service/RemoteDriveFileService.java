package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.RemoteDriveFileDTO;

import java.util.List;

public interface RemoteDriveFileService {
    List<RemoteDriveFileDTO> getAllRemoteDriveFilesHistory(int driveId);

    List<RemoteDriveFileDTO> getAllRemoteDriveFiles(int driveId);
}
