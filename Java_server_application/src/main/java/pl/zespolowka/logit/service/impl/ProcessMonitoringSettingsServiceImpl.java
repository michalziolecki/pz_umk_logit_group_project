package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.ProcessMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.mapper.ProcessMonitoringSettingsMapper;
import pl.zespolowka.logit.domain.model.ProcessMonitoringSettings;
import pl.zespolowka.logit.repository.ProcessMonitoringSettingsRepository;
import pl.zespolowka.logit.service.ProcessMonitoringSettingsService;
import pl.zespolowka.logit.utils.PostgreBoolean;

import javax.persistence.EntityNotFoundException;

@Service
public class ProcessMonitoringSettingsServiceImpl implements ProcessMonitoringSettingsService {

    @Autowired
    private ProcessMonitoringSettingsRepository processMonitoringSettingsRepository;

    @Override
    public ProcessMonitoringSettingsDTO getUserProcessMonitoringSettings(int userId) {
        return processMonitoringSettingsRepository.findByKeyloggerUserId(userId)
                .map(ProcessMonitoringSettingsMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Process monitoring settings for user id " + userId + "not found"));
    }

    @Override
    public ProcessMonitoringSettingsDTO changeUserActiveProcessMonitoringStatus(int userId) {
        ProcessMonitoringSettings processMonitoringSettings = getProcessMonitoringSettings(userId);
        processMonitoringSettings.setIsActiveProcessMonitored(PostgreBoolean.changeBoolean(processMonitoringSettings.getIsActiveProcessMonitored()));
        return saveFlushAndReturnDTO(processMonitoringSettings);
    }

    @Override
    public ProcessMonitoringSettingsDTO changeUserProcessesMonitoringStatus(int userId) {
        ProcessMonitoringSettings processMonitoringSettings = getProcessMonitoringSettings(userId);
        processMonitoringSettings.setAreProcessesMonitored(PostgreBoolean.changeBoolean(processMonitoringSettings.getAreProcessesMonitored()));
        return saveFlushAndReturnDTO(processMonitoringSettings);
    }

    @Override
    public ProcessMonitoringSettingsDTO changeUserProcessesMonitoringInterval(int userId, int interval) {
        ProcessMonitoringSettings processMonitoringSettings = getProcessMonitoringSettings(userId);
        processMonitoringSettings.setProcessMonitoringFrequency(interval);
        return saveFlushAndReturnDTO(processMonitoringSettings);
    }

    private ProcessMonitoringSettingsDTO saveFlushAndReturnDTO(ProcessMonitoringSettings processMonitoringSettings) {
        processMonitoringSettingsRepository.saveAndFlush(processMonitoringSettings);
        return ProcessMonitoringSettingsMapper.INSTANCE.map(processMonitoringSettings);
    }

    private ProcessMonitoringSettings getProcessMonitoringSettings(int userId) {
        return processMonitoringSettingsRepository.findByKeyloggerUserId(userId)
                .orElseThrow(() -> new EntityNotFoundException("Process monitoring settings for user id " + userId + "not found"));
    }
}
