package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.PingDTO;

import java.util.List;

public interface PingService {
    PingDTO getActualPing(int userId);

    List<PingDTO> getPingHistory(int userId);
}
