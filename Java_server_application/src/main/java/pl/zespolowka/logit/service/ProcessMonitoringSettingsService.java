package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.ProcessMonitoringSettingsDTO;

public interface ProcessMonitoringSettingsService {
    ProcessMonitoringSettingsDTO getUserProcessMonitoringSettings(int userId);

    ProcessMonitoringSettingsDTO changeUserActiveProcessMonitoringStatus(int userId);

    ProcessMonitoringSettingsDTO changeUserProcessesMonitoringStatus(int userId);

    ProcessMonitoringSettingsDTO changeUserProcessesMonitoringInterval(int userId, int interval);
}
