package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.NetworkCardInfoDTO;

import java.util.List;

public interface NetworkCardInfoService {

    NetworkCardInfoDTO getNetworkCardInfo(int userId);

    List<NetworkCardInfoDTO> getNetworkCardInfoHistory(int userId);
}
