package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.USBBlockingDTO;
import pl.zespolowka.logit.domain.mapper.USBBlockingMapper;
import pl.zespolowka.logit.domain.model.USBBlocking;
import pl.zespolowka.logit.repository.USBBlockingRepository;
import pl.zespolowka.logit.service.USBBlockingService;
import pl.zespolowka.logit.utils.PostgreBoolean;

import javax.persistence.EntityNotFoundException;

@Service
public class USBBlockingServiceImpl implements USBBlockingService {

    @Autowired
    private USBBlockingRepository usbBlockingRepository;

    @Override
    public USBBlockingDTO getUserUSBBlockingInfo(int userId) {
        return usbBlockingRepository.findByKeyloggerUserId(userId)
                .map(USBBlockingMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Usb blocking info not found"));
    }

    @Override
    public USBBlockingDTO changeUSBBlockingStatus(int userId) {
        USBBlocking usbBlockingInfo = getUSBBlockingInfo(userId);
        usbBlockingInfo.setBlockPorts(PostgreBoolean.changeBoolean(usbBlockingInfo.getBlockPorts()));
        return saveFlushAndReturnDTO(usbBlockingInfo);
    }

    private USBBlockingDTO saveFlushAndReturnDTO(USBBlocking usbBlockingInfo) {
        usbBlockingRepository.saveAndFlush(usbBlockingInfo);
        return USBBlockingMapper.INSTANCE.map(usbBlockingInfo);
    }

    private USBBlocking getUSBBlockingInfo(int userId) {
        return usbBlockingRepository.findByKeyloggerUserId(userId)
                .orElseThrow(() -> new EntityNotFoundException("Usb blocking info not found"));
    }
}
