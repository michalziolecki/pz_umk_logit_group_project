package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.UserProcessInfoDTO;

import java.util.List;

public interface UserProcessInfoService {
    List<UserProcessInfoDTO> getUserPresentProcesses(int userId);

    List<UserProcessInfoDTO> getUserProcessesHistory(int userId);
}
