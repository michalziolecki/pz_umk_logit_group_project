package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.KeyloggerUserDTO;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.List;


public interface KeyloggerUserService {

    List<KeyloggerUserDTO> getAllKeyloggerUsers();

    KeyloggerUserDTO getKeyloggerUserDTOById(int userId);

    KeyloggerUser getKeyloggerUser(int userId);

    List<KeyloggerUserDTO> getKeyloggerUserByUsernameQuery(String query);
}
