package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.ActiveProcessInfoDTO;
import pl.zespolowka.logit.domain.mapper.ActiveProcessInfoMapper;
import pl.zespolowka.logit.repository.ActiveProcessInfoRepository;
import pl.zespolowka.logit.service.ActiveProcessInfoService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ActiveProcessInfoServiceImpl implements ActiveProcessInfoService {

    @Autowired
    private ActiveProcessInfoRepository activeProcessInfoRepository;

    @Override
    public ActiveProcessInfoDTO getProcessByUserId(int userId) {
        return activeProcessInfoRepository.findFirstByKeyloggerUserIdOrderByWhenInfoCollectedDesc(userId)
                .map(ActiveProcessInfoMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Active process for user id " + userId + " not found"));
    }

    @Override
    public List<ActiveProcessInfoDTO> getProcessHistoryByUserId(int userId) {
        return activeProcessInfoRepository.findAllByKeyloggerUserId(userId)
                .stream()
                .map(ActiveProcessInfoMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
