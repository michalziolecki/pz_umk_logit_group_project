package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.KeyloggerFilterDTO;

import java.util.List;

public interface KeyloggerFilterService {

    List<KeyloggerFilterDTO> getKeyloggerUserFilters(int userId);

    KeyloggerFilterDTO deleteKeyloggerFilter(int keyloggerFilterId);

    KeyloggerFilterDTO addKeyloggerFilter(KeyloggerFilterDTO keyloggerFilterDTO);
}
