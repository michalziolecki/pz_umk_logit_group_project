package pl.zespolowka.logit.service;


import pl.zespolowka.logit.domain.dto.USBPortDTO;

import java.util.List;

public interface USBService {

    List<USBPortDTO> getUserUSBList(int userId);

    List<USBPortDTO> changeStatus(int userId, int usbPortId);

}

