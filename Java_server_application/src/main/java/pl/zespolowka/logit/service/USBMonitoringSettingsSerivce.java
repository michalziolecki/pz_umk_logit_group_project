package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.USBMonitoringSettingsDTO;

public interface USBMonitoringSettingsSerivce {
    USBMonitoringSettingsDTO getUserUSBMonitoringSettings(int userId);

    USBMonitoringSettingsDTO changeUserUSBMonitoringStatus(int userId);

    USBMonitoringSettingsDTO changeUserUSBScanningStatus(int userId);
}
