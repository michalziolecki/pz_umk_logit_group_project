package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.KeyloggerFilterDTO;
import pl.zespolowka.logit.domain.mapper.KeyloggerFilterMapper;
import pl.zespolowka.logit.domain.model.KeyloggerFilter;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.repository.KeyloggerFilterRepository;
import pl.zespolowka.logit.service.KeyloggerFilterService;
import pl.zespolowka.logit.service.KeyloggerUserService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class KeyloggerFilterServiceImpl implements KeyloggerFilterService {

    @Autowired
    KeyloggerFilterRepository keyloggerFilterRepository;

    @Autowired
    KeyloggerUserService keyloggerUserService;

    @Override
    public List<KeyloggerFilterDTO> getKeyloggerUserFilters(int userId) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(userId);
        return keyloggerFilterRepository.findAllByKeyloggerUser(keyloggerUser).stream().map(KeyloggerFilterMapper.INSTANCE::map).collect(Collectors.toList());
    }

    @Override
    public KeyloggerFilterDTO deleteKeyloggerFilter(int keyloggerFilterId) {
        KeyloggerFilter keyloggerFilter = getKeyloggerFilter(keyloggerFilterId);
        keyloggerFilterRepository.deleteById(keyloggerFilterId);
        return KeyloggerFilterMapper.INSTANCE.map(keyloggerFilter);
    }

    @Override
    public KeyloggerFilterDTO addKeyloggerFilter(KeyloggerFilterDTO keyloggerFilterDTO) {
        KeyloggerFilter keyloggerFilter = KeyloggerFilterMapper.INSTANCE.map(keyloggerFilterDTO);
        return saveFlushAndReturnDTO(keyloggerFilter);
    }


    private KeyloggerFilter getKeyloggerFilter(int keyloggerFilterId) {
        return keyloggerFilterRepository.findById(keyloggerFilterId).orElseThrow(() -> new EntityNotFoundException("Keylogger filter not found"));
    }

    private KeyloggerFilterDTO saveFlushAndReturnDTO(KeyloggerFilter keyloggerFilter) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(keyloggerFilter.getKeyloggerUserId());
        keyloggerFilter.setKeyloggerUser(keyloggerUser);
        keyloggerFilterRepository.saveAndFlush(keyloggerFilter);
        return KeyloggerFilterMapper.INSTANCE.map(keyloggerFilter);
    }
}
