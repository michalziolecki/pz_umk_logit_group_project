package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.DepartmentDTO;
import pl.zespolowka.logit.domain.model.Department;

import java.util.List;

public interface DepartmentService {
    Department findById(int id);

    List<DepartmentDTO> findAll();

    Department create(DepartmentDTO departmentDTO);
}
