package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.UserProcessInfoDTO;
import pl.zespolowka.logit.domain.mapper.UserProcessInfoMapper;
import pl.zespolowka.logit.domain.model.UserProcessInfo;
import pl.zespolowka.logit.repository.UserProcessInfoRepository;
import pl.zespolowka.logit.service.UserProcessInfoService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserProcessInfoServiceImpl implements UserProcessInfoService {

    @Autowired
    private UserProcessInfoRepository userProcessInfoRepository;

    @Override
    public List<UserProcessInfoDTO> getUserPresentProcesses(int userId) {
        UserProcessInfo userProcessInfo = userProcessInfoRepository.findFirstByKeyloggerUserIdOrderByWhenCollectedDesc(userId)
                .orElseThrow(() -> new EntityNotFoundException("Processes not found for user id " + userId));
        return userProcessInfoRepository.findAllByKeyloggerUserIdAndWhenCollectedEquals(userId, userProcessInfo.getWhenCollected())
                .stream()
                .map(UserProcessInfoMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<UserProcessInfoDTO> getUserProcessesHistory(int userId) {
        return userProcessInfoRepository.findAllByKeyloggerUserId(userId)
                .stream()
                .map(UserProcessInfoMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
