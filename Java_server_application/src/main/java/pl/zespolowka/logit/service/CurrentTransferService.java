package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.CurrentTransferDTO;

import java.util.List;

public interface CurrentTransferService {
    CurrentTransferDTO getCurrentTransfer(int userId);

    List<CurrentTransferDTO> getCurrentTransferHistory(int userId);
}
