package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.InternetSettingsDTO;

public interface InternetSettingsService {

    InternetSettingsDTO getKeyloggerUserInternetSettings(int userId);

    InternetSettingsDTO changeCurrentTransferMonitorStatus(int internetSettingsId);

    InternetSettingsDTO setCurrentTransferMonitorFrequency(int internetSettingsId, int frequency);

    InternetSettingsDTO changeUploadSpeedMonitorStatus(int internetSettingsId);

    InternetSettingsDTO changeDownloadSpeedMonitorStatus(int internetSettingsId);

    InternetSettingsDTO changeTransferMonitorSinceComputerStartedStatus(int internetSettingsId);

    InternetSettingsDTO setTransferSinceComputerStartedMonitorFrequency(int internetSettingsId, int frequency);

    InternetSettingsDTO changeShouldPingStatus(int internetSettingsId);

    InternetSettingsDTO setPingAddress(int internetSettingsId, String address);

    InternetSettingsDTO setPingFrequency(int internetSettingsId, int frequency);

    InternetSettingsDTO changeNetworkCardMonitorStatus(int internetSettingsId);

    InternetSettingsDTO setNetworkCardMonitorFrequency(int internetSettingsId, int frequency);
}
