package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.TransferSinceComputerStartedDTO;

import java.util.List;

public interface TransferSinceComputerStartedService {
    TransferSinceComputerStartedDTO getTransferSinceComputerStarted(int userId);

    List<TransferSinceComputerStartedDTO> getTransferSinceComputerStartedHistory(int userId);
}
