package pl.zespolowka.logit.service.impl;

import javafx.geometry.Pos;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.USBMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.mapper.USBMonitoringSettingsMapper;
import pl.zespolowka.logit.domain.model.USBMonitoringSettings;
import pl.zespolowka.logit.repository.USBMonitoringSettingsRepository;
import pl.zespolowka.logit.service.USBMonitoringSettingsSerivce;
import pl.zespolowka.logit.utils.PostgreBoolean;

import javax.persistence.EntityNotFoundException;

@Service
public class USBMonitoringSettingsSerivceImpl implements USBMonitoringSettingsSerivce {

    @Autowired
    private USBMonitoringSettingsRepository usbMonitoringSettingsRepository;

    @Override
    public USBMonitoringSettingsDTO getUserUSBMonitoringSettings(int userId) {
        return usbMonitoringSettingsRepository.findByKeyloggerUserId(userId)
                .map(USBMonitoringSettingsMapper.INSTANCE::map)
                .orElseThrow(() -> new EntityNotFoundException("Settings not found for user id: " + userId));
    }

    @Override
    public USBMonitoringSettingsDTO changeUserUSBMonitoringStatus(int userId) {
        USBMonitoringSettings usbMonitoringSettings = getSettings(userId);
        usbMonitoringSettings.setIsMonitored(PostgreBoolean.changeBoolean(usbMonitoringSettings.getIsMonitored()));
        return saveFlushAndReturnDTO(usbMonitoringSettings);
    }

    @Override
    public USBMonitoringSettingsDTO changeUserUSBScanningStatus(int userId) {
        USBMonitoringSettings usbMonitoringSettings = getSettings(userId);
        usbMonitoringSettings.setIsScaned(PostgreBoolean.changeBoolean(usbMonitoringSettings.getIsScaned()));
        return saveFlushAndReturnDTO(usbMonitoringSettings);
    }

    private USBMonitoringSettingsDTO saveFlushAndReturnDTO(USBMonitoringSettings usbMonitoringSettings) {
        usbMonitoringSettingsRepository.saveAndFlush(usbMonitoringSettings);
        return USBMonitoringSettingsMapper.INSTANCE.map(usbMonitoringSettings);
    }

    private USBMonitoringSettings getSettings(int userId) {
        return usbMonitoringSettingsRepository.findByKeyloggerUserId(userId)
                .orElseThrow(() -> new EntityNotFoundException("USB monitoring settings not found for user id: " + userId));
    }
}
