package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.RemoteDriveInfoDTO;
import pl.zespolowka.logit.domain.mapper.RemoteDriveInfoMapper;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.RemoteDriveInfo;
import pl.zespolowka.logit.repository.RemoteDriveInfoRepository;
import pl.zespolowka.logit.service.KeyloggerUserService;
import pl.zespolowka.logit.service.RemoteDriveInfoService;

import java.util.List;
import java.util.stream.Collectors;

import static pl.zespolowka.logit.configuration.LogITConsts.IN;

@Service
public class RemoteDriveInfoServiceImpl implements RemoteDriveInfoService {

    @Autowired
    private RemoteDriveInfoRepository remoteDriveInfoRepository;

    @Autowired
    private KeyloggerUserService keyloggerUserService;

    @Override
    public List<RemoteDriveInfoDTO> getAllUserCurrentlyConnectedRemoteDrives(int userId) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(userId);
        List<RemoteDriveInfo> byKeyloggerUserAndAndOperationStatus = remoteDriveInfoRepository.findAllByKeyloggerUserAndOperationStatus(keyloggerUser, IN);
        return byKeyloggerUserAndAndOperationStatus
                .stream()
                .map(RemoteDriveInfoMapper.INSTANCE::map)
                .collect(Collectors.toList());

    }

    @Override
    public List<RemoteDriveInfoDTO> getAllUserRemoteDrives(int userId) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(userId);
        return remoteDriveInfoRepository.findAllByKeyloggerUser(keyloggerUser)
                .stream()
                .map(RemoteDriveInfoMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
