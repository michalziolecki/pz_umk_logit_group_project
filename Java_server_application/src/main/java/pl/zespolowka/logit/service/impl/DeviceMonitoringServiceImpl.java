package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.DeviceMonitoringDTO;
import pl.zespolowka.logit.domain.mapper.DeviceMonitoringMapper;
import pl.zespolowka.logit.domain.model.DeviceMonitoring;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.repository.DeviceMonitoringRepository;
import pl.zespolowka.logit.service.DeviceMonitoringService;
import pl.zespolowka.logit.service.KeyloggerUserService;

import java.util.List;
import java.util.stream.Collectors;

import static pl.zespolowka.logit.configuration.LogITConsts.IN;

@Service
public class DeviceMonitoringServiceImpl implements DeviceMonitoringService {

    @Autowired
    private DeviceMonitoringRepository deviceMonitoringRepository;

    @Autowired
    private KeyloggerUserService keyloggerUserService;

    @Override
    //TODO filter out out devices
    public List<DeviceMonitoringDTO> getAllUserConnectedDevices(int userId) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(userId);
        return deviceMonitoringRepository.findAllByKeyloggerUserAndOperationStatusOrderByOperationDate(keyloggerUser, IN)
                .stream()
                .map(DeviceMonitoringMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<DeviceMonitoringDTO> getAllUserDevices(int userId) {
        KeyloggerUser keyloggerUser = keyloggerUserService.getKeyloggerUser(userId);
        return deviceMonitoringRepository.findAllByKeyloggerUserOrderByOperationDate(keyloggerUser)
                .stream()
                .map(DeviceMonitoringMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
