package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.RemoteDriveInfoDTO;

import java.util.List;

public interface RemoteDriveInfoService {
    List<RemoteDriveInfoDTO> getAllUserCurrentlyConnectedRemoteDrives(int userId);

    List<RemoteDriveInfoDTO> getAllUserRemoteDrives(int userId);
}
