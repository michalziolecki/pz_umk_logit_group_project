package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.DiscDriveMonitoringDTO;
import pl.zespolowka.logit.domain.model.DiscDriveMonitoring;

import java.util.Optional;

public interface DiscDriveMonitoringService {
    DiscDriveMonitoringDTO getDiscDriveMonitoringInfo(int userId);

    DiscDriveMonitoringDTO changeDiscDriveMonitoringStatus(int userId);

    DiscDriveMonitoringDTO changeDiscDriveScanningStatus(int userId);
}
