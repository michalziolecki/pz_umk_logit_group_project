package pl.zespolowka.logit.service;

import pl.zespolowka.logit.domain.dto.USBBlockingDTO;

public interface USBBlockingService {
    USBBlockingDTO getUserUSBBlockingInfo(int userId);

    USBBlockingDTO changeUSBBlockingStatus(int userId);
}
