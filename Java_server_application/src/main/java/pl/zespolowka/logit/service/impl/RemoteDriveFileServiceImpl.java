package pl.zespolowka.logit.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.domain.dto.RemoteDriveFileDTO;
import pl.zespolowka.logit.domain.mapper.RemoteDriveFileMapper;
import pl.zespolowka.logit.domain.model.RemoteDriveFile;
import pl.zespolowka.logit.repository.RemoteDriveFileRepository;
import pl.zespolowka.logit.service.RemoteDriveFileService;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RemoteDriveFileServiceImpl implements RemoteDriveFileService {

    @Autowired
    private RemoteDriveFileRepository remoteDriveFileRepository;

    @Override
    public List<RemoteDriveFileDTO> getAllRemoteDriveFilesHistory(int driveId) {
        return remoteDriveFileRepository.findAllByRemoteDriveInfoId(driveId)
                .stream()
                .map(RemoteDriveFileMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    @Override
    public List<RemoteDriveFileDTO> getAllRemoteDriveFiles(int driveId) {
        RemoteDriveFile remoteDriveFile = remoteDriveFileRepository.findFirstByRemoteDriveInfoIdOrderByOperationDateDesc(driveId).orElseThrow(() -> new EntityNotFoundException("Drive files not found for drive id: " + driveId));
        return remoteDriveFileRepository.findAllByRemoteDriveInfoIdAndOperationDate(driveId, remoteDriveFile.getOperationDate())
                .stream()
                .map(RemoteDriveFileMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }
}
