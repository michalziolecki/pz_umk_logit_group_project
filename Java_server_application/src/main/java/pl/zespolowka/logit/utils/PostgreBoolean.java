package pl.zespolowka.logit.utils;

public class PostgreBoolean {

    public static short changeBoolean(short bool) {
        return (short) (bool == 0 ? 1 : 0);
    }
}
