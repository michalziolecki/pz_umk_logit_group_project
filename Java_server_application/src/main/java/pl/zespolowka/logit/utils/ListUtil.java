package pl.zespolowka.logit.utils;

import java.util.List;

import static java.util.Objects.nonNull;

public final class ListUtil {
    private ListUtil(){}

    private static final int ONE_ELEMENT = 1;

    public static boolean containsOneElement(List list) {
        return nonNull(list) && list.size() == ONE_ELEMENT;
    }
}
