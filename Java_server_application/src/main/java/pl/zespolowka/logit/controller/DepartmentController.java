package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.DepartmentDTO;
import pl.zespolowka.logit.domain.mapper.DepartmentMapper;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.service.DepartmentService;

import javax.annotation.security.PermitAll;
import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/departaments", produces = APPLICATION_JSON)
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @GetMapping
    @ApiOperation("Get all departaments")
    public List<DepartmentDTO> getAll(){
        return departmentService.findAll();
    }

    @PostMapping("/create")
    @Secured("ADMIN")
    @ApiOperation("Create departament")
    public DepartmentDTO create(@RequestBody DepartmentDTO departmentDTO){
        return DepartmentMapper.INSTANCE.map(departmentService.create(departmentDTO));
    }

}
