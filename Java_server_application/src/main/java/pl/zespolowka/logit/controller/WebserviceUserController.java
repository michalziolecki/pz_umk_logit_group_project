package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.configuration.security.exception.UserExistsException;
import pl.zespolowka.logit.domain.dto.WebserviceUserDTO;
import pl.zespolowka.logit.service.WebserviceUserService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/webservice-users", produces = APPLICATION_JSON)
@Api(description = "Operations on Webservice users")
public class WebserviceUserController {

    @Autowired
    private WebserviceUserService webserviceUserService;

    @PostMapping("/create")
    @Secured("ADMIN")
    @ResponseStatus(HttpStatus.CREATED)
    @ApiOperation("Create new webservice user")
    public WebserviceUserDTO create(@RequestBody WebserviceUserDTO webserviceUserDTO) throws UserExistsException {
        return webserviceUserService.save(webserviceUserDTO);
    }

    @GetMapping
    @Secured("ADMIN")
    @ApiOperation("Return all webservice users")
    public List<WebserviceUserDTO> findAll(){
        return webserviceUserService.findAll();
    }
}
