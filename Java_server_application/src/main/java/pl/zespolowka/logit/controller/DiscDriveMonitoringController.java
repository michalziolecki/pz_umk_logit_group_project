package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.DiscDriveMonitoringDTO;
import pl.zespolowka.logit.service.DiscDriveMonitoringService;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/disc-drives", produces = APPLICATION_JSON)
@Api(description = "Disc drives settings")
public class DiscDriveMonitoringController {

    @Autowired
    private DiscDriveMonitoringService discDriveMonitoringService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Check disc drives monitoring status")
    public DiscDriveMonitoringDTO getDiscDriveMonitoringInfo(@PathVariable int userId) {
        return discDriveMonitoringService.getDiscDriveMonitoringInfo(userId);
    }

    @PutMapping("/{userId}/change-monitoring")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change monitoring status")
    public DiscDriveMonitoringDTO changeDiscDriveMonitoringStatus (@PathVariable int userId) {
        return discDriveMonitoringService.changeDiscDriveMonitoringStatus(userId);
    }

    @PutMapping("/{userId}/change-scanning")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change scanning status")
    public DiscDriveMonitoringDTO changeDiscDriveScanningStatus (@PathVariable int userId) {
        return discDriveMonitoringService.changeDiscDriveScanningStatus(userId);
    }

}
