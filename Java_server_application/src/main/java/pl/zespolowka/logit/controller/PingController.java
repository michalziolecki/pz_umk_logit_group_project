package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.PingDTO;
import pl.zespolowka.logit.service.PingService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/pings", produces = APPLICATION_JSON)
@Api(description = "Ping info")
public class PingController {

    @Autowired
    private PingService pingService;

    @GetMapping("/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Get most actual ping")
    public PingDTO getActualPing(@PathVariable int userId){
        return pingService.getActualPing(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Get pings history")
    public List<PingDTO> getPingHistory(@PathVariable int userId){
        return pingService.getPingHistory(userId);
    }

}
