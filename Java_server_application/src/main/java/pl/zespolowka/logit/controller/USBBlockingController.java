package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.DeviceMonitoringDTO;
import pl.zespolowka.logit.domain.dto.USBBlockingDTO;
import pl.zespolowka.logit.domain.model.USBBlocking;
import pl.zespolowka.logit.service.USBBlockingService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/usb-blocking", produces = APPLICATION_JSON)
@Api(description = "Usb blocking settings")
public class USBBlockingController {

    @Autowired
    private USBBlockingService usbBlockingService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Check if ports are blocked")
    public USBBlockingDTO getUserUSBBlockingInfo(@PathVariable int userId) {
        return usbBlockingService.getUserUSBBlockingInfo(userId);
    }

    @PutMapping("/{userId}/change-blocking")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Check if ports are blocked")
    public USBBlockingDTO changeUSBBlockingStatus(@PathVariable int userId) {
        return usbBlockingService.changeUSBBlockingStatus(userId);
    }
}
