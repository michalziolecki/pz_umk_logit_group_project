package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.configuration.security.service.RoleService;
import pl.zespolowka.logit.domain.dto.RoleDTO;
import pl.zespolowka.logit.domain.mapper.RoleMapper;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.repository.RoleRepository;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/roles", produces = APPLICATION_JSON)
public class RoleController {

    @Autowired
    private RoleService roleService;

    @GetMapping
    @Secured("ADMIN")
    @ApiOperation("Get all roles")
    public List<RoleDTO> getAllRoles(){
        return roleService.getRoles();
    }

    @PostMapping
    @Secured("ADMIN")
    @ApiOperation("Create role")
    @ResponseStatus(HttpStatus.CREATED)
    public RoleDTO create(@RequestBody RoleDTO roleDTO){
        return roleService.create(roleDTO);
    }
}
