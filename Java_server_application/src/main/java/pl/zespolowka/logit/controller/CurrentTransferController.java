package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.CurrentTransferDTO;
import pl.zespolowka.logit.service.CurrentTransferService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/current-transfer", produces = APPLICATION_JSON)
@Api(description = "Current transfer operations")
public class CurrentTransferController {

    @Autowired
    CurrentTransferService currentTransferService;

    @GetMapping("/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Find current transfer for given userId")
    public CurrentTransferDTO get(
            @PathVariable int userId) {
        return currentTransferService.getCurrentTransfer(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Find current transfer history for given userId")
    public List<CurrentTransferDTO> getHistory(
            @PathVariable int userId) {
        return currentTransferService.getCurrentTransferHistory(userId);
    }
}
