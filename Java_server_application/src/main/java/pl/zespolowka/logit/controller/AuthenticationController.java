package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.configuration.security.exception.InvalidUserException;
import pl.zespolowka.logit.configuration.security.service.AuthenticationService;
import pl.zespolowka.logit.configuration.security.transfer.AuthenticationDTO;
import pl.zespolowka.logit.configuration.security.transfer.CredentialsDTO;
import pl.zespolowka.logit.configuration.security.transfer.TokenRefreshDTO;

import javax.validation.Valid;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH, produces = APPLICATION_JSON)
@Api(description = "Authentication operations")
public class AuthenticationController {

    @Autowired
    private AuthenticationService authenticationService;

    @PostMapping("/login")
    @ApiOperation("Get authentication token using given credentials")
    public AuthenticationDTO authenticate(@RequestBody @Valid CredentialsDTO credentialsDto) throws InvalidUserException {
        return authenticationService.authenticate(credentialsDto);
    }

    @PostMapping("/refresh")
    @ApiOperation("Refresh authentication token")
    public AuthenticationDTO refreshToken(@RequestBody TokenRefreshDTO refreshDto) {
        return authenticationService.refreshToken(refreshDto);
    }
}
