package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.ActiveProcessInfoDTO;
import pl.zespolowka.logit.domain.dto.CurrentTransferDTO;
import pl.zespolowka.logit.domain.model.ActiveProcessInfo;
import pl.zespolowka.logit.service.ActiveProcessInfoService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/active-process", produces = APPLICATION_JSON)
@Api(description = "Active process info")
public class ActiveProcessInfoController {

    @Autowired
    private ActiveProcessInfoService activeProcessInfoService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Get active process by user id")
    public ActiveProcessInfoDTO getUserActiveProcess(@PathVariable int userId) {
        return activeProcessInfoService.getProcessByUserId(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Get active process history by user id")
    public List<ActiveProcessInfoDTO> getUserActiveProcessHistory(@PathVariable int userId) {
        return activeProcessInfoService.getProcessHistoryByUserId(userId);
    }

}
