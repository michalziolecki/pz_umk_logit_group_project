package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.ProcessMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.dto.USBBlockingDTO;
import pl.zespolowka.logit.service.ProcessMonitoringSettingsService;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/process-settings", produces = APPLICATION_JSON)
@Api(description = "Process settings")
public class ProcessMonitoringSettingsController {

    @Autowired
    private ProcessMonitoringSettingsService processMonitoringSettingsService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Check process monitoring settings for given user id")
    public ProcessMonitoringSettingsDTO getUserProcessMonitoringSettings(@PathVariable int userId) {
        return processMonitoringSettingsService.getUserProcessMonitoringSettings(userId);
    }

    @PutMapping("/{userId}/change-active-process-monitoring-status")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change active process monitoring status")
    public ProcessMonitoringSettingsDTO changeUserActiveProcessMonitoringStatus(@PathVariable int userId) {
        return processMonitoringSettingsService.changeUserActiveProcessMonitoringStatus(userId);
    }

    @PutMapping("/{userId}/change-processes-monitoring-status")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change all process monitoring status")
    public ProcessMonitoringSettingsDTO changeUserProcessesMonitoringStatus(@PathVariable int userId) {
        return processMonitoringSettingsService.changeUserProcessesMonitoringStatus(userId);
    }

    @PutMapping("/{userId}/change-processes-monitoring-interval/{interval}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change all process monitoring interval")
    public ProcessMonitoringSettingsDTO changeUserProcessesMonitoringInterval(@PathVariable int userId, @PathVariable int interval) {
        return processMonitoringSettingsService.changeUserProcessesMonitoringInterval(userId, interval);
    }
}
