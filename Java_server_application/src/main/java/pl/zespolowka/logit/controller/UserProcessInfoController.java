package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.CurrentTransferDTO;
import pl.zespolowka.logit.domain.dto.UserProcessInfoDTO;
import pl.zespolowka.logit.domain.model.UserProcessInfo;
import pl.zespolowka.logit.service.UserProcessInfoService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/processes", produces = APPLICATION_JSON)
@Api(description = "Information about user processes")
public class UserProcessInfoController {

    @Autowired
    private UserProcessInfoService userProcessInfoService;

    @GetMapping("/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Get present user processes")
    public List<UserProcessInfoDTO> getUserPresentProcesses(
            @PathVariable int userId) {
        return userProcessInfoService.getUserPresentProcesses(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Get user processes history")
    public List<UserProcessInfoDTO> getUserProcessesHistory(
            @PathVariable int userId) {
        return userProcessInfoService.getUserProcessesHistory(userId);
    }
}
