package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.USBPortDTO;
import pl.zespolowka.logit.service.USBService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/usb-ports", produces = APPLICATION_JSON)
@Api(description = "Operations on USB ports")
public class USBController {

    @Autowired
    USBService usbService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find USBports for given userId")
    public List<USBPortDTO> getUsbPorts(
            @PathVariable int userId){
        return usbService.getUserUSBList(userId);
    }

    @PostMapping("/{userId}/{usbPortId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change status for USBPort of given Id")
    public List<USBPortDTO> changeUSBStatus(
            @PathVariable int userId,
            @PathVariable int usbPortId){
     return usbService.changeStatus(userId, usbPortId);
    }

}
