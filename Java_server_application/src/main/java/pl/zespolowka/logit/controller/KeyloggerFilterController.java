package pl.zespolowka.logit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.KeyloggerFilterDTO;
import pl.zespolowka.logit.service.KeyloggerFilterService;

import java.util.List;

import static org.springframework.http.HttpStatus.CREATED;
import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/keylogger-filter", produces = APPLICATION_JSON)
public class KeyloggerFilterController {

    @Autowired
    KeyloggerFilterService keyloggerFilterService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Get keylogger filters by user id")
    public List<KeyloggerFilterDTO> getKeyloggerFilters(@PathVariable int userId) {
        return keyloggerFilterService.getKeyloggerUserFilters(userId);
    }

    @DeleteMapping("/{filterId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Delete keylogger filter by its id")
    public KeyloggerFilterDTO deleteKeyloggerFilter(@PathVariable int filterId) {
        return keyloggerFilterService.deleteKeyloggerFilter(filterId);
    }

    @PostMapping
    @ResponseStatus(CREATED)
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Post new keylogger filter")
    public KeyloggerFilterDTO addKeyloggerFilter(@RequestBody KeyloggerFilterDTO keyloggerFilterDTO) {
        return keyloggerFilterService.addKeyloggerFilter(keyloggerFilterDTO);
    }
}
