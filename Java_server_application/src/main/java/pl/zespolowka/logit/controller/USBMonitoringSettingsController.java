package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.ProcessMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.dto.USBMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.model.USBMonitoringSettings;
import pl.zespolowka.logit.service.USBMonitoringSettingsSerivce;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/usb-monitoring-settings", produces = APPLICATION_JSON)
@Api(description = "USB monitoring settings")
public class USBMonitoringSettingsController {

    @Autowired
    private USBMonitoringSettingsSerivce usbMonitoringSettingsSerivce;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Get USB monitoring settings for given user id")
    public USBMonitoringSettingsDTO getUserUSBMonitoringSettings(@PathVariable int userId) {
        return usbMonitoringSettingsSerivce.getUserUSBMonitoringSettings(userId);
    }

    @PutMapping("/{userId}/change-usb-monitoring-status")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change USB monitoring status")
    public USBMonitoringSettingsDTO changeUserActiveProcessMonitoringStatus(@PathVariable int userId) {
        return usbMonitoringSettingsSerivce.changeUserUSBMonitoringStatus(userId);
    }

    @PutMapping("/{userId}/change-usb-scanning-status")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Change USB scanning status")
    public USBMonitoringSettingsDTO changeUserProcessesMonitoringStatus(@PathVariable int userId) {
        return usbMonitoringSettingsSerivce.changeUserUSBScanningStatus(userId);
    }

}
