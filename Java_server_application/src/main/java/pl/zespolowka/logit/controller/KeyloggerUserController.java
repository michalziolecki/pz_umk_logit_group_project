package pl.zespolowka.logit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.KeyloggerUserDTO;
import pl.zespolowka.logit.service.KeyloggerUserService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/keylogger-users", produces = APPLICATION_JSON)
public class KeyloggerUserController {

    @Autowired
    KeyloggerUserService keyloggerUserService;

    @GetMapping
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all keylogger users")
    public List<KeyloggerUserDTO> getAllKeyloggerUsers(
            @PageableDefault (size = 100, sort = {"id"}, direction = Sort.Direction.DESC)Pageable pageable) {
        return keyloggerUserService.getAllKeyloggerUsers();
    }

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find keylogger user by id")
    public KeyloggerUserDTO getKeyloggerUserById(
            @PathVariable int userId){
        return keyloggerUserService.getKeyloggerUserDTOById(userId);
    }

    @GetMapping("/search/{query}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find keylogger user by username query")
    public List<KeyloggerUserDTO> getKeyloggerUserByUsernameQuery(
            @PathVariable String query){
        return keyloggerUserService.getKeyloggerUserByUsernameQuery(query);
    }
}
