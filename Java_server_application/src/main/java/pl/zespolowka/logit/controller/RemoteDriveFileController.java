package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.RemoteDriveFileDTO;
import pl.zespolowka.logit.service.RemoteDriveFileService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/drive-files", produces = APPLICATION_JSON)
@Api(description = "Remote drive file info")
public class RemoteDriveFileController {

    @Autowired
    private RemoteDriveFileService remoteDriveFileService;

    @GetMapping("/{driveId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all files for given drive id")
    public List<RemoteDriveFileDTO> getAllRemoteDriveFiles(@PathVariable int driveId) {
        return remoteDriveFileService.getAllRemoteDriveFiles(driveId);
    }

    @GetMapping("/history/{driveId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all files for given drive id")
    public List<RemoteDriveFileDTO> getAllRemoteDriveFilesHistory(@PathVariable int driveId) {
        return remoteDriveFileService.getAllRemoteDriveFilesHistory(driveId);
    }

}
