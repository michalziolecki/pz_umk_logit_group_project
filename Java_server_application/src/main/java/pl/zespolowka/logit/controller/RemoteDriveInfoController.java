package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.RemoteDriveInfoDTO;
import pl.zespolowka.logit.service.RemoteDriveInfoService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/drives", produces = APPLICATION_JSON)
@Api(description = "Remote drives info")
public class RemoteDriveInfoController {

    @Autowired
    private RemoteDriveInfoService remoteDriveInfoService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all connected user remote drives")
    public List<RemoteDriveInfoDTO> getAllUserConnectedRemoteDrives(@PathVariable int userId) {
        return remoteDriveInfoService.getAllUserCurrentlyConnectedRemoteDrives(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all user remote drives ever connected")
    public List<RemoteDriveInfoDTO> getAllUserRemoteDrives(@PathVariable int userId) {
        return remoteDriveInfoService.getAllUserRemoteDrives(userId);
    }
}
