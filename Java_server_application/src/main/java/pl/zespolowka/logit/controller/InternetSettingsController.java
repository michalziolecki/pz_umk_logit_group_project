package pl.zespolowka.logit.controller;

import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import pl.zespolowka.logit.domain.dto.InternetSettingsDTO;
import pl.zespolowka.logit.service.InternetSettingsService;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/internet-settings", produces = APPLICATION_JSON)
public class InternetSettingsController {

    @Autowired
    InternetSettingsService internetSettingsService;

    @GetMapping("/{userId}")
    @Secured("ADMIN")
    @ApiOperation("Find user settings by user id")
    public InternetSettingsDTO getInternetSettings(@PathVariable int userId) {
        return internetSettingsService.getKeyloggerUserInternetSettings(userId);
    }

    @PutMapping("/{id}/change-current-transfer-monitor-status")
    @Secured("ADMIN")
    @ApiOperation("Change current transfer monitor status")
    public InternetSettingsDTO changeCurrentTransferMonitorStatus(@PathVariable int id) {
        return internetSettingsService.changeCurrentTransferMonitorStatus(id);
    }

    @PutMapping("/{id}/current-transfer-monitor-frequency/{frequency}")
    @Secured("ADMIN")
    @ApiOperation("Set current transfer monitor frequency")
    public InternetSettingsDTO setCurrentTransferMonitorFrequency(@PathVariable int id, @PathVariable int frequency) {
        return internetSettingsService.setCurrentTransferMonitorFrequency(id, frequency);
    }

    @PutMapping("/{id}/change-upload-speed-monitor-status")
    @Secured("ADMIN")
    @ApiOperation("Change upload speed monitor status")
    public InternetSettingsDTO changeUploadSpeedMonitorStatus(@PathVariable int id) {
        return internetSettingsService.changeUploadSpeedMonitorStatus(id);
    }

    @PutMapping("/{id}/change-download-speed-monitor-status")
    @Secured("ADMIN")
    @ApiOperation("Change download speed monitor status")
    public InternetSettingsDTO changeDownloadSpeedMonitorStatus(@PathVariable int id) {
        return internetSettingsService.changeDownloadSpeedMonitorStatus(id);
    }

    @PutMapping("/{id}/change-transfer-monitor-since-computer-started-status")
    @Secured("ADMIN")
    @ApiOperation("Change if transfer should be monitored since computer has started")
    public InternetSettingsDTO changeTransferMonitorSinceComputerStarted(@PathVariable int id) {
        return internetSettingsService.changeTransferMonitorSinceComputerStartedStatus(id);
    }

    @PutMapping("/{id}/transfer-monitor-since-computer-started-frequency/{frequency}")
    @Secured("ADMIN")
    @ApiOperation("Set frequency of transfer monitor since computer started")
    public InternetSettingsDTO setTransferMonitorSinceComputerStartedFrequency(@PathVariable int id, @PathVariable int frequency) {
        return internetSettingsService.setTransferSinceComputerStartedMonitorFrequency(id, frequency);
    }

    @PutMapping("/{id}/should-ping")
    @Secured("ADMIN")
    @ApiOperation("Change if sbould ping")
    public InternetSettingsDTO changeIfShouldPing(@PathVariable int id) {
        return internetSettingsService.changeShouldPingStatus(id);
    }

    @PutMapping("/{id}/ping-address/{address}")
    @Secured("ADMIN")
    @ApiOperation("Set ping address")
    public InternetSettingsDTO setPingAddress(@PathVariable int id, @PathVariable String address) {
        return internetSettingsService.setPingAddress(id, address);
    }

    @PutMapping("/{id}/ping-frequency/{frequency}")
    @Secured("ADMIN")
    @ApiOperation("Set ping frequency")
    public InternetSettingsDTO setPingFrequency(@PathVariable int id, @PathVariable int frequency) {
        return internetSettingsService.setPingFrequency(id, frequency);
    }

    @PutMapping("/{id}/change-network-card-monitor-status")
    @Secured("ADMIN")
    @ApiOperation("Change if network card should be monitored")
    public InternetSettingsDTO changeNetworkCardMonitorStatus(@PathVariable int id) {
        return internetSettingsService.changeNetworkCardMonitorStatus(id);
    }

    @PutMapping("/{id}/network-card-monitor-frequency/{frequency}")
    @Secured("ADMIN")
    @ApiOperation("Set network card monitor frequency")
    public InternetSettingsDTO setNetworkCardMonitorFrequency(@PathVariable int id, @PathVariable int frequency) {
        return internetSettingsService.setNetworkCardMonitorFrequency(id, frequency);
    }

}
