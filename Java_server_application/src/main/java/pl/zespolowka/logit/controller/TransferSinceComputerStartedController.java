package pl.zespolowka.logit.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.CurrentTransferDTO;
import pl.zespolowka.logit.domain.dto.TransferSinceComputerStartedDTO;
import pl.zespolowka.logit.domain.model.TransferSinceComputerStarted;
import pl.zespolowka.logit.service.TransferSinceComputerStartedService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/since-boot-transfer", produces = APPLICATION_JSON)
@Api(description = "Transfer since computer has started")
public class TransferSinceComputerStartedController {

    @Autowired
    private TransferSinceComputerStartedService transferSinceComputerStartedService;

    @GetMapping("/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Find transfer since computer started for given userId")
    public TransferSinceComputerStartedDTO get(
            @PathVariable int userId) {
        return transferSinceComputerStartedService.getTransferSinceComputerStarted(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured("MANAGER")
    @ApiOperation("Find transfer since computer started history for given userId")
    public List<TransferSinceComputerStartedDTO> getHistory(
            @PathVariable int userId) {
        return transferSinceComputerStartedService.getTransferSinceComputerStartedHistory(userId);
    }
}
