package pl.zespolowka.logit.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.dto.DeviceMonitoringDTO;
import pl.zespolowka.logit.service.DeviceMonitoringService;

import java.util.List;

import static pl.zespolowka.logit.configuration.LogITConsts.API_PATH;
import static pl.zespolowka.logit.configuration.LogITConsts.APPLICATION_JSON;

@RestController
@RequestMapping(value = API_PATH + "/devices", produces = APPLICATION_JSON)
@Api(description = "Devices info")
public class DeviceController {

    @Autowired
    private DeviceMonitoringService deviceMonitoringService;

    @GetMapping("/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all connected user devices")
    public List<DeviceMonitoringDTO> getAllUserConnectedDevices(@PathVariable int userId) {
        return deviceMonitoringService.getAllUserConnectedDevices(userId);
    }

    @GetMapping("/history/{userId}")
    @Secured({"MANAGER", "ADMIN"})
    @ApiOperation("Find all ever connected user devices")
    public List<DeviceMonitoringDTO> getAllUserDevices(@PathVariable int userId) {
        return deviceMonitoringService.getAllUserDevices(userId);
    }


}
