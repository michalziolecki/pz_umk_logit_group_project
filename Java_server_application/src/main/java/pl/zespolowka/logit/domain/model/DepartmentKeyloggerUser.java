package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "departamenty_uzytkownicy_keylogger")
public class DepartmentKeyloggerUser {
    private int id;
    private Department department;
    private KeyloggerUser keyloggerUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DepartmentKeyloggerUser that = (DepartmentKeyloggerUser) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne
    @JoinColumn(name = "id_departamenty", referencedColumnName = "id", nullable = false)
    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @ManyToOne
    @JoinColumn(name = "uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
}
