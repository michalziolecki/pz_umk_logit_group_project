package pl.zespolowka.logit.domain.dto;

public class USBPortDTO {
    private long id;
    private short isDisabled;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public short getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(short isDisabled) {
        this.isDisabled = isDisabled;
    }
}
