package pl.zespolowka.logit.domain.dto;

import pl.zespolowka.logit.domain.model.KeyloggerUser;

public class DiscDriveMonitoringDTO {

    private int id;
    private short isMonitored;
    private short isScaned;
    private int keyloggerUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getIsMonitored() {
        return isMonitored;
    }

    public void setIsMonitored(short isMonitored) {
        this.isMonitored = isMonitored;
    }

    public short getIsScaned() {
        return isScaned;
    }

    public void setIsScaned(short isScaned) {
        this.isScaned = isScaned;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }
}
