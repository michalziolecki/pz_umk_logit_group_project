package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "informacje_o_procesach_usera")
public class UserProcessInfo {
    private int id;
    private Timestamp whenCollected;
    private String processName;
    private Integer threadsNumber;
    private Timestamp whenProcessStarted;
    private Long kilobytesEntry;
    private Long kilobytesRead;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "kiedy_zebrano_informacje", nullable = false)
    public Timestamp getWhenCollected() {
        return whenCollected;
    }

    public void setWhenCollected(Timestamp kiedyZebranoInformacje) {
        this.whenCollected = kiedyZebranoInformacje;
    }

    @Basic
    @Column(name = "nazwa_procesu", nullable = true, length = 100)
    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String nazwaProcesu) {
        this.processName = nazwaProcesu;
    }

    @Basic
    @Column(name = "ilosc_watkow", nullable = true)
    public Integer getThreadsNumber() {
        return threadsNumber;
    }

    public void setThreadsNumber(Integer iloscWatkow) {
        this.threadsNumber = iloscWatkow;
    }

    @Basic
    @Column(name = "od_kiedy_dziala_proces", nullable = true)
    public Timestamp getWhenProcessStarted() {
        return whenProcessStarted;
    }

    public void setWhenProcessStarted(Timestamp odKiedyDzialaProces) {
        this.whenProcessStarted = odKiedyDzialaProces;
    }

    @Basic
    @Column(name = "zapis_w_kilobytach", nullable = true)
    public Long getKilobytesEntry() {
        return kilobytesEntry;
    }

    public void setKilobytesEntry(Long zapisWKilobytach) {
        this.kilobytesEntry = zapisWKilobytach;
    }

    @Basic
    @Column(name = "odczyt_w_kilobytach", nullable = true)
    public Long getKilobytesRead() {
        return kilobytesRead;
    }

    public void setKilobytesRead(Long odczytWKilobytach) {
        this.kilobytesRead = odczytWKilobytach;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserProcessInfo that = (UserProcessInfo) o;
        return id == that.id &&
                Objects.equals(whenCollected, that.whenCollected) &&
                Objects.equals(processName, that.processName) &&
                Objects.equals(threadsNumber, that.threadsNumber) &&
                Objects.equals(whenProcessStarted, that.whenProcessStarted) &&
                Objects.equals(kilobytesEntry, that.kilobytesEntry) &&
                Objects.equals(kilobytesRead, that.kilobytesRead);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, whenCollected, processName, threadsNumber, whenProcessStarted, kilobytesEntry, kilobytesRead);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
