package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.WebserviceUserDTO;
import pl.zespolowka.logit.domain.model.WebserviceUser;

@Mapper
public interface WebserviceUserMapper {

    WebserviceUserMapper INSTANCE = Mappers.getMapper(WebserviceUserMapper.class);

    WebserviceUserDTO map(WebserviceUser webserviceUser);

    WebserviceUser map(WebserviceUserDTO webserviceUserDTO);
}
