package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "niesledzone_partycje")
public class UnmonitoredPartition {
    private int id;
    private String drivePartitionName;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "nazwa_partycji_dysku", nullable = false, length = 3)
    public String getDrivePartitionName() {
        return drivePartitionName;
    }

    public void setDrivePartitionName(String nazwaPartycjiDysku) {
        this.drivePartitionName = nazwaPartycjiDysku;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnmonitoredPartition that = (UnmonitoredPartition) o;
        return id == that.id &&
                Objects.equals(drivePartitionName, that.drivePartitionName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, drivePartitionName);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
