package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "pingi")
public class Ping {
    private int id;
    private int keyloggerUserId;
    private int pingMeasurementInMs;
    private String address;
    private Timestamp monitorTimestamp;
    private KeyloggerUser keyloggerUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "pomiar_pingu_w_ms", nullable = false)
    public int getPingMeasurementInMs() {
        return pingMeasurementInMs;
    }

    public void setPingMeasurementInMs(int pingMeasurementInMs) {
        this.pingMeasurementInMs = pingMeasurementInMs;
    }

    @Basic
    @Column(name = "adres", nullable = true, length = 50)
    public String getAddress() {
        return address;
    }

    public void setAddress(String adres) {
        this.address = adres;
    }

    @Basic
    @Column(name = "kiedy_rozpoczeto_pomiar", nullable = false)
    public Timestamp getMonitorTimestamp() {
        return monitorTimestamp;
    }

    public void setMonitorTimestamp(Timestamp monitorTimestamp) {
        this.monitorTimestamp = monitorTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ping that = (Ping) o;
        return id == that.id &&
                keyloggerUserId == that.keyloggerUserId &&
                pingMeasurementInMs == that.pingMeasurementInMs &&
                Objects.equals(address, that.address) &&
                Objects.equals(monitorTimestamp, that.monitorTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyloggerUserId, pingMeasurementInMs, address, monitorTimestamp);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
}
