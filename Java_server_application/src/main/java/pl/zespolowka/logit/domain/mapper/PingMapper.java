package pl.zespolowka.logit.domain.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.PingDTO;
import pl.zespolowka.logit.domain.model.Ping;

@Mapper
public interface PingMapper {

    PingMapper INSTANCE = Mappers.getMapper(PingMapper.class);

    Ping map(PingDTO pingDTO);

    PingDTO map(Ping ping);
}
