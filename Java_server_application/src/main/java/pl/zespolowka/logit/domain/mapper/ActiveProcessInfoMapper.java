package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.ActiveProcessInfoDTO;
import pl.zespolowka.logit.domain.model.ActiveProcessInfo;

@Mapper
public interface ActiveProcessInfoMapper {

    ActiveProcessInfoMapper INSTANCE = Mappers.getMapper(ActiveProcessInfoMapper.class);

    ActiveProcessInfo map(ActiveProcessInfoDTO activeProcessInfoDTO);

    ActiveProcessInfoDTO map(ActiveProcessInfo activeProcessInfo);
}
