package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "uzytkownicy_webservice")
public class WebserviceUser {
    private int id;
    private String username;
    private String password;
    private Collection<RoleAssignment> roleAssignments;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "login", nullable = false, length = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "haslo", nullable = false, length = 50)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @OneToMany(mappedBy = "webserviceUser")
    public Collection<RoleAssignment> getRoleAssignments() {
        return roleAssignments;
    }

    public void setRoleAssignments(Collection<RoleAssignment> roleDepartmentsAssignment) {
        this.roleAssignments = roleDepartmentsAssignment;
    }
}
