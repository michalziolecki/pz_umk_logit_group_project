package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.RoleAssignmentDTO;
import pl.zespolowka.logit.domain.model.RoleAssignment;

@Mapper
public interface RoleAssignmentMapper {

    RoleAssignmentMapper INSTANCE = Mappers.getMapper(RoleAssignmentMapper.class);

    RoleAssignment map(RoleAssignmentDTO roleAssignmentDTO);

    RoleAssignmentDTO map(RoleAssignment roleAssignment);
}
