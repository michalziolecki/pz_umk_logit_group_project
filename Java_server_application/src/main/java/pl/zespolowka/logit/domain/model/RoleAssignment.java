package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "role_przydzial")
public class RoleAssignment {
    private int id;
    private Role role;
    private WebserviceUser webserviceUser;
    private Set<RoleDepartment> roleDepartments = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleAssignment that = (RoleAssignment) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownik_webservice", referencedColumnName = "id", nullable = false)
    public WebserviceUser getWebserviceUser() {
        return webserviceUser;
    }

    public void setWebserviceUser(WebserviceUser webserviceUser) {
        this.webserviceUser = webserviceUser;
    }

    @ManyToOne
    @JoinColumn(name = "id_rola", referencedColumnName = "id", nullable = false)
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @OneToMany(mappedBy = "roleAssignment")
    public Set<RoleDepartment> getRoleDepartments() {
        return roleDepartments;
    }

    public void setRoleDepartments(Set<RoleDepartment> roleDepartments) {
        this.roleDepartments = roleDepartments;
    }
}
