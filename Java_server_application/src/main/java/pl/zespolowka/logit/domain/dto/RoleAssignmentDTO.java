package pl.zespolowka.logit.domain.dto;

import pl.zespolowka.logit.configuration.security.transfer.RoleDepartmentDTO;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.domain.model.RoleDepartment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RoleAssignmentDTO {
    private Role role;
    private WebserviceUser webserviceUser;
    private Set<RoleDepartment> roleDepartments;

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public WebserviceUser getWebserviceUser() {
        return webserviceUser;
    }

    public void setWebserviceUser(WebserviceUser webserviceUser) {
        this.webserviceUser = webserviceUser;
    }

    public Set<RoleDepartment> getRoleDepartments() {
        return roleDepartments;
    }

    public void setRoleDepartments(Set<RoleDepartment> roleDepartments) {
        this.roleDepartments = roleDepartments;
    }
}
