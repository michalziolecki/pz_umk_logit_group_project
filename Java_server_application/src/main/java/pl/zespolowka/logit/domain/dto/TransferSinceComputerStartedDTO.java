package pl.zespolowka.logit.domain.dto;

import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.sql.Timestamp;

public class TransferSinceComputerStartedDTO {

    private int id;
    private int keyloggerUserId;
    private Integer uploadedKB;
    private Integer downloadedKB;
    private Timestamp monitorTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    public Integer getUploadedKB() {
        return uploadedKB;
    }

    public void setUploadedKB(Integer uploadedKB) {
        this.uploadedKB = uploadedKB;
    }

    public Integer getDownloadedKB() {
        return downloadedKB;
    }

    public void setDownloadedKB(Integer downloadedKB) {
        this.downloadedKB = downloadedKB;
    }

    public Timestamp getMonitorTimestamp() {
        return monitorTimestamp;
    }

    public void setMonitorTimestamp(Timestamp monitorTimestamp) {
        this.monitorTimestamp = monitorTimestamp;
    }
}
