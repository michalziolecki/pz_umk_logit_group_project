package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.USBPortDTO;
import pl.zespolowka.logit.domain.model.USBPort;

@Mapper
public interface USBPortMapper {
        USBPortMapper INSTANCE = Mappers.getMapper(USBPortMapper.class);

        USBPortDTO map(USBPort usbPort);

        USBPort map(USBPortDTO usbPortDTO);
    }
