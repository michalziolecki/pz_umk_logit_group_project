package pl.zespolowka.logit.domain.dto;

import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.RemoteDriveInfo;

import java.sql.Timestamp;

public class RemoteDriveFileDTO {

    private int id;
    private String driveName;
    private Timestamp operationDate;
    private String fileName;
    private String filePath;
    private String fileType;
    private short isBinary;
    private String extension;
    private long size;
    private int keyloggerUserId;
    private int remoteDriveInfoId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDriveName() {
        return driveName;
    }

    public void setDriveName(String driveName) {
        this.driveName = driveName;
    }

    public Timestamp getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Timestamp operationDate) {
        this.operationDate = operationDate;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public short getIsBinary() {
        return isBinary;
    }

    public void setIsBinary(short isBinary) {
        this.isBinary = isBinary;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    public int getRemoteDriveInfoId() {
        return remoteDriveInfoId;
    }

    public void setRemoteDriveInfoId(int remoteDriveInfoId) {
        this.remoteDriveInfoId = remoteDriveInfoId;
    }
}
