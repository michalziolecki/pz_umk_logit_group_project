package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.UserProcessInfoDTO;
import pl.zespolowka.logit.domain.model.UserProcessInfo;

@Mapper
public interface UserProcessInfoMapper {

    UserProcessInfoMapper INSTANCE = Mappers.getMapper(UserProcessInfoMapper.class);

    UserProcessInfo map(UserProcessInfoDTO userProcessInfoDTO);

    UserProcessInfoDTO map(UserProcessInfo userProcessInfo);
}
