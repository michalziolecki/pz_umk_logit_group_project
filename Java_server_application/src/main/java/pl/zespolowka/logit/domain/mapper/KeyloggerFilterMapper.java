package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.KeyloggerFilterDTO;
import pl.zespolowka.logit.domain.model.KeyloggerFilter;

@Mapper
public interface KeyloggerFilterMapper {

    KeyloggerFilterMapper INSTANCE = Mappers.getMapper(KeyloggerFilterMapper.class);

    KeyloggerFilterDTO map(KeyloggerFilter keyloggerFilter);

    KeyloggerFilter map(KeyloggerFilterDTO keyloggerFilterDTO);
}
