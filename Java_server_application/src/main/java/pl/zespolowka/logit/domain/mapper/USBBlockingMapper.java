package pl.zespolowka.logit.domain.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.USBBlockingDTO;
import pl.zespolowka.logit.domain.model.USBBlocking;

@Mapper
public interface USBBlockingMapper {

    USBBlockingMapper INSTANCE = Mappers.getMapper(USBBlockingMapper.class);

    USBBlocking map(USBBlockingDTO usbBlockingDTO);

    USBBlockingDTO map(USBBlocking usbBlocking);
}
