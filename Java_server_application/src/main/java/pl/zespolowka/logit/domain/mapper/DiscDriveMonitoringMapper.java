package pl.zespolowka.logit.domain.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.DiscDriveMonitoringDTO;
import pl.zespolowka.logit.domain.model.DiscDriveMonitoring;

@Mapper
public interface DiscDriveMonitoringMapper {

    DiscDriveMonitoringMapper INSTANCE = Mappers.getMapper(DiscDriveMonitoringMapper.class);

    DiscDriveMonitoring map(DiscDriveMonitoringDTO discDriveMonitoringDTO);

    DiscDriveMonitoringDTO map(DiscDriveMonitoring discDriveMonitoring);
}
