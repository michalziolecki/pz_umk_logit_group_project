package pl.zespolowka.logit.domain.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.TransferSinceComputerStartedDTO;
import pl.zespolowka.logit.domain.model.TransferSinceComputerStarted;

@Mapper
public interface TransferSinceComputerStartedMapper {

    TransferSinceComputerStartedMapper INSTANCE = Mappers.getMapper(TransferSinceComputerStartedMapper.class);

    TransferSinceComputerStarted map(TransferSinceComputerStartedDTO transferSinceComputerStartedDTO);

    TransferSinceComputerStartedDTO map(TransferSinceComputerStarted transferSinceComputerStarted);
}
