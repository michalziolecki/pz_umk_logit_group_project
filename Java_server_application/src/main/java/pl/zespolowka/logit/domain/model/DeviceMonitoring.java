package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "urzadzenia_sledzenie")
public class DeviceMonitoring {
    private int id;
    private Timestamp operationDate;
    private String recognizedAs;
    private String operationStatus;
    private String deviceName;
    private String description;
    private String producer;
    private String deviceId;
    private String deviceGUID;
    private String deviceType;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "data_operacji", nullable = false)
    public Timestamp getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Timestamp dataOperacji) {
        this.operationDate = dataOperacji;
    }

    @Basic
    @Column(name = "rozpoznany_jako", nullable = false, length = 50)
    public String getRecognizedAs() {
        return recognizedAs;
    }

    public void setRecognizedAs(String rozpoznanyJako) {
        this.recognizedAs = rozpoznanyJako;
    }

    @Basic
    @Column(name = "status_operacji", nullable = false, length = 3)
    public String getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(String statusOperacji) {
        this.operationStatus = statusOperacji;
    }

    @Basic
    @Column(name = "nazwa_urzadzenia", nullable = false, length = 250)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String nazwaUrzadzenia) {
        this.deviceName = nazwaUrzadzenia;
    }

    @Basic
    @Column(name = "opis", nullable = false, length = 250)
    public String getDescription() {
        return description;
    }

    public void setDescription(String opis) {
        this.description = opis;
    }

    @Basic
    @Column(name = "producent", nullable = false, length = 250)
    public String getProducer() {
        return producer;
    }

    public void setProducer(String producent) {
        this.producer = producent;
    }

    @Basic
    @Column(name = "id_urzadzenia", nullable = false, length = 250)
    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String idUrzadzenia) {
        this.deviceId = idUrzadzenia;
    }

    @Basic
    @Column(name = "guid_urzadzenia", nullable = false, length = 250)
    public String getDeviceGUID() {
        return deviceGUID;
    }

    public void setDeviceGUID(String guidUrzadzenia) {
        this.deviceGUID = guidUrzadzenia;
    }

    @Basic
    @Column(name = "typ_urzadzenia", nullable = false, length = 100)
    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String typUrzadzenia) {
        this.deviceType = typUrzadzenia;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DeviceMonitoring that = (DeviceMonitoring) o;
        return id == that.id &&
                Objects.equals(operationDate, that.operationDate) &&
                Objects.equals(recognizedAs, that.recognizedAs) &&
                Objects.equals(operationStatus, that.operationStatus) &&
                Objects.equals(deviceName, that.deviceName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(producer, that.producer) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(deviceGUID, that.deviceGUID) &&
                Objects.equals(deviceType, that.deviceType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, operationDate, recognizedAs, operationStatus, deviceName, description, producer, deviceId, deviceGUID, deviceType);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
