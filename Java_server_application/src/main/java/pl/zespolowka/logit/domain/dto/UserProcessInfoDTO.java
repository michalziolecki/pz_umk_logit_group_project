package pl.zespolowka.logit.domain.dto;

import java.sql.Timestamp;

public class UserProcessInfoDTO {
    private int id;
    private Timestamp whenCollected;
    private String processName;
    private Integer threadsNumber;
    private Timestamp whenProcessStarted;
    private Long kilobytesEntry;
    private Long kilobytesRead;
    private int keyloggerUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getWhenCollected() {
        return whenCollected;
    }

    public void setWhenCollected(Timestamp whenCollected) {
        this.whenCollected = whenCollected;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Integer getThreadsNumber() {
        return threadsNumber;
    }

    public void setThreadsNumber(Integer threadsNumber) {
        this.threadsNumber = threadsNumber;
    }

    public Timestamp getWhenProcessStarted() {
        return whenProcessStarted;
    }

    public void setWhenProcessStarted(Timestamp whenProcessStarted) {
        this.whenProcessStarted = whenProcessStarted;
    }

    public Long getKilobytesEntry() {
        return kilobytesEntry;
    }

    public void setKilobytesEntry(Long kilobytesEntry) {
        this.kilobytesEntry = kilobytesEntry;
    }

    public Long getKilobytesRead() {
        return kilobytesRead;
    }

    public void setKilobytesRead(Long kilobytesRead) {
        this.kilobytesRead = kilobytesRead;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }
}
