package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "skanowanie_dyskow_zdalnych")
public class RemoteDriveFile {
    private int id;
    private String driveName;
    private Timestamp operationDate;
    private String fileName;
    private String filePath;
    private String fileType;
    private short isBinary;
    private String extension;
    private long size;
    private KeyloggerUser keyloggerUser;
    private RemoteDriveInfo remoteDriveInfo;
    private int keyloggerUserId;
    private int remoteDriveInfoId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "id_dysku", nullable = false, insertable = false, updatable = false)
    public int getRemoteDriveInfoId() {
        return remoteDriveInfoId;
    }

    public void setRemoteDriveInfoId(int remoteDriveInfoId) {
        this.remoteDriveInfoId = remoteDriveInfoId;
    }

    @Basic
    @Column(name = "nazwa_dysku", nullable = false, length = 3)
    public String getDriveName() {
        return driveName;
    }

    public void setDriveName(String nazwaDysku) {
        this.driveName = nazwaDysku;
    }

    @Basic
    @Column(name = "data_operacji", nullable = false)
    public Timestamp getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Timestamp dataOperacji) {
        this.operationDate = dataOperacji;
    }

    @Basic
    @Column(name = "nazwa_pliku", nullable = false, length = 250)
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String nazwaPliku) {
        this.fileName = nazwaPliku;
    }

    @Basic
    @Column(name = "sciezka_pliku", nullable = false, length = 512)
    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String sciezkaPliku) {
        this.filePath = sciezkaPliku;
    }

    @Basic
    @Column(name = "typ_pliku", nullable = false, length = 100)
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String typPliku) {
        this.fileType = typPliku;
    }

    @Basic
    @Column(name = "jest_binarny", nullable = false)
    public short getIsBinary() {
        return isBinary;
    }

    public void setIsBinary(short jestBinarny) {
        this.isBinary = jestBinarny;
    }

    @Basic
    @Column(name = "rozszerzenie", nullable = false, length = 10)
    public String getExtension() {
        return extension;
    }

    public void setExtension(String rozszerzenie) {
        this.extension = rozszerzenie;
    }

    @Basic
    @Column(name = "rozmiar", nullable = false)
    public long getSize() {
        return size;
    }

    public void setSize(long rozmiar) {
        this.size = rozmiar;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RemoteDriveFile that = (RemoteDriveFile) o;
        return id == that.id &&
                isBinary == that.isBinary &&
                size == that.size &&
                Objects.equals(driveName, that.driveName) &&
                Objects.equals(operationDate, that.operationDate) &&
                Objects.equals(fileName, that.fileName) &&
                Objects.equals(filePath, that.filePath) &&
                Objects.equals(fileType, that.fileType) &&
                Objects.equals(extension, that.extension);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, driveName, operationDate, fileName, filePath, fileType, isBinary, extension, size);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }

    @ManyToOne
    @JoinColumn(name = "id_dysku", referencedColumnName = "id", nullable = false)
    public RemoteDriveInfo getRemoteDriveInfo() {
        return remoteDriveInfo;
    }

    public void setRemoteDriveInfo(RemoteDriveInfo informacjeDyskiZdalneByIdDysku) {
        this.remoteDriveInfo = informacjeDyskiZdalneByIdDysku;
    }
}
