package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.RemoteDriveInfoDTO;
import pl.zespolowka.logit.domain.model.RemoteDriveInfo;

import java.rmi.Remote;

@Mapper
public interface RemoteDriveInfoMapper {

    RemoteDriveInfoMapper INSTANCE = Mappers.getMapper(RemoteDriveInfoMapper.class);

    RemoteDriveInfo map(RemoteDriveInfoDTO remoteDriveInfoDTO);

    RemoteDriveInfoDTO map(RemoteDriveInfo remoteDriveInfo);
}
