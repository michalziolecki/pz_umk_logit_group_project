package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.DeviceMonitoringDTO;
import pl.zespolowka.logit.domain.model.DeviceMonitoring;

@Mapper
public interface DeviceMonitoringMapper {

    DeviceMonitoringMapper INSTANCE = Mappers.getMapper(DeviceMonitoringMapper.class);

    DeviceMonitoring map(DeviceMonitoringDTO deviceMonitoringDTO);

    DeviceMonitoringDTO map(DeviceMonitoring deviceMonitoring);
}
