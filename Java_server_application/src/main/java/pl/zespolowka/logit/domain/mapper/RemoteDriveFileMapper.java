package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.RemoteDriveFileDTO;
import pl.zespolowka.logit.domain.model.RemoteDriveFile;

@Mapper
public interface RemoteDriveFileMapper {

    RemoteDriveFileMapper INSTANCE = Mappers.getMapper(RemoteDriveFileMapper.class);

    RemoteDriveFile map(RemoteDriveFileDTO remoteDriveFileDTO);

    RemoteDriveFileDTO map(RemoteDriveFile remoteDriveFile);
}
