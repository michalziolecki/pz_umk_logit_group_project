package pl.zespolowka.logit.domain.mapper;


import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.ProcessMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.model.ProcessMonitoringSettings;

@Mapper
public interface ProcessMonitoringSettingsMapper {

    ProcessMonitoringSettingsMapper INSTANCE = Mappers.getMapper(ProcessMonitoringSettingsMapper.class);

    ProcessMonitoringSettings map(ProcessMonitoringSettingsDTO processMonitoringSettingsDTO);

    ProcessMonitoringSettingsDTO map(ProcessMonitoringSettings processMonitoringSettings);
}
