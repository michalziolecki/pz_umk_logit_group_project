package pl.zespolowka.logit.domain.model;

import javax.persistence.*;

@Entity
@Table(name = "blokowanie_usb")
public class USBBlocking {

    private int id;
    private KeyloggerUser keyloggerUser;
    private short blockPorts;
    private int keyloggerUserId;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
    @Basic
    @Column(name = "blokuj_porty", nullable = false)
    public short getBlockPorts() {
        return blockPorts;
    }

    public void setBlockPorts(short blockPorts) {
        this.blockPorts = blockPorts;
    }
}
