package pl.zespolowka.logit.domain.dto;

import java.sql.Timestamp;

public class PingDTO {
    private int id;
    private int keyloggerUserId;
    private int pingMeasurementInMs;
    private String address;
    private Timestamp monitorTimestamp;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    public int getPingMeasurementInMs() {
        return pingMeasurementInMs;
    }

    public void setPingMeasurementInMs(int pingMeasurementInMs) {
        this.pingMeasurementInMs = pingMeasurementInMs;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Timestamp getMonitorTimestamp() {
        return monitorTimestamp;
    }

    public void setMonitorTimestamp(Timestamp monitorTimestamp) {
        this.monitorTimestamp = monitorTimestamp;
    }
}
