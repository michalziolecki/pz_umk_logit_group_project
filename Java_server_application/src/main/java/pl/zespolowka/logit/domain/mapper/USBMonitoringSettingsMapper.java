package pl.zespolowka.logit.domain.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import pl.zespolowka.logit.domain.dto.USBMonitoringSettingsDTO;
import pl.zespolowka.logit.domain.model.USBMonitoringSettings;

@Mapper
public interface USBMonitoringSettingsMapper {

    USBMonitoringSettingsMapper INSTANCE = Mappers.getMapper(USBMonitoringSettingsMapper.class);

    USBMonitoringSettings map(USBMonitoringSettingsDTO usbMonitoringSettingsDTO);

    USBMonitoringSettingsDTO map(USBMonitoringSettings usbMonitoringSettings);
}
