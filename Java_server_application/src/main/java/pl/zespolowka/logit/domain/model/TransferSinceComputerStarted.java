package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "transfer_od_wlaczenia_komputera")
public class TransferSinceComputerStarted {
    private int id;
    private int keyloggerUserId;
    private Integer uploadedKB;
    private Integer downloadedKB;
    private Timestamp monitorTimestamp;
    private KeyloggerUser keyloggerUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "ilosc_wyslanych_kb", nullable = true)
    public Integer getUploadedKB() {
        return uploadedKB;
    }

    public void setUploadedKB(Integer uploadedKB) {
        this.uploadedKB = uploadedKB;
    }

    @Basic
    @Column(name = "ilosc_pobranych_kb", nullable = true)
    public Integer getDownloadedKB() {
        return downloadedKB;
    }

    public void setDownloadedKB(Integer downloadedKB) {
        this.downloadedKB = downloadedKB;
    }

    @Basic
    @Column(name = "kiedy_rozpoczeto_pomiar", nullable = false)
    public Timestamp getMonitorTimestamp() {
        return monitorTimestamp;
    }

    public void setMonitorTimestamp(Timestamp monitorTimestamp) {
        this.monitorTimestamp = monitorTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransferSinceComputerStarted that = (TransferSinceComputerStarted) o;
        return id == that.id &&
                keyloggerUserId == that.keyloggerUserId &&
                Objects.equals(uploadedKB, that.uploadedKB) &&
                Objects.equals(downloadedKB, that.downloadedKB) &&
                Objects.equals(monitorTimestamp, that.monitorTimestamp);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyloggerUserId, uploadedKB, downloadedKB, monitorTimestamp);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
}
