package pl.zespolowka.logit.domain.dto;

import pl.zespolowka.logit.domain.model.KeyloggerUser;

public class USBBlockingDTO {

    private int id;
    private short blockPorts;
    private int keyloggerUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getBlockPorts() {
        return blockPorts;
    }

    public void setBlockPorts(short blockPorts) {
        this.blockPorts = blockPorts;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }
}
