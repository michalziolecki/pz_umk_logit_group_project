package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "informacje_dyski_zdalne")
public class RemoteDriveInfo {
    private int id;
    private Timestamp operationDate;
    private String recognizedAs;
    private String operationStatus;
    private String driveName;
    private String description;
    private String fileSystem;
    private long freeSpace;
    private long totalSpace;
    private String volumeName;
    private String volumeId;
    private KeyloggerUser keyloggerUser;
    private Collection<RemoteDriveFile> remoteDisksScanList;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "data_operacji", nullable = false)
    public Timestamp getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Timestamp dataOperacji) {
        this.operationDate = dataOperacji;
    }

    @Basic
    @Column(name = "rozpoznany_jako", nullable = false, length = 50)
    public String getRecognizedAs() {
        return recognizedAs;
    }

    public void setRecognizedAs(String rozpoznanyJako) {
        this.recognizedAs = rozpoznanyJako;
    }

    @Basic
    @Column(name = "status_operacji", nullable = false, length = 3)
    public String getOperationStatus() {
        return operationStatus;
    }

    public void setOperationStatus(String statusOperacji) {
        this.operationStatus = statusOperacji;
    }

    @Basic
    @Column(name = "nazwa_dysku", nullable = false, length = 3)
    public String getDriveName() {
        return driveName;
    }

    public void setDriveName(String nazwaDysku) {
        this.driveName = nazwaDysku;
    }

    @Basic
    @Column(name = "opis", nullable = false, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String opis) {
        this.description = opis;
    }

    @Basic
    @Column(name = "system_plikow", nullable = false, length = 10)
    public String getFileSystem() {
        return fileSystem;
    }

    public void setFileSystem(String systemPlikow) {
        this.fileSystem = systemPlikow;
    }

    @Basic
    @Column(name = "wolna_przestrzen", nullable = false)
    public long getFreeSpace() {
        return freeSpace;
    }

    public void setFreeSpace(long wolnaPrzestrzen) {
        this.freeSpace = wolnaPrzestrzen;
    }

    @Basic
    @Column(name = "calkowita_przestrzen", nullable = false)
    public long getTotalSpace() {
        return totalSpace;
    }

    public void setTotalSpace(long calkowitaPrzestrzen) {
        this.totalSpace = calkowitaPrzestrzen;
    }

    @Basic
    @Column(name = "nazwa_woluminu", nullable = false, length = 100)
    public String getVolumeName() {
        return volumeName;
    }

    public void setVolumeName(String nazwaWoluminu) {
        this.volumeName = nazwaWoluminu;
    }

    @Basic
    @Column(name = "identyfikator_woluminu", nullable = false, length = 100)
    public String getVolumeId() {
        return volumeId;
    }

    public void setVolumeId(String identyfikatorWoluminu) {
        this.volumeId = identyfikatorWoluminu;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RemoteDriveInfo that = (RemoteDriveInfo) o;
        return id == that.id &&
                freeSpace == that.freeSpace &&
                totalSpace == that.totalSpace &&
                Objects.equals(operationDate, that.operationDate) &&
                Objects.equals(recognizedAs, that.recognizedAs) &&
                Objects.equals(operationStatus, that.operationStatus) &&
                Objects.equals(driveName, that.driveName) &&
                Objects.equals(description, that.description) &&
                Objects.equals(fileSystem, that.fileSystem) &&
                Objects.equals(volumeName, that.volumeName) &&
                Objects.equals(volumeId, that.volumeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, operationDate, recognizedAs, operationStatus, driveName, description, fileSystem, freeSpace, totalSpace, volumeName, volumeId);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }

    @OneToMany(mappedBy = "remoteDriveInfo")
    public Collection<RemoteDriveFile> getRemoteDisksScanList() {
        return remoteDisksScanList;
    }

    public void setRemoteDisksScanList(Collection<RemoteDriveFile> skanowanieDyskowZdalnychesById) {
        this.remoteDisksScanList = skanowanieDyskowZdalnychesById;
    }
}
