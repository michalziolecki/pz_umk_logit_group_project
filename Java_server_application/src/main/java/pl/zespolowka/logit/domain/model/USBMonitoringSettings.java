package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "sledzenie_usb")
public class USBMonitoringSettings {
    private int id;
    private short isMonitored;
    private short isScaned;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "czy_sledzic", nullable = false)
    public short getIsMonitored() {
        return isMonitored;
    }

    public void setIsMonitored(short czySledzic) {
        this.isMonitored = czySledzic;
    }

    @Basic
    @Column(name = "czy_skanowac", nullable = false)
    public short getIsScaned() {
        return isScaned;
    }

    public void setIsScaned(short czySkanowac) {
        this.isScaned = czySkanowac;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        USBMonitoringSettings that = (USBMonitoringSettings) o;
        return id == that.id &&
                isMonitored == that.isMonitored &&
                isScaned == that.isScaned;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isMonitored, isScaned);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
