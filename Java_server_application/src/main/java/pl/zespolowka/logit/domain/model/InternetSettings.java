package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ustawienia_internetowe")
public class InternetSettings {
    private int id;
    private int keyloggerUserId;
    private short isCurrentTransferMonitored;
    private Integer currentTransferMonitorFrequency;
    private short isUploadSpeedMonitored;
    private short isDownloadSpeedMonitored;
    private short isTransferMonitoredSinceComputerStarted;
    private Integer transferSinceComputerStartedMonitorFrequency;
    private short shouldPing;
    private String pingAddress;
    private Integer pingFrequency;
    private short isNetworkCardInfoMonitor;
    private Integer networkCardMonitorFrequency;
    private KeyloggerUser keyloggerUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "czy_monitorowac_transfer_obecny", nullable = false)
    public short getIsCurrentTransferMonitored() {
        return isCurrentTransferMonitored;
    }

    public void setIsCurrentTransferMonitored(short isCurrentTransferMonitored) {
        this.isCurrentTransferMonitored = isCurrentTransferMonitored;
    }

    @Basic
    @Column(name = "co_ile_monitorowac_transfer_obecny", nullable = true)
    public Integer getCurrentTransferMonitorFrequency() {
        return currentTransferMonitorFrequency;
    }

    public void setCurrentTransferMonitorFrequency(Integer currentTransferMonitorFrequency) {
        this.currentTransferMonitorFrequency = currentTransferMonitorFrequency;
    }

    @Basic
    @Column(name = "czy_predkosc_wysylania", nullable = false)
    public short getIsUploadSpeedMonitored() {
        return isUploadSpeedMonitored;
    }

    public void setIsUploadSpeedMonitored(short isUploadSpeedMonitored) {
        this.isUploadSpeedMonitored = isUploadSpeedMonitored;
    }

    @Basic
    @Column(name = "czy_predkosc_pobierania", nullable = false)
    public short getIsDownloadSpeedMonitored() {
        return isDownloadSpeedMonitored;
    }

    public void setIsDownloadSpeedMonitored(short isDownloadSpeedMonitored) {
        this.isDownloadSpeedMonitored = isDownloadSpeedMonitored;
    }

    @Basic
    @Column(name = "czy_monitorowac_transfer_od_wlaczenia_komputera", nullable = false)
    public short getIsTransferMonitoredSinceComputerStarted() {
        return isTransferMonitoredSinceComputerStarted;
    }

    public void setIsTransferMonitoredSinceComputerStarted(short isTransferMonitoredSinceComputerStarted) {
        this.isTransferMonitoredSinceComputerStarted = isTransferMonitoredSinceComputerStarted;
    }

    @Basic
    @Column(name = "co_ile_monitorowac_transfer_od_wlaczenia_komputera", nullable = true)
    public Integer getTransferSinceComputerStartedMonitorFrequency() {
        return transferSinceComputerStartedMonitorFrequency;
    }

    public void setTransferSinceComputerStartedMonitorFrequency(Integer transferSinceComputerStartedMonitorFrequency) {
        this.transferSinceComputerStartedMonitorFrequency = transferSinceComputerStartedMonitorFrequency;
    }

    @Basic
    @Column(name = "czy_pingowac", nullable = false)
    public short getShouldPing() {
        return shouldPing;
    }

    public void setShouldPing(short shouldPing) {
        this.shouldPing = shouldPing;
    }

    @Basic
    @Column(name = "adres_dla_pingu", nullable = true, length = 50)
    public String getPingAddress() {
        return pingAddress;
    }

    public void setPingAddress(String pingAddress) {
        this.pingAddress = pingAddress;
    }

    @Basic
    @Column(name = "co_ile_pingowac", nullable = true)
    public Integer getPingFrequency() {
        return pingFrequency;
    }

    public void setPingFrequency(Integer pingFrequency) {
        this.pingFrequency = pingFrequency;
    }

    @Basic
    @Column(name = "czy_zbierac_informacje_o_karcie_sieciowej", nullable = false)
    public short getIsNetworkCardInfoMonitor() {
        return isNetworkCardInfoMonitor;
    }

    public void setIsNetworkCardInfoMonitor(short isNetworkCardInfoMonitor) {
        this.isNetworkCardInfoMonitor = isNetworkCardInfoMonitor;
    }

    @Basic
    @Column(name = "co_ile_zbierac_informacje_o_karcie_sieciowej", nullable = true)
    public Integer getNetworkCardMonitorFrequency() {
        return networkCardMonitorFrequency;
    }

    public void setNetworkCardMonitorFrequency(Integer networkCardMonitorFrequency) {
        this.networkCardMonitorFrequency = networkCardMonitorFrequency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InternetSettings that = (InternetSettings) o;
        return id == that.id &&
                keyloggerUserId == that.keyloggerUserId &&
                isCurrentTransferMonitored == that.isCurrentTransferMonitored &&
                isUploadSpeedMonitored == that.isUploadSpeedMonitored &&
                isDownloadSpeedMonitored == that.isDownloadSpeedMonitored &&
                isTransferMonitoredSinceComputerStarted == that.isTransferMonitoredSinceComputerStarted &&
                shouldPing == that.shouldPing &&
                isNetworkCardInfoMonitor == that.isNetworkCardInfoMonitor &&
                Objects.equals(currentTransferMonitorFrequency, that.currentTransferMonitorFrequency) &&
                Objects.equals(transferSinceComputerStartedMonitorFrequency, that.transferSinceComputerStartedMonitorFrequency) &&
                Objects.equals(pingAddress, that.pingAddress) &&
                Objects.equals(pingFrequency, that.pingFrequency) &&
                Objects.equals(networkCardMonitorFrequency, that.networkCardMonitorFrequency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyloggerUserId, isCurrentTransferMonitored, currentTransferMonitorFrequency, isUploadSpeedMonitored, isDownloadSpeedMonitored, isTransferMonitoredSinceComputerStarted, transferSinceComputerStartedMonitorFrequency, shouldPing, pingAddress, pingFrequency, isNetworkCardInfoMonitor, networkCardMonitorFrequency);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
}
