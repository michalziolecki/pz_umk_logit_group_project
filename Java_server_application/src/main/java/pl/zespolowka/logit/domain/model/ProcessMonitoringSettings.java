package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ustawienia_proces")
public class ProcessMonitoringSettings {
    private int id;
    private short isActiveProcessMonitored;
    private short areProcessesMonitored;
    private Integer processMonitoringFrequency;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "czy_zbierac_aktywny_proces", nullable = false)
    public short getIsActiveProcessMonitored() {
        return isActiveProcessMonitored;
    }

    public void setIsActiveProcessMonitored(short czyZbieracAktywnyProces) {
        this.isActiveProcessMonitored = czyZbieracAktywnyProces;
    }

    @Basic
    @Column(name = "czy_zbierac_procesy_usera", nullable = false)
    public short getAreProcessesMonitored() {
        return areProcessesMonitored;
    }

    public void setAreProcessesMonitored(short czyZbieracProcesyUsera) {
        this.areProcessesMonitored = czyZbieracProcesyUsera;
    }

    @Basic
    @Column(name = "co_ile_zbierac_procesy_usera", nullable = true)
    public Integer getProcessMonitoringFrequency() {
        return processMonitoringFrequency;
    }

    public void setProcessMonitoringFrequency(Integer coIleZbieracProcesyUsera) {
        this.processMonitoringFrequency = coIleZbieracProcesyUsera;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessMonitoringSettings that = (ProcessMonitoringSettings) o;
        return id == that.id &&
                isActiveProcessMonitored == that.isActiveProcessMonitored &&
                areProcessesMonitored == that.areProcessesMonitored &&
                Objects.equals(processMonitoringFrequency, that.processMonitoringFrequency);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, isActiveProcessMonitored, areProcessesMonitored, processMonitoringFrequency);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
