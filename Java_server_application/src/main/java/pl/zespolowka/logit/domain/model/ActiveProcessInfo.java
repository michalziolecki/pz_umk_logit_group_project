package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "informacje_o_aktywnym_procesie")
public class ActiveProcessInfo {
    private int id;
    private Timestamp whenInfoCollected;
    private String processName;
    private Integer threadsNumber;
    private Timestamp whenStarted;
    private Integer kilobytesEntry;
    private Integer kilobytesRead;
    private KeyloggerUser keyloggerUser;
    private int keyloggerUserId;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "kiedy_zebrano_informacje", nullable = false)
    public Timestamp getWhenInfoCollected() {
        return whenInfoCollected;
    }

    public void setWhenInfoCollected(Timestamp kiedyZebranoInformacje) {
        this.whenInfoCollected = kiedyZebranoInformacje;
    }

    @Basic
    @Column(name = "nazwa_procesu", nullable = true, length = 100)
    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String nazwaProcesu) {
        this.processName = nazwaProcesu;
    }

    @Basic
    @Column(name = "ilosc_watkow", nullable = true)
    public Integer getThreadsNumber() {
        return threadsNumber;
    }

    public void setThreadsNumber(Integer iloscWatkow) {
        this.threadsNumber = iloscWatkow;
    }

    @Basic
    @Column(name = "od_kiedy_dziala_proces", nullable = true)
    public Timestamp getWhenStarted() {
        return whenStarted;
    }

    public void setWhenStarted(Timestamp odKiedyDzialaProces) {
        this.whenStarted = odKiedyDzialaProces;
    }

    @Basic
    @Column(name = "zapis_w_kilobytach", nullable = true)
    public Integer getKilobytesEntry() {
        return kilobytesEntry;
    }

    public void setKilobytesEntry(Integer zapisWKilobytach) {
        this.kilobytesEntry = zapisWKilobytach;
    }

    @Basic
    @Column(name = "odczyt_w_kilobytach", nullable = true)
    public Integer getKilobytesRead() {
        return kilobytesRead;
    }

    public void setKilobytesRead(Integer odczytWKilobytach) {
        this.kilobytesRead = odczytWKilobytach;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActiveProcessInfo that = (ActiveProcessInfo) o;
        return id == that.id &&
                Objects.equals(whenInfoCollected, that.whenInfoCollected) &&
                Objects.equals(processName, that.processName) &&
                Objects.equals(threadsNumber, that.threadsNumber) &&
                Objects.equals(whenStarted, that.whenStarted) &&
                Objects.equals(kilobytesEntry, that.kilobytesEntry) &&
                Objects.equals(kilobytesRead, that.kilobytesRead);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, whenInfoCollected, processName, threadsNumber, whenStarted, kilobytesEntry, kilobytesRead);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser uzytkownicyKeyloggerByIdUzytkownicyKeylogger) {
        this.keyloggerUser = uzytkownicyKeyloggerByIdUzytkownicyKeylogger;
    }
}
