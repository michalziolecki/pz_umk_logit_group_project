package pl.zespolowka.logit.domain.model;

import jdk.Exported;
import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;

@Data
@Entity
@Table(name = "rola_departamenty")
public class RoleDepartment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "id_role_przydzial")
    private RoleAssignment roleAssignment;

    @ManyToOne
    @JoinColumn(name = "id_departament")
    private Department department;

}
