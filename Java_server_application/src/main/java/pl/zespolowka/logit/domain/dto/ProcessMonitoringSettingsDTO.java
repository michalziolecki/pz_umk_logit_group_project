package pl.zespolowka.logit.domain.dto;

public class ProcessMonitoringSettingsDTO {

    private int id;
    private short isActiveProcessMonitored;
    private short areProcessesMonitored;
    private Integer processMonitoringFrequency;
    private int keyloggerUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public short getIsActiveProcessMonitored() {
        return isActiveProcessMonitored;
    }

    public void setIsActiveProcessMonitored(short isActiveProcessMonitored) {
        this.isActiveProcessMonitored = isActiveProcessMonitored;
    }

    public short getAreProcessesMonitored() {
        return areProcessesMonitored;
    }

    public void setAreProcessesMonitored(short areProcessesMonitored) {
        this.areProcessesMonitored = areProcessesMonitored;
    }

    public Integer getProcessMonitoringFrequency() {
        return processMonitoringFrequency;
    }

    public void setProcessMonitoringFrequency(Integer processMonitoringFrequency) {
        this.processMonitoringFrequency = processMonitoringFrequency;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }
}
