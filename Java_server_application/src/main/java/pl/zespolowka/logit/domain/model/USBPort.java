package pl.zespolowka.logit.domain.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "porty_usb")
public class USBPort {
    private int id;
    private int keyloggerUserId;
    private short isDisabled;
    private KeyloggerUser keyloggerUser;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "id_uzytkownicy_keylogger", nullable = false, insertable = false, updatable = false)
    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }

    @Basic
    @Column(name = "czy_zablokowany", nullable = false)
    public short getIsDisabled() {
        return isDisabled;
    }

    public void setIsDisabled(short isDisabled) {
        this.isDisabled = isDisabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        USBPort USBPort = (USBPort) o;
        return id == USBPort.id &&
                keyloggerUserId == USBPort.keyloggerUserId &&
                isDisabled == USBPort.isDisabled;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, keyloggerUserId, isDisabled);
    }

    @ManyToOne
    @JoinColumn(name = "id_uzytkownicy_keylogger", referencedColumnName = "id", nullable = false)
    public KeyloggerUser getKeyloggerUser() {
        return keyloggerUser;
    }

    public void setKeyloggerUser(KeyloggerUser keyloggerUser) {
        this.keyloggerUser = keyloggerUser;
    }
}
