package pl.zespolowka.logit.domain.dto;

import java.sql.Timestamp;

public class ActiveProcessInfoDTO {

    private int id;
    private Timestamp whenInfoCollected;
    private String processName;
    private Integer threadsNumber;
    private Timestamp whenStarted;
    private Integer kilobytesEntry;
    private Integer kilobytesRead;
    private int keyloggerUserId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getWhenInfoCollected() {
        return whenInfoCollected;
    }

    public void setWhenInfoCollected(Timestamp whenInfoCollected) {
        this.whenInfoCollected = whenInfoCollected;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public Integer getThreadsNumber() {
        return threadsNumber;
    }

    public void setThreadsNumber(Integer threadsNumber) {
        this.threadsNumber = threadsNumber;
    }

    public Timestamp getWhenStarted() {
        return whenStarted;
    }

    public void setWhenStarted(Timestamp whenStarted) {
        this.whenStarted = whenStarted;
    }

    public Integer getKilobytesEntry() {
        return kilobytesEntry;
    }

    public void setKilobytesEntry(Integer kilobytesEntry) {
        this.kilobytesEntry = kilobytesEntry;
    }

    public Integer getKilobytesRead() {
        return kilobytesRead;
    }

    public void setKilobytesRead(Integer kilobytesRead) {
        this.kilobytesRead = kilobytesRead;
    }

    public int getKeyloggerUserId() {
        return keyloggerUserId;
    }

    public void setKeyloggerUserId(int keyloggerUserId) {
        this.keyloggerUserId = keyloggerUserId;
    }
}
