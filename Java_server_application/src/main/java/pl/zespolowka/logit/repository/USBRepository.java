package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.USBPort;

import java.util.List;

@Repository
public interface USBRepository extends JpaRepository<USBPort, Integer> {
    List<USBPort> findAllByKeyloggerUser(KeyloggerUser keyloggerUser);
}
