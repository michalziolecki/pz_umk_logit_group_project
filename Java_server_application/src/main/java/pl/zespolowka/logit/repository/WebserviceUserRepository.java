package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.List;

@Repository
public interface WebserviceUserRepository extends JpaRepository<WebserviceUser, Integer>, JpaSpecificationExecutor<WebserviceUser> {
    WebserviceUser findByUsernameIgnoreCase(String username);
}
