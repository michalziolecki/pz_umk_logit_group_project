package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role findByCode(String code);
}
