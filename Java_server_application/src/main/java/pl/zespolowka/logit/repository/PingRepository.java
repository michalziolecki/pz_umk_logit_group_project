package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.Ping;

import java.util.List;
import java.util.Optional;

@Repository
public interface PingRepository extends JpaRepository<Ping, Integer> {

    Optional<Ping> findFirstByKeyloggerUserIdOrderByMonitorTimestampDesc(int keyloggerUserId);

    List<Ping> findAllByKeyloggerUserIdOrderByMonitorTimestampDesc(int keyloggerUserId);
}
