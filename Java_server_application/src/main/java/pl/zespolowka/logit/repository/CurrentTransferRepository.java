package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.mapper.CurrentTransferMapper;
import pl.zespolowka.logit.domain.model.CurrentTransfer;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.List;
import java.util.Optional;

@Repository
public interface CurrentTransferRepository extends JpaRepository<CurrentTransfer, Integer> {
    Optional<CurrentTransfer> findFirstByKeyloggerUserOrderByWhenStartedDesc(KeyloggerUser user);

    List<CurrentTransfer> findByKeyloggerUserOrderByWhenStartedDesc(KeyloggerUser keyloggerUser);
}
