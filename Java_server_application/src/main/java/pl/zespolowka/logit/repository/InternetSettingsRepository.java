package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.InternetSettings;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.Optional;

@Repository
public interface InternetSettingsRepository extends JpaRepository<InternetSettings, Integer> {
    Optional<InternetSettings> findByKeyloggerUser(KeyloggerUser keyloggerUser);
}
