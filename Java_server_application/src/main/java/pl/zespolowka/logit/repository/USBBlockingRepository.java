package pl.zespolowka.logit.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.USBBlocking;

import java.util.Optional;

@Repository
public interface USBBlockingRepository extends JpaRepository<USBBlocking, Integer> {

    Optional<USBBlocking> findByKeyloggerUserId(int userId);
}
