package pl.zespolowka.logit.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.model.TransferSinceComputerStarted;

import java.util.List;
import java.util.Optional;

@Repository
public interface TransferSinceComputerStartedRepository extends JpaRepository<TransferSinceComputerStarted, Integer> {

    List<TransferSinceComputerStarted> findAllByKeyloggerUserIdOrderByMonitorTimestampDesc(int keyloggerUserId);

    Optional<TransferSinceComputerStarted> findFirstByKeyloggerUserIdOrderByMonitorTimestampDesc(int keyloggerUserId);
}
