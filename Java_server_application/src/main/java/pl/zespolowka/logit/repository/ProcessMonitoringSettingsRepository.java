package pl.zespolowka.logit.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.ProcessMonitoringSettings;

import java.util.Optional;

@Repository
public interface ProcessMonitoringSettingsRepository extends JpaRepository<ProcessMonitoringSettings, Integer> {

    Optional<ProcessMonitoringSettings> findByKeyloggerUserId(int keyloggerUserId);
}
