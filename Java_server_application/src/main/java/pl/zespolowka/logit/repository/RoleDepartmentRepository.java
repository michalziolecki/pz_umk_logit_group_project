package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.RoleDepartment;

import java.util.Collection;
import java.util.List;

@Repository
public interface RoleDepartmentRepository extends JpaRepository<RoleDepartment, Integer> {

    List<RoleDepartment> findByDepartmentAndRoleAssignmentIn(Department department, Collection<RoleAssignment> roleAssignment);

    List<RoleDepartment> findByRoleAssignmentIn(List<RoleAssignment> roleAssignments);
}
