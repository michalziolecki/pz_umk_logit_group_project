package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.RestController;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.RemoteDriveFile;
import pl.zespolowka.logit.domain.model.UserProcessInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface RemoteDriveFileRepository extends JpaRepository<RemoteDriveFile, Integer> {

    List<RemoteDriveFile> findAllByRemoteDriveInfoId(int driveId);
    List<RemoteDriveFile> findAllByRemoteDriveInfoIdAndOperationDate(int driveId, Timestamp whenCollected);
    Optional<RemoteDriveFile> findFirstByRemoteDriveInfoIdOrderByOperationDateDesc(int driveId);
}
