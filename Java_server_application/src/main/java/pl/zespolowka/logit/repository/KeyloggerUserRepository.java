package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.dto.KeyloggerUserDTO;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.List;


@Repository
public interface KeyloggerUserRepository extends JpaRepository<KeyloggerUser, Integer> {
    @Query("SELECT distinct keyloggerUser FROM KeyloggerUser keyloggerUser" +
            " WHERE keyloggerUser.computerName LIKE :query OR keyloggerUser.username LIKE :query")
    List<KeyloggerUser> findByUsernameQuery(@Param("query") String query);
}
