package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.KeyloggerFilter;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.List;

@Repository
public interface KeyloggerFilterRepository extends JpaRepository<KeyloggerFilter, Integer> {

   List<KeyloggerFilter> findAllByKeyloggerUser(KeyloggerUser keyloggerUser);
}
