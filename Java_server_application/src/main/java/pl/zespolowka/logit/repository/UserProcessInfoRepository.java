package pl.zespolowka.logit.repository;

import org.apache.catalina.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.UserProcessInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface UserProcessInfoRepository extends JpaRepository<UserProcessInfo, Integer> {
    List<UserProcessInfo> findAllByKeyloggerUserIdAndWhenCollectedEquals(int keyloggerUserId, Timestamp whenCollected);
    Optional<UserProcessInfo> findFirstByKeyloggerUserIdOrderByWhenCollectedDesc(int keyloggerUserId);
    List<UserProcessInfo> findAllByKeyloggerUserId(int keyloggerUserId);
}
