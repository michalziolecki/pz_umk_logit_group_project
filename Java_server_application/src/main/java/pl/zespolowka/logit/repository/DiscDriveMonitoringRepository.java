package pl.zespolowka.logit.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.DiscDriveMonitoring;

import java.util.Optional;

@Repository
public interface DiscDriveMonitoringRepository extends JpaRepository<DiscDriveMonitoring, Integer> {

    Optional<DiscDriveMonitoring> findByKeyloggerUserId(int userId);
}
