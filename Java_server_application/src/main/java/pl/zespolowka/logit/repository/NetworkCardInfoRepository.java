package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.mapper.NetworkCardInfoMapper;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.NetworkCardInfo;

import java.util.List;
import java.util.Optional;

@Repository
public interface NetworkCardInfoRepository extends JpaRepository<NetworkCardInfo, Integer> {
    Optional<NetworkCardInfo> findFirstByKeyloggerUserOrderByWhenInfoCollectedDesc(KeyloggerUser keyloggerUser);

    List<NetworkCardInfo> findByKeyloggerUserOrderByWhenInfoCollectedDesc(KeyloggerUser keyloggerUser);
}
