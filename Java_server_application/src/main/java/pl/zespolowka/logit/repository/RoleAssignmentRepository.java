package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.List;

@Repository
public interface RoleAssignmentRepository extends JpaRepository<RoleAssignment, Integer> {
    List<RoleAssignment> findAllByWebserviceUser(WebserviceUser webserviceUser);

    List<RoleAssignment> findByWebserviceUserAndRole(WebserviceUser webserviceUser, Role role);

    List<RoleAssignment> findByWebserviceUser(WebserviceUser webserviceUser);
}
