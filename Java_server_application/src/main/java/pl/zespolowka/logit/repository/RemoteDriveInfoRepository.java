package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.KeyloggerUser;
import pl.zespolowka.logit.domain.model.RemoteDriveInfo;

import java.sql.Timestamp;
import java.util.List;
import java.util.Optional;

@Repository
public interface RemoteDriveInfoRepository extends JpaRepository<RemoteDriveInfo, Integer> {

    List<RemoteDriveInfo> findAllByKeyloggerUserAndOperationStatus(KeyloggerUser keyloggerUser, String operationStatus);

    List<RemoteDriveInfo> findAllByKeyloggerUser(KeyloggerUser keyloggerUser);
}
