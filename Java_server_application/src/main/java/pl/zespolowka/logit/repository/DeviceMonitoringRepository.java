package pl.zespolowka.logit.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.DeviceMonitoring;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.Arrays;
import java.util.List;

@Repository
public interface DeviceMonitoringRepository extends JpaRepository<DeviceMonitoring, Integer> {
    List<DeviceMonitoring> findAllByKeyloggerUserAndOperationStatusOrderByOperationDate(KeyloggerUser keyloggerUser, String operationStatus);

    List<DeviceMonitoring> findAllByKeyloggerUserOrderByOperationDate(KeyloggerUser keyloggerUser);
}
