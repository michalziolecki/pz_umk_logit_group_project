package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.USBMonitoringSettings;

import java.util.Optional;

@Repository
public interface USBMonitoringSettingsRepository extends JpaRepository<USBMonitoringSettings, Integer> {
    Optional<USBMonitoringSettings> findByKeyloggerUserId(int keyloggerUserId);
}
