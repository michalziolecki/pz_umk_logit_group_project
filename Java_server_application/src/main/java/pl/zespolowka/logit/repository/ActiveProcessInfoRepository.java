package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.ActiveProcessInfo;

import java.util.List;
import java.util.Optional;

@Repository
public interface ActiveProcessInfoRepository extends JpaRepository<ActiveProcessInfo, Integer> {
    Optional<ActiveProcessInfo> findFirstByKeyloggerUserIdOrderByWhenInfoCollectedDesc(int keyloggerUserId);

    List<ActiveProcessInfo>  findAllByKeyloggerUserId(int keyloggerUserId);
}
