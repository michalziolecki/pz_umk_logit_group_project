package pl.zespolowka.logit.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.zespolowka.logit.domain.model.KeyloggerMessage;
import pl.zespolowka.logit.domain.model.KeyloggerUser;

import java.util.List;

@Repository
public interface KeyloggerMessageRepository extends JpaRepository<KeyloggerMessage, Integer> {

    List<KeyloggerMessage> findAllByKeyloggerUser(KeyloggerUser keyloggerUser);

    @Query("SELECT distinct keyloggerMessage FROM KeyloggerMessage keyloggerMessage" +
            " WHERE keyloggerMessage.keyloggerUserId = :keyloggerUserId AND keyloggerMessage.message LIKE :query")
    List<KeyloggerMessage> findAllByKeyloggerUserIdAndMessageLikeQuery(int keyloggerUserId, String query);
}
