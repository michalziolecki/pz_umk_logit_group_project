package pl.zespolowka.logit.configuration.security.exception;

public class InvalidUserException extends Exception {
    public InvalidUserException() {
    }

    public InvalidUserException(String message) {
        super(message);
    }
}

