package pl.zespolowka.logit.configuration.security.service;

import lombok.NonNull;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.security.exception.InvalidUserException;
import pl.zespolowka.logit.configuration.security.transfer.AuthenticationDTO;
import pl.zespolowka.logit.configuration.security.transfer.CredentialsDTO;
import pl.zespolowka.logit.configuration.security.transfer.TokenRefreshDTO;

@Service
public interface AuthenticationService {
    AuthenticationDTO authenticate(@NonNull CredentialsDTO credentials) throws InvalidUserException;
    AuthenticationDTO refreshToken(@NonNull TokenRefreshDTO token);
}
