package pl.zespolowka.logit.configuration.security.transfer;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class RoleDepartmentDTO {
    private String role;
    private String department;

    public RoleDepartmentDTO(String role, String department) {
        this.role = role;
        this.department = department;
    }
}
