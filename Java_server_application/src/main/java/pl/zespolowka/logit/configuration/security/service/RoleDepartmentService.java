package pl.zespolowka.logit.configuration.security.service;

import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.RoleDepartment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.Collection;
import java.util.List;

public interface RoleDepartmentService {

    List<RoleDepartment> getWebserviceUserRoleDepartments(Department department, Collection<RoleAssignment> roleAssignments);
}
