package pl.zespolowka.logit.configuration.security.transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.enumerate.RoleEnum;

import java.io.Serializable;

@Getter
@AllArgsConstructor
public class CurrentContext implements Serializable {

    private RoleEnum role;
    private WebserviceUser webserviceUser;
    private String username;
    private Department department;

    public CurrentContext(RoleEnum role, WebserviceUser webserviceUser, Department department) {
        this.role = role;
        this.webserviceUser = webserviceUser;
        this.department = department;
    }

    public boolean isAdmin() {
        return RoleEnum.ADMIN.equals(role);
    }
}