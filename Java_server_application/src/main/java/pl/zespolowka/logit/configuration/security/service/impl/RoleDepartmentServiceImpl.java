package pl.zespolowka.logit.configuration.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.security.service.RoleDepartmentService;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.RoleDepartment;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.repository.RoleDepartmentRepository;

import java.util.Collection;
import java.util.List;

@Service
public class RoleDepartmentServiceImpl implements RoleDepartmentService {

    @Autowired
    private RoleDepartmentRepository roleDepartmentRepository;


    @Override
    public List<RoleDepartment> getWebserviceUserRoleDepartments(Department department, Collection<RoleAssignment> roleAssignments) {
        return roleDepartmentRepository.findByDepartmentAndRoleAssignmentIn(department, roleAssignments);
    }
}
