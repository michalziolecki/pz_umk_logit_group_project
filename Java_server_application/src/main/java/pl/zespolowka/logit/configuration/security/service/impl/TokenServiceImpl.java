package pl.zespolowka.logit.configuration.security.service.impl;

import com.google.common.base.Preconditions;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.NonNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.zespolowka.logit.configuration.security.service.TokenService;
import pl.zespolowka.logit.domain.dto.DepartmentDTO;
import pl.zespolowka.logit.domain.mapper.DepartmentMapper;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TokenServiceImpl implements TokenService {

    @Value("${token.secret")
    private String secret;

    @Value("${token.validity.in.minutes}")
    private long validityPeriodInMinutes;

    private static final int MILLISECONDS_PER_MINUTE = 60 * 1000;
    private String EMPTY_TOKEN = "Token can not be empty";


    @Override
    public String generateToken(WebserviceUser webserviceUser, List<RoleAssignment> roleAssignments, Department department) {
        DepartmentDTO departmentDto = DepartmentMapper.INSTANCE.map(department);

        Claims claims = Jwts.claims().setSubject(webserviceUser.getUsername());
        claims.setExpiration(Date.from(LocalDateTime.now().plusMinutes(validityPeriodInMinutes).atZone(ZoneId.systemDefault()).toInstant()));
        List<Role> roles = roleAssignments.stream().map(RoleAssignment::getRole).collect(Collectors.toList());

        claims.put("userId", webserviceUser.getId());
        claims.put("department", departmentDto);
        claims.put("roles", roles);

        return Jwts.builder()
                .setClaims(claims)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();

    }

    @Override
    public long getValidityPeriodInMillis() {
        return validityPeriodInMinutes * MILLISECONDS_PER_MINUTE;
    }

    @Override
    public Claims parseToken(final String token) {
        Preconditions.checkArgument(!StringUtils.isEmpty(token), EMPTY_TOKEN);

        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    @Override
    public boolean validateExpirationDate(@NonNull Date expires) {
        return expires.after(new Date());
    }
}
