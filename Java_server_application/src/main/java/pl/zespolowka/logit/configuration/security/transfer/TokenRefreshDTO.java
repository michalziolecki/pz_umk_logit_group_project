package pl.zespolowka.logit.configuration.security.transfer;


public class TokenRefreshDTO {
    private String token;
    private int departmentId;

    public TokenRefreshDTO(String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public void setDepartmentId(int departmentId) {
        this.departmentId = departmentId;
    }
}
