package pl.zespolowka.logit.configuration.security.service;

import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.List;

public interface RoleAssignmentService {

    List<RoleAssignment> getRoles(WebserviceUser webserviceUser);

    List<RoleAssignment> getUserRolesForDepartment(WebserviceUser webserviceUser, int departmentId);
}
