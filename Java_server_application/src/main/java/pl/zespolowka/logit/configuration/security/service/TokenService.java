package pl.zespolowka.logit.configuration.security.service;

import io.jsonwebtoken.Claims;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;

import java.util.Date;
import java.util.List;

public interface TokenService {

    String generateToken(WebserviceUser webserviceUser, List<RoleAssignment> roleAssignments, Department department);

    long getValidityPeriodInMillis();

    Claims parseToken(final String token);

    boolean validateExpirationDate(Date expiration);
}