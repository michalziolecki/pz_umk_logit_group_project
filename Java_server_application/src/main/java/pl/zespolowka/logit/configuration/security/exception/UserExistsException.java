package pl.zespolowka.logit.configuration.security.exception;

public class UserExistsException extends Exception {
    public UserExistsException() {
    }

    public UserExistsException(String message) {
        super(message);
    }
}
