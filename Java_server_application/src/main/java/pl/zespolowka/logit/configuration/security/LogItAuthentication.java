package pl.zespolowka.logit.configuration.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Objects;

public class LogItAuthentication extends UsernamePasswordAuthenticationToken {

    private Integer userId;

    public LogItAuthentication(Integer userId, Object principal, Object credentials) {
        super(principal, credentials);
        this.userId = userId;
    }

    public LogItAuthentication(Integer userId, Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities) {
        super(principal, credentials, authorities);
        this.userId = userId;
    }

    public Integer getUserId() {
        return userId;
    }

    @Override
    public boolean equals(Object object) {
        if (!super.equals(object)) {
            return false;
        }
        LogItAuthentication authentication = (LogItAuthentication) object;
        return Objects.equals(userId, authentication.getUserId());
    }

    @Override
    public int hashCode() {
        return super.hashCode() + Objects.hash(userId);
    }
}
