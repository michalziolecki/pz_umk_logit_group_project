package pl.zespolowka.logit.configuration.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import pl.zespolowka.logit.configuration.security.exception.InvalidCredentialsException;
import pl.zespolowka.logit.configuration.security.exception.InvalidUserException;
import pl.zespolowka.logit.configuration.security.service.*;
import pl.zespolowka.logit.configuration.security.transfer.AuthenticationDTO;
import pl.zespolowka.logit.configuration.security.transfer.CredentialsDTO;
import pl.zespolowka.logit.configuration.security.transfer.TokenRefreshDTO;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.repository.WebserviceUserRepository;
import pl.zespolowka.logit.service.*;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

    private static final String INVALID_CREDENTIALS = "Invalid credentials – Login denied.";
    private static final String USER_DOES_NOT_EXIST_IN_THE_SYSTEM = "User does not exist or is inactive in the system.";
    private static final String INVALID_REFRESH_TOKEN_MESSAGE = "Requested role and/or department context is not available";

    @Autowired
    private SecurityContextService securityContextService;

    @Autowired
    private WebserviceUserService webserviceUserService;

    @Autowired
    private RoleDepartmentService roleDepartmentService;

    @Autowired
    private WebserviceUserRepository webserviceUserRepository;

    @Autowired
    private TokenService tokenService;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private RoleService roleService;

    @Autowired
    private RoleAssignmentService roleAssignmentService;

    @Autowired
    private DepartmentService departmentService;

    @Override
    @Transactional
    public AuthenticationDTO authenticate(CredentialsDTO credentials) throws InvalidUserException {
        String username = credentials.getUsername();
        String password = credentials.getPassword();
        if (StringUtils.isEmpty(username) || StringUtils.isEmpty(password)) {
            throw new InvalidCredentialsException(INVALID_CREDENTIALS);
        }

        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        try {
            authenticationManager.authenticate(token);
        } catch (AuthenticationException ex) {
            throw new InvalidCredentialsException(INVALID_CREDENTIALS, ex);
        }

        securityContextService.injectTemporaryTokenIntoSecurityContextFor(username);

        WebserviceUser webserviceUser = webserviceUserService.createWebserviceUserAccountIfDoesNotExists(username);
//        if (webserviceUser.getRoleAssignments().isEmpty()) {
//            throw new InvalidUserException(USER_DOES_NOT_EXIST_IN_THE_SYSTEM);
//        }

        Department department = departmentService.findById(credentials.getDepartmentId());
        List<RoleAssignment> roles = roleAssignmentService.getUserRolesForDepartment(webserviceUser, department.getId());
        webserviceUserRepository.save(webserviceUser);
        AuthenticationDTO token1 = createToken(webserviceUser, roles, department);
        return token1;
    }

    @Override
    public AuthenticationDTO refreshToken(TokenRefreshDTO refreshDto) {
        String username = tokenService.parseToken(refreshDto.getToken()).getSubject();
        WebserviceUser webserviceUser = webserviceUserService.getWebserviceUserByUsername(username);

        List<RoleAssignment> roles = roleAssignmentService.getUserRolesForDepartment(webserviceUser, refreshDto.getDepartmentId());
        Department department = departmentService.findById(refreshDto.getDepartmentId());

        String token = tokenService.generateToken(webserviceUser, roles, department);

        long validityPeriod = tokenService.getValidityPeriodInMillis();
        return new AuthenticationDTO(token, validityPeriod);
    }

    private AuthenticationDTO createToken(WebserviceUser webserviceUser, List<RoleAssignment> roleAssignments, Department department) {
        String token;

        token = tokenService.generateToken(webserviceUser, roleAssignments, department);

        long validityPeriod = tokenService.getValidityPeriodInMillis();
        return new AuthenticationDTO(token, validityPeriod);
    }
}
