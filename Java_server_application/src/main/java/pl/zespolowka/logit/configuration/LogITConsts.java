package pl.zespolowka.logit.configuration;

public class LogITConsts {
    public static final String API_PATH = "/api";
    public static final String APPLICATION_JSON = "application/json";
    public static final String IN = "IN";
    public static final String OUT = "OUT";

}
