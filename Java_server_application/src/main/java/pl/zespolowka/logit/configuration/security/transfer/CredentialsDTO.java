package pl.zespolowka.logit.configuration.security.transfer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import pl.zespolowka.logit.domain.model.Department;

@AllArgsConstructor
@Getter
@Setter
public class CredentialsDTO {
    private String username;
    private String password;
    private int departmentId;
}
