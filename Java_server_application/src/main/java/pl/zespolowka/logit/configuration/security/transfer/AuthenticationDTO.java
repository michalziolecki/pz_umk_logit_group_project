package pl.zespolowka.logit.configuration.security.transfer;

public class AuthenticationDTO {
    private String token;
    private long validityPeriodInMillis;

    public AuthenticationDTO(String token, long validityPeriodInMillis) {
        this.token = token;
        this.validityPeriodInMillis = validityPeriodInMillis;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public long getValidityPeriodInMillis() {
        return validityPeriodInMillis;
    }

    public void setValidityPeriodInMillis(long validityPeriodInMillis) {
        this.validityPeriodInMillis = validityPeriodInMillis;
    }
}
