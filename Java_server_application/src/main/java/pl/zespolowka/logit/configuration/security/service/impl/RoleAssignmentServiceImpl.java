package pl.zespolowka.logit.configuration.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.security.service.RoleAssignmentService;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.repository.DepartmentRepository;
import pl.zespolowka.logit.repository.RoleAssignmentRepository;
import pl.zespolowka.logit.repository.RoleDepartmentRepository;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class RoleAssignmentServiceImpl implements RoleAssignmentService {

    @Autowired
    private RoleDepartmentRepository roleDepartmentRepository;

    @Autowired
    private RoleAssignmentRepository roleAssignmentRepository;

    @Autowired
    private DepartmentRepository departmentRepository;


    @Override
    public List<RoleAssignment> getRoles(WebserviceUser webserviceUser) {
        return roleAssignmentRepository.findByWebserviceUser(webserviceUser);
    }


    @Override
    public List<RoleAssignment> getUserRolesForDepartment(WebserviceUser webserviceUser, int departmentId) {
        List<RoleAssignment> departmentRoleAssignment = new ArrayList<>();
        Department department = departmentRepository.findById(departmentId).orElseThrow(() -> new EntityNotFoundException("Department not found"));
        List<RoleAssignment> roleAssignments = roleAssignmentRepository.findByWebserviceUser(webserviceUser);
        roleDepartmentRepository.findByDepartmentAndRoleAssignmentIn(department, roleAssignments).forEach(roleDepartment -> departmentRoleAssignment.add(roleDepartment.getRoleAssignment()));
        return departmentRoleAssignment;
    }
}
