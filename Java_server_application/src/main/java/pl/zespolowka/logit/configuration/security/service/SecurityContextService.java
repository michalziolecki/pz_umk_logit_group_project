package pl.zespolowka.logit.configuration.security.service;

import pl.zespolowka.logit.configuration.security.LogItAuthentication;
import pl.zespolowka.logit.configuration.security.transfer.CurrentContext;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.enumerate.RoleEnum;

public interface SecurityContextService {

    WebserviceUser getCurrentWebserviceUser();

    String getUsername();

    LogItAuthentication getAuthentication();

    void injectTemporaryTokenIntoSecurityContextFor(String username);

    CurrentContext getCurrentContext();
}