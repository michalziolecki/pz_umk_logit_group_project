package pl.zespolowka.logit.configuration.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.security.service.RoleService;
import pl.zespolowka.logit.domain.dto.RoleDTO;
import pl.zespolowka.logit.domain.mapper.RoleMapper;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.repository.RoleRepository;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public List<RoleDTO> getRoles() {
        List<Role> roles = roleRepository.findAll();
        return roles
                .stream()
                .map(RoleMapper.INSTANCE::map)
                .collect(Collectors.toList());
    }

    @Override
    public Role findById(int id) {
        return roleRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Role not found"));
    }

    @Override
    public Role findByCode(String code) {
        return roleRepository.findByCode(code);
    }

    @Override
    public RoleDTO create(RoleDTO roleDTO) {
        Role role = RoleMapper.INSTANCE.map(roleDTO);
        roleRepository.save(role);
        return roleDTO;
    }
}
