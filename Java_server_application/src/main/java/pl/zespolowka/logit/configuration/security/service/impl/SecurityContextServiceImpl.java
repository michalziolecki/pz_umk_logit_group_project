package pl.zespolowka.logit.configuration.security.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.CustomUserDetails;
import pl.zespolowka.logit.configuration.security.LogItAuthentication;
import pl.zespolowka.logit.configuration.security.exception.InvalidContextException;
import pl.zespolowka.logit.configuration.security.service.SecurityContextService;
import pl.zespolowka.logit.configuration.security.transfer.CurrentContext;
import pl.zespolowka.logit.domain.model.Department;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.enumerate.RoleEnum;
import pl.zespolowka.logit.service.DepartmentService;
import pl.zespolowka.logit.service.WebserviceUserService;

import java.util.Collections;
import java.util.List;

import static java.util.Objects.nonNull;
import static java.util.Objects.requireNonNull;

@Service
public class SecurityContextServiceImpl implements SecurityContextService {

    @Autowired
    private WebserviceUserService webserviceUserService;
    @Autowired
    private DepartmentService departamentService;

    private static final int INVALID_USER_ID = -1;
    private static final String NO_CONTEXT_ROLE = "No context role set";

    @Override
    public WebserviceUser getCurrentWebserviceUser() {
        return getWebserviceUser(getAuthentication());
    }

    @Override
    public String getUsername() {
        return getCustomUserDetails().getUsername();
    }

    @Override
    public LogItAuthentication getAuthentication() {
        return (LogItAuthentication) SecurityContextHolder.getContext().getAuthentication();
    }

    @Override
    public void injectTemporaryTokenIntoSecurityContextFor(String username) {
        CustomUserDetails customUserDetails = new CustomUserDetails(username, Collections.emptyList());
        prepareAndInjectToken(customUserDetails);
    }

    @Override
    public CurrentContext getCurrentContext() {
        LogItAuthentication authentication = getAuthentication();
        CustomUserDetails customUserDetails = (CustomUserDetails) authentication.getPrincipal();
        RoleEnum role = getUserRole(customUserDetails);
        WebserviceUser webserviceUser = getWebserviceUser(authentication);
        String username = customUserDetails.getUsername();
        Department department = getDepartment(customUserDetails);
        return new CurrentContext(role, webserviceUser, username, department);
    }

    private void prepareAndInjectToken(CustomUserDetails customUserDetails) {
        UsernamePasswordAuthenticationToken token = new LogItAuthentication(INVALID_USER_ID, customUserDetails, null, customUserDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    private CustomUserDetails getCustomUserDetails() {
        return (CustomUserDetails) getAuthentication().getPrincipal();
    }

    private WebserviceUser getWebserviceUser(LogItAuthentication authentication) {
        return nonNull(authentication) ? webserviceUserService.findById(authentication.getUserId()) : null;
    }

    private RoleEnum getUserRole(CustomUserDetails customUserDetails) {
        List<Role> roles = (List<Role>) customUserDetails.getAuthorities();
        if (roles.isEmpty()) {
            throw new InvalidContextException(NO_CONTEXT_ROLE);
        }
        return RoleEnum.valueOf(roles.get(0).getCode());
    }

    private Department getDepartment(CustomUserDetails customUserDetails){
        return nonNull(customUserDetails.getDepartment()) ? departamentService.findById(customUserDetails.getDepartment().getId()) : null;
    }
}
