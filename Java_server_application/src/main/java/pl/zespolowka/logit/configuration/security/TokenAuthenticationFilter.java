package pl.zespolowka.logit.configuration.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.util.StringUtils;
import pl.zespolowka.logit.configuration.CustomUserDetails;
import pl.zespolowka.logit.configuration.security.service.TokenService;
import pl.zespolowka.logit.domain.model.Role;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TokenAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private static final String X_AUTH_TOKEN_HEADER_NAME = "X-Auth-Token";
    private static final String TOKEN_EXPIRED_ERROR_MESSAGE = "Token Expired.";
    private static final String INVALID_TOKEN_ERROR_MESSAGE = "Invalid Token.";

    @Autowired
    TokenService tokenService;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            String authToken = httpServletRequest.getHeader(X_AUTH_TOKEN_HEADER_NAME);
            if (StringUtils.hasText(authToken)) {
                Claims userTokenInfo;
                try {
                    userTokenInfo = this.tokenService.parseToken(authToken);
                } catch (ExpiredJwtException ex) {
                    sendError(response, TOKEN_EXPIRED_ERROR_MESSAGE);
                    return;
                } catch (JwtException jex) {
                    sendError(response, INVALID_TOKEN_ERROR_MESSAGE);
                    return;
                }
                List<HashMap<String, Object>> roleList = (List<HashMap<String, Object>>) userTokenInfo.get("roles");
                List<GrantedAuthority> grantedAuthorities = parseRoles(roleList);
                Integer userId = parseUserId(userTokenInfo.get("userId"));
                CustomUserDetails customUserDetails = new CustomUserDetails(userTokenInfo.getSubject(), grantedAuthorities);

                if (this.tokenService.validateExpirationDate(userTokenInfo.getExpiration())) {
                    UsernamePasswordAuthenticationToken token = new LogItAuthentication(userId, customUserDetails,null, customUserDetails.getAuthorities());
                    SecurityContextHolder.getContext().setAuthentication(token);
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    private static void sendError(ServletResponse response, String invalidTokenErrorMessage) throws IOException {
        if (response instanceof HttpServletResponse) {
            final HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper((HttpServletResponse) response);
            wrapper.sendError(HttpServletResponse.SC_UNAUTHORIZED, invalidTokenErrorMessage);
        }
    }

    private Integer parseUserId(Object userId) {
        return (Integer) userId;
    }

    private static List<GrantedAuthority> parseRoles(List<HashMap<String, Object>> roles) {
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        roles.forEach(r -> {
            Role role = new Role();
            role.setId(((Integer) r.get("id")));
            role.setCode((String) r.get("code"));
            role.setName((String) r.get("name"));
            grantedAuthorities.add(role);
        });
        return grantedAuthorities;
    }
}
