package pl.zespolowka.logit.configuration.security.service;


import pl.zespolowka.logit.domain.dto.RoleDTO;
import pl.zespolowka.logit.domain.model.Role;

import java.util.List;

public interface RoleService {
    List<RoleDTO> getRoles();

    Role findById(int id);

    Role findByCode(String code);

    RoleDTO create(RoleDTO roleDTO);
}
