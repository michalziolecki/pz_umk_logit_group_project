package pl.zespolowka.logit.configuration.security.exception;

public class InvalidContextException extends RuntimeException {
    public InvalidContextException() {
    }

    public InvalidContextException(String message) {
        super(message);
    }
}
