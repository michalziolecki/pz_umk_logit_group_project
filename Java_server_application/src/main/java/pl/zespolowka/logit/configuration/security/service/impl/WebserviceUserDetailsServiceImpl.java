package pl.zespolowka.logit.configuration.security.service.impl;

import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import pl.zespolowka.logit.configuration.CustomUserDetails;
import pl.zespolowka.logit.domain.model.Role;
import pl.zespolowka.logit.domain.model.RoleAssignment;
import pl.zespolowka.logit.domain.model.WebserviceUser;
import pl.zespolowka.logit.repository.RoleAssignmentRepository;
import pl.zespolowka.logit.repository.WebserviceUserRepository;

import java.util.ArrayList;
import java.util.List;

import static java.util.Objects.isNull;
import static java.util.Objects.requireNonNull;

@Service
public class WebserviceUserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private WebserviceUserRepository webserviceUserRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        WebserviceUser webserviceUser = webserviceUserRepository.findByUsernameIgnoreCase(username);
        if (isNull(webserviceUser)) {
            throw new UsernameNotFoundException(username);
        }
        return new CustomUserDetails(username, webserviceUser.getPassword(), getAuthorities(new ArrayList<>()));
    }

    private static List<GrantedAuthority> getAuthorities (List<String> roles) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String role : roles) {
            authorities.add(new SimpleGrantedAuthority(role));
        }
        return authorities;
    }
}
