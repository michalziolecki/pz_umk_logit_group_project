package pl.zespolowka.logit.configuration;

import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import pl.zespolowka.logit.domain.dto.DepartmentDTO;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

@NoArgsConstructor
public class CustomUserDetails implements UserDetails {

    private String username;
    private String password;
    private List<GrantedAuthority> authorities;
    private DepartmentDTO department;
    private boolean credentialsExpired;
    private boolean accountExpired;
    private boolean accountLocked;
    private boolean enabled = true;

    public CustomUserDetails(String username, List<GrantedAuthority> roles) {
        this.username = username;
        this.authorities = Collections.unmodifiableList(roles);
    }

    public CustomUserDetails(String username) {
        this.username = username;
    }

    public CustomUserDetails(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public CustomUserDetails(String username, String password, List<GrantedAuthority> authorities) {
        this.username = username;
        this.password = password;
        this.authorities = authorities;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setAuthorities(List<GrantedAuthority> authorities) {
        this.authorities = Collections.unmodifiableList(authorities);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.unmodifiableList(authorities);
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return !accountExpired;
    }

    @Override
    public boolean isAccountNonLocked() {
        return !accountLocked;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return !credentialsExpired;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public DepartmentDTO getDepartment() {
        return department;
    }

    public void setDepartment(DepartmentDTO department) {
        this.department = department;
    }
}
