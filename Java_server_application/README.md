# Java Server module
    
konekty z przodem aplikacji

## Getting Started

zanim cokolwiek zrobimy trzeba ustawic polaczenie z baza danych
(jeżeli zakladamy ze narazie kazdy ma swoja lokalnie) 

w pliku application.properties ustawiamy bazę

```
# Datasource
spring.datasource.url=jdbc:postgresql://{adres bazy}
spring.datasource.username={uzytkownik}
spring.datasource.password={haslo}
```
apke uruchamiamy z lini komend
```
mvn spring-boot:run
```
w testowych insertach do bazy danych zawarci są uzytkownicy webserwisu z zahashowanymi haslami kolejno:

```
login: admin
password: admin

login: manager
passowrd: manager
```

wiekszosc endpointow jest zabezpieczone dla admina lub admina i managera za wyjatkiem:

```
# endpointow swaggera
# /departaments
# /login
# /refresh
```

aby korzytać z reszty endpointów musimy korzystać z tokena umieszczonego w headerze

przykładowo:

```
X-Auth-Token:eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJzem9wYSIsImV4cCI6MTU1MTQwNjcxNCwidXNlcklkIjo4LCJkZXBhcnRtZW50Ijp7ImlkIjoxLCJuYW1lIjoibWFya2V0aW5nIiwiZGVzY3JpcHRpb24iOiJvcGlzIG9kZHppYWx1In0sInJvbGVzIjpbeyJpZCI6MSwibmFtZSI6IkFETUlOIiwiY29kZSI6IjEiLCJhdXRob3JpdHkiOiIxIn1dfQ.TUd1GqNa-IxxojzevQYU_SwvAotL53CfqR6FgRHEK4kFVALkjHFnvIbUi22ybHj6LlBnl0g2-7txsCCH0w1M4A
```

aby go uzyskać trzeba uderzyć do endpointu ```[POST] /login``` podając w body login i haslo użytkownika oraz departament do ktorego sie chcemy zalogowac(narazie tylko Marketing):
```
{
  "username": "admin",
  "password": "admin",
  "departamentId": 1
}
```

w zwrotce otrzymamy body z tokenem i jego czasem wygasniecia w milisekundach:
```
{
  "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTU1MjI0MTI2MSwidXNlcklkIjoxLCJkZXBhcnRtZW50Ijp7ImlkIjoxLCJuYW1lIjoiTWFya2V0aW5nIiwiZGVzY3JpcHRpb24iOiJvcGlzIn0sInJvbGVzIjpbeyJpZCI6MSwibmFtZSI6IkFETUlOIiwiY29kZSI6IjEiLCJhdXRob3JpdHkiOiIxIn0seyJpZCI6MiwibmFtZSI6Ik1BTkFHRVIiLCJjb2RlIjoiMiIsImF1dGhvcml0eSI6IjIifV19.-t98OQV13qD8TX3xJ7wqSfBgBnEij1aMOczMpYDqe1gWlGAKxfAZf4EP12j3sETJev808qD-HVSPKfWmgEQmZg",
  "validityPeriodInMillis": 3600000
}
```

wtedy można zadawać pytania do endpointów

dostępne endpointy można podejrzeć przez swaggera

wpisz w przeglądarce poniższy adres:

```
http://localhost:8080/api/swagger
```


