var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Home2Component } from './home2/home2.component';
import { DetailsComponent } from './details/details.component';
import { UsbComponent } from './usb/usb.component';
import { GeneralDataComponent } from './general-data/general-data.component'; // <-Add here
import { NetSettingsComponent } from './net-settings/net-settings.component';
import { MessagesComponent } from './messages/messages.component';
import { KeyloggerComponent } from './keylogger/keylogger.component';
import { FiltersComponent } from './filters/filters.component';
import { NetworkComponent } from './network/network.component';
import { NetDataComponent } from './net-data/net-data.component';
import { LoginComponent } from './login/login.component';
import { ProcessesComponent } from './processes/processes.component';
import { SecurityComponent } from './security/security.component';
var routes = [
    {
        path: '',
        component: LoginComponent
    },
    {
        path: 'home2',
        component: Home2Component
    },
    {
        path: 'details/:id',
        component: DetailsComponent
    },
    {
        path: 'usb/:id',
        component: UsbComponent
    },
    {
        path: 'general-data/:id',
        component: GeneralDataComponent
    },
    {
        path: 'net-settings/:id',
        component: NetSettingsComponent
    },
    {
        path: 'keylogger/:id',
        component: KeyloggerComponent
    },
    {
        path: 'messages/:id',
        component: MessagesComponent
    },
    {
        path: 'filters/:id',
        component: FiltersComponent
    },
    {
        path: 'network/:id',
        component: NetworkComponent
    },
    {
        path: 'net-data/:id',
        component: NetDataComponent
    },
    {
        path: 'processes/:id',
        component: ProcessesComponent
    },
    {
        path: 'security/:id',
        component: SecurityComponent
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());
export { AppRoutingModule };
//# sourceMappingURL=app-routing.module.js.map