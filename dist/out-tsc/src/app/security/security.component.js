var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
var SecurityComponent = /** @class */ (function () {
    function SecurityComponent(route, data, location) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.title = "Uprawnienia";
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    SecurityComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    SecurityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.subscription = timer(0, 7000).pipe(switchMap(function () { return _this.data.getMessages(_this.userId); })).subscribe(function (data) { return _this.messageDatas = data; });
    };
    SecurityComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    SecurityComponent.prototype.logout = function () {
        console.log("wylogowanie");
    };
    SecurityComponent = __decorate([
        Component({
            selector: 'app-security',
            templateUrl: './security.component.html',
            styleUrls: ['./security.component.css', '../app.component.css'],
            animations: [
                trigger('listStagger', [
                    transition('* <=> *', [
                        query(':enter', [
                            style({ opacity: 0, transform: 'translateY(-15px)' }),
                            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        query(':leave', animate('50ms', style({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [ActivatedRoute, DataService, Location])
    ], SecurityComponent);
    return SecurityComponent;
}());
export { SecurityComponent };
//# sourceMappingURL=security.component.js.map