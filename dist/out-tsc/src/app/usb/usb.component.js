var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
var UsbComponent = /** @class */ (function () {
    function UsbComponent(route, data, location) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.title = "Urządzenia i pamięć";
        this.is1 = true;
        this.is2 = true;
        this.is3 = true;
        this.is4 = true;
        this.is5 = true;
        this.freq = 1;
        this.bookmark = 1; // 1, 2 or 3
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    UsbComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        //  this.subscription = timer(0, 7000).pipe(
        //    switchMap(() => this.data.getMessages(this.userId))
        //  ).subscribe(data => this.messageDatas = data);
    };
    UsbComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    //  ngOnDestroy() {
    //    this.subscription.unsubscribe();
    //  }
    UsbComponent.prototype.logout = function () {
        console.log("wylogowanie");
    };
    UsbComponent.prototype.change1 = function () { this.is1 = !this.is1; };
    UsbComponent.prototype.change2 = function () { this.is2 = !this.is2; };
    UsbComponent.prototype.change3 = function () { this.is3 = !this.is3; };
    UsbComponent.prototype.change4 = function () { this.is4 = !this.is4; };
    UsbComponent.prototype.change5 = function () { this.is5 = !this.is4; };
    UsbComponent.prototype.changeBookmark = function (nr) {
        this.bookmark = nr;
    };
    UsbComponent.prototype.freqM = function () { };
    ;
    UsbComponent = __decorate([
        Component({
            selector: 'app-usb',
            templateUrl: './usb.component.html',
            styleUrls: ['./usb.component.css', '../app.component.css'],
            animations: [
                trigger('listStagger', [
                    transition('* <=> *', [
                        query(':enter', [
                            style({ opacity: 0, transform: 'translateY(-15px)' }),
                            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        query(':leave', animate('50ms', style({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [ActivatedRoute, DataService, Location])
    ], UsbComponent);
    return UsbComponent;
}());
export { UsbComponent };
//# sourceMappingURL=usb.component.js.map