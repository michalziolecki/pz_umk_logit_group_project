var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
var Home2Component = /** @class */ (function () {
    // constructor(private router: Router,public authService: AuthService) { }
    // ngOnInit() {
    //   this.id = localStorage.getItem('token');
    // }
    function Home2Component(data, router, authService) {
        this.data = data;
        this.router = router;
        this.authService = authService;
        this.title = 'LogIT';
        this.p = 1;
    }
    Home2Component.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUsers().subscribe(function (data) { return _this.users$ = data; });
        this.id = localStorage.getItem('token');
    };
    Home2Component.prototype.logout = function () {
        console.log("Logout");
        this.authService.logout();
        this.router.navigate(['/login']);
    };
    Home2Component = __decorate([
        Component({
            selector: 'app-home2',
            templateUrl: './home2.component.html',
            styleUrls: ['./home2.component.css'],
            animations: [
                trigger('listStagger', [
                    transition('* <=> *', [
                        query(':enter', [
                            style({ opacity: 0, transform: 'translateY(-15px)' }),
                            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        query(':leave', animate('50ms', style({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [DataService, Router, AuthService])
    ], Home2Component);
    return Home2Component;
}());
export { Home2Component };
//# sourceMappingURL=home2.component.js.map