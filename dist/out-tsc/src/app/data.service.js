var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
    }
    // endpoint = 'http://localhost:8080/api/usb-ports/<id użytkownika>';
    // httpOptions = 
    // {
    //   headers: new HttpHeaders
    //   ({
    //     'Content-Type':  'application/json'
    //   })
    // };
    DataService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    DataService.prototype.getUser = function (userId) {
        return this.http.get('http://localhost:8080/api/keylogger-users/' + userId);
        // return this.http.get('https://jsonplaceholder.typicode.com/users/'+userId)
    };
    DataService.prototype.getPosts = function () {
        return this.http.get('https://jsonplaceholder.typicode.com/posts');
    };
    DataService.prototype.getUsers = function () {
        return this.http.get('http://localhost:8080/api/keylogger-users');
        // return this.http.get('https://jsonplaceholder.typicode.com/users')
    };
    DataService.prototype.getPorts = function (userId) {
        // getPorts(idU): Observable<any> {
        return this.http.get('http://localhost:8080/api/usb-ports/' + userId);
    };
    DataService.prototype.getInternetSettings = function (userId) {
        return this.http.get('http://localhost:8080/api/internet-settings/' + userId);
    };
    DataService.prototype.getTransfer = function (userId) {
        return this.http.get('http://localhost:8080//api/current-transfer/' + userId);
    };
    DataService.prototype.getNetworkCard = function (userId) {
        return this.http.get('http://localhost:8080//api/networkcard-info/' + userId);
    };
    DataService.prototype.getMessages = function (userId) {
        return this.http.get('http://localhost:8080/api/messages/' + userId);
    };
    DataService.prototype.getFilters = function (userId) {
        return this.http.get('http://localhost:8080/api/keylogger-filter/' + userId);
    };
    // getFilters(userId) {
    //   return timer(0, 10000)
    //       .pipe(
    //          switchMap(_ => this.http.get('http://localhost:8080/api/keylogger-filter/' + userId)),
    //          catchError(error => of(`Bad request: ${error}`))
    //       );
    // }
    //-----------------------------------------------------------------------------------------------
    // postFilter() {
    //   return this.http.post('http://localhost:8080//api/keylogger-filter');
    // }
    // postFilter() 
    // {
    //   return this.http.post('http://localhost:8080//api/keylogger-filter',
    //      {
    //         filter: "dm",
    //         // id: 2, 
    //         keyloggerUserId: 2
    //      }
    //   );
    // }
    DataService.prototype.postFilter = function (word) {
        var body = "filter=" + word;
        this.http.post("http://localhost:8080/user/all", body).subscribe(function (data) { });
    };
    // deleteFilter() {
    //   return this.http.delete('http://localhost:8080//api/keylogger-filter');
    // }
    DataService.prototype.getKeyloggerSettings = function (userId) {
        return this.http.get('http://localhost:8080/api/keylogger-settings/' + userId);
    };
    DataService = __decorate([
        Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [HttpClient])
    ], DataService);
    return DataService;
}());
export { DataService };
//# sourceMappingURL=data.service.js.map