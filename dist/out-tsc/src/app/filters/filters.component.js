var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// import {map} from 'rxjs/add/operator/map';
// import { map } from "rxjs/operators";
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
var FiltersComponent = /** @class */ (function () {
    function FiltersComponent(http, route, data, location) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.title = 'Ustawienia filtrów';
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    FiltersComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    FiltersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        // this.data.getFilters(this.user$).subscribe(
        //   // data => this.messages$ = data
        //   data => this.filters = data
        //   // data => console.log(data)
        // );  
        // this.subscription = timer(0, 10000).pipe(
        //   switchMap(() => this.data.getFilters(this.user$))
        // ).subscribe(data => this.filters = data);
        this.subscription = timer(0, 2000).pipe(switchMap(function () { return _this.data.getFilters(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.filters = data; });
        // this.refreshData();
        // this.interval = setInterval(() => {
        //     this.refreshData();
        // }, 5000);
    };
    FiltersComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    //   refreshData(){
    //     this.data.getFilters(this.user$).subscribe(
    //       // data => this.messages$ = data
    //       data => this.filters = data
    //       // data => console.log(data)
    //     );
    // }
    // public data$: BehaviorSubject<any> = new BehaviorSubject({});
    // updateData() {
    //     let data = this.data.getFilters(this.user$).map((data)=>{
    //         return data.json();
    //     }).do((data)=>{
    //         this.data$.next(data);
    //     })
    // }
    FiltersComponent.prototype.postFilter = function (idUser) {
        var headers = new HttpHeaders()
            .set('Content-Type', 'application/json');
        this.http.post('http://localhost:8080/api/keylogger-filter', {
            'filter': this.newFiltr,
            'keyloggerUserId': idUser
        }, { headers: headers })
            .subscribe(function (data) {
            console.log('POST Request is successful ', data);
        }, function (error) {
            console.log('Error in post request Iwona :(', error);
        });
        this.newFiltr = '';
    };
    FiltersComponent.prototype.deleteFilter = function (id) {
        var filterId = id;
        var headers = new HttpHeaders()
            .set('Content-Type', 'application/json');
        this.http.delete('http://localhost:8080/api/keylogger-filter/' + filterId, { headers: headers })
            .subscribe(function (data) {
            console.log('DELETE Request is successful ');
        }, function (error) {
            console.log('Error in delete request Iwona :(', error);
        });
        // let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        // // let options = new RequestOptions({ headers: cpHeaders });
        // return this.http.delete('http://localhost:8080/api/keylogger-filter')
    };
    FiltersComponent.prototype.save = function () {
        this.oldFiltr = this.newFiltr;
        this.newFiltr = '';
    };
    FiltersComponent.prototype.logout = function () {
        console.log("wylogowanie");
    };
    FiltersComponent = __decorate([
        Component({
            selector: 'app-filters',
            templateUrl: './filters.component.html',
            styleUrls: ['./filters.component.css', '../app.component.css'],
            animations: [
                trigger('listStagger', [
                    transition('* <=> *', [
                        query(':enter', [
                            style({ opacity: 0, transform: 'translateY(-15px)' }),
                            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        query(':leave', animate('50ms', style({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [HttpClient, ActivatedRoute, DataService, Location])
    ], FiltersComponent);
    return FiltersComponent;
}());
export { FiltersComponent };
//# sourceMappingURL=filters.component.js.map