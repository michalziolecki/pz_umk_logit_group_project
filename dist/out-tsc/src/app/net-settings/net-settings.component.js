var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Component } from '@angular/core';
import { DataService } from '../data.service';
import { Location } from '@angular/common';
import { ActivatedRoute } from "@angular/router";
import { trigger, style, transition, animate, query, stagger } from '@angular/animations';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
var NetSettingsComponent = /** @class */ (function () {
    function NetSettingsComponent(http, route, data, location) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.title = 'Ustawienia internetowe';
        this.timeCurrentTransfer = 1;
        this.is1 = true;
        this.is2 = true;
        this.is3 = true;
        this.is4 = true;
        this.is5 = true;
        this.is6 = true;
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    NetSettingsComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    NetSettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.subscription = timer(0, 2000).pipe(switchMap(function () { return _this.data.getInternetSettings(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.settings$ = data; });
        // this.refreshData();
        // this.interval = setInterval(() => {
        //     this.refreshData();
        // }, 5000);
    };
    NetSettingsComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    // this.data.getInternetSettings(this.user$).subscribe( 
    //   data => this.settings$ = data
    //   // data => console.log(data)
    // );
    NetSettingsComponent.prototype.change1 = function () { this.is1 = !this.is1; };
    NetSettingsComponent.prototype.change2 = function () { this.is2 = !this.is2; };
    NetSettingsComponent.prototype.change3 = function () { this.is3 = !this.is3; };
    NetSettingsComponent.prototype.change4 = function () { this.is4 = !this.is4; };
    NetSettingsComponent.prototype.change5 = function () { this.is5 = !this.is5; };
    NetSettingsComponent.prototype.change6 = function () { this.is6 = !this.is6; };
    NetSettingsComponent.prototype.updateCurrentTransferMonitorStatus = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-current-transfer-monitor-status', {
            "isCurrentTransferMonitored": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCurrentTransferMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCurrentTransferMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateCurrentTransferFrequency = function (settingsId, timeCurrentTransfer) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/current-transfer-monitor-frequency/' + timeCurrentTransfer, {})
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCurrentTransferFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCurrentTransferFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateUploadSpeedMonitorStatus = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-upload-speed-monitor-status', {
            "isUploadSpeedMonitored": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateUploadSpeedMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateUploadSpeedMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateDownloadSpeedMonitorStatus = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-download-speed-monitor-status', {
            "isDownloadSpeedMonitored": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateDownloadSpeedMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateDownloadSpeedMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateSinceStartedTransferMonitorStatus = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-transfer-monitor-since-computer-started-status', {
            "isTransferMonitoredSinceComputerStarted": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateSinceStartedTransferMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateSinceStartedTransferMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateSinceStartTransferFrequency = function (settingsId, timeStartTransfer) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/transfer-monitor-since-computer-started-frequency/' + timeStartTransfer, {})
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateSinceStartTransferFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateSinceStartTransferFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateShouldPing = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/should-ping', {
            "shouldPing": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateShouldPing", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateShouldPing", error);
        });
    };
    NetSettingsComponent.prototype.updatePingFrequency = function (settingsId, timePing) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/ping-frequency/' + timePing, {})
            .subscribe(function (data) {
            console.log("PUT Request is successful - updatePingFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updatePingFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateNetworkCardMonitorStatus = function (settingsId, is) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-network-card-monitor-status', {
            "isNetworkCardInfoMonitor": !is
        })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateNetworkCardMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateNetworkCardMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateCardFrequency = function (settingsId, timeCard) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/network-card-monitor-frequency/' + timeCard, {})
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCardFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCardFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updatePingAddress = function (settingsId, address) {
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/ping-address/' + address, {})
            .subscribe(function (data) {
            console.log("PUT Request is successful - updatePingAddress", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updatePingAddress", error);
        });
    };
    NetSettingsComponent.prototype.logout = function () {
        console.log("wylogowanie");
    };
    NetSettingsComponent = __decorate([
        Component({
            selector: 'app-net-settings',
            templateUrl: './net-settings.component.html',
            styleUrls: ['./net-settings.component.css', '../app.component.css'],
            animations: [
                trigger('listStagger', [
                    transition('* <=> *', [
                        query(':enter', [
                            style({ opacity: 0, transform: 'translateY(-15px)' }),
                            stagger('50ms', animate('550ms ease-out', style({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        query(':leave', animate('50ms', style({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [HttpClient, ActivatedRoute, DataService, Location])
    ], NetSettingsComponent);
    return NetSettingsComponent;
}());
export { NetSettingsComponent };
//# sourceMappingURL=net-settings.component.js.map