(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/_services/authentication.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/_services/authentication.service.ts ***!
  \*****************************************************/
/*! exports provided: AuthenticationService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthenticationService", function() { return AuthenticationService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(http, router) {
        this.http = http;
        this.router = router;
    }
    AuthenticationService.prototype.login = function (username, password, departmentId) {
        var _this = this;
        return this.http.post("http://localhost:8080/api/login", { username: username, password: password, departmentId: departmentId })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["map"])(function (user) {
            // login successful if there's a jwt token in the response
            if (user && user.token) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('token', user.token);
                localStorage.setItem('username', username);
                console.log("Dostalismy tokena");
                _this.router.navigate(['/home2']);
            }
            else {
                console.log("Nie dostalismy tokena");
            }
            return user;
        }));
    };
    AuthenticationService.prototype.logout = function () {
        // remove user from local storage to log user out
        // console.log(localStorage.getItem('currentUser'));
        localStorage.removeItem('token');
        console.log("token po usunieciu");
        console.log(localStorage.getItem('token'));
        this.router.navigate(['']);
    };
    AuthenticationService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({ providedIn: 'root' }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], AuthenticationService);
    return AuthenticationService;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home2_home2_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home2/home2.component */ "./src/app/home2/home2.component.ts");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _usb_usb_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./usb/usb.component */ "./src/app/usb/usb.component.ts");
/* harmony import */ var _general_data_general_data_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./general-data/general-data.component */ "./src/app/general-data/general-data.component.ts");
/* harmony import */ var _net_settings_net_settings_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./net-settings/net-settings.component */ "./src/app/net-settings/net-settings.component.ts");
/* harmony import */ var _messages_messages_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./messages/messages.component */ "./src/app/messages/messages.component.ts");
/* harmony import */ var _keylogger_keylogger_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./keylogger/keylogger.component */ "./src/app/keylogger/keylogger.component.ts");
/* harmony import */ var _filters_filters_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./filters/filters.component */ "./src/app/filters/filters.component.ts");
/* harmony import */ var _network_network_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./network/network.component */ "./src/app/network/network.component.ts");
/* harmony import */ var _net_data_net_data_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./net-data/net-data.component */ "./src/app/net-data/net-data.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _processes_processes_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./processes/processes.component */ "./src/app/processes/processes.component.ts");
/* harmony import */ var _security_security_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./security/security.component */ "./src/app/security/security.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





 // <-Add here









var routes = [
    {
        path: '',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"]
    },
    {
        path: 'home2',
        component: _home2_home2_component__WEBPACK_IMPORTED_MODULE_2__["Home2Component"]
    },
    {
        path: 'details/:id',
        component: _details_details_component__WEBPACK_IMPORTED_MODULE_3__["DetailsComponent"]
    },
    {
        path: 'usb/:id',
        component: _usb_usb_component__WEBPACK_IMPORTED_MODULE_4__["UsbComponent"]
    },
    {
        path: 'general-data/:id',
        component: _general_data_general_data_component__WEBPACK_IMPORTED_MODULE_5__["GeneralDataComponent"]
    },
    {
        path: 'net-settings/:id',
        component: _net_settings_net_settings_component__WEBPACK_IMPORTED_MODULE_6__["NetSettingsComponent"]
    },
    {
        path: 'keylogger/:id',
        component: _keylogger_keylogger_component__WEBPACK_IMPORTED_MODULE_8__["KeyloggerComponent"]
    },
    {
        path: 'messages/:id',
        component: _messages_messages_component__WEBPACK_IMPORTED_MODULE_7__["MessagesComponent"]
    },
    {
        path: 'filters/:id',
        component: _filters_filters_component__WEBPACK_IMPORTED_MODULE_9__["FiltersComponent"]
    },
    {
        path: 'network/:id',
        component: _network_network_component__WEBPACK_IMPORTED_MODULE_10__["NetworkComponent"]
    },
    {
        path: 'net-data/:id',
        component: _net_data_net_data_component__WEBPACK_IMPORTED_MODULE_11__["NetDataComponent"]
    },
    {
        path: 'processes/:id',
        component: _processes_processes_component__WEBPACK_IMPORTED_MODULE_13__["ProcessesComponent"]
    },
    {
        path: 'security/:id',
        component: _security_security_component__WEBPACK_IMPORTED_MODULE_14__["SecurityComponent"]
    },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#begining\r\n{\r\n  box-shadow:inset 0 0 80px #238b31;\r\n  width: 980px;\r\n  height: 25px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  /* margin-top: 20px; */\r\n  margin-top: 10px;\r\n  margin-bottom: 20px;\r\n  padding: 5px;\r\n  font-weight: normal;\r\n  border-radius: 10px;\r\n  color: #032c09;\r\n  /* text-align: center; */\r\n  font-weight: 600;\r\n}\r\n\r\n#tracked\r\n{\r\n  /* margin-left: auto;\r\n  margin-right: auto; */\r\n  text-align: celeftnter;\r\n  float: left;\r\n  /* margin-right: 10px; */\r\n  padding-left: 10px;\r\n  width: 590px;\r\n  /* background-color: aqua; */\r\n}\r\n\r\n#tracking\r\n{\r\n  text-align: right;\r\n  float: left;\r\n  /* margin-right: 10px; */\r\n  width: 270px;\r\n  color: white;\r\n  padding-right: 5px;\r\n  /* background-color: rgb(245, 16, 149); */\r\n}\r\n\r\n#line\r\n{\r\n  text-align: center;\r\n  float: left;\r\n  width: 10px;\r\n  color: white;\r\n  /* background-color: rgb(253, 228, 2); */\r\n}\r\n\r\n.black\r\n{\r\n  background-color: #000000ce;\r\n  width:300px;\r\n  margin:auto;\r\n}\r\n\r\n#Logout\r\n{\r\n  text-align: left;\r\n  padding-left: 5px;\r\n  float: left;\r\n  width: 90px;\r\n  color: white;\r\n  /* background-color: rgb(253, 228, 2); */\r\n}\r\n\r\n#Logout:hover\r\n{\r\n  color: #032c09;\r\n  cursor: pointer;\r\n}\r\n\r\n#container1\r\n{\r\n  box-shadow:inset 0 0 80px #38e903;\r\n  width: 980px;\r\n  height: 50px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  padding: 10px;\r\n  padding-top: 0px;\r\n  border-style: outset;\r\n}\r\n\r\n#sectionTitle\r\n{\r\n  width: 900px;\r\n  letter-spacing: 2px;\r\n  float: left;\r\n  font-size: 15px;\r\n}\r\n\r\n#back\r\n{\r\n  width: 40px;\r\n  float: left;\r\n  padding-top: 10px;\r\n  font-size: 44px;\r\n}\r\n\r\n#back:hover\r\n{\r\n  opacity: 0.5;\r\n  cursor: pointer;\r\n}\r\n\r\n#home\r\n{\r\n  width: 40px;\r\n  float: left;\r\n  padding-top: 10px;\r\n  font-size: 44px;\r\n}\r\n\r\n#home:hover\r\n{\r\n  opacity: 0.5;\r\n}\r\n\r\n#container\r\n{\r\n  box-shadow:inset 0 0 300px  #6be41b;\r\n  width: 1000px;\r\n  /* height: 1000px;  tu nie okreslam, zeby moc  w kazdym komponencie osobno dopasowac ten parametr*/\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  /* padding-top: 80px; */\r\n  font-size: 24px;\r\n  font-family: 'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n#block/* filters & messages are using it*/\r\n{\r\n  /* box-shadow:inset 0 0 100px #235169; */\r\n  box-shadow:inset 0 0 300px  #6be41b;\r\n  width: 940px;\r\n  /* height: 600px; */\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 35px;\r\n  padding: 30px;\r\n  color: #110831;\r\n  font-stretch: ultra-expanded;\r\n  border-radius: 10px;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsa0NBQWtDO0VBQ2xDLGFBQWE7RUFDYixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQix1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLGlCQUFpQjtDQUNsQjs7QUFFRDs7RUFFRTt3QkFDc0I7RUFDdEIsdUJBQXVCO0VBQ3ZCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYiw2QkFBNkI7Q0FDOUI7O0FBRUQ7O0VBRUUsa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWix5QkFBeUI7RUFDekIsYUFBYTtFQUNiLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsMENBQTBDO0NBQzNDOztBQUVEOztFQUVFLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYix5Q0FBeUM7Q0FDMUM7O0FBRUQ7O0VBRUUsNEJBQTRCO0VBQzVCLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixZQUFZO0VBQ1osWUFBWTtFQUNaLGFBQWE7RUFDYix5Q0FBeUM7Q0FDMUM7O0FBRUQ7O0VBRUUsZUFBZTtFQUNmLGdCQUFnQjtDQUNqQjs7QUFFRDs7RUFFRSxrQ0FBa0M7RUFDbEMsYUFBYTtFQUNiLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLHFCQUFxQjtDQUN0Qjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLFlBQVk7RUFDWixnQkFBZ0I7Q0FDakI7O0FBQ0Q7O0VBRUUsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0NBQ2pCOztBQUNEOztFQUVFLGFBQWE7RUFDYixnQkFBZ0I7Q0FDakI7O0FBRUQ7O0VBRUUsWUFBWTtFQUNaLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCO0NBQ2pCOztBQUNEOztFQUVFLGFBQWE7Q0FDZDs7QUFFRDs7RUFFRSxvQ0FBb0M7RUFDcEMsY0FBYztFQUNkLG1HQUFtRztFQUNuRyxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHdCQUF3QjtFQUN4QixnQkFBZ0I7RUFDaEIsOEVBQThFO0VBQzlFLG9CQUFvQjtDQUNyQjs7QUFFRDs7RUFFRSx5Q0FBeUM7RUFDekMsb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsNkJBQTZCO0VBQzdCLG9CQUFvQjtDQUNyQiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2JlZ2luaW5nXHJcbntcclxuICBib3gtc2hhZG93Omluc2V0IDAgMCA4MHB4ICMyMzhiMzE7XHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIGhlaWdodDogMjVweDtcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgLyogbWFyZ2luLXRvcDogMjBweDsgKi9cclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBjb2xvcjogIzAzMmMwOTtcclxuICAvKiB0ZXh0LWFsaWduOiBjZW50ZXI7ICovXHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxufVxyXG5cclxuI3RyYWNrZWRcclxue1xyXG4gIC8qIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bzsgKi9cclxuICB0ZXh0LWFsaWduOiBjZWxlZnRudGVyO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIC8qIG1hcmdpbi1yaWdodDogMTBweDsgKi9cclxuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgd2lkdGg6IDU5MHB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IGFxdWE7ICovXHJcbn1cclxuXHJcbiN0cmFja2luZ1xyXG57XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgLyogbWFyZ2luLXJpZ2h0OiAxMHB4OyAqL1xyXG4gIHdpZHRoOiAyNzBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgcGFkZGluZy1yaWdodDogNXB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6IHJnYigyNDUsIDE2LCAxNDkpOyAqL1xyXG59XHJcblxyXG4jbGluZVxyXG57XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHdpZHRoOiAxMHB4O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjUzLCAyMjgsIDIpOyAqL1xyXG59XHJcblxyXG4uYmxhY2tcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBjZTtcclxuICB3aWR0aDozMDBweDtcclxuICBtYXJnaW46YXV0bztcclxufVxyXG5cclxuI0xvZ291dFxyXG57XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBwYWRkaW5nLWxlZnQ6IDVweDtcclxuICBmbG9hdDogbGVmdDtcclxuICB3aWR0aDogOTBweDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1MywgMjI4LCAyKTsgKi9cclxufVxyXG5cclxuI0xvZ291dDpob3ZlclxyXG57XHJcbiAgY29sb3I6ICMwMzJjMDk7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4jY29udGFpbmVyMVxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgODBweCAjMzhlOTAzO1xyXG4gIHdpZHRoOiA5ODBweDtcclxuICBoZWlnaHQ6IDUwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gIGJvcmRlci1zdHlsZTogb3V0c2V0O1xyXG59XHJcblxyXG4jc2VjdGlvblRpdGxlXHJcbntcclxuICB3aWR0aDogOTAwcHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcclxuICBmbG9hdDogbGVmdDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuI2JhY2tcclxue1xyXG4gIHdpZHRoOiA0MHB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIGZvbnQtc2l6ZTogNDRweDtcclxufVxyXG4jYmFjazpob3ZlclxyXG57XHJcbiAgb3BhY2l0eTogMC41O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuI2hvbWVcclxue1xyXG4gIHdpZHRoOiA0MHB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIGZvbnQtc2l6ZTogNDRweDtcclxufVxyXG4jaG9tZTpob3ZlclxyXG57XHJcbiAgb3BhY2l0eTogMC41O1xyXG59XHJcblxyXG4jY29udGFpbmVyXHJcbntcclxuICBib3gtc2hhZG93Omluc2V0IDAgMCAzMDBweCAgIzZiZTQxYjtcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIC8qIGhlaWdodDogMTAwMHB4OyAgdHUgbmllIG9rcmVzbGFtLCB6ZWJ5IG1vYyAgdyBrYXpkeW0ga29tcG9uZW5jaWUgb3NvYm5vIGRvcGFzb3dhYyB0ZW4gcGFyYW1ldHIqL1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAvKiBwYWRkaW5nLXRvcDogODBweDsgKi9cclxuICBmb250LXNpemU6IDI0cHg7XHJcbiAgZm9udC1mYW1pbHk6ICdHaWxsIFNhbnMnLCAnR2lsbCBTYW5zIE1UJywgQ2FsaWJyaSwgJ1RyZWJ1Y2hldCBNUycsIHNhbnMtc2VyaWY7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuI2Jsb2NrLyogZmlsdGVycyAmIG1lc3NhZ2VzIGFyZSB1c2luZyBpdCovXHJcbntcclxuICAvKiBib3gtc2hhZG93Omluc2V0IDAgMCAxMDBweCAjMjM1MTY5OyAqL1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDMwMHB4ICAjNmJlNDFiO1xyXG4gIHdpZHRoOiA5NDBweDtcclxuICAvKiBoZWlnaHQ6IDYwMHB4OyAqL1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDM1cHg7XHJcbiAgcGFkZGluZzogMzBweDtcclxuICBjb2xvcjogIzExMDgzMTtcclxuICBmb250LXN0cmV0Y2g6IHVsdHJhLWV4cGFuZGVkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<!--div style=\"text-align:center\">\r\n  <h1>\r\n    Welcome to {{ title }}!\r\n  </h1>\r\n  <img width=\"300\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\r\n</div>\r\n<h2>Here are some links to help you start: </h2>\r\n<ul>\r\n  <li>\r\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>\r\n  </li>\r\n  <li>\r\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://github.com/angular/angular-cli/wiki\">CLI Documentation</a></h2>\r\n  </li>\r\n  <li>\r\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://blog.angular.io/\">Angular blog</a></h2>\r\n  </li>\r\n</ul-->\r\n\r\n<router-outlet></router-outlet>\r\n\r\n<!-- <div id=\"container\">\r\n  <app-sidebar></app-sidebar>\r\n\r\n  <div id=\"content\">\r\n    <router-outlet></router-outlet>\r\n  </div>\r\n</div> -->\r\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'LogIT';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _details_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./details/details.component */ "./src/app/details/details.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _usb_usb_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./usb/usb.component */ "./src/app/usb/usb.component.ts");
/* harmony import */ var _home2_home2_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./home2/home2.component */ "./src/app/home2/home2.component.ts");
/* harmony import */ var _general_data_general_data_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./general-data/general-data.component */ "./src/app/general-data/general-data.component.ts");
/* harmony import */ var _net_settings_net_settings_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./net-settings/net-settings.component */ "./src/app/net-settings/net-settings.component.ts");
/* harmony import */ var ngx_pagination__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ngx-pagination */ "./node_modules/ngx-pagination/dist/ngx-pagination.js");
/* harmony import */ var _messages_messages_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./messages/messages.component */ "./src/app/messages/messages.component.ts");
/* harmony import */ var _keylogger_keylogger_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./keylogger/keylogger.component */ "./src/app/keylogger/keylogger.component.ts");
/* harmony import */ var _filters_filters_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./filters/filters.component */ "./src/app/filters/filters.component.ts");
/* harmony import */ var _network_network_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./network/network.component */ "./src/app/network/network.component.ts");
/* harmony import */ var _net_data_net_data_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./net-data/net-data.component */ "./src/app/net-data/net-data.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _auth_guard__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! ./auth.guard */ "./src/app/auth.guard.ts");
/* harmony import */ var _processes_processes_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! ./processes/processes.component */ "./src/app/processes/processes.component.ts");
/* harmony import */ var _security_security_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! ./security/security.component */ "./src/app/security/security.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};











 // <-Add here










var appRoutes = [
    { path: 'login', component: _login_login_component__WEBPACK_IMPORTED_MODULE_18__["LoginComponent"] },
    { path: 'home2', component: _home2_home2_component__WEBPACK_IMPORTED_MODULE_9__["Home2Component"], canActivate: [_auth_guard__WEBPACK_IMPORTED_MODULE_19__["AuthGuard"]] }
];
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
                _details_details_component__WEBPACK_IMPORTED_MODULE_6__["DetailsComponent"],
                _usb_usb_component__WEBPACK_IMPORTED_MODULE_8__["UsbComponent"],
                _home2_home2_component__WEBPACK_IMPORTED_MODULE_9__["Home2Component"],
                _general_data_general_data_component__WEBPACK_IMPORTED_MODULE_10__["GeneralDataComponent"],
                _net_settings_net_settings_component__WEBPACK_IMPORTED_MODULE_11__["NetSettingsComponent"],
                _messages_messages_component__WEBPACK_IMPORTED_MODULE_13__["MessagesComponent"],
                _keylogger_keylogger_component__WEBPACK_IMPORTED_MODULE_14__["KeyloggerComponent"],
                _filters_filters_component__WEBPACK_IMPORTED_MODULE_15__["FiltersComponent"],
                _network_network_component__WEBPACK_IMPORTED_MODULE_16__["NetworkComponent"],
                _net_data_net_data_component__WEBPACK_IMPORTED_MODULE_17__["NetDataComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_18__["LoginComponent"],
                _processes_processes_component__WEBPACK_IMPORTED_MODULE_20__["ProcessesComponent"],
                _security_security_component__WEBPACK_IMPORTED_MODULE_21__["SecurityComponent"]
            ],
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"],
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_7__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                ngx_pagination__WEBPACK_IMPORTED_MODULE_12__["NgxPaginationModule"]
            ],
            providers: [_auth_guard__WEBPACK_IMPORTED_MODULE_19__["AuthGuard"]],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/auth.guard.ts":
/*!*******************************!*\
  !*** ./src/app/auth.guard.ts ***!
  \*******************************/
/*! exports provided: AuthGuard */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthGuard", function() { return AuthGuard; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
// import { Observable } from 'rxjs';
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuard implements CanActivate {
//   canActivate(
//     next: ActivatedRouteSnapshot,
//     state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
//     return true;
//   }
// }


var AuthGuard = /** @class */ (function () {
    function AuthGuard(router) {
        this.router = router;
    }
    AuthGuard.prototype.canActivate = function (route, state) {
        var url = state.url;
        return this.verifyLogin(url);
    };
    AuthGuard.prototype.verifyLogin = function (url) {
        if (!this.isLoggedIn()) {
            this.router.navigate(['/login']);
            return false;
        }
        else if (this.isLoggedIn()) {
            return true;
        }
    };
    AuthGuard.prototype.isLoggedIn = function () {
        var status = false;
        if (localStorage.getItem('isLoggedIn') == "true") {
            status = true;
        }
        else {
            status = false;
        }
        return status;
    };
    AuthGuard = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_1__["Router"]])
    ], AuthGuard);
    return AuthGuard;
}());



/***/ }),

/***/ "./src/app/auth.service.ts":
/*!*********************************!*\
  !*** ./src/app/auth.service.ts ***!
  \*********************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var AuthService = /** @class */ (function () {
    function AuthService() {
    }
    AuthService.prototype.logout = function () {
        localStorage.setItem('isLoggedIn', "false");
        localStorage.removeItem('token');
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/data.service.ts":
/*!*********************************!*\
  !*** ./src/app/data.service.ts ***!
  \*********************************/
/*! exports provided: DataService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataService", function() { return DataService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DataService = /** @class */ (function () {
    function DataService(http) {
        this.http = http;
        this.token = localStorage.getItem('token');
    }
    // endpoint = 'http://localhost:8080/api/usb-ports/<id użytkownika>';
    // httpOptions =
    // {
    //   headers: new HttpHeaders
    //   ({
    //     'Content-Type':  'application/json'
    //   })
    // };
    DataService.prototype.getUser = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/keylogger-users/' + userId, {
            headers: headers
        });
        // return this.http.get('https://jsonplaceholder.typicode.com/users/'+userId)
    };
    DataService.prototype.getUsers = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/keylogger-users', {
            headers: headers
        });
        // return this.http.get('https://jsonplaceholder.typicode.com/users')
    };
    DataService.prototype.getFilteredUsers = function (query) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/keylogger-users/search/' + query, {
            headers: headers
        });
    };
    DataService.prototype.getFilteredMessages = function (query, userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/messages/' + userId + '/search/' + query, {
            headers: headers
        });
    };
    DataService.prototype.getDepartments = function () {
        return this.http.get('http://localhost:8080/api/departaments');
    };
    // getToken() 
    // {
    //   const headers = new HttpHeaders()
    //     .set('Content-Type', 'application/json');
    //   this.http.post('http://localhost:8080/api/login',
    //     {
    //       // 'username': this.username,
    //       // 'password': this.password,
    //       'username': 'admin',
    //       'password': 'admin',
    //       'departamentId': 1
    //     }, {headers})
    //     .subscribe(
    //       data => {
    //         console.log('POST Request with login is successful ', data);
    //       },
    //       error => {
    //         console.log('Error in post request with login Iwona :(', error);
    //       }
    //     );
    // }
    DataService.prototype.getAllDevices = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/devices/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getAllDevicesHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/devices/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getAllDiscDrives = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/drives/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getAllDiscDrivesHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/drives/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getInternetSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/internet-settings/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getTransfer = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/current-transfer/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getTransferHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/current-transfer/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getTransferSinceComputerStarted = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/since-boot-transfer/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getTransferSinceComputerStartedHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/since-boot-transfer/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getNetworkCard = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/networkcard-info/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getNetworkCardHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/networkcard-info/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getPing = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/pings/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getPingHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/pings/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getActiveProcess = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/active-process/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getActiveProcessHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/active-process/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getUserProcess = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/processes/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getUserProcessHistory = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/processes/history/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getMessages = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/messages/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getFilters = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/keylogger-filter/' + userId, {
            headers: headers
        });
    };
    // getFilters(userId) {
    //   return timer(0, 10000)
    //       .pipe(
    //          switchMap(_ => this.http.get('http://localhost:8080/api/keylogger-filter/' + userId)),
    //          catchError(error => of(`Bad request: ${error}`))
    //       );
    // }
    //-----------------------------------------------------------------------------------------------
    // postFilter() {
    //   return this.http.post('http://localhost:8080//api/keylogger-filter');
    // }
    // postFilter() 
    // {
    //   return this.http.post('http://localhost:8080//api/keylogger-filter',
    //      {
    //         filter: "dm",
    //         // id: 2, 
    //         keyloggerUserId: 2
    //      }
    //   );
    // }
    DataService.prototype.postFilter = function (word) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        var body = 'filter=' + word;
        this.http.post('http://localhost:8080/user/all', body).subscribe(function (data) {
        });
    };
    DataService.prototype.getKeyloggerSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/keylogger-settings/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getUSBBlockingSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/usb-blocking/' + userId, {
            headers: headers
        });
    };
    // getActiveProcessMonitoringSettings(userId) 
    // {
    //   const headers = new HttpHeaders()
    //   .set('X-Auth-Token', this.token);
    //   return this.http.get('http://localhost:8080/api/process-settings/' + userId,
    //       //NIE MA ENDPOINTA! 
    //   {
    //     headers 
    //   });
    // }
    DataService.prototype.getActiveAndUserProcessMonitoringSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/process-settings/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getDiscMonitoringAndScaningSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/disc-drives/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getRemoteMemoryMonitoringAndScaningSettings = function (userId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/usb-monitoring-settings/' + userId, {
            headers: headers
        });
    };
    DataService.prototype.getDriveFiles = function (driveId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/drive-files/' + driveId, {
            headers: headers
        });
    };
    DataService.prototype.getHistoryDriveFiles = function (driveId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        return this.http.get('http://localhost:8080/api/drive-files/history/' + driveId, {
            headers: headers
        });
    };
    DataService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], DataService);
    return DataService;
}());



/***/ }),

/***/ "./src/app/details/details.component.css":
/*!***********************************************!*\
  !*** ./src/app/details/details.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#tracked\r\n{\r\n  opacity: 0;\r\n}\r\n\r\n#container\r\n{\r\n  height: 440px;  \r\n  /* background-color: #5e3981;  */\r\n}\r\n\r\n#username\r\n{\r\n  width: 940px;\r\n  letter-spacing: 2px;\r\n  float: left;\r\n  font-size: 15px;\r\n}\r\n\r\n/* #top\r\n{\r\n  width: 850px;\r\n  height: 170px;\r\n  text-align: center;\r\n  margin-left: 75px;\r\n} */\r\n\r\n#top\r\n{\r\n  width: 850px;\r\n  height: 140px;\r\n  margin-left: 75px;\r\n  /* background-color: #920b16; */\r\n  margin-top: 10px;\r\n  margin-bottom: 0px;\r\n}\r\n\r\n#down\r\n{\r\n  width: 850px;\r\n  height: 140px;\r\n  margin-left: 75px;\r\n  /* background-color: #930aa0; */\r\n  /* margin-top: 20px; */\r\n  margin-top: 0px;\r\n  margin-bottom: 0px;\r\n}\r\n\r\n#secondDown\r\n{\r\n  /* width: 1000px;\r\n  height: 130px;\r\n  margin-left: 75px;\r\n  margin-bottom: 5px;\r\n  margin-top: 5px; */\r\n  width: 850px;\r\n  height: 140px;\r\n  margin-left: 75px;\r\n  /* background-color: #16cc1f; */\r\n  margin-bottom: 0px;\r\n  margin-top: 0px;\r\n}\r\n\r\n.middle\r\n{\r\n  width: 800px;\r\n  height: 5px;\r\n  margin-bottom: 0px;\r\n  margin-top: 0px;\r\n  /* background-color: aqua; */\r\n}\r\n\r\n.black\r\n{\r\n  background-color: #000000ab;\r\n  width:300px;\r\n  margin:auto;\r\n}\r\n\r\n/* #middle1\r\n{\r\n  width: 800px;\r\n  height: 10px;\r\n}\r\n#middle2\r\n{\r\n  width: 800px;\r\n  height: 10px;\r\n} */\r\n\r\n#red_tile{\r\n    background-color: #7f3f3f;\r\n    width: 380px;\r\n    /* width: 460px; */\r\n    height: 70px;\r\n    float: left;\r\n    margin: 20px;\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n    /*display:table-cell;\r\n    vertical-align:middle;*/\r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('dane_komputerowe1.jpg');\r\n    /* color: blanchedalmond; */\r\n  }\r\n\r\n#red_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor: pointer;\r\n  }\r\n\r\n#green_tile{\r\n    background-color: #326647;\r\n    width: 380px;\r\n    height: 70px;\r\n    float: left;\r\n    margin: 20px;\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n    \r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('pamiec.jpg');\r\n    color: blanchedalmond;\r\n  }\r\n\r\n#green_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor: pointer;\r\n  }\r\n\r\n#gold_tile{\r\n    /* background-color: #aa8213; */\r\n    width: 380px;\r\n    height: 70px;\r\n    float: left;\r\n    margin: 20px;\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    /* box-shadow:inset 0 0 80px #000000; */\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n\r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('polaczenie_internetowe_5.jpg');\r\n    color: blanchedalmond;\r\n  }\r\n\r\n#gold_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor:pointer;\r\n  }\r\n\r\n#purple_tile{\r\n    background-color: #5e3981;\r\n    width: 380px;\r\n    height: 70px;\r\n    float: left;\r\n    margin: 20px;\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('monitorowanie_1.jpg');\r\n    color: blanchedalmond;\r\n  }\r\n\r\n#purple_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor:pointer;\r\n  }\r\n\r\n#orange_tile{\r\n    background-color: #db6809;\r\n    width: 380px;\r\n    height: 70px;\r\n    float: left;\r\n    /* margin: 20px; */ margin-left: 235px; /*850/2 - 380/2 = 425 - 190 = 325 - 90 = 235*/\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('processes.jpg');\r\n    color: blanchedalmond;\r\n  }\r\n\r\n#orange_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor:pointer;\r\n  }\r\n\r\n#orange2_tile{\r\n    background-color: #db6809;\r\n    width: 380px;\r\n    height: 70px;\r\n    float: left;\r\n    margin: 20px;\r\n    margin-top: 2px;\r\n    margin-bottom: 2px;\r\n    box-shadow:inset 0 0 80px #000000;\r\n    padding-top: 60px;\r\n\r\n    -webkit-text-decoration-line: overline;\r\n\r\n            text-decoration-line: overline;\r\n    \r\n    /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n    background-image: url('uprawnienia1.jpg');\r\n    color: blanchedalmond;\r\n  }\r\n\r\n#orange2_tile:hover\r\n  {\r\n    box-shadow:inset 0 0 10px #000000;\r\n    cursor:pointer;\r\n  }\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZGV0YWlscy9kZXRhaWxzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsV0FBVztDQUNaOztBQUVEOztFQUVFLGNBQWM7RUFDZCxpQ0FBaUM7Q0FDbEM7O0FBRUQ7O0VBRUUsYUFBYTtFQUNiLG9CQUFvQjtFQUNwQixZQUFZO0VBQ1osZ0JBQWdCO0NBQ2pCOztBQUVEOzs7Ozs7SUFNSTs7QUFFSjs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixnQ0FBZ0M7RUFDaEMsaUJBQWlCO0VBQ2pCLG1CQUFtQjtDQUNwQjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixnQ0FBZ0M7RUFDaEMsdUJBQXVCO0VBQ3ZCLGdCQUFnQjtFQUNoQixtQkFBbUI7Q0FDcEI7O0FBRUQ7O0VBRUU7Ozs7cUJBSW1CO0VBQ25CLGFBQWE7RUFDYixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLGdDQUFnQztFQUNoQyxtQkFBbUI7RUFDbkIsZ0JBQWdCO0NBQ2pCOztBQUVEOztFQUVFLGFBQWE7RUFDYixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQiw2QkFBNkI7Q0FDOUI7O0FBRUQ7O0VBRUUsNEJBQTRCO0VBQzVCLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBQ0Q7Ozs7Ozs7OztJQVNJOztBQUVKO0lBQ0ksMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsYUFBYTtJQUNiLFlBQVk7SUFDWixhQUFhO0lBQ2IsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixrQ0FBa0M7SUFDbEMsa0JBQWtCOztJQUVsQix1Q0FBK0I7O1lBQS9CLCtCQUErQjtJQUMvQjs0QkFDd0I7SUFDeEIsK0VBQStFOztJQUUvRSwrQ0FBK0M7SUFDL0MsNEJBQTRCO0dBQzdCOztBQUVEOztJQUVFLGtDQUFrQztJQUNsQyxnQkFBZ0I7R0FDakI7O0FBRUQ7SUFDRSwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsa0NBQWtDO0lBQ2xDLGtCQUFrQjs7SUFFbEIsdUNBQStCOztZQUEvQiwrQkFBK0I7O0lBRS9CLCtFQUErRTs7SUFFL0Usb0NBQW9DO0lBQ3BDLHNCQUFzQjtHQUN2Qjs7QUFFRDs7SUFFRSxrQ0FBa0M7SUFDbEMsZ0JBQWdCO0dBQ2pCOztBQUVEO0lBQ0UsZ0NBQWdDO0lBQ2hDLGFBQWE7SUFDYixhQUFhO0lBQ2IsWUFBWTtJQUNaLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLHdDQUF3QztJQUN4QyxrQ0FBa0M7SUFDbEMsa0JBQWtCOztJQUVsQix1Q0FBK0I7O1lBQS9CLCtCQUErQjs7SUFFL0IsK0VBQStFOztJQUUvRSxzREFBc0Q7SUFDdEQsc0JBQXNCO0dBQ3ZCOztBQUVEOztJQUVFLGtDQUFrQztJQUNsQyxlQUFlO0dBQ2hCOztBQUVEO0lBQ0UsMEJBQTBCO0lBQzFCLGFBQWE7SUFDYixhQUFhO0lBQ2IsWUFBWTtJQUNaLGFBQWE7SUFDYixnQkFBZ0I7SUFDaEIsbUJBQW1CO0lBQ25CLGtDQUFrQztJQUNsQyxrQkFBa0I7O0lBRWxCLHVDQUErQjs7WUFBL0IsK0JBQStCO0lBQy9CLCtFQUErRTs7SUFFL0UsNkNBQTZDO0lBQzdDLHNCQUFzQjtHQUN2Qjs7QUFFRDs7SUFFRSxrQ0FBa0M7SUFDbEMsZUFBZTtHQUNoQjs7QUFFRDtJQUNFLDBCQUEwQjtJQUMxQixhQUFhO0lBQ2IsYUFBYTtJQUNiLFlBQVk7SUFDWixtQkFBbUIsQ0FBQyxtQkFBbUIsQ0FBQyw4Q0FBOEM7SUFDdEYsZ0JBQWdCO0lBQ2hCLG1CQUFtQjtJQUNuQixrQ0FBa0M7SUFDbEMsa0JBQWtCOztJQUVsQix1Q0FBK0I7O1lBQS9CLCtCQUErQjtJQUMvQiwrRUFBK0U7O0lBRS9FLHVDQUF1QztJQUN2QyxzQkFBc0I7R0FDdkI7O0FBRUQ7O0lBRUUsa0NBQWtDO0lBQ2xDLGVBQWU7R0FDaEI7O0FBRUQ7SUFDRSwwQkFBMEI7SUFDMUIsYUFBYTtJQUNiLGFBQWE7SUFDYixZQUFZO0lBQ1osYUFBYTtJQUNiLGdCQUFnQjtJQUNoQixtQkFBbUI7SUFDbkIsa0NBQWtDO0lBQ2xDLGtCQUFrQjs7SUFFbEIsdUNBQStCOztZQUEvQiwrQkFBK0I7O0lBRS9CLCtFQUErRTs7SUFFL0UsMENBQTBDO0lBQzFDLHNCQUFzQjtHQUN2Qjs7QUFFRDs7SUFFRSxrQ0FBa0M7SUFDbEMsZUFBZTtHQUNoQiIsImZpbGUiOiJzcmMvYXBwL2RldGFpbHMvZGV0YWlscy5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3RyYWNrZWRcclxue1xyXG4gIG9wYWNpdHk6IDA7XHJcbn1cclxuXHJcbiNjb250YWluZXJcclxue1xyXG4gIGhlaWdodDogNDQwcHg7ICBcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjNWUzOTgxOyAgKi9cclxufVxyXG5cclxuI3VzZXJuYW1lXHJcbntcclxuICB3aWR0aDogOTQwcHg7XHJcbiAgbGV0dGVyLXNwYWNpbmc6IDJweDtcclxuICBmbG9hdDogbGVmdDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbi8qICN0b3Bcclxue1xyXG4gIHdpZHRoOiA4NTBweDtcclxuICBoZWlnaHQ6IDE3MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tbGVmdDogNzVweDtcclxufSAqL1xyXG5cclxuI3RvcFxyXG57XHJcbiAgd2lkdGg6IDg1MHB4O1xyXG4gIGhlaWdodDogMTQwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDc1cHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogIzkyMGIxNjsgKi9cclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuI2Rvd25cclxue1xyXG4gIHdpZHRoOiA4NTBweDtcclxuICBoZWlnaHQ6IDE0MHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA3NXB4O1xyXG4gIC8qIGJhY2tncm91bmQtY29sb3I6ICM5MzBhYTA7ICovXHJcbiAgLyogbWFyZ2luLXRvcDogMjBweDsgKi9cclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4jc2Vjb25kRG93blxyXG57XHJcbiAgLyogd2lkdGg6IDEwMDBweDtcclxuICBoZWlnaHQ6IDEzMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA3NXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxuICBtYXJnaW4tdG9wOiA1cHg7ICovXHJcbiAgd2lkdGg6IDg1MHB4O1xyXG4gIGhlaWdodDogMTQwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDc1cHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogIzE2Y2MxZjsgKi9cclxuICBtYXJnaW4tYm90dG9tOiAwcHg7XHJcbiAgbWFyZ2luLXRvcDogMHB4O1xyXG59XHJcblxyXG4ubWlkZGxlXHJcbntcclxuICB3aWR0aDogODAwcHg7XHJcbiAgaGVpZ2h0OiA1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG4gIG1hcmdpbi10b3A6IDBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhOyAqL1xyXG59XHJcblxyXG4uYmxhY2tcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDBhYjtcclxuICB3aWR0aDozMDBweDtcclxuICBtYXJnaW46YXV0bztcclxufVxyXG4vKiAjbWlkZGxlMVxyXG57XHJcbiAgd2lkdGg6IDgwMHB4O1xyXG4gIGhlaWdodDogMTBweDtcclxufVxyXG4jbWlkZGxlMlxyXG57XHJcbiAgd2lkdGg6IDgwMHB4O1xyXG4gIGhlaWdodDogMTBweDtcclxufSAqL1xyXG5cclxuI3JlZF90aWxle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzdmM2YzZjtcclxuICAgIHdpZHRoOiAzODBweDtcclxuICAgIC8qIHdpZHRoOiA0NjBweDsgKi9cclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMnB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gICAgYm94LXNoYWRvdzppbnNldCAwIDAgODBweCAjMDAwMDAwO1xyXG4gICAgcGFkZGluZy10b3A6IDYwcHg7XHJcblxyXG4gICAgdGV4dC1kZWNvcmF0aW9uLWxpbmU6IG92ZXJsaW5lO1xyXG4gICAgLypkaXNwbGF5OnRhYmxlLWNlbGw7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjptaWRkbGU7Ki9cclxuICAgIC8qdHV0YWogamFraWXFmyBkb2RhdGtvd2Ugc3R5bGUsIGphayBucC4gYm94LXNoYWRvdywgdGV4dC1zaGFkb3csIGJvcmRlciwgaXRwLiovXHJcblxyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdkYW5lX2tvbXB1dGVyb3dlMS5qcGcnKTtcclxuICAgIC8qIGNvbG9yOiBibGFuY2hlZGFsbW9uZDsgKi9cclxuICB9IFxyXG5cclxuICAjcmVkX3RpbGU6aG92ZXJcclxuICB7XHJcbiAgICBib3gtc2hhZG93Omluc2V0IDAgMCAxMHB4ICMwMDAwMDA7XHJcbiAgICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgfVxyXG5cclxuICAjZ3JlZW5fdGlsZXtcclxuICAgIGJhY2tncm91bmQtY29sb3I6ICMzMjY2NDc7XHJcbiAgICB3aWR0aDogMzgwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIG1hcmdpbjogMjBweDtcclxuICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDtcclxuICAgIHBhZGRpbmctdG9wOiA2MHB4O1xyXG5cclxuICAgIHRleHQtZGVjb3JhdGlvbi1saW5lOiBvdmVybGluZTtcclxuICAgIFxyXG4gICAgLyp0dXRhaiBqYWtpZcWbIGRvZGF0a293ZSBzdHlsZSwgamFrIG5wLiBib3gtc2hhZG93LCB0ZXh0LXNoYWRvdywgYm9yZGVyLCBpdHAuKi9cclxuXHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ3BhbWllYy5qcGcnKTtcclxuICAgIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxuICB9XHJcblxyXG4gICNncmVlbl90aWxlOmhvdmVyXHJcbiAge1xyXG4gICAgYm94LXNoYWRvdzppbnNldCAwIDAgMTBweCAjMDAwMDAwO1xyXG4gICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIH1cclxuXHJcbiAgI2dvbGRfdGlsZXtcclxuICAgIC8qIGJhY2tncm91bmQtY29sb3I6ICNhYTgyMTM7ICovXHJcbiAgICB3aWR0aDogMzgwcHg7XHJcbiAgICBoZWlnaHQ6IDcwcHg7XHJcbiAgICBmbG9hdDogbGVmdDtcclxuICAgIG1hcmdpbjogMjBweDtcclxuICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDsgKi9cclxuICAgIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDtcclxuICAgIHBhZGRpbmctdG9wOiA2MHB4O1xyXG5cclxuICAgIHRleHQtZGVjb3JhdGlvbi1saW5lOiBvdmVybGluZTtcclxuXHJcbiAgICAvKnR1dGFqIGpha2llxZsgZG9kYXRrb3dlIHN0eWxlLCBqYWsgbnAuIGJveC1zaGFkb3csIHRleHQtc2hhZG93LCBib3JkZXIsIGl0cC4qL1xyXG5cclxuICAgIGJhY2tncm91bmQtaW1hZ2U6IHVybCgncG9sYWN6ZW5pZV9pbnRlcm5ldG93ZV81LmpwZycpO1xyXG4gICAgY29sb3I6IGJsYW5jaGVkYWxtb25kO1xyXG4gIH1cclxuXHJcbiAgI2dvbGRfdGlsZTpob3ZlclxyXG4gIHtcclxuICAgIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwcHggIzAwMDAwMDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG4gIH1cclxuXHJcbiAgI3B1cnBsZV90aWxle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogIzVlMzk4MTtcclxuICAgIHdpZHRoOiAzODBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgbWFyZ2luOiAyMHB4O1xyXG4gICAgbWFyZ2luLXRvcDogMnB4O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnB4O1xyXG4gICAgYm94LXNoYWRvdzppbnNldCAwIDAgODBweCAjMDAwMDAwO1xyXG4gICAgcGFkZGluZy10b3A6IDYwcHg7XHJcblxyXG4gICAgdGV4dC1kZWNvcmF0aW9uLWxpbmU6IG92ZXJsaW5lO1xyXG4gICAgLyp0dXRhaiBqYWtpZcWbIGRvZGF0a293ZSBzdHlsZSwgamFrIG5wLiBib3gtc2hhZG93LCB0ZXh0LXNoYWRvdywgYm9yZGVyLCBpdHAuKi9cclxuXHJcbiAgICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ21vbml0b3Jvd2FuaWVfMS5qcGcnKTtcclxuICAgIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxuICB9XHJcblxyXG4gICNwdXJwbGVfdGlsZTpob3ZlclxyXG4gIHtcclxuICAgIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwcHggIzAwMDAwMDtcclxuICAgIGN1cnNvcjpwb2ludGVyO1xyXG4gIH1cclxuXHJcbiAgI29yYW5nZV90aWxle1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2RiNjgwOTtcclxuICAgIHdpZHRoOiAzODBweDtcclxuICAgIGhlaWdodDogNzBweDtcclxuICAgIGZsb2F0OiBsZWZ0O1xyXG4gICAgLyogbWFyZ2luOiAyMHB4OyAqLyBtYXJnaW4tbGVmdDogMjM1cHg7IC8qODUwLzIgLSAzODAvMiA9IDQyNSAtIDE5MCA9IDMyNSAtIDkwID0gMjM1Ki9cclxuICAgIG1hcmdpbi10b3A6IDJweDtcclxuICAgIG1hcmdpbi1ib3R0b206IDJweDtcclxuICAgIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDtcclxuICAgIHBhZGRpbmctdG9wOiA2MHB4O1xyXG5cclxuICAgIHRleHQtZGVjb3JhdGlvbi1saW5lOiBvdmVybGluZTtcclxuICAgIC8qdHV0YWogamFraWXFmyBkb2RhdGtvd2Ugc3R5bGUsIGphayBucC4gYm94LXNoYWRvdywgdGV4dC1zaGFkb3csIGJvcmRlciwgaXRwLiovXHJcblxyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdwcm9jZXNzZXMuanBnJyk7XHJcbiAgICBjb2xvcjogYmxhbmNoZWRhbG1vbmQ7XHJcbiAgfVxyXG5cclxuICAjb3JhbmdlX3RpbGU6aG92ZXJcclxuICB7XHJcbiAgICBib3gtc2hhZG93Omluc2V0IDAgMCAxMHB4ICMwMDAwMDA7XHJcbiAgICBjdXJzb3I6cG9pbnRlcjtcclxuICB9XHJcblxyXG4gICNvcmFuZ2UyX3RpbGV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGI2ODA5O1xyXG4gICAgd2lkdGg6IDM4MHB4O1xyXG4gICAgaGVpZ2h0OiA3MHB4O1xyXG4gICAgZmxvYXQ6IGxlZnQ7XHJcbiAgICBtYXJnaW46IDIwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICBib3gtc2hhZG93Omluc2V0IDAgMCA4MHB4ICMwMDAwMDA7XHJcbiAgICBwYWRkaW5nLXRvcDogNjBweDtcclxuXHJcbiAgICB0ZXh0LWRlY29yYXRpb24tbGluZTogb3ZlcmxpbmU7XHJcbiAgICBcclxuICAgIC8qdHV0YWogamFraWXFmyBkb2RhdGtvd2Ugc3R5bGUsIGphayBucC4gYm94LXNoYWRvdywgdGV4dC1zaGFkb3csIGJvcmRlciwgaXRwLiovXHJcblxyXG4gICAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCd1cHJhd25pZW5pYTEuanBnJyk7XHJcbiAgICBjb2xvcjogYmxhbmNoZWRhbG1vbmQ7XHJcbiAgfVxyXG5cclxuICAjb3JhbmdlMl90aWxlOmhvdmVyXHJcbiAge1xyXG4gICAgYm94LXNoYWRvdzppbnNldCAwIDAgMTBweCAjMDAwMDAwO1xyXG4gICAgY3Vyc29yOnBvaW50ZXI7XHJcbiAgfVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/details/details.component.html":
/*!************************************************!*\
  !*** ./src/app/details/details.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">  \r\n  <div id = \"tracked\" >\r\n      Obserwujesz użytkownika {{user$.username}} \r\n  </div>\r\n  <div id = \"tracking\" >\r\n      {{logName}}\r\n  </div>\r\n  <div id = \"line\" >\r\n      |\r\n  </div> \r\n  <div id = \"Logout\" (click)=\"logout()\" >\r\n      Wyloguj \r\n  </div> \r\n<div class=\"middle\" style= \"clear:both\"></div>                      \r\n</div>\r\n<div id=\"container1\">\r\n    <div id=\"username\" ><h1>{{ user$.username }}</h1> </div>\r\n    <div id=\"home\">\r\n        <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\r\n          <i class=\"material-icons\">home</i>\r\n        </a>\r\n    </div>\r\n</div>\r\n<div class=\"middle\" style= \"clear:both\"></div>\r\n<div id=\"container\"> \r\n  <div id=\"top\">\r\n    <div id=\"red_tile\" routerLink=\"/general-data/{{user$.id}}\" style=\"color:white; text-decoration: none; font-weight: 700; text-align: center\">\r\n      <div class=\"black\">\r\n        <i class=\"material-icons\">personal_video</i>  Dane komputerowe\r\n      </div>\r\n    </div> \r\n    <div id=\"green_tile\" routerLink=\"/usb/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\r\n      <div class=\"black\">\r\n        <i class=\"material-icons\">usb</i>   Urządzenia i pamięć \r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"middle\" style= \"clear:both\"></div>\r\n  \r\n  <div id=\"down\" >\r\n    <div id=\"gold_tile\" routerLink=\"/network/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\r\n      <div class=\"black\">\r\n        <i class=\"material-icons\">wifi</i>   Połączenie internetowe\r\n      </div>\r\n    </div>\r\n    <div id=\"purple_tile\" routerLink=\"/keylogger/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\r\n      <div class=\"black\">\r\n        <i class=\"material-icons\">fingerprint</i>   Monitorowanie aktywności\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"middle\" style= \"clear:both\"></div>\r\n\r\n  <div id=\"secondDown\" >\r\n    <div id=\"orange_tile\" routerLink=\"/processes/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\r\n      <div class=\"black\">\r\n        <i class=\"material-icons\">poll</i>   Procesy\r\n      </div>\r\n    </div>\r\n  </div>\r\n  <div class=\"middle\" style= \"clear:both\"></div>\r\n  \r\n</div>\r\n"

/***/ }),

/***/ "./src/app/details/details.component.ts":
/*!**********************************************!*\
  !*** ./src/app/details/details.component.ts ***!
  \**********************************************/
/*! exports provided: DetailsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsComponent", function() { return DetailsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var DetailsComponent = /** @class */ (function () {
    function DetailsComponent(route, data, authenticationService) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.authenticationService = authenticationService;
        this.route.params.subscribe(function (params) { return _this.user$ = params.id; });
    }
    DetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.user$).subscribe(function (data) { return _this.user$ = data; });
        this.data.getUsers().subscribe(function (data) { return _this.users$ = data; });
        this.logName = localStorage.getItem('username');
    };
    DetailsComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    DetailsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-details',
            template: __webpack_require__(/*! ./details.component.html */ "./src/app/details/details.component.html"),
            styles: [__webpack_require__(/*! ./details.component.css */ "./src/app/details/details.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"]])
    ], DetailsComponent);
    return DetailsComponent;
}());



/***/ }),

/***/ "./src/app/filters/filters.component.css":
/*!***********************************************!*\
  !*** ./src/app/filters/filters.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".title\r\n{\r\n  background-color: #179e88;\r\n  padding: 5px;\r\n  margin-top: 30px;\r\n  margin-bottom: 30px;\r\n  font-weight: 900;\r\n}\r\n\r\n.title:hover\r\n{\r\n  cursor: default;\r\n}\r\n\r\n#filtersList\r\n{\r\n  padding: 5px;\r\n}\r\n\r\n.filtrContent\r\n{\r\n  padding: 5px;\r\n  background-color: rgb(212, 221, 80);\r\n  float: left;\r\n}\r\n\r\n.filtrContent:hover\r\n{\r\n  cursor: default;\r\n}\r\n\r\n.icon\r\n{\r\n  padding: 5px;\r\n  float: left;\r\n}\r\n\r\n.icon:hover\r\n{\r\n  opacity: 0.5;\r\n  cursor: pointer;\r\n}\r\n\r\n#placeForNew\r\n{\r\n  background-color: aliceblue;\r\n  padding: 5px;\r\n  float: left;\r\n  width: 400px;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZmlsdGVycy9maWx0ZXJzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGlCQUFpQjtDQUNsQjs7QUFFRDs7RUFFRSxnQkFBZ0I7Q0FDakI7O0FBRUQ7O0VBRUUsYUFBYTtDQUNkOztBQUVEOztFQUVFLGFBQWE7RUFDYixvQ0FBb0M7RUFDcEMsWUFBWTtDQUNiOztBQUVEOztFQUVFLGdCQUFnQjtDQUNqQjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsWUFBWTtDQUNiOztBQUVEOztFQUVFLGFBQWE7RUFDYixnQkFBZ0I7Q0FDakI7O0FBRUQ7O0VBRUUsNEJBQTRCO0VBQzVCLGFBQWE7RUFDYixZQUFZO0VBQ1osYUFBYTtDQUNkIiwiZmlsZSI6InNyYy9hcHAvZmlsdGVycy9maWx0ZXJzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIudGl0bGVcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxNzllODg7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICBmb250LXdlaWdodDogOTAwO1xyXG59XHJcblxyXG4udGl0bGU6aG92ZXJcclxue1xyXG4gIGN1cnNvcjogZGVmYXVsdDtcclxufVxyXG5cclxuI2ZpbHRlcnNMaXN0XHJcbntcclxuICBwYWRkaW5nOiA1cHg7XHJcbn1cclxuXHJcbi5maWx0ckNvbnRlbnRcclxue1xyXG4gIHBhZGRpbmc6IDVweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjEyLCAyMjEsIDgwKTtcclxuICBmbG9hdDogbGVmdDtcclxufVxyXG5cclxuLmZpbHRyQ29udGVudDpob3ZlclxyXG57XHJcbiAgY3Vyc29yOiBkZWZhdWx0O1xyXG59XHJcblxyXG4uaWNvblxyXG57XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4uaWNvbjpob3ZlclxyXG57XHJcbiAgb3BhY2l0eTogMC41O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuI3BsYWNlRm9yTmV3XHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBhbGljZWJsdWU7XHJcbiAgcGFkZGluZzogNXB4O1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG4gIHdpZHRoOiA0MDBweDtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/filters/filters.component.html":
/*!************************************************!*\
  !*** ./src/app/filters/filters.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}} \n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\" >\n      Wyloguj \n  </div> \n<div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"block\" >\n  <div class=\"title\">\n    Aktywne filtry\n  </div>\n\n  <div id=\"filtersList\" *ngFor=\"let filter of filters \">\n      <div class=\"filtrContent\">\n        {{filter.filter}}\n      </div>\n      <div class=\"icon\">\n        <i (click)=\"deleteFilter(filter.id)\" class=\"material-icons\">delete</i>\n      </div>\n      <div class=\"middle\" style= \"clear:both\"></div>\n    </div>\n\n  <div class=\"title\">\n    Dodaj filtr\n  </div>\n  <div id=\"placeForNew\">\n    <input type=\"text\" [(ngModel)]='newFiltr' style=\"width: 395px\"/>\n  </div>\n  <div class=\"icon\">\n    <i (click)=\"postFilter(user$.id)\" class=\"material-icons\">add_box</i>\n  </div>\n  <div class=\"middle\" style= \"clear:both\"></div>\n  \n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/filters/filters.component.ts":
/*!**********************************************!*\
  !*** ./src/app/filters/filters.component.ts ***!
  \**********************************************/
/*! exports provided: FiltersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersComponent", function() { return FiltersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






// import {map} from 'rxjs/add/operator/map';
// import { map } from "rxjs/operators";



var FiltersComponent = /** @class */ (function () {
    function FiltersComponent(http, route, data, location, authenticationService) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = 'Ustawienia filtrów';
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
        this.token = localStorage.getItem('token');
    }
    FiltersComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    FiltersComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        // this.data.getFilters(this.user$).subscribe(
        //   // data => this.messages$ = data
        //   data => this.filters = data
        //   // data => console.log(data)
        // );  
        // this.subscription = timer(0, 10000).pipe(
        //   switchMap(() => this.data.getFilters(this.user$))
        // ).subscribe(data => this.filters = data);
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(0, 2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(function () { return _this.data.getFilters(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.filters = data; });
        this.logName = localStorage.getItem('username');
        // this.refreshData();
        // this.interval = setInterval(() => {
        //     this.refreshData();
        // }, 5000);
    };
    FiltersComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    //   refreshData(){
    //     this.data.getFilters(this.user$).subscribe(
    //       // data => this.messages$ = data
    //       data => this.filters = data
    //       // data => console.log(data)
    //     );
    // }
    // public data$: BehaviorSubject<any> = new BehaviorSubject({});
    // updateData() {
    //     let data = this.data.getFilters(this.user$).map((data)=>{
    //         return data.json();
    //     }).do((data)=>{
    //         this.data$.next(data);
    //     })
    // }
    FiltersComponent.prototype.postFilter = function (idUser) {
        // const headers = new HttpHeaders()
        //   .set('Content-Type', 'application/json');
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.post('http://localhost:8080/api/keylogger-filter', {
            'filter': this.newFiltr,
            'keyloggerUserId': idUser
        }, { headers: headers })
            .subscribe(function (data) {
            console.log('POST Request is successful ', data);
        }, function (error) {
            console.log('Error in post request Iwona :(', error);
        });
        this.newFiltr = '';
    };
    FiltersComponent.prototype.deleteFilter = function (id) {
        var filterId = id;
        // const headers = new HttpHeaders()
        //   .set('Content-Type', 'application/json');
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.delete('http://localhost:8080/api/keylogger-filter/' + filterId, { headers: headers })
            .subscribe(function (data) {
            console.log('DELETE Request is successful ');
        }, function (error) {
            console.log('Error in delete request Iwona :(', error);
        });
        // let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        // // let options = new RequestOptions({ headers: cpHeaders });
        // return this.http.delete('http://localhost:8080/api/keylogger-filter')
    };
    FiltersComponent.prototype.save = function () {
        this.oldFiltr = this.newFiltr;
        this.newFiltr = '';
    };
    FiltersComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    FiltersComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-filters',
            template: __webpack_require__(/*! ./filters.component.html */ "./src/app/filters/filters.component.html"),
            styles: [__webpack_require__(/*! ./filters.component.css */ "./src/app/filters/filters.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__["AuthenticationService"]])
    ], FiltersComponent);
    return FiltersComponent;
}());



/***/ }),

/***/ "./src/app/general-data/general-data.component.css":
/*!*********************************************************!*\
  !*** ./src/app/general-data/general-data.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".part\r\n{\r\n  box-shadow:inset 0 0 50px #aa9a0a;\r\n  width: 980px;\r\n  height: 30px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  font-size: 10px;\r\n  padding: 10px;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n  color: #475302;\r\n  border-color: #cc5a0e;\r\n  border-bottom-style: groove;\r\n}\r\n\r\n.textpart\r\n{\r\n  width: 980px;\r\n  /* height: 30px; */\r\n  border-bottom-left-radius: 10px;\r\n  border-bottom-right-radius: 10px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  font-size: 14px;\r\n  padding: 10px;\r\n  /* background-color: rgb(140, 209, 209); */\r\n  background-color: rgb(172, 214, 55);\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvZ2VuZXJhbC1kYXRhL2dlbmVyYWwtZGF0YS5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLGtDQUFrQztFQUNsQyxhQUFhO0VBQ2IsYUFBYTtFQUNiLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixlQUFlO0VBQ2Ysc0JBQXNCO0VBQ3RCLDRCQUE0QjtDQUM3Qjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsbUJBQW1CO0VBQ25CLGdDQUFnQztFQUNoQyxpQ0FBaUM7RUFDakMsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixnQkFBZ0I7RUFDaEIsY0FBYztFQUNkLDJDQUEyQztFQUMzQyxvQ0FBb0M7Q0FDckMiLCJmaWxlIjoic3JjL2FwcC9nZW5lcmFsLWRhdGEvZ2VuZXJhbC1kYXRhLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIucGFydFxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgNTBweCAjYWE5YTBhO1xyXG4gIHdpZHRoOiA5ODBweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgcGFkZGluZy10b3A6IDJweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMnB4O1xyXG4gIGNvbG9yOiAjNDc1MzAyO1xyXG4gIGJvcmRlci1jb2xvcjogI2NjNWEwZTtcclxuICBib3JkZXItYm90dG9tLXN0eWxlOiBncm9vdmU7XHJcbn1cclxuXHJcbi50ZXh0cGFydFxyXG57XHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIC8qIGhlaWdodDogMzBweDsgKi9cclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTQwLCAyMDksIDIwOSk7ICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDE3MiwgMjE0LCA1NSk7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/general-data/general-data.component.html":
/*!**********************************************************!*\
  !*** ./src/app/general-data/general-data.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}}\n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n</div> \n<div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"middle0\" style= \"clear:both\"></div>\n<div class=\"part\"><h2>Identyfikator</h2></div>\n<div class = \"textpart\">{{user$.id}}</div>\n<div class=\"part\"><h2>Nazwa komputera</h2></div>\n<div class = \"textpart\">{{user$.computerName}}</div>\n<div class=\"part\"><h2>Nazwa użytkownika na komputerze</h2></div>\n<div class = \"textpart\">{{user$.username}}</div>\n<div class=\"part\"><h2>Opis</h2></div>\n<div class = \"textpart\">{{user$.description}}</div>\n\n\n"

/***/ }),

/***/ "./src/app/general-data/general-data.component.ts":
/*!********************************************************!*\
  !*** ./src/app/general-data/general-data.component.ts ***!
  \********************************************************/
/*! exports provided: GeneralDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GeneralDataComponent", function() { return GeneralDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var GeneralDataComponent = /** @class */ (function () {
    // constructor(private data: DataService, private location: Location) { }
    function GeneralDataComponent(route, data, location, authenticationService) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = 'Dane komputerowe';
        this.route.params.subscribe(function (params) { return _this.user$ = params.id; });
    }
    GeneralDataComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    GeneralDataComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.user$).subscribe(function (data) { return _this.user$ = data; });
        this.logName = localStorage.getItem('username');
    };
    GeneralDataComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    GeneralDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-general-data',
            template: __webpack_require__(/*! ./general-data.component.html */ "./src/app/general-data/general-data.component.html"),
            styles: [__webpack_require__(/*! ./general-data.component.css */ "./src/app/general-data/general-data.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], GeneralDataComponent);
    return GeneralDataComponent;
}());



/***/ }),

/***/ "./src/app/home2/home2.component.css":
/*!*******************************************!*\
  !*** ./src/app/home2/home2.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#titleApp\r\n{\r\n  box-shadow:inset 0 0 2000px #65c709;\r\n  width: 1000px;\r\n  height: 150px;\r\n  /* height: 100px; */\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  padding: 10px;\r\n  padding-top: 0px;\r\n  font-size: 50px;\r\n  color: #044404;\r\n  text-align: center;\r\n  border-bottom-style: groove;\r\n  border-radius: 20px;\r\n  margin-top: -50px;\r\n}\r\n\r\n#filtr\r\n{\r\n    /* padding-left: 20px; */\r\n    padding-left: 0px;\r\n    margin-top: 5px;\r\n}\r\n\r\n#input\r\n{\r\n    /* margin-top: 20px; */\r\n    /* margin-top: 5px; */\r\n    height: 20px;\r\n    width: 200px;\r\n    padding-left: 0px;\r\n    margin-left: 0 px;\r\n}\r\n\r\n#container2\r\n{\r\n  /* box-shadow:inset 0 0 5500px #aaa703; bbb806*/ \r\n  /* box-shadow:inset 0 0 300px #bbb806; */\r\n  box-shadow:inset 0 0 300px #65c709;\r\n  width: 980px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 15px;\r\n  padding: 10px;\r\n  padding-top: 0px;\r\n  color: #164109;\r\n  font-size: 22px;\r\n  height: 420px;\r\n  /* background-color: #bbb806; */\r\n}\r\n\r\n#left\r\n{\r\n  width: 580px;\r\n  /* background-color: aqua; */\r\n  float: left;\r\n}\r\n\r\n#search\r\n{\r\n  margin-top: 0px;\r\n  margin-left: 5px;\r\n  /* width: 60px;\r\n  height: 20px; */\r\n  width: 70px;\r\n  height: 25px;\r\n  font-weight: 600;\r\n  vertical-align: 0px;\r\n  /* border-color: #ecf022; */\r\n  border-radius: 30px;\r\n  /* color: #e08712; */\r\n  border-color: #7c5604;\r\n  color: #0c0b0a;\r\n  background-color: rgb(245, 245, 88);\r\n}\r\n\r\n#right\r\n{\r\n  float: left;\r\n  padding-top: 5px;\r\n  width: 400px;\r\n  /* height: 400px;   */\r\n  /* text-align: center; */\r\n  /* background-image: url('logo_transparent.png');\r\n  border-color: black; */\r\n  border-width: 2px; \r\n}\r\n\r\n/* #rightTop\r\n{ */\r\n\r\n/* text-align: center; */\r\n\r\n/* }  */\r\n\r\n#rightDown\r\n{\r\n  width: 350px;\r\n  height: 340px;\r\n  background-image: url('logo_transparent.png');\r\n  /* background-color: #032c09; */\r\n  margin: 0px;\r\n  padding: 0px;\r\n}\r\n\r\n.middle\r\n{\r\n  width: 980px;\r\n  height: 5px;\r\n  /* background-color: blue; */\r\n}\r\n\r\n.user:hover\r\n{\r\n  /* border-style: dashed; */\r\n  border-style: none;\r\n  border-color: #164109;\r\n  /* border-color:rgb(129, 223, 75); */\r\n  background-color: rgb(129, 223, 75);\r\n  cursor: pointer;\r\n}\r\n\r\n#LogoutHomePage\r\n{\r\n  /* text-align: left; */\r\n  font-weight: 700;\r\n  padding-left: 140px;\r\n  padding-top: 5px;\r\n  float: left;\r\n  /* width: 90px; */\r\n  color: rgb(9, 145, 61);\r\n  /* background-color: rgb(253, 228, 2); */\r\n}\r\n\r\n#LogoutHomePage:hover\r\n{\r\n  color: #032c09;\r\n  cursor: pointer;\r\n}\r\n\r\n\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvaG9tZTIvaG9tZTIuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFFRSxvQ0FBb0M7RUFDcEMsY0FBYztFQUNkLGNBQWM7RUFDZCxvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLGdCQUFnQjtFQUNoQixlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLDRCQUE0QjtFQUM1QixvQkFBb0I7RUFDcEIsa0JBQWtCO0NBQ25COztBQUVEOztJQUVJLHlCQUF5QjtJQUN6QixrQkFBa0I7SUFDbEIsZ0JBQWdCO0NBQ25COztBQUVEOztJQUVJLHVCQUF1QjtJQUN2QixzQkFBc0I7SUFDdEIsYUFBYTtJQUNiLGFBQWE7SUFDYixrQkFBa0I7SUFDbEIsa0JBQWtCO0NBQ3JCOztBQUVEOztFQUVFLGdEQUFnRDtFQUNoRCx5Q0FBeUM7RUFDekMsbUNBQW1DO0VBQ25DLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxnQ0FBZ0M7Q0FDakM7O0FBRUQ7O0VBRUUsYUFBYTtFQUNiLDZCQUE2QjtFQUM3QixZQUFZO0NBQ2I7O0FBRUQ7O0VBRUUsZ0JBQWdCO0VBQ2hCLGlCQUFpQjtFQUNqQjtrQkFDZ0I7RUFDaEIsWUFBWTtFQUNaLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLDRCQUE0QjtFQUM1QixvQkFBb0I7RUFDcEIscUJBQXFCO0VBQ3JCLHNCQUFzQjtFQUN0QixlQUFlO0VBQ2Ysb0NBQW9DO0NBQ3JDOztBQUVEOztFQUVFLFlBQVk7RUFDWixpQkFBaUI7RUFDakIsYUFBYTtFQUNiLHNCQUFzQjtFQUN0Qix5QkFBeUI7RUFDekI7eUJBQ3VCO0VBQ3ZCLGtCQUFrQjtDQUNuQjs7QUFFRDtJQUNJOztBQUNGLHlCQUF5Qjs7QUFDM0IsUUFBUTs7QUFFUjs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLDhDQUE4QztFQUM5QyxnQ0FBZ0M7RUFDaEMsWUFBWTtFQUNaLGFBQWE7Q0FDZDs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsWUFBWTtFQUNaLDZCQUE2QjtDQUM5Qjs7QUFFRDs7RUFFRSwyQkFBMkI7RUFDM0IsbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0QixxQ0FBcUM7RUFDckMsb0NBQW9DO0VBQ3BDLGdCQUFnQjtDQUNqQjs7QUFFRDs7RUFFRSx1QkFBdUI7RUFDdkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQix1QkFBdUI7RUFDdkIseUNBQXlDO0NBQzFDOztBQUVEOztFQUVFLGVBQWU7RUFDZixnQkFBZ0I7Q0FDakIiLCJmaWxlIjoic3JjL2FwcC9ob21lMi9ob21lMi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI3RpdGxlQXBwXHJcbntcclxuICBib3gtc2hhZG93Omluc2V0IDAgMCAyMDAwcHggIzY1YzcwOTtcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgLyogaGVpZ2h0OiAxMDBweDsgKi9cclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gIGZvbnQtc2l6ZTogNTBweDtcclxuICBjb2xvcjogIzA0NDQwNDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgYm9yZGVyLWJvdHRvbS1zdHlsZTogZ3Jvb3ZlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgbWFyZ2luLXRvcDogLTUwcHg7XHJcbn1cclxuXHJcbiNmaWx0clxyXG57XHJcbiAgICAvKiBwYWRkaW5nLWxlZnQ6IDIwcHg7ICovXHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuI2lucHV0XHJcbntcclxuICAgIC8qIG1hcmdpbi10b3A6IDIwcHg7ICovXHJcbiAgICAvKiBtYXJnaW4tdG9wOiA1cHg7ICovXHJcbiAgICBoZWlnaHQ6IDIwcHg7XHJcbiAgICB3aWR0aDogMjAwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDBweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAwIHB4O1xyXG59XHJcblxyXG4jY29udGFpbmVyMlxyXG57XHJcbiAgLyogYm94LXNoYWRvdzppbnNldCAwIDAgNTUwMHB4ICNhYWE3MDM7IGJiYjgwNiovIFxyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDMwMHB4ICNiYmI4MDY7ICovXHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMzAwcHggIzY1YzcwOTtcclxuICB3aWR0aDogOTgwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTVweDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgY29sb3I6ICMxNjQxMDk7XHJcbiAgZm9udC1zaXplOiAyMnB4O1xyXG4gIGhlaWdodDogNDIwcHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogI2JiYjgwNjsgKi9cclxufVxyXG5cclxuI2xlZnRcclxue1xyXG4gIHdpZHRoOiA1ODBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiBhcXVhOyAqL1xyXG4gIGZsb2F0OiBsZWZ0O1xyXG59XHJcblxyXG4jc2VhcmNoXHJcbntcclxuICBtYXJnaW4tdG9wOiAwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICAvKiB3aWR0aDogNjBweDtcclxuICBoZWlnaHQ6IDIwcHg7ICovXHJcbiAgd2lkdGg6IDcwcHg7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgdmVydGljYWwtYWxpZ246IDBweDtcclxuICAvKiBib3JkZXItY29sb3I6ICNlY2YwMjI7ICovXHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAvKiBjb2xvcjogI2UwODcxMjsgKi9cclxuICBib3JkZXItY29sb3I6ICM3YzU2MDQ7XHJcbiAgY29sb3I6ICMwYzBiMGE7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI0NSwgMjQ1LCA4OCk7XHJcbn1cclxuXHJcbiNyaWdodFxyXG57XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgcGFkZGluZy10b3A6IDVweDtcclxuICB3aWR0aDogNDAwcHg7XHJcbiAgLyogaGVpZ2h0OiA0MDBweDsgICAqL1xyXG4gIC8qIHRleHQtYWxpZ246IGNlbnRlcjsgKi9cclxuICAvKiBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ2xvZ29fdHJhbnNwYXJlbnQucG5nJyk7XHJcbiAgYm9yZGVyLWNvbG9yOiBibGFjazsgKi9cclxuICBib3JkZXItd2lkdGg6IDJweDsgXHJcbn0gXHJcblxyXG4vKiAjcmlnaHRUb3BcclxueyAqL1xyXG4gIC8qIHRleHQtYWxpZ246IGNlbnRlcjsgKi9cclxuLyogfSAgKi9cclxuXHJcbiNyaWdodERvd25cclxue1xyXG4gIHdpZHRoOiAzNTBweDtcclxuICBoZWlnaHQ6IDM0MHB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnbG9nb190cmFuc3BhcmVudC5wbmcnKTtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiAjMDMyYzA5OyAqL1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufSBcclxuXHJcbi5taWRkbGVcclxue1xyXG4gIHdpZHRoOiA5ODBweDtcclxuICBoZWlnaHQ6IDVweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiBibHVlOyAqL1xyXG59XHJcblxyXG4udXNlcjpob3ZlclxyXG57XHJcbiAgLyogYm9yZGVyLXN0eWxlOiBkYXNoZWQ7ICovXHJcbiAgYm9yZGVyLXN0eWxlOiBub25lO1xyXG4gIGJvcmRlci1jb2xvcjogIzE2NDEwOTtcclxuICAvKiBib3JkZXItY29sb3I6cmdiKDEyOSwgMjIzLCA3NSk7ICovXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDEyOSwgMjIzLCA3NSk7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcblxyXG4jTG9nb3V0SG9tZVBhZ2Vcclxue1xyXG4gIC8qIHRleHQtYWxpZ246IGxlZnQ7ICovXHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBwYWRkaW5nLWxlZnQ6IDE0MHB4O1xyXG4gIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgLyogd2lkdGg6IDkwcHg7ICovXHJcbiAgY29sb3I6IHJnYig5LCAxNDUsIDYxKTtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjUzLCAyMjgsIDIpOyAqL1xyXG59XHJcblxyXG4jTG9nb3V0SG9tZVBhZ2U6aG92ZXJcclxue1xyXG4gIGNvbG9yOiAjMDMyYzA5O1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxufVxyXG5cclxuXHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/home2/home2.component.html":
/*!********************************************!*\
  !*** ./src/app/home2/home2.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"titleApp\">\r\n  <h1>\r\n    {{ title }} \r\n  </h1>\r\n</div> \r\n<div id=\"container2\">\r\n    <div id=\"left\">\r\n      <h2>Nadzorowani pracownicy</h2>\r\n      <ul>\r\n        <li *ngFor=\"let user of users | paginate: { itemsPerPage: 10, currentPage: p }\">\r\n          <div class = \"user\" routerLink=\"/details/{{user.id}}\" style=\"color:black; text-decoration: none; font-size: 23px\">\r\n            <p>\r\n                {{ user.username }} - {{user.computerName}}\r\n            </p>\r\n          </div>\r\n        </li>\r\n      </ul>\r\n      \r\n    </div>\r\n    <div id=\"right\">\r\n      <div id=\"rightTop\">\r\n        <div id=\"filtr\">\r\n          <input id= \"input\" type=\"text\" [(ngModel)]='query' placeholder= \"Wyszukaj użytkownika\"/>\r\n          <button id= \"search\" (click)=search()>Szukaj</button>\r\n        </div>\r\n        <div id = \"LogoutHomePage\" (click)=\"logout()\" >\r\n            Wyloguj \r\n        </div> \r\n      </div>\r\n      <div id=\"rightDown\">\r\n      </div>\r\n\r\n    </div>\r\n    <div class=\"middle\" style= \"clear:both\"></div>\r\n    <pagination-controls (pageChange)=\"p = $event\" previousLabel=\"Poprzednia\"\r\n        nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\">\r\n      </pagination-controls>\r\n  </div>\r\n\r\n<!-- <div>\r\n  <a routerLink=\"\" style=\"color:black; text-decoration: none; font-size: 23px\">sekretne przejscie do logowania</a>\r\n</div> -->\r\n<!-- <div class=\"containerX\">\r\n  <div class=\"col-md-6 col-md-offset-3 loginBox\">\r\n    Welcome,  {{id}}\r\n    <a href=\"javascript:void(0);\" (click)=\"logout()\">Logout</a>\r\n  </div>\r\n</div> -->\r\n\r\n \r\n\r\n  \r\n\r\n"

/***/ }),

/***/ "./src/app/home2/home2.component.ts":
/*!******************************************!*\
  !*** ./src/app/home2/home2.component.ts ***!
  \******************************************/
/*! exports provided: Home2Component */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Home2Component", function() { return Home2Component; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../auth.service */ "./src/app/auth.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var Home2Component = /** @class */ (function () {
    function Home2Component(http, data, router, authService, authenticationService) {
        this.http = http;
        this.data = data;
        this.router = router;
        this.authService = authService;
        this.authenticationService = authenticationService;
        this.title = 'LogIT';
        this.p = 1;
        //NA NIE LADOWANIE SIE W ODPOWIEDNIM MOMENCIE (CO SKUTKOWALO NIE WYSWIETLENIEM SIE) USEROW POMOGLO
        // PRZENIESIENIE TEGO DO KONSTRUKTORA
        // this.data.getUsers().subscribe(
        //   data => this.users$ = data,
        //   // data => {
        //   //             console.log('GET USERS ');
        //   //           },
        //   error => {
        //     console.log('Error GET USERS :( ', localStorage.getItem('token'), error);
        //   }
        // );
    }
    Home2Component.prototype.ngOnInit = function () {
        var _this = this;
        this.ionViewDidLoad();
        this.data.getUsers().subscribe(function (data) { return _this.users = data; }, 
        // data => {
        //             console.log('GET USERS ');
        //           },
        function (error) {
            console.log('Error GET USERS :( ', localStorage.getItem('token'), error);
        });
        // this.subscription = timer(0, 2000).pipe(
        //   switchMap(() => this.data.getUsers())
        // // ).subscribe(data => console.log('poszlo', data));
        // ).subscribe(data => this.users$ = data);
    };
    Home2Component.prototype.search = function () {
        var _this = this;
        if (this.query === "") {
            this.data.getUsers().subscribe(function (data) { return _this.users = data; }, function (error) {
                console.log('Error GET USERS :( ', localStorage.getItem('token'), error);
            });
        }
        else {
            this.data.getFilteredUsers(this.query).subscribe(function (data) { return _this.users = data; }, // MOZE WLASNIE NA USERS SAMYM POWINNO BYC, MOZE FILTEREDUSERS NIEPOTRZEBNE
            // data => {
            //             console.log('GET USERS ');
            //           },
            function (// MOZE WLASNIE NA USERS SAMYM POWINNO BYC, MOZE FILTEREDUSERS NIEPOTRZEBNE
            error) {
                console.log('Error GET FILTERED USERS :( ', localStorage.getItem('token'), error);
            });
            this.query = "";
        }
    };
    // ionViewDidEnter() 
    // ionViewWillEnter()
    Home2Component.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log("ionViewDidLoad");
        this.data.getUsers().subscribe(function (data) { return _this.users = data; }, 
        // data => {
        //             console.log('GET USERS ');
        //           },
        function (error) {
            console.log('Error GET USERS :( ', localStorage.getItem('token'), error);
        });
    };
    Home2Component.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    Home2Component = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home2',
            template: __webpack_require__(/*! ./home2.component.html */ "./src/app/home2/home2.component.html"),
            styles: [__webpack_require__(/*! ./home2.component.css */ "./src/app/home2/home2.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_2__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"], _auth_service__WEBPACK_IMPORTED_MODULE_3__["AuthService"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], Home2Component);
    return Home2Component;
}());



/***/ }),

/***/ "./src/app/keylogger/keylogger.component.css":
/*!***************************************************!*\
  !*** ./src/app/keylogger/keylogger.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#container\r\n{\r\n  height: 430px;\r\n}\r\n\r\n#blue_tile{\r\n  background-color: #1552ad;\r\n  width: 380px;\r\n  height: 70px;\r\n  float: left;\r\n  margin: 20px;\r\n  box-shadow:inset 0 0 80px #000000;\r\n  padding-top: 60px;\r\n  /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n  -webkit-text-decoration-line: overline;\r\n\r\n          text-decoration-line: overline;\r\n  background-image: url('ustawienia_filtrow1.jpg');\r\n  /* color: blanchedalmond; */\r\n  color: rgb(8, 165, 3);\r\n}\r\n\r\n#blue_tile:hover\r\n{\r\n  box-shadow:inset 0 0 10px #000000;\r\n  cursor:pointer;\r\n}\r\n\r\n#green_tile{\r\n  background-color: #5aa11f;\r\n  width: 380px;\r\n  height: 70px;\r\n  float: left;\r\n  margin: 20px;\r\n  box-shadow:inset 0 0 80px #000000;\r\n  padding-top: 60px;\r\n  /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n  -webkit-text-decoration-line: overline;\r\n\r\n          text-decoration-line: overline; \r\n  background-image: url('monitorowanie3.jpg');\r\n  /* color: blanchedalmond; */\r\n  color: rgb(8, 165, 3);\r\n}\r\n\r\n#green_tile:hover\r\n{\r\n  box-shadow:inset 0 0 10px #000000;\r\n  cursor:pointer;\r\n}\r\n\r\n.middle\r\n{\r\n  width: 800px;\r\n  height: 5px;\r\n}\r\n\r\n#top\r\n{\r\n  width: 980px;\r\n  height: 100px;\r\n  padding: 10px;\r\n}\r\n\r\n#down\r\n{\r\n  width: 850px;\r\n  height: 150px;\r\n  margin-left: 75px;\r\n  margin-bottom: 5px;\r\n  margin-top: 5px;\r\n}\r\n\r\n.toggle-label {\r\n  position: relative;\r\n  display: block;\r\n  width: 240px;\r\n  height: 80px;\r\n  margin-top: 8px;\r\n  border: 1px solid #808080;\r\n  margin:  20px auto;\r\n}\r\n\r\n.toggle-label input[type=checkbox] { \r\n  opacity: 0;\r\n  position: absolute;\r\n  width: 100%;\r\n  height: 100%;\r\n}\r\n\r\n.toggle-label input[type=checkbox]+.back {\r\n  position: absolute;\r\n  width: 100%;\r\n  height: 100%;\r\n  background: #ed1c24;\r\n  transition: background 150ms linear;  \r\n}\r\n\r\n.toggle-label input[type=checkbox]:checked+.back {\r\n  background: #00a651; /*green*/\r\n}\r\n\r\n.toggle-label input[type=checkbox]+.back .toggle {\r\n  display: block;\r\n  position: absolute;\r\n  content: ' ';\r\n  background: #fff;\r\n  width: 50%; \r\n  height: 100%;\r\n  transition: margin 80ms linear;\r\n  border: 1px solid #808080;\r\n  border-radius: 0;\r\n}\r\n\r\n.toggle-label input[type=checkbox]:checked+.back .toggle {\r\n  margin-left: 120px;\r\n}\r\n\r\n.toggle-label .label {\r\n  display: block;\r\n  position: absolute;\r\n  width: 50%;\r\n  color: #ddd;\r\n  line-height: 80px;\r\n  text-align: center;\r\n  font-size: 2em;\r\n}\r\n\r\n.toggle-label .label.on { left: 0px; }\r\n\r\n.toggle-label .label.off { right: 0px; }\r\n\r\n.toggle-label input[type=checkbox]:checked+.back .label.on {\r\n  color: #fff;\r\n}\r\n\r\n.toggle-label input[type=checkbox]+.back .label.off {\r\n  color: #fff;\r\n}\r\n\r\n.toggle-label input[type=checkbox]:checked+.back .label.off {\r\n  color: #ddd;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAva2V5bG9nZ2VyL2tleWxvZ2dlci5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLGNBQWM7Q0FDZjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLFlBQVk7RUFDWixhQUFhO0VBQ2Isa0NBQWtDO0VBQ2xDLGtCQUFrQjtFQUNsQiwrRUFBK0U7O0VBRS9FLHVDQUErQjs7VUFBL0IsK0JBQStCO0VBQy9CLGlEQUFpRDtFQUNqRCw0QkFBNEI7RUFDNUIsc0JBQXNCO0NBQ3ZCOztBQUVEOztFQUVFLGtDQUFrQztFQUNsQyxlQUFlO0NBQ2hCOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixrQ0FBa0M7RUFDbEMsa0JBQWtCO0VBQ2xCLCtFQUErRTs7RUFFL0UsdUNBQStCOztVQUEvQiwrQkFBK0I7RUFDL0IsNENBQTRDO0VBQzVDLDRCQUE0QjtFQUM1QixzQkFBc0I7Q0FDdkI7O0FBRUQ7O0VBRUUsa0NBQWtDO0VBQ2xDLGVBQWU7Q0FDaEI7O0FBR0Q7O0VBRUUsYUFBYTtFQUNiLFlBQVk7Q0FDYjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGNBQWM7Q0FDZjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0NBQ2pCOztBQUVEO0VBQ0UsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixhQUFhO0VBQ2IsYUFBYTtFQUNiLGdCQUFnQjtFQUNoQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0NBQ3BCOztBQUNEO0VBQ0UsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixZQUFZO0VBQ1osYUFBYTtDQUNkOztBQUNEO0VBQ0UsbUJBQW1CO0VBQ25CLFlBQVk7RUFDWixhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLG9DQUFvQztDQUNyQzs7QUFDRDtFQUNFLG9CQUFvQixDQUFDLFNBQVM7Q0FDL0I7O0FBRUQ7RUFDRSxlQUFlO0VBQ2YsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsV0FBVztFQUNYLGFBQWE7RUFDYiwrQkFBK0I7RUFDL0IsMEJBQTBCO0VBQzFCLGlCQUFpQjtDQUNsQjs7QUFDRDtFQUNFLG1CQUFtQjtDQUNwQjs7QUFDRDtFQUNFLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsV0FBVztFQUNYLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7Q0FDaEI7O0FBQ0QsMEJBQTBCLFVBQVUsRUFBRTs7QUFDdEMsMkJBQTJCLFdBQVcsRUFBRTs7QUFFeEM7RUFDRSxZQUFZO0NBQ2I7O0FBQ0Q7RUFDRSxZQUFZO0NBQ2I7O0FBQ0Q7RUFDRSxZQUFZO0NBQ2IiLCJmaWxlIjoic3JjL2FwcC9rZXlsb2dnZXIva2V5bG9nZ2VyLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyXHJcbntcclxuICBoZWlnaHQ6IDQzMHB4O1xyXG59XHJcblxyXG4jYmx1ZV90aWxle1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxNTUyYWQ7XHJcbiAgd2lkdGg6IDM4MHB4O1xyXG4gIGhlaWdodDogNzBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW46IDIwcHg7XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgODBweCAjMDAwMDAwO1xyXG4gIHBhZGRpbmctdG9wOiA2MHB4O1xyXG4gIC8qdHV0YWogamFraWXFmyBkb2RhdGtvd2Ugc3R5bGUsIGphayBucC4gYm94LXNoYWRvdywgdGV4dC1zaGFkb3csIGJvcmRlciwgaXRwLiovXHJcblxyXG4gIHRleHQtZGVjb3JhdGlvbi1saW5lOiBvdmVybGluZTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ3VzdGF3aWVuaWFfZmlsdHJvdzEuanBnJyk7XHJcbiAgLyogY29sb3I6IGJsYW5jaGVkYWxtb25kOyAqL1xyXG4gIGNvbG9yOiByZ2IoOCwgMTY1LCAzKTtcclxufVxyXG5cclxuI2JsdWVfdGlsZTpob3ZlclxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTBweCAjMDAwMDAwO1xyXG4gIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG4jZ3JlZW5fdGlsZXtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWFhMTFmO1xyXG4gIHdpZHRoOiAzODBweDtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDtcclxuICBwYWRkaW5nLXRvcDogNjBweDtcclxuICAvKnR1dGFqIGpha2llxZsgZG9kYXRrb3dlIHN0eWxlLCBqYWsgbnAuIGJveC1zaGFkb3csIHRleHQtc2hhZG93LCBib3JkZXIsIGl0cC4qL1xyXG5cclxuICB0ZXh0LWRlY29yYXRpb24tbGluZTogb3ZlcmxpbmU7IFxyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnbW9uaXRvcm93YW5pZTMuanBnJyk7XHJcbiAgLyogY29sb3I6IGJsYW5jaGVkYWxtb25kOyAqL1xyXG4gIGNvbG9yOiByZ2IoOCwgMTY1LCAzKTtcclxufVxyXG5cclxuI2dyZWVuX3RpbGU6aG92ZXJcclxue1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwcHggIzAwMDAwMDtcclxuICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuXHJcbi5taWRkbGVcclxue1xyXG4gIHdpZHRoOiA4MDBweDtcclxuICBoZWlnaHQ6IDVweDtcclxufVxyXG5cclxuI3RvcFxyXG57XHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxufVxyXG5cclxuI2Rvd25cclxue1xyXG4gIHdpZHRoOiA4NTBweDtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiA3NXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbn1cclxuXHJcbi50b2dnbGUtbGFiZWwge1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMjQwcHg7XHJcbiAgaGVpZ2h0OiA4MHB4O1xyXG4gIG1hcmdpbi10b3A6IDhweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjODA4MDgwO1xyXG4gIG1hcmdpbjogIDIwcHggYXV0bztcclxufVxyXG4udG9nZ2xlLWxhYmVsIGlucHV0W3R5cGU9Y2hlY2tib3hdIHsgXHJcbiAgb3BhY2l0eTogMDtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbi50b2dnbGUtbGFiZWwgaW5wdXRbdHlwZT1jaGVja2JveF0rLmJhY2sge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogI2VkMWMyNDtcclxuICB0cmFuc2l0aW9uOiBiYWNrZ3JvdW5kIDE1MG1zIGxpbmVhcjsgIFxyXG59XHJcbi50b2dnbGUtbGFiZWwgaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCsuYmFjayB7XHJcbiAgYmFja2dyb3VuZDogIzAwYTY1MTsgLypncmVlbiovXHJcbn1cclxuXHJcbi50b2dnbGUtbGFiZWwgaW5wdXRbdHlwZT1jaGVja2JveF0rLmJhY2sgLnRvZ2dsZSB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGNvbnRlbnQ6ICcgJztcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIHdpZHRoOiA1MCU7IFxyXG4gIGhlaWdodDogMTAwJTtcclxuICB0cmFuc2l0aW9uOiBtYXJnaW4gODBtcyBsaW5lYXI7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzgwODA4MDtcclxuICBib3JkZXItcmFkaXVzOiAwO1xyXG59XHJcbi50b2dnbGUtbGFiZWwgaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCsuYmFjayAudG9nZ2xlIHtcclxuICBtYXJnaW4tbGVmdDogMTIwcHg7XHJcbn1cclxuLnRvZ2dsZS1sYWJlbCAubGFiZWwge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB3aWR0aDogNTAlO1xyXG4gIGNvbG9yOiAjZGRkO1xyXG4gIGxpbmUtaGVpZ2h0OiA4MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDJlbTtcclxufVxyXG4udG9nZ2xlLWxhYmVsIC5sYWJlbC5vbiB7IGxlZnQ6IDBweDsgfVxyXG4udG9nZ2xlLWxhYmVsIC5sYWJlbC5vZmYgeyByaWdodDogMHB4OyB9XHJcblxyXG4udG9nZ2xlLWxhYmVsIGlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWQrLmJhY2sgLmxhYmVsLm9uIHtcclxuICBjb2xvcjogI2ZmZjtcclxufVxyXG4udG9nZ2xlLWxhYmVsIGlucHV0W3R5cGU9Y2hlY2tib3hdKy5iYWNrIC5sYWJlbC5vZmYge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG59XHJcbi50b2dnbGUtbGFiZWwgaW5wdXRbdHlwZT1jaGVja2JveF06Y2hlY2tlZCsuYmFjayAubGFiZWwub2ZmIHtcclxuICBjb2xvcjogI2RkZDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/keylogger/keylogger.component.html":
/*!****************************************************!*\
  !*** ./src/app/keylogger/keylogger.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}} \n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n    <div id=\"sectionTitle\">\n      <h1>\n        {{ title }}\n      </h1>\n    </div>\n    <div id=\"back\">\n      <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n    </div>\n    <div id=\"home\">\n        <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n          <i class=\"material-icons\">home</i>\n        </a>\n    </div>\n</div>\n<div id=\"container\" >\n  <div id=\"top\">\n    Czy nadzorować aktywność użytkownika {{user$.username}}?\n    <!-- {{setting$.areMessagesMonitored}} -->\n    <label class='toggle-label' >\n      <!-- <input type=\"checkbox\" [(ngModel)]='isActive'/> -->\n      <input type=\"checkbox\" [(ngModel)]=\"setting$['areMessagesMonitored']\" (click)= \"update(setting$.id)\"/>\n        <span class='back'>\n        <span class='toggle'></span>\n        <span class='label on'>ON</span>\n        <span class='label off'>OFF</span>  \n      </span>\n    </label>\n  </div>\n  <div class=\"middle\" style= \"clear:both\"></div>\n  <!-- <p>{{isActive}}</p> -->\n  <div id=\"down\" >\n    <div id=\"blue_tile\" routerLink=\"/filters/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\n      <div class=\"black\">\n        <i class=\"material-icons\">settings</i> Ustawienia filtrów\n      </div>\n    </div>\n    <div id=\"green_tile\" routerLink=\"/messages/{{user$.id}}\" style=\"text-align:center; color: white; text-decoration: none; font-weight: 700\" >\n      <div class=\"black\">\n        <i class=\"material-icons\">message</i> Podgląd rejestratora \n      </div>\n      <!-- <i class=\"material-icons\">keyboard</i> -->\n    </div>\n  </div>\n  <div class=\"middle\" style= \"clear:both\"></div>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/keylogger/keylogger.component.ts":
/*!**************************************************!*\
  !*** ./src/app/keylogger/keylogger.component.ts ***!
  \**************************************************/
/*! exports provided: KeyloggerComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "KeyloggerComponent", function() { return KeyloggerComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var KeyloggerComponent = /** @class */ (function () {
    function KeyloggerComponent(http, route, data, location, authenticationService) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = "Monitorowanie aktywności";
        this.route.params.subscribe(function (params) { return _this.user$ = params.id; });
        this.token = localStorage.getItem('token');
        this.logName = localStorage.getItem('username');
    }
    KeyloggerComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    KeyloggerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.user$).subscribe(function (data) { return _this.user$ = data; });
        this.data.getKeyloggerSettings(this.user$).subscribe(
        // data => this.messages$ = data 
        function (data) { return _this.setting$ = data; }
        // data => console.log(data)
        );
        // this.logName =  localStorage.getItem('username');
        // this.isA = this.setting$['areMessagesMonitored'];
    };
    KeyloggerComponent.prototype.update = function (settingsId) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/keylogger-settings/' + settingsId + '/change-messages-monitor', {
        // "id": 1,
        // "keyloggerUserId": 1,
        // "areMessagesMonitored": 1
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful ", data);
        }, function (error) {
            console.log("Error in put request Iwona :( ", error);
        });
    };
    KeyloggerComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    KeyloggerComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-keylogger',
            template: __webpack_require__(/*! ./keylogger.component.html */ "./src/app/keylogger/keylogger.component.html"),
            styles: [__webpack_require__(/*! ./keylogger.component.css */ "./src/app/keylogger/keylogger.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_6__["AuthenticationService"]])
    ], KeyloggerComponent);
    return KeyloggerComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#titleApp\r\n{\r\n  box-shadow:inset 0 0 2000px #65c709;\r\n  width: 1000px;\r\n  height: 150px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  padding: 10px;\r\n  padding-top: 0px;\r\n  font-size: 50px;\r\n  color: #044404;\r\n  text-align: center;\r\n  border-bottom-style: groove;\r\n  border-radius: 20px;\r\n  margin-top: -50px;\r\n}\r\n\r\n#container2\r\n{\r\n  /* box-shadow:inset 0 0 300px #bbb806; */\r\n  width: 980px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  padding: 10px;\r\n  padding-top: 0px;\r\n  /* color: #164109; */\r\n  font-size: 22px;\r\n}\r\n\r\nhtml { height: 100% }\r\n\r\n::-moz-selection { background: #fe57a1; color: #fff; text-shadow: none; }\r\n\r\n::selection { background: #fe57a1; color: #fff; text-shadow: none; }\r\n\r\nbody { background-image: radial-gradient( cover, rgba(92,100,111,1) 0%,rgba(31,35,40,1) 100%), url('http://i.minus.com/io97fW9I0NqJq.png') }\r\n\r\n.login {\r\n  /* background: #9eb905; */\r\n  box-shadow:inset 0 0 2000px #06df06;\r\n  border: 3px solid #090a01;\r\n  border-radius: 6px;\r\n  height: 300px;\r\n  margin: 10px auto 0;\r\n  width: 298px;\r\n}\r\n\r\n.login h1 {\r\n  background-image: linear-gradient(top, #f1f3f3, #d4dae0);\r\n  border-bottom: 1px solid #a6abaf;\r\n  border-radius: 6px 6px 0 0;\r\n  box-sizing: border-box;\r\n  color: #727678;\r\n  display: block;\r\n  height: 43px;\r\n  font: 600 14px/1 'Open Sans', sans-serif;\r\n  padding-top: 14px;\r\n  margin: 0;\r\n  text-align: center;\r\n  text-shadow: 0 -1px 0 rgba(0,0,0,0.2), 0 1px 0 #fff;\r\n}\r\n\r\ninput[type=\"password\"], input[type=\"text\"] {\r\n  background: url('http://i.minus.com/ibhqW9Buanohx2.png') center left no-repeat, linear-gradient(top, #d6d7d7, #dee0e0);\r\n  border: 1px solid #125003;\r\n  border-radius: 4px;\r\n  box-shadow: 0 1px rgb(255, 255, 255);\r\n  box-sizing: border-box;\r\n  color: #252424;\r\n  height: 39px;\r\n  margin: 31px 0 0 29px;\r\n  padding-left: 37px;\r\n  transition: box-shadow 0.3s;\r\n  width: 240px;\r\n  /* background-color: #e5fcaf; */\r\n  background-color: #9ce755;\r\n}\r\n\r\ninput[type=\"password\"]:focus, input[type=\"text\"]:focus {\r\n  box-shadow: 0 0 4px 1px rgba(55, 166, 155, 0.3);\r\n  outline: 0;\r\n}\r\n\r\n.show-password {\r\n  display: block;\r\n  height: 16px;\r\n  margin: 26px 0 0 28px;\r\n  width: 87px;\r\n}\r\n\r\ninput[type=\"checkbox\"] {\r\n  cursor: pointer;\r\n  height: 16px;\r\n  opacity: 0;\r\n  position: relative;\r\n  width: 64px;\r\n}\r\n\r\ninput[type=\"checkbox\"]:checked {\r\n  left: 29px;\r\n  width: 58px;\r\n}\r\n\r\n.toggle {\r\n  background: url(http://i.minus.com/ibitS19pe8PVX6.png) no-repeat;\r\n  display: block;\r\n  height: 16px;\r\n  margin-top: -20px;\r\n  width: 87px;\r\n  z-index: -1;\r\n}\r\n\r\ninput[type=\"checkbox\"]:checked + .toggle { background-position: 0 -16px }\r\n\r\n.forgot {\r\n  color: #7f7f7f;\r\n  display: inline-block;\r\n  /* float: right; */\r\n  font: 12px/1 sans-serif;\r\n  left: -19px;\r\n  position: relative;\r\n  text-decoration: none;\r\n  top: 5px;\r\n  transition: color .4s;\r\n}\r\n\r\n.forgot:hover { color: #3b3b3b }\r\n\r\ninput[type=\"submit\"] {\r\n  width:240px;\r\n  height:35px;\r\n  display:block;\r\n  font-family:Arial, \"Helvetica\", sans-serif;\r\n  font-size:16px;\r\n  font-weight:bold;\r\n  color:#044404;\r\n  text-decoration:none;\r\n  text-transform:uppercase;\r\n  text-align:center;\r\n  text-shadow:1px 1px 0px rgb(243, 247, 30);\r\n  padding-top:6px;\r\n  margin: 29px 0 0 29px;\r\n  position:relative;\r\n  cursor:pointer;\r\n  border: none;  \r\n  background-color: #b6db0d;\r\n  /* background-image: linear-gradient(top,#3db0a6,#3111); */\r\n  border-top-left-radius: 5px;\r\n  border-top-right-radius: 5px;\r\n  border-bottom-right-radius: 5px;\r\n  border-bottom-left-radius:5px;\r\n  /* box-shadow: inset 0px 1px 0px #2ab7ec, 0px 5px 0px 0px #497a78, 0px 10px 5px #999; */\r\n  box-shadow: inset 0px 1px 0px #b6db0d, 0px 2px 0px 0px #f3f72a, 0px 5px 3px rgb(243, 247, 30);\r\n}\r\n\r\n.shadow {\r\n  background: #000;\r\n  border-radius: 12px 12px 4px 4px;\r\n  box-shadow: 0 0 20px 10px #000;\r\n  height: 12px;\r\n  margin: 30px auto;\r\n  opacity: 0.2;\r\n  width: 270px;\r\n}\r\n\r\ninput[type=\"submit\"]:active {\r\n  top:3px;\r\n  box-shadow: inset 0px 1px 0px #b6db0d, 0px 2px 0px 0px #f3f72a, 0px 5px 3px rgb(243, 247, 30);\r\n  color: #044404;\r\n}\r\n\r\n#background\r\n{\r\n  padding: 10px;\r\n  margin-top: 20px;\r\n  margin-bottom: 20px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  width: 980px;\r\n  height: 370px;\r\n  background-image: url('loginBackground.jpg');\r\n  border-radius: 20px;\r\n}\r\n\r\n#comboBox\r\n{\r\n  margin-top: 20px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  background-color: #9ce755;\r\n  width: 240px;\r\n  height: 39px;\r\n  margin: 31px 0 0 29px;\r\n  padding-left: 37px;\r\n  border: 1px solid #125003;\r\n  border-radius: 4px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7RUFFRSxvQ0FBb0M7RUFDcEMsY0FBYztFQUNkLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGVBQWU7RUFDZixtQkFBbUI7RUFDbkIsNEJBQTRCO0VBQzVCLG9CQUFvQjtFQUNwQixrQkFBa0I7Q0FDbkI7O0FBRUQ7O0VBRUUseUNBQXlDO0VBQ3pDLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGlCQUFpQjtFQUNqQixjQUFjO0VBQ2QsaUJBQWlCO0VBQ2pCLHFCQUFxQjtFQUNyQixnQkFBZ0I7Q0FDakI7O0FBRUQsT0FBTyxZQUFZLEVBQUU7O0FBQ3JCLG1CQUFtQixvQkFBb0IsQ0FBQyxZQUFZLENBQUMsa0JBQWtCLEVBQUU7O0FBQ3pFLGNBQWMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLGtCQUFrQixFQUFFOztBQUNwRSxPQUFPLG1JQUFtSSxFQUFFOztBQUM1STtFQUNFLDBCQUEwQjtFQUMxQixvQ0FBb0M7RUFDcEMsMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQixjQUFjO0VBQ2Qsb0JBQW9CO0VBQ3BCLGFBQWE7Q0FDZDs7QUFDRDtFQUNFLHlEQUF5RDtFQUN6RCxpQ0FBaUM7RUFDakMsMkJBQTJCO0VBQzNCLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsZUFBZTtFQUNmLGFBQWE7RUFDYix5Q0FBeUM7RUFDekMsa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixtQkFBbUI7RUFDbkIsb0RBQW9EO0NBQ3JEOztBQUNEO0VBQ0UsdUhBQXVIO0VBQ3ZILDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIscUNBQXFDO0VBQ3JDLHVCQUF1QjtFQUN2QixlQUFlO0VBQ2YsYUFBYTtFQUNiLHNCQUFzQjtFQUN0QixtQkFBbUI7RUFDbkIsNEJBQTRCO0VBQzVCLGFBQWE7RUFDYixnQ0FBZ0M7RUFDaEMsMEJBQTBCO0NBQzNCOztBQUNEO0VBQ0UsZ0RBQWdEO0VBQ2hELFdBQVc7Q0FDWjs7QUFDRDtFQUNFLGVBQWU7RUFDZixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLFlBQVk7Q0FDYjs7QUFDRDtFQUNFLGdCQUFnQjtFQUNoQixhQUFhO0VBQ2IsV0FBVztFQUNYLG1CQUFtQjtFQUNuQixZQUFZO0NBQ2I7O0FBQ0Q7RUFDRSxXQUFXO0VBQ1gsWUFBWTtDQUNiOztBQUNEO0VBQ0UsaUVBQWlFO0VBQ2pFLGVBQWU7RUFDZixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixZQUFZO0NBQ2I7O0FBQ0QsMkNBQTJDLDRCQUE0QixFQUFFOztBQUN6RTtFQUNFLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsbUJBQW1CO0VBQ25CLHdCQUF3QjtFQUN4QixZQUFZO0VBQ1osbUJBQW1CO0VBQ25CLHNCQUFzQjtFQUN0QixTQUFTO0VBQ1Qsc0JBQXNCO0NBQ3ZCOztBQUNELGdCQUFnQixjQUFjLEVBQUU7O0FBQ2hDO0VBQ0UsWUFBWTtFQUNaLFlBQVk7RUFDWixjQUFjO0VBQ2QsMkNBQTJDO0VBQzNDLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsY0FBYztFQUNkLHFCQUFxQjtFQUNyQix5QkFBeUI7RUFDekIsa0JBQWtCO0VBQ2xCLDBDQUEwQztFQUMxQyxnQkFBZ0I7RUFDaEIsc0JBQXNCO0VBQ3RCLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsYUFBYTtFQUNiLDBCQUEwQjtFQUMxQiwyREFBMkQ7RUFDM0QsNEJBQTRCO0VBQzVCLDZCQUE2QjtFQUM3QixnQ0FBZ0M7RUFDaEMsOEJBQThCO0VBQzlCLHdGQUF3RjtFQUN4Riw4RkFBOEY7Q0FDL0Y7O0FBRUQ7RUFDRSxpQkFBaUI7RUFDakIsaUNBQWlDO0VBQ2pDLCtCQUErQjtFQUMvQixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLGFBQWE7RUFDYixhQUFhO0NBQ2Q7O0FBR0Q7RUFDRSxRQUFRO0VBQ1IsOEZBQThGO0VBQzlGLGVBQWU7Q0FDaEI7O0FBRUQ7O0VBRUUsY0FBYztFQUNkLGlCQUFpQjtFQUNqQixvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsY0FBYztFQUNkLDZDQUE2QztFQUM3QyxvQkFBb0I7Q0FDckI7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2Isc0JBQXNCO0VBQ3RCLG1CQUFtQjtFQUNuQiwwQkFBMEI7RUFDMUIsbUJBQW1CO0NBQ3BCIiwiZmlsZSI6InNyYy9hcHAvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIiN0aXRsZUFwcFxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMjAwMHB4ICM2NWM3MDk7XHJcbiAgd2lkdGg6IDEwMDBweDtcclxuICBoZWlnaHQ6IDE1MHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIGNvbG9yOiAjMDQ0NDA0O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBib3JkZXItYm90dG9tLXN0eWxlOiBncm9vdmU7XHJcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcclxuICBtYXJnaW4tdG9wOiAtNTBweDtcclxufVxyXG5cclxuI2NvbnRhaW5lcjJcclxue1xyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDMwMHB4ICNiYmI4MDY7ICovXHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgcGFkZGluZy10b3A6IDBweDtcclxuICAvKiBjb2xvcjogIzE2NDEwOTsgKi9cclxuICBmb250LXNpemU6IDIycHg7XHJcbn1cclxuXHJcbmh0bWwgeyBoZWlnaHQ6IDEwMCUgfVxyXG46Oi1tb3otc2VsZWN0aW9uIHsgYmFja2dyb3VuZDogI2ZlNTdhMTsgY29sb3I6ICNmZmY7IHRleHQtc2hhZG93OiBub25lOyB9XHJcbjo6c2VsZWN0aW9uIHsgYmFja2dyb3VuZDogI2ZlNTdhMTsgY29sb3I6ICNmZmY7IHRleHQtc2hhZG93OiBub25lOyB9XHJcbmJvZHkgeyBiYWNrZ3JvdW5kLWltYWdlOiByYWRpYWwtZ3JhZGllbnQoIGNvdmVyLCByZ2JhKDkyLDEwMCwxMTEsMSkgMCUscmdiYSgzMSwzNSw0MCwxKSAxMDAlKSwgdXJsKCdodHRwOi8vaS5taW51cy5jb20vaW85N2ZXOUkwTnFKcS5wbmcnKSB9XHJcbi5sb2dpbiB7XHJcbiAgLyogYmFja2dyb3VuZDogIzllYjkwNTsgKi9cclxuICBib3gtc2hhZG93Omluc2V0IDAgMCAyMDAwcHggIzA2ZGYwNjtcclxuICBib3JkZXI6IDNweCBzb2xpZCAjMDkwYTAxO1xyXG4gIGJvcmRlci1yYWRpdXM6IDZweDtcclxuICBoZWlnaHQ6IDMwMHB4O1xyXG4gIG1hcmdpbjogMTBweCBhdXRvIDA7XHJcbiAgd2lkdGg6IDI5OHB4O1xyXG59XHJcbi5sb2dpbiBoMSB7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvcCwgI2YxZjNmMywgI2Q0ZGFlMCk7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkICNhNmFiYWY7XHJcbiAgYm9yZGVyLXJhZGl1czogNnB4IDZweCAwIDA7XHJcbiAgYm94LXNpemluZzogYm9yZGVyLWJveDtcclxuICBjb2xvcjogIzcyNzY3ODtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBoZWlnaHQ6IDQzcHg7XHJcbiAgZm9udDogNjAwIDE0cHgvMSAnT3BlbiBTYW5zJywgc2Fucy1zZXJpZjtcclxuICBwYWRkaW5nLXRvcDogMTRweDtcclxuICBtYXJnaW46IDA7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHRleHQtc2hhZG93OiAwIC0xcHggMCByZ2JhKDAsMCwwLDAuMiksIDAgMXB4IDAgI2ZmZjtcclxufVxyXG5pbnB1dFt0eXBlPVwicGFzc3dvcmRcIl0sIGlucHV0W3R5cGU9XCJ0ZXh0XCJdIHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJ2h0dHA6Ly9pLm1pbnVzLmNvbS9pYmhxVzlCdWFub2h4Mi5wbmcnKSBjZW50ZXIgbGVmdCBuby1yZXBlYXQsIGxpbmVhci1ncmFkaWVudCh0b3AsICNkNmQ3ZDcsICNkZWUwZTApO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICMxMjUwMDM7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG4gIGJveC1zaGFkb3c6IDAgMXB4IHJnYigyNTUsIDI1NSwgMjU1KTtcclxuICBib3gtc2l6aW5nOiBib3JkZXItYm94O1xyXG4gIGNvbG9yOiAjMjUyNDI0O1xyXG4gIGhlaWdodDogMzlweDtcclxuICBtYXJnaW46IDMxcHggMCAwIDI5cHg7XHJcbiAgcGFkZGluZy1sZWZ0OiAzN3B4O1xyXG4gIHRyYW5zaXRpb246IGJveC1zaGFkb3cgMC4zcztcclxuICB3aWR0aDogMjQwcHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogI2U1ZmNhZjsgKi9cclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjOWNlNzU1O1xyXG59XHJcbmlucHV0W3R5cGU9XCJwYXNzd29yZFwiXTpmb2N1cywgaW5wdXRbdHlwZT1cInRleHRcIl06Zm9jdXMge1xyXG4gIGJveC1zaGFkb3c6IDAgMCA0cHggMXB4IHJnYmEoNTUsIDE2NiwgMTU1LCAwLjMpO1xyXG4gIG91dGxpbmU6IDA7XHJcbn1cclxuLnNob3ctcGFzc3dvcmQge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGhlaWdodDogMTZweDtcclxuICBtYXJnaW46IDI2cHggMCAwIDI4cHg7XHJcbiAgd2lkdGg6IDg3cHg7XHJcbn1cclxuaW5wdXRbdHlwZT1cImNoZWNrYm94XCJdIHtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiAxNnB4O1xyXG4gIG9wYWNpdHk6IDA7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIHdpZHRoOiA2NHB4O1xyXG59XHJcbmlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkIHtcclxuICBsZWZ0OiAyOXB4O1xyXG4gIHdpZHRoOiA1OHB4O1xyXG59XHJcbi50b2dnbGUge1xyXG4gIGJhY2tncm91bmQ6IHVybChodHRwOi8vaS5taW51cy5jb20vaWJpdFMxOXBlOFBWWDYucG5nKSBuby1yZXBlYXQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgaGVpZ2h0OiAxNnB4O1xyXG4gIG1hcmdpbi10b3A6IC0yMHB4O1xyXG4gIHdpZHRoOiA4N3B4O1xyXG4gIHotaW5kZXg6IC0xO1xyXG59XHJcbmlucHV0W3R5cGU9XCJjaGVja2JveFwiXTpjaGVja2VkICsgLnRvZ2dsZSB7IGJhY2tncm91bmQtcG9zaXRpb246IDAgLTE2cHggfVxyXG4uZm9yZ290IHtcclxuICBjb2xvcjogIzdmN2Y3ZjtcclxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgLyogZmxvYXQ6IHJpZ2h0OyAqL1xyXG4gIGZvbnQ6IDEycHgvMSBzYW5zLXNlcmlmO1xyXG4gIGxlZnQ6IC0xOXB4O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XHJcbiAgdG9wOiA1cHg7XHJcbiAgdHJhbnNpdGlvbjogY29sb3IgLjRzO1xyXG59XHJcbi5mb3Jnb3Q6aG92ZXIgeyBjb2xvcjogIzNiM2IzYiB9XHJcbmlucHV0W3R5cGU9XCJzdWJtaXRcIl0ge1xyXG4gIHdpZHRoOjI0MHB4O1xyXG4gIGhlaWdodDozNXB4O1xyXG4gIGRpc3BsYXk6YmxvY2s7XHJcbiAgZm9udC1mYW1pbHk6QXJpYWwsIFwiSGVsdmV0aWNhXCIsIHNhbnMtc2VyaWY7XHJcbiAgZm9udC1zaXplOjE2cHg7XHJcbiAgZm9udC13ZWlnaHQ6Ym9sZDtcclxuICBjb2xvcjojMDQ0NDA0O1xyXG4gIHRleHQtZGVjb3JhdGlvbjpub25lO1xyXG4gIHRleHQtdHJhbnNmb3JtOnVwcGVyY2FzZTtcclxuICB0ZXh0LWFsaWduOmNlbnRlcjtcclxuICB0ZXh0LXNoYWRvdzoxcHggMXB4IDBweCByZ2IoMjQzLCAyNDcsIDMwKTtcclxuICBwYWRkaW5nLXRvcDo2cHg7XHJcbiAgbWFyZ2luOiAyOXB4IDAgMCAyOXB4O1xyXG4gIHBvc2l0aW9uOnJlbGF0aXZlO1xyXG4gIGN1cnNvcjpwb2ludGVyO1xyXG4gIGJvcmRlcjogbm9uZTsgIFxyXG4gIGJhY2tncm91bmQtY29sb3I6ICNiNmRiMGQ7XHJcbiAgLyogYmFja2dyb3VuZC1pbWFnZTogbGluZWFyLWdyYWRpZW50KHRvcCwjM2RiMGE2LCMzMTExKTsgKi9cclxuICBib3JkZXItdG9wLWxlZnQtcmFkaXVzOiA1cHg7XHJcbiAgYm9yZGVyLXRvcC1yaWdodC1yYWRpdXM6IDVweDtcclxuICBib3JkZXItYm90dG9tLXJpZ2h0LXJhZGl1czogNXB4O1xyXG4gIGJvcmRlci1ib3R0b20tbGVmdC1yYWRpdXM6NXB4O1xyXG4gIC8qIGJveC1zaGFkb3c6IGluc2V0IDBweCAxcHggMHB4ICMyYWI3ZWMsIDBweCA1cHggMHB4IDBweCAjNDk3YTc4LCAwcHggMTBweCA1cHggIzk5OTsgKi9cclxuICBib3gtc2hhZG93OiBpbnNldCAwcHggMXB4IDBweCAjYjZkYjBkLCAwcHggMnB4IDBweCAwcHggI2YzZjcyYSwgMHB4IDVweCAzcHggcmdiKDI0MywgMjQ3LCAzMCk7XHJcbn1cclxuXHJcbi5zaGFkb3cge1xyXG4gIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgYm9yZGVyLXJhZGl1czogMTJweCAxMnB4IDRweCA0cHg7XHJcbiAgYm94LXNoYWRvdzogMCAwIDIwcHggMTBweCAjMDAwO1xyXG4gIGhlaWdodDogMTJweDtcclxuICBtYXJnaW46IDMwcHggYXV0bztcclxuICBvcGFjaXR5OiAwLjI7XHJcbiAgd2lkdGg6IDI3MHB4O1xyXG59XHJcblxyXG5cclxuaW5wdXRbdHlwZT1cInN1Ym1pdFwiXTphY3RpdmUge1xyXG4gIHRvcDozcHg7XHJcbiAgYm94LXNoYWRvdzogaW5zZXQgMHB4IDFweCAwcHggI2I2ZGIwZCwgMHB4IDJweCAwcHggMHB4ICNmM2Y3MmEsIDBweCA1cHggM3B4IHJnYigyNDMsIDI0NywgMzApO1xyXG4gIGNvbG9yOiAjMDQ0NDA0O1xyXG59XHJcblxyXG4jYmFja2dyb3VuZFxyXG57XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIHdpZHRoOiA5ODBweDtcclxuICBoZWlnaHQ6IDM3MHB4O1xyXG4gIGJhY2tncm91bmQtaW1hZ2U6IHVybCgnbG9naW5CYWNrZ3JvdW5kLmpwZycpO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbn1cclxuXHJcbiNjb21ib0JveFxyXG57XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzljZTc1NTtcclxuICB3aWR0aDogMjQwcHg7XHJcbiAgaGVpZ2h0OiAzOXB4O1xyXG4gIG1hcmdpbjogMzFweCAwIDAgMjlweDtcclxuICBwYWRkaW5nLWxlZnQ6IDM3cHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgIzEyNTAwMztcclxuICBib3JkZXItcmFkaXVzOiA0cHg7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id=\"titleApp\">\r\n  <h1>\r\n    {{ title }}\r\n  </h1>\r\n</div> \r\n\r\n\r\n<div id = \"background\">\r\n  <!--The content below is only a placeholder and can be replaced.-->\r\n  <div class=\"login\">\r\n    <input type=\"text\" placeholder=\"login\" id=\"login\" [(ngModel)] = \"username\">  \r\n  <input type=\"password\" placeholder=\"hasło\" id=\"password\" [(ngModel)] = \"password\">  \r\n  <!-- <a href=\"#\" class=\"forgot\">forgot password?</a> -->\r\n\r\n  <!-- <select id = \"comboBox\" \r\n    ng-model=\"selectedNum\"\r\n    ng-options=\"n for n in nums\">\r\n  </select>  -->\r\n\r\n  <select id = \"comboBox\" [(ngModel)] = \"departmentId\">\r\n    <option *ngFor=\"let department of departments\" value = {{department.id}}>{{department.name}}</option>\r\n  </select> \r\n\r\n<!-- \r\n  [(ngModel)] = \"departmentId\" \r\n  <input type=\"submit\" value=\"Zaloguj się\" routerLink=\"/home2\" (click) = \"log(username, password, departamentId)\"> -->\r\n  \r\n  <input type=\"submit\" value=\"Zaloguj się\" (click) = \"onSubmit()\">\r\n  </div>\r\n  <!-- <div class=\"shadow\"></div> -->\r\n</div>\r\n\r\n<div class=\"container\">\r\n <router-outlet></router-outlet>\r\n</div>\r\n  \r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var LoginComponent = /** @class */ (function () {
    function LoginComponent(http, route, location, data, authenticationService) {
        this.http = http;
        this.route = route;
        this.location = location;
        this.data = data;
        this.authenticationService = authenticationService;
        this.title = 'LogIT';
    }
    // constructor(private formBuilder: FormBuilder,private router: Router, public authService: AuthService) { }
    LoginComponent.prototype.ngOnInit = function () {
        //    this.data.getDepartments().subscribe(
        //   data => this.departments = data
        // )
        // .subscribe(
        //           data => {
        //             console.log('POST Request with login is successful ', data);
        //           },
        //           error => {
        //             console.log('Error in post request with login Iwona :(', error);
        //           } );
        var _this = this;
        this.data.getDepartments().subscribe(function (data) { return _this.departments = data; }, function (error) {
            console.log('Error GET DEPARTMENTS :( ', error);
        });
    };
    // log(username, password, departamentId) // zastanow sie czy to wgl przekazywac tedy czy przez this.
    LoginComponent.prototype.onSubmit = function () {
        // stop here if form is invalid
        // this.authenticationService.login('admin', 'admin', 1)
        this.authenticationService.login(this.username, this.password, this.departmentId)
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["first"])())
            .subscribe(function (data) {
            console.log(localStorage.getItem('token'));
        });
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_5__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_1__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _data_service__WEBPACK_IMPORTED_MODULE_0__["DataService"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_4__["AuthenticationService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/messages/messages.component.css":
/*!*************************************************!*\
  !*** ./src/app/messages/messages.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".line-break {\r\n  width: 90px; \r\n  overflow-wrap: break-word;\r\n  }\r\n\r\n.message\r\n{\r\n  background-blend-mode: darken;\r\n  padding-left: 5px;\r\n  background-color: rgb(212, 221, 80);\r\n  padding: 5px;\r\n\r\n  overflow-wrap: break-word;\r\n}\r\n\r\n.date\r\n{\r\n  letter-spacing: 2px;\r\n  font-size: 15px;\r\n  border-width: 2px;\r\n  border-bottom-style: groove;\r\n  font-weight: 900;\r\n}\r\n\r\n#search\r\n{\r\n  /* margin-top: 0px; */\r\n  margin-left: 5px;\r\n  width: 70px;\r\n  height: 25px;\r\n  font-weight: 600;\r\n  /* vertical-align: 0px; */\r\n  border-color: #7c5604;\r\n  border-radius: 30px;\r\n  color: #0c0b0a;\r\n  background-color: rgb(245, 245, 88);\r\n}\r\n\r\n#filtr\r\n{\r\n    /* padding-left: 20px; */\r\n    /* padding-left: 0px;\r\n    margin-top: 5px; */\r\n    height: 40px;\r\n    width: 300px;\r\n    margin-left: 650px;\r\n}\r\n\r\n#input\r\n{\r\n    /* margin-top: 20px; */\r\n    /* margin-top: 5px; */\r\n    height: 20px;\r\n    width: 200px;\r\n    padding-left: 0px;\r\n    margin-left: 0px;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbWVzc2FnZXMvbWVzc2FnZXMuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFlBQVk7RUFDWiwwQkFBMEI7R0FDekI7O0FBRUg7O0VBRUUsOEJBQThCO0VBQzlCLGtCQUFrQjtFQUNsQixvQ0FBb0M7RUFDcEMsYUFBYTs7RUFFYiwwQkFBMEI7Q0FDM0I7O0FBRUQ7O0VBRUUsb0JBQW9CO0VBQ3BCLGdCQUFnQjtFQUNoQixrQkFBa0I7RUFDbEIsNEJBQTRCO0VBQzVCLGlCQUFpQjtDQUNsQjs7QUFFRDs7RUFFRSxzQkFBc0I7RUFDdEIsaUJBQWlCO0VBQ2pCLFlBQVk7RUFDWixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLDBCQUEwQjtFQUMxQixzQkFBc0I7RUFDdEIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixvQ0FBb0M7Q0FDckM7O0FBRUQ7O0lBRUkseUJBQXlCO0lBQ3pCO3VCQUNtQjtJQUNuQixhQUFhO0lBQ2IsYUFBYTtJQUNiLG1CQUFtQjtDQUN0Qjs7QUFFRDs7SUFFSSx1QkFBdUI7SUFDdkIsc0JBQXNCO0lBQ3RCLGFBQWE7SUFDYixhQUFhO0lBQ2Isa0JBQWtCO0lBQ2xCLGlCQUFpQjtDQUNwQiIsImZpbGUiOiJzcmMvYXBwL21lc3NhZ2VzL21lc3NhZ2VzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGluZS1icmVhayB7XHJcbiAgd2lkdGg6IDkwcHg7IFxyXG4gIG92ZXJmbG93LXdyYXA6IGJyZWFrLXdvcmQ7XHJcbiAgfVxyXG5cclxuLm1lc3NhZ2Vcclxue1xyXG4gIGJhY2tncm91bmQtYmxlbmQtbW9kZTogZGFya2VuO1xyXG4gIHBhZGRpbmctbGVmdDogNXB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyMTIsIDIyMSwgODApO1xyXG4gIHBhZGRpbmc6IDVweDtcclxuXHJcbiAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcclxufVxyXG5cclxuLmRhdGVcclxue1xyXG4gIGxldHRlci1zcGFjaW5nOiAycHg7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGJvcmRlci13aWR0aDogMnB4O1xyXG4gIGJvcmRlci1ib3R0b20tc3R5bGU6IGdyb292ZTtcclxuICBmb250LXdlaWdodDogOTAwO1xyXG59XHJcblxyXG4jc2VhcmNoXHJcbntcclxuICAvKiBtYXJnaW4tdG9wOiAwcHg7ICovXHJcbiAgbWFyZ2luLWxlZnQ6IDVweDtcclxuICB3aWR0aDogNzBweDtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAvKiB2ZXJ0aWNhbC1hbGlnbjogMHB4OyAqL1xyXG4gIGJvcmRlci1jb2xvcjogIzdjNTYwNDtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIGNvbG9yOiAjMGMwYjBhO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigyNDUsIDI0NSwgODgpO1xyXG59XHJcblxyXG4jZmlsdHJcclxue1xyXG4gICAgLyogcGFkZGluZy1sZWZ0OiAyMHB4OyAqL1xyXG4gICAgLyogcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiA1cHg7ICovXHJcbiAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICB3aWR0aDogMzAwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogNjUwcHg7XHJcbn1cclxuXHJcbiNpbnB1dFxyXG57XHJcbiAgICAvKiBtYXJnaW4tdG9wOiAyMHB4OyAqL1xyXG4gICAgLyogbWFyZ2luLXRvcDogNXB4OyAqL1xyXG4gICAgaGVpZ2h0OiAyMHB4O1xyXG4gICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgcGFkZGluZy1sZWZ0OiAwcHg7XHJcbiAgICBtYXJnaW4tbGVmdDogMHB4O1xyXG59XHJcblxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/messages/messages.component.html":
/*!**************************************************!*\
  !*** ./src/app/messages/messages.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}} \n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }} \n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"block\" >\n  <div id=\"filtr\">\n    <input id= \"input\" type=\"text\" [(ngModel)]='query' placeholder= \"Wyszukaj frazę\"/>\n    <button id= \"search\" (click)=search()>Szukaj</button>\n  </div>\n  <div class=\"message\" *ngFor=\"let messageData of messageDatas | paginate: { itemsPerPage: 10, currentPage: p }\" style=\"white-space:pre-wrap;\">\n    \n        <div class=\"date\">\n          {{messageData.messageTimestamp}}\n        </div>\n        {{messageData.message}}\n  </div>\n  <pagination-controls (pageChange)=\"p = $event\" previousLabel=\"Poprzednia\"\n  nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/messages/messages.component.ts":
/*!************************************************!*\
  !*** ./src/app/messages/messages.component.ts ***!
  \************************************************/
/*! exports provided: MessagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MessagesComponent", function() { return MessagesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MessagesComponent = /** @class */ (function () {
    function MessagesComponent(route, data, location, authenticationService) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = "Wiadomości";
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    MessagesComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    MessagesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        // this.data.getMessages(this.user$).subscribe(
        //   // data => this.messages$ = data 
        //   data => this.messageDatas = data
        //   // data => console.log(data)
        // );
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(0, 7000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function () { return _this.data.getMessages(_this.userId); })).subscribe(function (data) { return _this.messageDatas = data; });
        this.logName = localStorage.getItem('username');
    };
    MessagesComponent.prototype.search = function () {
        var _this = this;
        if (this.query === "") {
            this.data.getMessages(this.userId).subscribe(function (data) { return _this.messageDatas = data; }, function (error) {
                console.log('Error GET MESSAGES :( ', localStorage.getItem('token'), error);
            });
        }
        else {
            this.data.getFilteredMessages(this.query, this.userId).subscribe(function (data) { return _this.messageDatas = data; }, function (error) {
                console.log('Error GET FILTERED MESSAGES :( ', localStorage.getItem('token'), error);
            });
        }
    };
    MessagesComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    MessagesComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    MessagesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-messages',
            template: __webpack_require__(/*! ./messages.component.html */ "./src/app/messages/messages.component.html"),
            styles: [__webpack_require__(/*! ./messages.component.css */ "./src/app/messages/messages.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"]])
    ], MessagesComponent);
    return MessagesComponent;
}());



/***/ }),

/***/ "./src/app/net-data/net-data.component.css":
/*!*************************************************!*\
  !*** ./src/app/net-data/net-data.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .part\r\n{\r\n  box-shadow:inset 0 0 50px #074681;\r\n  width: 980px;\r\n  height: 30px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  font-size: 10px;\r\n  padding: 10px;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n  color: #110831;\r\n  border-color: #4d50f1;\r\n  border-bottom-style: groove;\r\n} */\r\n\r\n.part\r\n{\r\n  box-shadow:inset 0 0 50px #aa9a0a;\r\n  width: 980px;\r\n  height: 30px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  font-size: 10px;\r\n  padding: 10px;\r\n  padding-top: 2px;\r\n  padding-bottom: 2px;\r\n  color: #475302;\r\n  border-color: #cc5a0e;\r\n  border-bottom-style: groove;\r\n}\r\n\r\n.textpart\r\n{\r\n  width: 980px;\r\n  /* height: 30px; */\r\n  border-bottom-left-radius: 10px;\r\n  border-bottom-right-radius: 10px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  font-size: 14px;\r\n  padding: 10px;\r\n  background-color: rgb(172, 214, 55);\r\n  font-style: italic;\r\n}\r\n\r\n#blockNet\r\n{\r\n  width: 1000px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-bottom: 30px;\r\n  /* background-color: rgb(184, 39, 160); */\r\n}\r\n\r\n#bookmarkPanel\r\n{\r\n  width: 1000px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  /* padding-left: 30px; */\r\n  /* margin-bottom: 30px; */\r\n}\r\n\r\n.bookmark\r\n{\r\n  background-color: #3dac4d;\r\n  width: 500px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActive\r\n{\r\n  background-color: #3dac4d;\r\n  /* width: 313px; */\r\n  width: 500px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n#bookmarkPanelSecond\r\n{\r\n  width: 1000px;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.bookmarkSecond\r\n{\r\n  background-color: #5df173;\r\n  width: 250px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActiveSecond\r\n{\r\n  background-color: #5df173;\r\n  /* width: 313px; */\r\n  width: 250px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n.areaSecond\r\n{\r\n  /* box-shadow:inset 0 0 100px #235169; */\r\n  box-shadow:inset 0 0 100px  #6be41b;\r\n  width: 940px;\r\n  /* height: 610px; */\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 35px;\r\n  padding: 30px;\r\n  color: #110831;\r\n  font-stretch: ultra-expanded;\r\n  border-radius: 10px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV0LWRhdGEvbmV0LWRhdGEuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7Ozs7Ozs7Ozs7Ozs7O0lBZUk7O0FBRUo7O0VBRUUsa0NBQWtDO0VBQ2xDLGFBQWE7RUFDYixhQUFhO0VBQ2Isa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGVBQWU7RUFDZixzQkFBc0I7RUFDdEIsNEJBQTRCO0NBQzdCOztBQUVEOztFQUVFLGFBQWE7RUFDYixtQkFBbUI7RUFDbkIsZ0NBQWdDO0VBQ2hDLGlDQUFpQztFQUNqQyxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGdCQUFnQjtFQUNoQixjQUFjO0VBQ2Qsb0NBQW9DO0VBQ3BDLG1CQUFtQjtDQUNwQjs7QUFFRDs7RUFFRSxjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixvQkFBb0I7RUFDcEIsMENBQTBDO0NBQzNDOztBQUVEOztFQUVFLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLHlCQUF5QjtFQUN6QiwwQkFBMEI7Q0FDM0I7O0FBRUQ7O0VBRUUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDs7RUFFRSwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHNCQUFzQjtDQUN2Qjs7QUFFRDs7RUFFRSxjQUFjO0VBQ2Qsb0JBQW9CO0NBQ3JCOztBQUVEOztFQUVFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7O0VBRUUsMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHdCQUF3QjtFQUN4QixzQkFBc0I7Q0FDdkI7O0FBRUQ7O0VBRUUseUNBQXlDO0VBQ3pDLG9DQUFvQztFQUNwQyxhQUFhO0VBQ2Isb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixjQUFjO0VBQ2QsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QixvQkFBb0I7Q0FDckIiLCJmaWxlIjoic3JjL2FwcC9uZXQtZGF0YS9uZXQtZGF0YS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyogLnBhcnRcclxue1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDUwcHggIzA3NDY4MTtcclxuICB3aWR0aDogOTgwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAycHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDJweDtcclxuICBjb2xvcjogIzExMDgzMTtcclxuICBib3JkZXItY29sb3I6ICM0ZDUwZjE7XHJcbiAgYm9yZGVyLWJvdHRvbS1zdHlsZTogZ3Jvb3ZlO1xyXG59ICovXHJcblxyXG4ucGFydFxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgNTBweCAjYWE5YTBhO1xyXG4gIHdpZHRoOiA5ODBweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgcGFkZGluZy10b3A6IDJweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMnB4O1xyXG4gIGNvbG9yOiAjNDc1MzAyO1xyXG4gIGJvcmRlci1jb2xvcjogI2NjNWEwZTtcclxuICBib3JkZXItYm90dG9tLXN0eWxlOiBncm9vdmU7XHJcbn1cclxuXHJcbi50ZXh0cGFydFxyXG57XHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIC8qIGhlaWdodDogMzBweDsgKi9cclxuICBib3JkZXItYm90dG9tLWxlZnQtcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlci1ib3R0b20tcmlnaHQtcmFkaXVzOiAxMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICBmb250LXNpemU6IDE0cHg7XHJcbiAgcGFkZGluZzogMTBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTcyLCAyMTQsIDU1KTtcclxuICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbn1cclxuXHJcbiNibG9ja05ldFxyXG57XHJcbiAgd2lkdGg6IDEwMDBweDtcclxuICBtYXJnaW4tbGVmdDogYXV0bztcclxuICBtYXJnaW4tcmlnaHQ6IGF1dG87XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTg0LCAzOSwgMTYwKTsgKi9cclxufSBcclxuXHJcbiNib29rbWFya1BhbmVsXHJcbntcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAvKiBwYWRkaW5nLWxlZnQ6IDMwcHg7ICovXHJcbiAgLyogbWFyZ2luLWJvdHRvbTogMzBweDsgKi9cclxufVxyXG5cclxuLmJvb2ttYXJrXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2RhYzRkO1xyXG4gIHdpZHRoOiA1MDBweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLmJvb2ttYXJrQWN0aXZlXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjM2RhYzRkO1xyXG4gIC8qIHdpZHRoOiAzMTNweDsgKi9cclxuICB3aWR0aDogNTAwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICMxNzMwMTc7XHJcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogNHB4O1xyXG4gIGJvcmRlci1yaWdodC13aWR0aDogNHB4O1xyXG4gIGJvcmRlci1jb2xvcjogIzE3MzAxNztcclxufVxyXG5cclxuI2Jvb2ttYXJrUGFuZWxTZWNvbmRcclxue1xyXG4gIHdpZHRoOiAxMDAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLmJvb2ttYXJrU2Vjb25kXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWRmMTczO1xyXG4gIHdpZHRoOiAyNTBweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLmJvb2ttYXJrQWN0aXZlU2Vjb25kXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWRmMTczO1xyXG4gIC8qIHdpZHRoOiAzMTNweDsgKi9cclxuICB3aWR0aDogMjUwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgY29sb3I6ICMxNzMwMTc7XHJcbiAgYm9yZGVyLWJvdHRvbS13aWR0aDogNHB4O1xyXG4gIGJvcmRlci1yaWdodC13aWR0aDogNHB4O1xyXG4gIGJvcmRlci1jb2xvcjogIzE3MzAxNztcclxufVxyXG5cclxuLmFyZWFTZWNvbmRcclxue1xyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwMHB4ICMyMzUxNjk7ICovXHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTAwcHggICM2YmU0MWI7XHJcbiAgd2lkdGg6IDk0MHB4O1xyXG4gIC8qIGhlaWdodDogNjEwcHg7ICovXHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzVweDtcclxuICBwYWRkaW5nOiAzMHB4O1xyXG4gIGNvbG9yOiAjMTEwODMxO1xyXG4gIGZvbnQtc3RyZXRjaDogdWx0cmEtZXhwYW5kZWQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/net-data/net-data.component.html":
/*!**************************************************!*\
  !*** ./src/app/net-data/net-data.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}} \n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n  <div id=\"middle0\" style= \"clear:both\"></div>\n\n<div id = \"blockNet\">\n  <div  id = \"bookmarkPanel\" >\n    <button class = \"bookmarkActive\" *ngIf = \"bookmark === 2\">BIEŻĄCE</button> \n    <button class = \"bookmark\" *ngIf = \"bookmark != 2\" (click)=\"changeBookmark(2)\">BIEŻĄCE</button>\n    <button class = \"bookmarkActive\" *ngIf = \"bookmark === 1\">HISTORIA</button>\n    <button class = \"bookmark\" *ngIf = \"bookmark != 1\" (click)=\"changeBookmark(1)\">HISTORIA</button> \n  </div>\n  <div id = \"bookmark1\" *ngIf = \"bookmark === 2\">\n    <div class=\"part\"><h2>Transfer obecny</h2></div>\n    <!-- <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed < 1024\" >Prędkość wysyłania: {{transfer.uploadSpeed}} b/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed >= 1024 && transfer.uploadSpeed < 1024*1024\" >Prędkość wysyłania: {{calculate(transfer.uploadSpeed, 1)}} kb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed >= 1024*1024 && transfer.uploadSpeed < 1024*1024*1024\" >Prędkość wysyłania: {{calculate(transfer.uploadSpeed, 2)}} Mb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed >= 1024*1024*1024 && transfer.uploadSpeed < 1024*1024*1024*1024\" >Prędkość wysyłania: {{calculate(transfer.uploadSpeed, 3)}} Gb/s</div> -->\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed < 1024\" >Prędkość wysyłania: {{transfer.uploadSpeed}} kb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed >= 1024 && transfer.uploadSpeed < 1024*1024\" >Prędkość wysyłania: {{calculate(transfer.uploadSpeed, 1)}} Mb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.uploadSpeed >= 1024*1024 && transfer.uploadSpeed < 1024*1024*1024\" >Prędkość wysyłania: {{calculate(transfer.uploadSpeed, 2)}} Gb/s</div>\n\n    <!-- <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed < 1024\" >Prędkość pobierania: {{transfer.downloadSpeed}} b/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed >= 1024 && transfer.downloadSpeed < 1024*1024\" >Prędkość pobierania: {{calculate(transfer.downloadSpeed, 1)}} kb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed >= 1024*1024 && transfer.downloadSpeed < 1024*1024*1024\" >Prędkość pobierania: {{calculate(transfer.downloadSpeed, 2)}} Mb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed >= 1024*1024*1024 && transfer.downloadSpeed < 1024*1024*1024*1024\" >Prędkość pobierania: {{calculate(transfer.downloadSpeed, 3)}} Gb/s</div> -->\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed < 1024\" >Prędkość pobierania: {{transfer.downloadSpeed}} kb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed >= 1024 && transfer.downloadSpeed < 1024*1024\" >Prędkość pobierania: {{calculate(transfer.downloadSpeed, 1)}} Mb/s</div>\n    <div class = \"textpart\" *ngIf = \"transfer.downloadSpeed >= 1024*1024 && transfer.downloadSpeed < 1024*1024*1024\" >Prędkość pobierania: {{calculate(transfer.downloadSpeed, 2)}} Gb/s</div>\n    <div class = \"textpart\">Kiedy rozpoczęto pomiar: {{transfer.whenStarted}}</div>\n\n    <div class=\"part\"><h2>Transfer od włączenia komputera</h2></div>\n    <div class = \"textpart\">Ilość wysłanych danych: </div>\n    <!-- <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB < 1024\" >Ilość wysłanych danych: {{transferSinceComputerStarted.uploadedKB}} b</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB >= 1024 && transferSinceComputerStarted.uploadedKB < 1024*1024\" >Ilość wysłanych danych: {{calculate(transferSinceComputerStarted.uploadedKB, 1)}} kb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB >= 1024*1024 && transferSinceComputerStarted.uploadedKB < 1024*1024*1024\" >Ilość wysłanych danych: {{calculate(transferSinceComputerStarted.uploadedKB, 2)}} Mb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB >= 1024*1024*1024 && transferSinceComputerStarted.uploadedKB < 1024*1024*1024*1024\" >Ilość wysłanych danych: {{calculate(transferSinceComputerStarted.uploadedKB, 3)}} Gb</div> -->\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB < 1024\" >Ilość wysłanych danych: {{transferSinceComputerStarted.uploadedKB}} kb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB >= 1024 && transferSinceComputerStarted.uploadedKB < 1024*1024\" >Ilość wysłanych danych: {{calculate(transferSinceComputerStarted.uploadedKB, 1)}} Mb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.uploadedKB >= 1024*1024 && transferSinceComputerStarted.uploadedKB < 1024*1024*1024\" >Ilość wysłanych danych: {{calculate(transferSinceComputerStarted.uploadedKB, 2)}} Gb</div>\n    <div class = \"textpart\">Ilość pobranych danych: </div>\n    <!-- <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB < 1024\" >Ilość pobranych danych: {{transferSinceComputerStarted.downloadedKB}} b</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB >= 1024 && transferSinceComputerStarted.downloadedKB < 1024*1024\" >Ilość pobranych danych: {{calculate(transferSinceComputerStarted.downloadedKB, 1)}} kb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB >= 1024*1024 && transferSinceComputerStarted.downloadedKB < 1024*1024*1024\" >Ilość pobranych danych: {{calculate(transferSinceComputerStarted.downloadedKB, 2)}} Mb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB >= 1024*1024*1024 && transferSinceComputerStarted.downloadedKB < 1024*1024*1024*1024\" >Ilość pobranych danych: {{calculate(transferSinceComputerStarted.downloadedKB, 3)}} Gb</div> -->\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB < 1024\" >Ilość pobranych danych: {{transferSinceComputerStarted.downloadedKB}} kb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB >= 1024 && transferSinceComputerStarted.downloadedKB < 1024*1024\" >Ilość pobranych danych: {{calculate(transferSinceComputerStarted.downloadedKB, 1)}} Mb</div>\n    <div class = \"textpart\" *ngIf = \"transferSinceComputerStarted.downloadedKB >= 1024*1024 && transferSinceComputerStarted.downloadedKB < 1024*1024*1024\" >Ilość pobranych danych: {{calculate(transferSinceComputerStarted.downloadedKB, 2)}} Gb</div>\n    <div class = \"textpart\">Kiedy rozpoczęto pomiar: {{transferSinceComputerStarted.monitorTimestamp}}</div>\n    <div class=\"part\"><h2>Stan połączenia internetowego</h2></div>\n    <div class = \"textpart\">Wyniki testu łącza: {{ping.pingMeasurementInMs}} ms </div>\n    <div class = \"textpart\">Adres używany do sprawdzania połączenia: {{ping.address}}</div>\n    <div class = \"textpart\">Kiedy rozpoczęto pomiar: {{ping.monitorTimestamp}}</div>\n    <div class=\"part\"><h2>Informacje o karcie sieciowej</h2></div>\n    <div class = \"textpart\">Interfejs sieciowy: {{card.interface}}</div>\n    <div class = \"textpart\">Adres MAC: {{card.mac}}</div>\n    <div class = \"textpart\">Adres IP: {{card.ip}}</div>\n    <div class = \"textpart\">Maska: {{card.mask}}</div>\n    <div class = \"textpart\">Adres broadcast: {{card.broadcast}}</div>\n    <div class = \"textpart\">Kiedy zebrano informację: {{card.whenInfoCollected}}</div>\n  </div>\n  <div id = \"bookmark1\" *ngIf = \"bookmark === 1\">\n    <div  id = \"bookmarkPanelSecond\" >\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 1\">TRANSFER OBECNY</button> \n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 1\" (click)=\"changeBookmarkSecond(1)\">TRANSFER OBECNY</button>\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 2\">TRANSFER</button>\n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 2\" (click)=\"changeBookmarkSecond(2)\">TRANSFER</button>\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 3\">STAN POŁĄCZENIA</button> \n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 3\" (click)=\"changeBookmarkSecond(3)\">STAN POŁĄCZENIA</button>\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 4\">KARTA SIECIOWA</button>\n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 4\" (click)=\"changeBookmarkSecond(4)\">KARTA SIECIOWA</button> \n    </div>\n    <div class = \"areaSecond\" *ngIf = \"bookmarkSecond === 1\">\n      <style> th { border: 2px solid black; color: green;} </style>\n      <style> td { border: 1px solid black;} </style>\n      <style> caption { color: black; font-weight: 700;} </style>\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n          <caption>TRANSFER OBECNY</caption>\n        <tr>\n          <th style = \"width: 300px\">Prędkość wysyłania [kB/s]</th>\n          <th style = \"width: 300px\">Prędkość pobierania [kB/s]</th>\n          <!-- <th style = \"width: 300px\">Prędkość wysyłania [MB/s]</th>\n          <th style = \"width: 300px\">Prędkość pobierania [MB/s]</th> -->\n          <th style = \"width: 300px\">Kiedy rozpoczęto pomiar</th>\n        </tr>\n        <tr *ngFor=\"let historyTransfer of historyTransfers | paginate: { itemsPerPage: 20, currentPage: p }\">\n          <td>{{historyTransfer.uploadSpeed}}</td>\n          <!-- <td>{{calculate(historyTransfer.uploadSpeed, 2)}}</td> -->\n          <td>{{historyTransfer.downloadSpeed}}</td>\n          <!-- <td>{{calculate(historyTransfer.downloadSpeed, 2)}}</td> -->\n          <td>{{historyTransfer.whenStarted}}</td>\n        </tr>\n      </table>\n      <pagination-controls (pageChange)=\"p = $event\" previousLabel=\"Poprzednia\"\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n    </div>\n    <div class = \"areaSecond\" *ngIf = \"bookmarkSecond === 2\">\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n          <caption>TRANSFER OD WŁĄCZENIA KOMPUTERA</caption>\n        <tr>\n          <!-- <th style = \"width: 300px\">Ilość wysłanych danych [kB]</th>\n          <th style = \"width: 300px\">Ilość pobranych danych [kB]</th> -->\n          <th style = \"width: 300px\">Ilość wysłanych danych [MB]</th>\n          <th style = \"width: 300px\">Ilość pobranych danych [MB]</th>\n          <th style = \"width: 300px\">Kiedy rozpoczęto pomiar</th>\n        </tr>\n        <tr *ngFor=\"let historySinceComputerStartedTransfer of historySinceComputerStartedTransfers | paginate: { itemsPerPage: 20, currentPage: r }\">\n          <!-- <td>{{historySinceComputerStartedTransfer.uploadedKB}}</td> -->\n          <td>{{calculate(historySinceComputerStartedTransfer.uploadedKB, 1)}}</td>\n          <!-- <td>{{historySinceComputerStartedTransfer.downloadedKB}}</td> -->\n          <td>{{calculate(historySinceComputerStartedTransfer.downloadedKB, 1)}}</td>\n          <td>{{historySinceComputerStartedTransfer.monitorTimestamp}}</td>\n        </tr>\n      </table> \n      <pagination-controls (pageChange)=\"r = $event\" previousLabel=\"Poprzednia\"\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n    </div>\n    <div class = \"areaSecond\" *ngIf = \"bookmarkSecond === 3\">\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n          <caption>STAN POŁĄCZENIA INTERNETOWEGO</caption>\n        <tr>\n          <th style = \"width: 300px\">Wyniki testu łącza [ms]</th>\n          <th style = \"width: 300px\">Adres używany do sprawdzania połączenia</th>\n          <th style = \"width: 300px\">Kiedy rozpoczęto pomiar</th>\n        </tr>\n        <tr *ngFor=\"let historyPing of historyPings | paginate: { itemsPerPage: 20, currentPage: s}\">\n          <td>{{historyPing.pingMeasurementInMs}}</td>\n          <td>{{historyPing.address}}</td>\n          <td>{{historyPing.monitorTimestamp}}</td>\n        </tr>\n      </table> \n      <pagination-controls (pageChange)=\"s = $event\" previousLabel=\"Poprzednia\"\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n    </div>\n    <div class = \"areaSecond\" *ngIf = \"bookmarkSecond === 4\">\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n          <caption>INFORMACJE O KARCIE SIECIOWEJ</caption>\n        <tr>\n          <th style = \"width: 300px\">Interfejs sieciowy</th>\n          <th style = \"width: 300px\">Adres MAC</th>\n          <th style = \"width: 300px\">Adres IP</th>\n          <th style = \"width: 300px\">Maska</th>\n          <th style = \"width: 300px\">Adres broadcast</th>\n          <th style = \"width: 300px\">Kiedy zebrano informację</th>\n        </tr>\n        <tr *ngFor=\"let historyCard of historyCards | paginate: { itemsPerPage: 20, currentPage: t }\">\n          <td>{{historyCard.interface}}</td>\n          <td>{{historyCard.mac}}</td>\n          <td>{{historyCard.ip}}</td>\n          <td>{{historyCard.mask}}</td>\n          <td>{{historyCard.broadcast}}</td>\n          <td>{{historyCard.whenInfoCollected}}</td>\n        </tr>\n      </table> \n      <pagination-controls (pageChange)=\"t = $event\" previousLabel=\"Poprzednia\"\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n    </div>\n\n  </div>\n</div>\n<div id = \"space\" ></div>\n\n\n"

/***/ }),

/***/ "./src/app/net-data/net-data.component.ts":
/*!************************************************!*\
  !*** ./src/app/net-data/net-data.component.ts ***!
  \************************************************/
/*! exports provided: NetDataComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetDataComponent", function() { return NetDataComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NetDataComponent = /** @class */ (function () {
    function NetDataComponent(route, data, location, authenticationService) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = 'Dane internetowe';
        this.bookmark = 1; // 1 or 2
        this.bookmarkSecond = 1; // 1, 2, 3 or 4
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    NetDataComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    NetDataComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.data.getInternetSettings(this.userId).subscribe(function (data) { return _this.netData = data; }
        // data => console.log(data)
        );
        this.data.getTransfer(this.userId).subscribe(function (data) { return _this.transfer = data; }
        // data => console.log(data)
        );
        this.data.getTransferHistory(this.userId).subscribe(function (data) { return _this.historyTransfers = data; }
        // data => console.log(data)
        );
        this.data.getTransferSinceComputerStarted(this.userId).subscribe(function (data) { return _this.transferSinceComputerStarted = data; }
        // data => console.log(data)
        );
        this.data.getTransferSinceComputerStartedHistory(this.userId).subscribe(function (data) { return _this.historySinceComputerStartedTransfers = data; }
        // data => console.log(data)
        );
        this.data.getNetworkCard(this.userId).subscribe(function (data) { return _this.card = data; }
        // data => console.log(data)
        );
        this.data.getNetworkCardHistory(this.userId).subscribe(function (data) { return _this.historyCards = data; }
        // data => console.log(data)
        );
        this.data.getPing(this.userId).subscribe(function (data) { return _this.ping = data; }
        // data => console.log(data)
        );
        this.data.getPingHistory(this.userId).subscribe(function (data) { return _this.historyPings = data; }
        // data => console.log(data)
        );
        this.logName = localStorage.getItem('username');
    };
    NetDataComponent.prototype.calculate = function (number, power) {
        number = number / Math.pow(1024, power);
        // var factor = Math.pow(10, k);
        // return Math.round(number*factor)/factor;
        return Math.round(number);
    };
    NetDataComponent.prototype.changeBookmark = function (nr) {
        this.bookmark = nr;
    };
    NetDataComponent.prototype.changeBookmarkSecond = function (nr) {
        this.bookmarkSecond = nr;
    };
    NetDataComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    NetDataComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-net-data',
            template: __webpack_require__(/*! ./net-data.component.html */ "./src/app/net-data/net-data.component.html"),
            styles: [__webpack_require__(/*! ./net-data.component.css */ "./src/app/net-data/net-data.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], NetDataComponent);
    return NetDataComponent;
}());



/***/ }),

/***/ "./src/app/net-settings/net-settings.component.css":
/*!*********************************************************!*\
  !*** ./src/app/net-settings/net-settings.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".buttons\r\n{\r\n  border-color: #7c6a05;\r\n  border-radius: 30px;\r\n  color: #7c6a05;\r\n}\r\n\r\n.buttons:hover\r\n{\r\n  border-color: #f7d30c;\r\n  border-radius: 30px;\r\n  /* color: #f7d30c; */\r\n}\r\n\r\n#block\r\n{\r\n  /* box-shadow:inset 0 0 100px #235169; */\r\n  box-shadow:inset 0 0 100px  #6be41b;\r\n  width: 940px;\r\n  height: 610px;\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 35px;\r\n  padding: 30px;\r\n  color: #110831;\r\n  font-stretch: ultra-expanded;\r\n  border-radius: 10px;\r\n}\r\n\r\n.label\r\n{\r\n  font-weight: 700;\r\n}\r\n\r\n.question\r\n{\r\n  margin-top: 30px;\r\n  /* background-color: aqua; */\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV0LXNldHRpbmdzL25ldC1zZXR0aW5ncy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLHNCQUFzQjtFQUN0QixvQkFBb0I7RUFDcEIsZUFBZTtDQUNoQjs7QUFFRDs7RUFFRSxzQkFBc0I7RUFDdEIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtDQUN0Qjs7QUFFRDs7RUFFRSx5Q0FBeUM7RUFDekMsb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYixjQUFjO0VBQ2Qsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxlQUFlO0VBQ2YsNkJBQTZCO0VBQzdCLG9CQUFvQjtDQUNyQjs7QUFFRDs7RUFFRSxpQkFBaUI7Q0FDbEI7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLDZCQUE2QjtDQUM5QiIsImZpbGUiOiJzcmMvYXBwL25ldC1zZXR0aW5ncy9uZXQtc2V0dGluZ3MuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5idXR0b25zXHJcbntcclxuICBib3JkZXItY29sb3I6ICM3YzZhMDU7XHJcbiAgYm9yZGVyLXJhZGl1czogMzBweDtcclxuICBjb2xvcjogIzdjNmEwNTtcclxufVxyXG5cclxuLmJ1dHRvbnM6aG92ZXJcclxue1xyXG4gIGJvcmRlci1jb2xvcjogI2Y3ZDMwYztcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIC8qIGNvbG9yOiAjZjdkMzBjOyAqL1xyXG59XHJcblxyXG4jYmxvY2tcclxue1xyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwMHB4ICMyMzUxNjk7ICovXHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTAwcHggICM2YmU0MWI7XHJcbiAgd2lkdGg6IDk0MHB4O1xyXG4gIGhlaWdodDogNjEwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzVweDtcclxuICBwYWRkaW5nOiAzMHB4O1xyXG4gIGNvbG9yOiAjMTEwODMxO1xyXG4gIGZvbnQtc3RyZXRjaDogdWx0cmEtZXhwYW5kZWQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLmxhYmVsXHJcbntcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4ucXVlc3Rpb25cclxue1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgLyogYmFja2dyb3VuZC1jb2xvcjogYXF1YTsgKi9cclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/net-settings/net-settings.component.html":
/*!**********************************************************!*\
  !*** ./src/app/net-settings/net-settings.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" > \n      {{logName}} \n  </div>\n  <div id = \"line\" >\n      |\n  </div> \n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"middle0\" style= \"clear:both\"></div> \n<div id=\"block\">\n  <div  class = \"question\" >Identyfikator użytkownika: <font   class = \"label\" color = \"blue\" >{{user$.id}} </font> </div>\n  <div class = \"question\" *ngIf = \"!settings$.isCurrentTransferMonitored\">\n    <p>Czy monitorować obecny transfer: \n        <font color = \"red\" >  \n      <label  class = \"label\"> NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateCurrentTransferMonitorStatus(settings$.id, settings$.isCurrentTransferMonitored)\" >ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isCurrentTransferMonitored\">\n    <p>Czy monitorować obecny transfer:                                   \n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\"  (click)=\"updateCurrentTransferMonitorStatus(settings$.id, settings$.isCurrentTransferMonitored)\" >ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isCurrentTransferMonitored\">\n    <p> Co ile czasu monitorować obecny transfer:    \n      <font color = \"blue\" >\n        <label   class = \"label\" *ngIf = \"settings$.currentTransferMonitorFrequency === 1\" >{{settings$.currentTransferMonitorFrequency}} sekunda </label> \n        <label   class = \"label\" *ngIf = \"settings$.currentTransferMonitorFrequency > 1 && settings$.currentTransferMonitorFrequency < 5\" >{{settings$.currentTransferMonitorFrequency}} sekundy </label>\n        <label   class = \"label\" *ngIf = \"settings$.currentTransferMonitorFrequency >=5\" >{{settings$.currentTransferMonitorFrequency}} sekund  </label>\n      </font> \n      <input type=\"number\" [(ngModel)]= 'currentTransferFrequency' placeholder = \"Nowa liczba\" style=\"width: 90px; margin-left: 25px; margin-right: 15px\"/> \n      <button class = \"buttons\"  (click)=\"updateCurrentTransferFrequency(settings$.id, currentTransferFrequency)\" (keyup.enter)=\"updateCurrentTransferFrequency(settings$.id, currentTransferFrequency)\">ZMIEŃ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"!settings$.isUploadSpeedMonitored\">\n    <p>Czy monitorować prędkość wysyłania:  \n        <font color = \"red\" >  \n      <label  class = \"label\"  >NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateUploadSpeedMonitorStatus(settings$.id, settings$.isUploadSpeedMonitored)\">ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isUploadSpeedMonitored\">\n    <p>Czy monitorować prędkość wysyłania:\n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\" (click)=\"updateUploadSpeedMonitorStatus(settings$.id, settings$.isUploadSpeedMonitored)\">ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"!settings$.isDownloadSpeedMonitored\">\n    <p>Czy monitorować prędkość pobierania:  \n        <font color = \"red\" >  \n      <label  class = \"label\"  >NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateDownloadSpeedMonitorStatus(settings$.id, settings$.isDownloadSpeedMonitored)\">ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isDownloadSpeedMonitored\">\n    <p>Czy monitorować prędkość pobierania:  \n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\" (click)=\"updateDownloadSpeedMonitorStatus(settings$.id, settings$.isDownloadSpeedMonitored)\">ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"!settings$.isTransferMonitoredSinceComputerStarted\">\n    <p>Czy monitorować transfer od włączenia komputera: \n        <font color = \"red\" >  \n      <label  class = \"label\"  >NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateSinceStartedTransferMonitorStatus(settings$.id, settings$.isTransferMonitoredSinceComputerStarted)\">ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isTransferMonitoredSinceComputerStarted\">\n    <p>Czy monitorować transfer od włączenia komputera:   \n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\" (click)=\"updateSinceStartedTransferMonitorStatus(settings$.id, settings$.isTransferMonitoredSinceComputerStarted)\">ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isTransferMonitoredSinceComputerStarted\">\n    <p> Co ile czasu monitorować transfer od wlaczenia komputera:\n      <font color = \"blue\" >\n        <label   class = \"label\" *ngIf = \"settings$.transferSinceComputerStartedMonitorFrequency === 1\" >{{settings$.transferSinceComputerStartedMonitorFrequency}} sekunda </label> \n        <label   class = \"label\" *ngIf = \"settings$.transferSinceComputerStartedMonitorFrequency > 1 && settings$.transferSinceComputerStartedMonitorFrequency < 5\" >{{settings$.transferSinceComputerStartedMonitorFrequency}} sekundy </label>\n        <label   class = \"label\" *ngIf = \"settings$.transferSinceComputerStartedMonitorFrequency >=5\" >{{settings$.transferSinceComputerStartedMonitorFrequency}} sekund  </label>\n      </font> \n        <input type=\"number\" [(ngModel)]= 'sinceStartTransferFrequency' placeholder = \"Nowa liczba\" style=\"width: 90px; margin-left: 25px; margin-right: 15px\"/> \n        <button class = \"buttons\"  (click)=\"updateSinceStartTransferFrequency(settings$.id, sinceStartTransferFrequency)\" >ZMIEŃ</button>\n    </p>\n  </div>\n\n  <div  class = \"question\" *ngIf = \"!settings$.shouldPing\">\n    <p>Czy sprawdzać połączenie: \n        <font color = \"red\" >  \n      <label  class = \"label\"  >NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateShouldPing(settings$.id, settings$.shouldPing)\">ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.shouldPing\">\n    <p>Czy sprawdzać połączenie: \n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\" (click)=\"updateShouldPing(settings$.id, settings$.shouldPing)\">ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.shouldPing\">\n    <p> Adres używany do sprawdzania połączenia:  \n      <font color = \"blue\" >\n        <label  class = \"label\"  > {{settings$.pingAddress}}  </label> \n      </font> \n      <input type=\"text\" [(ngModel)]= 'address' placeholder = \"Nowy adres\"  style=\"width: 300px; margin-left: 25px; margin-right: 15px\"/> \n      <button class = \"buttons\"  (click)=\"updatePingAddress(settings$.id, address)\" >ZMIEŃ</button>\n    </p>\n  </div>\n\n  <div  class = \"question\" *ngIf = \"settings$.shouldPing\">\n    <p> Co ile czasu sprawdzać połączenie:  \n      <font color = \"blue\" >\n        <label   class = \"label\" *ngIf = \"settings$.pingFrequency === 1\" >{{settings$.pingFrequency}} sekunda </label> \n        <label   class = \"label\" *ngIf = \"settings$.pingFrequency > 1 && settings$.pingFrequency < 5\" >{{settings$.pingFrequency}} sekundy </label>\n        <label   class = \"label\" *ngIf = \"settings$.pingFrequency >=5\" >{{settings$.pingFrequency}} sekund  </label>\n      </font> \n      <input type=\"number\" [(ngModel)]= 'pingFrequency' placeholder = \"Nowa liczba\" style=\"width: 90px; margin-left: 25px; margin-right: 15px\"/> \n      <button class = \"buttons\"  (click)=\"updatePingFrequency(settings$.id, pingFrequency)\" >ZMIEŃ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"!settings$.isNetworkCardInfoMonitor\">\n    <p>Czy zbierać informacje o karcie sieciowej: \n        <font color = \"red\" >  \n      <label  class = \"label\"  >NIE   </label> </font> \n      <button class = \"buttons\" (click)=\"updateNetworkCardMonitorStatus(settings$.id, settings$.isNetworkCardInfoMonitor)\">ROZPOCZNIJ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isNetworkCardInfoMonitor\">\n    <p>Czy zbierać informacje o karcie sieciowej:  \n      <font color = \"lime\" >  \n      <label  class = \"label\" >TAK     </label> </font>\n      <button class = \"buttons\" (click)=\"updateNetworkCardMonitorStatus(settings$.id, settings$.isNetworkCardInfoMonitor)\">ZAKOŃCZ</button>\n    </p>\n  </div>\n  <div  class = \"question\" *ngIf = \"settings$.isNetworkCardInfoMonitor\"> \n    <p> Co ile czasu zbierać informacje o karcie sieciowej: \n      <font color = \"blue\" >\n          <label   class = \"label\" *ngIf = \"settings$.networkCardMonitorFrequency === 1\" >{{settings$.networkCardMonitorFrequency}} sekunda </label> \n          <label   class = \"label\" *ngIf = \"settings$.networkCardMonitorFrequency > 1 && settings$.networkCardMonitorFrequency < 5\" >{{settings$.networkCardMonitorFrequency}} sekundy </label>\n          <label   class = \"label\" *ngIf = \"settings$.networkCardMonitorFrequency >=5\" >{{settings$.networkCardMonitorFrequency}} sekund  </label>\n      </font> \n      <input type=\"number\" [(ngModel)]= 'cardFrequency' placeholder = \"Nowa liczba\" style=\"width: 90px; margin-left: 25px; margin-right: 15px\"/> \n      <button class = \"buttons\"  (click)=\"updateCardFrequency(settings$.id, cardFrequency)\" >ZMIEŃ</button>\n    </p>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./src/app/net-settings/net-settings.component.ts":
/*!********************************************************!*\
  !*** ./src/app/net-settings/net-settings.component.ts ***!
  \********************************************************/
/*! exports provided: NetSettingsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetSettingsComponent", function() { return NetSettingsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var NetSettingsComponent = /** @class */ (function () {
    function NetSettingsComponent(http, route, data, location, authenticationService) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = 'Ustawienia internetowe';
        this.timeCurrentTransfer = 1;
        this.is1 = true;
        this.is2 = true;
        this.is3 = true;
        this.is4 = true;
        this.is5 = true;
        this.is6 = true;
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
        this.token = localStorage.getItem('token');
    }
    NetSettingsComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    NetSettingsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_6__["timer"])(0, 2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_7__["switchMap"])(function () { return _this.data.getInternetSettings(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.settings$ = data; });
        this.logName = localStorage.getItem('username');
        // this.refreshData();
        // this.interval = setInterval(() => {
        //     this.refreshData();
        // }, 5000);
    };
    NetSettingsComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    // this.data.getInternetSettings(this.user$).subscribe( 
    //   data => this.settings$ = data
    //   // data => console.log(data)
    // );
    NetSettingsComponent.prototype.change1 = function () { this.is1 = !this.is1; };
    NetSettingsComponent.prototype.change2 = function () { this.is2 = !this.is2; };
    NetSettingsComponent.prototype.change3 = function () { this.is3 = !this.is3; };
    NetSettingsComponent.prototype.change4 = function () { this.is4 = !this.is4; };
    NetSettingsComponent.prototype.change5 = function () { this.is5 = !this.is5; };
    NetSettingsComponent.prototype.change6 = function () { this.is6 = !this.is6; };
    NetSettingsComponent.prototype.updateCurrentTransferMonitorStatus = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-current-transfer-monitor-status', {
            "isCurrentTransferMonitored": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCurrentTransferMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCurrentTransferMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateCurrentTransferFrequency = function (settingsId, timeCurrentTransfer) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/current-transfer-monitor-frequency/' + timeCurrentTransfer, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCurrentTransferFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCurrentTransferFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateUploadSpeedMonitorStatus = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-upload-speed-monitor-status', {
            "isUploadSpeedMonitored": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateUploadSpeedMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateUploadSpeedMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateDownloadSpeedMonitorStatus = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-download-speed-monitor-status', {
            "isDownloadSpeedMonitored": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateDownloadSpeedMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateDownloadSpeedMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateSinceStartedTransferMonitorStatus = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-transfer-monitor-since-computer-started-status', {
            "isTransferMonitoredSinceComputerStarted": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateSinceStartedTransferMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateSinceStartedTransferMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateSinceStartTransferFrequency = function (settingsId, timeStartTransfer) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/transfer-monitor-since-computer-started-frequency/' + timeStartTransfer, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateSinceStartTransferFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateSinceStartTransferFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateShouldPing = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/should-ping', {
            "shouldPing": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateShouldPing", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateShouldPing", error);
        });
    };
    NetSettingsComponent.prototype.updatePingFrequency = function (settingsId, timePing) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/ping-frequency/' + timePing, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updatePingFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updatePingFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updateNetworkCardMonitorStatus = function (settingsId, is) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/change-network-card-monitor-status', {
            "isNetworkCardInfoMonitor": !is
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateNetworkCardMonitorStatus", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateNetworkCardMonitorStatus", error);
        });
    };
    NetSettingsComponent.prototype.updateCardFrequency = function (settingsId, timeCard) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/network-card-monitor-frequency/' + timeCard, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateCardFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateCardFrequency", error);
        });
    };
    NetSettingsComponent.prototype.updatePingAddress = function (settingsId, address) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/internet-settings/' + settingsId + '/ping-address/' + address, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updatePingAddress", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updatePingAddress", error);
        });
    };
    NetSettingsComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    NetSettingsComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-net-settings',
            template: __webpack_require__(/*! ./net-settings.component.html */ "./src/app/net-settings/net-settings.component.html"),
            styles: [__webpack_require__(/*! ./net-settings.component.css */ "./src/app/net-settings/net-settings.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_8__["AuthenticationService"]])
    ], NetSettingsComponent);
    return NetSettingsComponent;
}());



/***/ }),

/***/ "./src/app/network/network.component.css":
/*!***********************************************!*\
  !*** ./src/app/network/network.component.css ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "#container\r\n{\r\n  height: 250px;\r\n}\r\n\r\n#blue_tile{\r\n  background-color: #1552ad;\r\n  width: 380px;\r\n  height: 70px;\r\n  float: left;\r\n  margin: 20px;\r\n  box-shadow:inset 0 0 80px #000000;\r\n  padding-top: 60px;\r\n  /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n  -webkit-text-decoration-line: overline;\r\n\r\n          text-decoration-line: overline;\r\n  background-image: url('ustawienia_internetowe2.jpg');\r\n  color: blanchedalmond;\r\n}\r\n\r\n#blue_tile:hover\r\n{\r\n  box-shadow:inset 0 0 10px #000000;\r\n  cursor:pointer;\r\n}\r\n\r\n#green_tile{\r\n  background-color: #5aa11f;\r\n  width: 380px;\r\n  height: 70px;\r\n  float: left;\r\n  margin: 20px;\r\n  box-shadow:inset 0 0 80px #000000;\r\n  padding-top: 60px;\r\n  /*tutaj jakieś dodatkowe style, jak np. box-shadow, text-shadow, border, itp.*/\r\n\r\n  -webkit-text-decoration-line: overline;\r\n\r\n          text-decoration-line: overline;\r\n  background-image: url('dane_internetowe.jpg');\r\n  color: blanchedalmond;\r\n}\r\n\r\n#green_tile:hover\r\n{\r\n  box-shadow:inset 0 0 10px #000000;\r\n  cursor:pointer;\r\n}\r\n\r\n.middle\r\n{\r\n  width: 800px;\r\n  height: 5px;\r\n}\r\n\r\n#down\r\n{\r\n  width: 850px;\r\n  height: 150px;\r\n  margin-left: 75px;\r\n  margin-bottom: 5px;\r\n  margin-top: 5px;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvbmV0d29yay9uZXR3b3JrLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0VBRUUsY0FBYztDQUNmOztBQUVEO0VBQ0UsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2IsWUFBWTtFQUNaLGFBQWE7RUFDYixrQ0FBa0M7RUFDbEMsa0JBQWtCO0VBQ2xCLCtFQUErRTs7RUFFL0UsdUNBQStCOztVQUEvQiwrQkFBK0I7RUFDL0IscURBQXFEO0VBQ3JELHNCQUFzQjtDQUN2Qjs7QUFFRDs7RUFFRSxrQ0FBa0M7RUFDbEMsZUFBZTtDQUNoQjs7QUFFRDtFQUNFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLFlBQVk7RUFDWixhQUFhO0VBQ2Isa0NBQWtDO0VBQ2xDLGtCQUFrQjtFQUNsQiwrRUFBK0U7O0VBRS9FLHVDQUErQjs7VUFBL0IsK0JBQStCO0VBQy9CLDhDQUE4QztFQUM5QyxzQkFBc0I7Q0FDdkI7O0FBRUQ7O0VBRUUsa0NBQWtDO0VBQ2xDLGVBQWU7Q0FDaEI7O0FBRUQ7O0VBRUUsYUFBYTtFQUNiLFlBQVk7Q0FDYjs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsZ0JBQWdCO0NBQ2pCIiwiZmlsZSI6InNyYy9hcHAvbmV0d29yay9uZXR3b3JrLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjY29udGFpbmVyXHJcbntcclxuICBoZWlnaHQ6IDI1MHB4O1xyXG59XHJcblxyXG4jYmx1ZV90aWxle1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMxNTUyYWQ7XHJcbiAgd2lkdGg6IDM4MHB4O1xyXG4gIGhlaWdodDogNzBweDtcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW46IDIwcHg7XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgODBweCAjMDAwMDAwO1xyXG4gIHBhZGRpbmctdG9wOiA2MHB4O1xyXG4gIC8qdHV0YWogamFraWXFmyBkb2RhdGtvd2Ugc3R5bGUsIGphayBucC4gYm94LXNoYWRvdywgdGV4dC1zaGFkb3csIGJvcmRlciwgaXRwLiovXHJcblxyXG4gIHRleHQtZGVjb3JhdGlvbi1saW5lOiBvdmVybGluZTtcclxuICBiYWNrZ3JvdW5kLWltYWdlOiB1cmwoJ3VzdGF3aWVuaWFfaW50ZXJuZXRvd2UyLmpwZycpO1xyXG4gIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxufVxyXG5cclxuI2JsdWVfdGlsZTpob3ZlclxyXG57XHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTBweCAjMDAwMDAwO1xyXG4gIGN1cnNvcjpwb2ludGVyO1xyXG59XHJcblxyXG4jZ3JlZW5fdGlsZXtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWFhMTFmO1xyXG4gIHdpZHRoOiAzODBweDtcclxuICBoZWlnaHQ6IDcwcHg7XHJcbiAgZmxvYXQ6IGxlZnQ7XHJcbiAgbWFyZ2luOiAyMHB4O1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDgwcHggIzAwMDAwMDtcclxuICBwYWRkaW5nLXRvcDogNjBweDtcclxuICAvKnR1dGFqIGpha2llxZsgZG9kYXRrb3dlIHN0eWxlLCBqYWsgbnAuIGJveC1zaGFkb3csIHRleHQtc2hhZG93LCBib3JkZXIsIGl0cC4qL1xyXG5cclxuICB0ZXh0LWRlY29yYXRpb24tbGluZTogb3ZlcmxpbmU7XHJcbiAgYmFja2dyb3VuZC1pbWFnZTogdXJsKCdkYW5lX2ludGVybmV0b3dlLmpwZycpO1xyXG4gIGNvbG9yOiBibGFuY2hlZGFsbW9uZDtcclxufVxyXG5cclxuI2dyZWVuX3RpbGU6aG92ZXJcclxue1xyXG4gIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwcHggIzAwMDAwMDtcclxuICBjdXJzb3I6cG9pbnRlcjtcclxufVxyXG5cclxuLm1pZGRsZVxyXG57XHJcbiAgd2lkdGg6IDgwMHB4O1xyXG4gIGhlaWdodDogNXB4O1xyXG59XHJcblxyXG4jZG93blxyXG57XHJcbiAgd2lkdGg6IDg1MHB4O1xyXG4gIGhlaWdodDogMTUwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDc1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/network/network.component.html":
/*!************************************************!*\
  !*** ./src/app/network/network.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}}   \n  </div> \n  <div id = \"line\" >\n        |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"container\" >\n<div id=\"down\" >\n  <div id=\"blue_tile\" routerLink=\"/net-settings/{{user$.id}}\" style=\"text-align:center;  color:  white; text-decoration: none; font-weight: 700\" >\n    <div class=\"black\">\n      <i class=\"material-icons\">settings_remote</i> Ustawienia internetowe\n    </div>\n  </div>\n  <div id=\"green_tile\" routerLink=\"/net-data/{{user$.id}}\" style=\"text-align:center; color:  white; text-decoration: none; font-weight: 700\" >\n    <div class=\"black\">\n      <i class=\"material-icons\">description</i> Dane internetowe\n    </div> \n  </div>\n</div>\n<div class=\"middle\" style= \"clear:both\"></div>\n</div>\n\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/network/network.component.ts":
/*!**********************************************!*\
  !*** ./src/app/network/network.component.ts ***!
  \**********************************************/
/*! exports provided: NetworkComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NetworkComponent", function() { return NetworkComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var NetworkComponent = /** @class */ (function () {
    function NetworkComponent(route, data, location, authenticationService) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = 'Połączenie internetowe';
        this.route.params.subscribe(function (params) { return _this.user$ = params.id; });
    }
    NetworkComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    NetworkComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.user$).subscribe(function (data) { return _this.user$ = data; });
        this.logName = localStorage.getItem('username');
    };
    NetworkComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    NetworkComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-network',
            template: __webpack_require__(/*! ./network.component.html */ "./src/app/network/network.component.html"),
            styles: [__webpack_require__(/*! ./network.component.css */ "./src/app/network/network.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], NetworkComponent);
    return NetworkComponent;
}());



/***/ }),

/***/ "./src/app/processes/processes.component.css":
/*!***************************************************!*\
  !*** ./src/app/processes/processes.component.css ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".buttons\r\n{\r\n  border-color: #7c6a05;\r\n  border-radius: 30px;\r\n  color: #7c6a05;\r\n}\r\n\r\n#blockP\r\n{\r\n  /* box-shadow:inset 0 0 100px #235169; */\r\n  box-shadow:inset 0 0 100px  #6be41b;\r\n  width: 940px;\r\n  /* height: 610px; */\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 35px;\r\n  padding: 30px;\r\n  padding-top: 0px;\r\n  color: #110831;\r\n  font-stretch: ultra-expanded;\r\n  border-radius: 10px;\r\n}\r\n\r\n.label\r\n{\r\n  font-weight: 700;\r\n}\r\n\r\n.question1\r\n{\r\n  margin-top: 30px;\r\n  /* margin-bottom: 50px; */\r\n}\r\n\r\n#activeProcess\r\n{\r\n  margin-top: 50px;\r\n  margin-bottom: 100px;\r\n}\r\n\r\n#usersProcess\r\n{\r\n  margin-top: 50px;\r\n  margin-bottom: 100px;\r\n}\r\n\r\n#bookmarkPanel\r\n{\r\n  width: 1000px;\r\n  /* margin-bottom: 30px; */\r\n}\r\n\r\n#bookmarkPanelSecond\r\n{\r\n  width: 1000px;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.bookmark\r\n{\r\n  background-color: #3dac4d;\r\n  width: 470px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActive\r\n{\r\n  background-color: #3dac4d;\r\n  /* width: 313px; */\r\n  width: 470px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n.bookmarkSecond\r\n{\r\n  background-color: #5df173;\r\n  width: 470px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActiveSecond\r\n{\r\n  background-color: #5df173;\r\n  /* width: 313px; */\r\n  width: 470px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcHJvY2Vzc2VzL3Byb2Nlc3Nlcy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLHNCQUFzQjtFQUN0QixvQkFBb0I7RUFDcEIsZUFBZTtDQUNoQjs7QUFFRDs7RUFFRSx5Q0FBeUM7RUFDekMsb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYixvQkFBb0I7RUFDcEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLGNBQWM7RUFDZCxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QixvQkFBb0I7Q0FDckI7O0FBRUQ7O0VBRUUsaUJBQWlCO0NBQ2xCOztBQUVEOztFQUVFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLHFCQUFxQjtDQUN0Qjs7QUFFRDs7RUFFRSxpQkFBaUI7RUFDakIscUJBQXFCO0NBQ3RCOztBQUVEOztFQUVFLGNBQWM7RUFDZCwwQkFBMEI7Q0FDM0I7O0FBRUQ7O0VBRUUsY0FBYztFQUNkLG9CQUFvQjtDQUNyQjs7QUFFRDs7RUFFRSwwQkFBMEI7RUFDMUIsYUFBYTtFQUNiLGFBQWE7RUFDYixlQUFlO0VBQ2YsaUJBQWlCO0NBQ2xCOztBQUVEOztFQUVFLDBCQUEwQjtFQUMxQixtQkFBbUI7RUFDbkIsYUFBYTtFQUNiLGFBQWE7RUFDYixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLHlCQUF5QjtFQUN6Qix3QkFBd0I7RUFDeEIsc0JBQXNCO0NBQ3ZCOztBQUVEOztFQUVFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7O0VBRUUsMEJBQTBCO0VBQzFCLG1CQUFtQjtFQUNuQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGlCQUFpQjtFQUNqQixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHdCQUF3QjtFQUN4QixzQkFBc0I7Q0FDdkIiLCJmaWxlIjoic3JjL2FwcC9wcm9jZXNzZXMvcHJvY2Vzc2VzLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYnV0dG9uc1xyXG57XHJcbiAgYm9yZGVyLWNvbG9yOiAjN2M2YTA1O1xyXG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgY29sb3I6ICM3YzZhMDU7XHJcbn1cclxuXHJcbiNibG9ja1Bcclxue1xyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwMHB4ICMyMzUxNjk7ICovXHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTAwcHggICM2YmU0MWI7XHJcbiAgd2lkdGg6IDk0MHB4O1xyXG4gIC8qIGhlaWdodDogNjEwcHg7ICovXHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzVweDtcclxuICBwYWRkaW5nOiAzMHB4O1xyXG4gIHBhZGRpbmctdG9wOiAwcHg7XHJcbiAgY29sb3I6ICMxMTA4MzE7XHJcbiAgZm9udC1zdHJldGNoOiB1bHRyYS1leHBhbmRlZDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG59XHJcblxyXG4ubGFiZWxcclxue1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5xdWVzdGlvbjFcclxue1xyXG4gIG1hcmdpbi10b3A6IDMwcHg7XHJcbiAgLyogbWFyZ2luLWJvdHRvbTogNTBweDsgKi9cclxufVxyXG5cclxuI2FjdGl2ZVByb2Nlc3Ncclxue1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTAwcHg7XHJcbn1cclxuXHJcbiN1c2Vyc1Byb2Nlc3Ncclxue1xyXG4gIG1hcmdpbi10b3A6IDUwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMTAwcHg7XHJcbn1cclxuXHJcbiNib29rbWFya1BhbmVsXHJcbntcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIC8qIG1hcmdpbi1ib3R0b206IDMwcHg7ICovXHJcbn1cclxuXHJcbiNib29rbWFya1BhbmVsU2Vjb25kXHJcbntcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbi5ib29rbWFya1xyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNkYWM0ZDtcclxuICB3aWR0aDogNDcwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5ib29rbWFya0FjdGl2ZVxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzNkYWM0ZDtcclxuICAvKiB3aWR0aDogMzEzcHg7ICovXHJcbiAgd2lkdGg6IDQ3MHB4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTczMDE3O1xyXG4gIGJvcmRlci1ib3R0b20td2lkdGg6IDRweDtcclxuICBib3JkZXItcmlnaHQtd2lkdGg6IDRweDtcclxuICBib3JkZXItY29sb3I6ICMxNzMwMTc7XHJcbn1cclxuXHJcbi5ib29rbWFya1NlY29uZFxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzVkZjE3MztcclxuICB3aWR0aDogNDcwcHg7XHJcbiAgaGVpZ2h0OiAzMHB4O1xyXG4gIGNvbG9yOiAjZmZmZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn1cclxuXHJcbi5ib29rbWFya0FjdGl2ZVNlY29uZFxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzVkZjE3MztcclxuICAvKiB3aWR0aDogMzEzcHg7ICovXHJcbiAgd2lkdGg6IDQ3MHB4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTczMDE3O1xyXG4gIGJvcmRlci1ib3R0b20td2lkdGg6IDRweDtcclxuICBib3JkZXItcmlnaHQtd2lkdGg6IDRweDtcclxuICBib3JkZXItY29sb3I6ICMxNzMwMTc7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/processes/processes.component.html":
/*!****************************************************!*\
  !*** ./src/app/processes/processes.component.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      {{logName}} \n  </div>\n  <div id = \"line\" >\n      |\n  </div> \n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }}\n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"middle0\" style= \"clear:both\"></div>\n<div id=\"blockP\">\n    <div  id = \"bookmarkPanel\" >\n      <button class = \"bookmarkActive\" *ngIf = \"bookmark === 1\">BIEŻĄCE</button> \n      <button class = \"bookmark\" *ngIf = \"bookmark != 1\" (click)=\"changeBookmark(1)\">BIEŻĄCE</button>\n      <button class = \"bookmarkActive\" *ngIf = \"bookmark === 2\">HISTORIA</button>\n      <button class = \"bookmark\" *ngIf = \"bookmark != 2\" (click)=\"changeBookmark(2)\">HISTORIA</button> \n    </div>\n    \n    <div id = \"bookmark1\" *ngIf = \"bookmark === 1\">\n      <div class = \"question1\" *ngIf = \"!activeAndUserProcessMonitorSettings.isActiveProcessMonitored\">\n        <p>Czy monitorować aktywny proces: \n            <font color = \"red\" >  \n          <label  class = \"label\"> NIE   </label> </font> \n          <button class = \"buttons\" (click)=\"updateActiveProcessMonitoringSettings()\" >ROZPOCZNIJ</button>\n        </p>\n      </div>\n      <div  class = \"question1\" *ngIf = \"activeAndUserProcessMonitorSettings.isActiveProcessMonitored\">\n        <p>Czy monitorować aktywny proces:                                   \n          <font color = \"lime\" >  \n          <label  class = \"label\" >TAK     </label> </font>\n          <button class = \"buttons\"  (click)=\"updateActiveProcessMonitoringSettings()\" >ZAKOŃCZ</button>\n        </p>\n      </div>\n      <div id = \"activeProcess\" *ngIf = \"activeAndUserProcessMonitorSettings.isActiveProcessMonitored\">\n        <style> th { border: 2px solid black; color: green;} </style>\n        <style> td { border: 1px solid black;} </style>\n        <style> caption { color: black; font-weight: 700;} </style>\n        <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n            <caption>AKTYWNY PROCES</caption>\n          <tr>\n              <th style = \"width: 170px\">Kiedy zebrano informację</th>\n              <th style = \"width: 270px\">Nazwa</th>\n              <th style = \"width: 70px\">Liczba wątków</th>\n              <th style = \"width: 170px\">Od kiedy działa</th>\n              <th style = \"width: 170px\">Zapis [kB]</th>\n              <th style = \"width: 170px\">Odczyt [kB]</th>\n          </tr>\n          <tr *ngFor=\"let activeProcess of activeProcesses\">\n            <td>{{activeProcess.whenInfoCollected}}</td>\n            <td>{{activeProcess.processName}}</td>\n            <td>{{activeProcess.threadsNumber}}</td>\n            <td>{{activeProcess.whenStarted}}</td>\n            <td>{{activeProcess.kilobytesEntry}}</td>\n            <td>{{activeProcess.kilobytesRead}}</td>\n          </tr>\n        </table>\n      </div>\n    <hr />\n\n    <div class = \"question2\" *ngIf = \"!activeAndUserProcessMonitorSettings.areProcessesMonitored\">\n        <p>Czy monitorować procesy użytkownika: \n            <font color = \"red\" >  \n          <label  class = \"label\"> NIE   </label> </font> \n          <button class = \"buttons\" (click)=\"updateUserProcessMonitoringSettings()\" >ROZPOCZNIJ</button>\n        </p>\n      </div>\n      <div  class = \"question2\" *ngIf = \"activeAndUserProcessMonitorSettings.areProcessesMonitored\">\n        <p>Czy monitorować procesy użytkownika:                                 \n          <font color = \"lime\" >  \n          <label  class = \"label\" >TAK     </label> </font>\n          <button class = \"buttons\"  (click)=\"updateUserProcessMonitoringSettings()\" >ZAKOŃCZ</button>\n        </p>\n      </div>\n      <div  class = \"question2\" *ngIf = \"activeAndUserProcessMonitorSettings.areProcessesMonitored\">\n        <p> Co ile czasu zbierać informacje o procesach użytkownika:    \n          <font color = \"blue\" >\n            <label   class = \"label\" *ngIf = \"activeAndUserProcessMonitorSettings.processMonitoringFrequency === 1\" >{{activeAndUserProcessMonitorSettings.processMonitoringFrequency}} sekunda </label> \n            <label   class = \"label\" *ngIf = \"activeAndUserProcessMonitorSettings.processMonitoringFrequency > 1 && activeAndUserProcessMonitorSettings.processMonitoringFrequency < 5\" >{{activeAndUserProcessMonitorSettings.processMonitoringFrequency}} sekundy </label>\n            <label   class = \"label\" *ngIf = \"activeAndUserProcessMonitorSettings.processMonitoringFrequency >=5\" >{{activeAndUserProcessMonitorSettings.processMonitoringFrequency}} sekund  </label>\n          </font> \n          <input type=\"number\" [(ngModel)]= 'freq' placeholder = \"Nowa liczba\" style=\"width: 90px; margin-left: 25px; margin-right: 15px\"/> \n          <button class = \"buttons\"  (click)=\"updateUserProcessMonitorFrequency()\" >ZMIEŃ</button>\n        </p>\n      </div>\n      \n      <div id = \"usersProcess\" *ngIf = \"activeAndUserProcessMonitorSettings.areProcessesMonitored\">\n        <style> th { border: 2px solid black; color: green;} </style>\n        <style> td { border: 1px solid black;} </style>\n        <style> caption { color: black; font-weight: 700;} </style>\n        <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n            <caption>PROCESY UŻYTKOWNIKA</caption>\n          <tr>\n              <th style = \"width: 170px\">Kiedy zebrano informację</th>\n              <th style = \"width: 270px\">Nazwa</th>\n              <th style = \"width: 70px\">Liczba wątków</th>\n              <th style = \"width: 170px\">Od kiedy działa</th>\n              <th style = \"width: 170px\">Zapis [kB]</th>\n              <th style = \"width: 170px\">Odczyt [kB]</th>\n          </tr>\n          <tr *ngFor=\"let userProcess of userProcesses\">\n            <td>{{userProcess.whenCollected}}</td>\n            <td>{{userProcess.processName}}</td>\n            <td>{{userProcess.threadsNumber}}</td>\n            <td>{{userProcess.whenProcessStarted}}</td>\n            <td>{{userProcess.kilobytesEntry}}</td>\n            <td>{{userProcess.kilobytesRead}}</td>\n          </tr>\n        </table>\n        </div>\n    </div>\n\n    <div id = \"bookmark1\" *ngIf = \"bookmark === 2\">\n        <div  id = \"bookmarkPanelSecond\" >\n            <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 1\">PROCESY AKTYWNE</button> \n            <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 1\" (click)=\"changeBookmarkSecond(1)\">PROCESY AKTYWNE</button>\n            <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 2\">PROCESY UŻYTKOWNIKA</button>\n            <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 2\" (click)=\"changeBookmarkSecond(2)\">PROCESY UŻYTKOWNIKA</button> \n          </div>\n          <div id = \"bookmarkSecond1\" *ngIf = \"bookmarkSecond === 1\">\n            <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n                <caption>AKTYWNY PROCES</caption>\n              <tr>\n                  <th style = \"width: 170px\">Kiedy zebrano informację</th>\n                  <th style = \"width: 270px\">Nazwa</th>\n                  <th style = \"width: 70px\">Liczba wątków</th>\n                  <th style = \"width: 170px\">Od kiedy działa</th>\n                  <th style = \"width: 170px\">Zapis [kB]</th>\n                  <th style = \"width: 170px\">Odczyt [kB]</th>\n              </tr>\n              <tr *ngFor=\"let historyActiveProcess of historyActiveProcesses  | paginate: { itemsPerPage: 20, currentPage: p }\">\n                <td>{{historyActiveProcess.whenInfoCollected}}</td>\n                <td>{{historyActiveProcess.processName}}</td>\n                <td>{{historyActiveProcess.threadsNumber}}</td>\n                <td>{{historyActiveProcess.whenStarted}}</td>\n                <td>{{historyActiveProcess.kilobytesEntry}}</td>\n                <td>{{historyActiveProcess.kilobytesRead}}</td>\n              </tr>\n            </table>\n            <pagination-controls (pageChange)=\"p = $event\" previousLabel=\"Poprzednia\"\n            nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n          </div>\n          <div id = \"bookmarkSecond2\" *ngIf = \"bookmarkSecond === 2\">\n            <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\n                  <caption>PROCESY UŻYTKOWNIKA</caption>\n                <tr>\n                    <th style = \"width: 170px\">Kiedy zebrano informację</th>\n                    <th style = \"width: 270px\">Nazwa</th>\n                    <th style = \"width: 70px\">Liczba wątków</th>\n                    <th style = \"width: 170px\">Od kiedy działa</th>\n                    <th style = \"width: 170px\">Zapis [kB]</th>\n                    <th style = \"width: 170px\">Odczyt [kB]</th>\n                </tr>\n                <tr *ngFor=\"let historyUserProcess of historyUserProcesses   | paginate: { itemsPerPage: 20, currentPage: q }\">\n                  <td>{{historyUserProcess.whenCollected}}</td>\n                  <td>{{historyUserProcess.processName}}</td>\n                  <td>{{historyUserProcess.threadsNumber}}</td>\n                  <td>{{historyUserProcess.whenProcessStarted}}</td>\n                  <td>{{historyUserProcess.kilobytesEntry}}</td>\n                  <td>{{historyUserProcess.kilobytesRead}}</td>\n                </tr>\n              </table> \n              <pagination-controls (pageChange)=\"q = $event\" previousLabel=\"Poprzednia\"\n              nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\n          </div>\n    </div>\n\n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/processes/processes.component.ts":
/*!**************************************************!*\
  !*** ./src/app/processes/processes.component.ts ***!
  \**************************************************/
/*! exports provided: ProcessesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProcessesComponent", function() { return ProcessesComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ProcessesComponent = /** @class */ (function () {
    function ProcessesComponent(http, route, data, location, authenticationService) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = "Procesy";
        this.is1 = true;
        this.is2 = true;
        this.freq = 1;
        this.bookmark = 1; // 1 or 2
        this.bookmarkSecond = 1; // 1 or 2
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
        this.token = localStorage.getItem('token');
    }
    ProcessesComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    ProcessesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.data.getActiveProcess(this.userId).subscribe(function (data) { return _this.activeProcesses = data; }
        // data => console.log(data)
        );
        this.data.getActiveProcessHistory(this.userId).subscribe(function (data) { return _this.historyActiveProcesses = data; }
        // data => console.log(data)
        );
        this.data.getUserProcess(this.userId).subscribe(function (data) { return _this.userProcesses = data; }
        // data => console.log(data)
        );
        this.data.getUserProcessHistory(this.userId).subscribe(function (data) { return _this.historyUserProcesses = data; }
        // data => console.log(data)
        );
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(0, 7000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function () { return _this.data.getActiveAndUserProcessMonitoringSettings(_this.userId); })).subscribe(function (data) { return _this.activeAndUserProcessMonitorSettings = data; });
        this.logName = localStorage.getItem('username');
    };
    ProcessesComponent.prototype.updateActiveProcessMonitoringSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/process-settings/' + this.userId + '/change-active-process-monitoring-status', {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateActiveProcessMonitoringSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateActiveProcessMonitoringSettings", error);
        });
    };
    ProcessesComponent.prototype.updateUserProcessMonitoringSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/process-settings/' + this.userId + '/change-processes-monitoring-status', {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateUserProcessMonitoringSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateUserProcessMonitoringSettings", error);
        });
    };
    ProcessesComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    ProcessesComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    ProcessesComponent.prototype.change1 = function () { this.is1 = !this.is1; };
    ProcessesComponent.prototype.change2 = function () { this.is2 = !this.is2; };
    ProcessesComponent.prototype.changeBookmark = function (nr) {
        this.bookmark = nr;
    };
    ProcessesComponent.prototype.changeBookmarkSecond = function (nr) {
        this.bookmarkSecond = nr;
    };
    // nie musze przekazywac paraetrow, bo userId jest widoczny w komponencie, a na freq jest 
    // wiazanie ng model i tez pochodzi z tego komponentu, wiec zawsze swieze widoczne 
    ProcessesComponent.prototype.updateUserProcessMonitorFrequency = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/process-settings/' + this.userId + '/change-processes-monitoring-interval/' + this.freq, {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateUserProcessMonitorFrequency", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateUserProcessMonitorFrequency", error);
        });
    };
    ProcessesComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-processes',
            template: __webpack_require__(/*! ./processes.component.html */ "./src/app/processes/processes.component.html"),
            styles: [__webpack_require__(/*! ./processes.component.css */ "./src/app/processes/processes.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_8__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_7__["AuthenticationService"]])
    ], ProcessesComponent);
    return ProcessesComponent;
}());



/***/ }),

/***/ "./src/app/security/security.component.css":
/*!*************************************************!*\
  !*** ./src/app/security/security.component.css ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".line-break {\r\n    width: 90px; \r\n    overflow-wrap: break-word;\r\n    }\r\n \r\n  \r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VjdXJpdHkvc2VjdXJpdHkuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLFlBQVk7SUFDWiwwQkFBMEI7S0FDekIiLCJmaWxlIjoic3JjL2FwcC9zZWN1cml0eS9zZWN1cml0eS5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmxpbmUtYnJlYWsge1xyXG4gICAgd2lkdGg6IDkwcHg7IFxyXG4gICAgb3ZlcmZsb3ctd3JhcDogYnJlYWstd29yZDtcclxuICAgIH1cclxuIFxyXG4gIFxyXG4gICJdfQ== */"

/***/ }),

/***/ "./src/app/security/security.component.html":
/*!**************************************************!*\
  !*** ./src/app/security/security.component.html ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\n  <div id = \"tracked\" >\n    Obserwujesz użytkownika {{user$.username}} \n  </div>  \n  <div id = \"tracking\" >\n      Logname \n  </div> \n  <div id = \"line\" >\n      |\n  </div>\n  <div id = \"Logout\" (click)=\"logout()\">\n      Wyloguj \n  </div> \n  <div class=\"middle\" style= \"clear:both\"></div>                      \n</div>\n<div id=\"container1\">\n  <div id=\"sectionTitle\">\n    <h1>\n      {{ title }} \n    </h1>\n  </div>\n  <div id=\"back\">\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\n  </div>\n  <div id=\"home\">\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\n        <i class=\"material-icons\">home</i>\n      </a>\n  </div>\n</div>\n<div id=\"block\" >\n  \n \n</div>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/security/security.component.ts":
/*!************************************************!*\
  !*** ./src/app/security/security.component.ts ***!
  \************************************************/
/*! exports provided: SecurityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SecurityComponent", function() { return SecurityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var SecurityComponent = /** @class */ (function () {
    function SecurityComponent(route, data, location) {
        var _this = this;
        this.route = route;
        this.data = data;
        this.location = location;
        this.title = "Uprawnienia";
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
    }
    SecurityComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    SecurityComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_5__["timer"])(0, 7000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_6__["switchMap"])(function () { return _this.data.getMessages(_this.userId); })).subscribe(function (data) { return _this.messageDatas = data; });
    };
    SecurityComponent.prototype.ngOnDestroy = function () {
        this.subscription.unsubscribe();
    };
    SecurityComponent.prototype.logout = function () {
        console.log("wylogowanie");
    };
    SecurityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-security',
            template: __webpack_require__(/*! ./security.component.html */ "./src/app/security/security.component.html"),
            styles: [__webpack_require__(/*! ./security.component.css */ "./src/app/security/security.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"]])
    ], SecurityComponent);
    return SecurityComponent;
}());



/***/ }),

/***/ "./src/app/usb/usb.component.css":
/*!***************************************!*\
  !*** ./src/app/usb/usb.component.css ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".buttons\r\n{\r\n  border-color: #7c6a05;\r\n  border-radius: 30px;\r\n  color: #7c6a05;\r\n  margin-right: 20px;\r\n}\r\n\r\n.buttons:hover\r\n{\r\n  border-color: #f7d30c;\r\n  border-radius: 30px;\r\n  /* color: #f7d30c; */\r\n}\r\n\r\n#blockUSB\r\n{\r\n  /* box-shadow:inset 0 0 100px #235169; */\r\n  box-shadow:inset 0 0 100px  #6be41b;\r\n  width: 980px;\r\n  /* width: auto; */\r\n  /* height: 610px; */\r\n  margin-left: auto;\r\n  margin-right: auto;\r\n  margin-top: 15px;\r\n  margin-bottom: 35px;\r\n  padding-bottom: 30px;\r\n  padding-left: 10px;\r\n  padding-right: 10px;\r\n  padding-top: 0px;\r\n  color: #110831;\r\n  font-stretch: ultra-expanded;\r\n  border-radius: 10px;\r\n}\r\n\r\n.label\r\n{\r\n  font-weight: 700;\r\n}\r\n\r\n.question1\r\n{\r\n  margin-top: 30px;\r\n  /* margin-bottom: 50px; */\r\n}\r\n\r\n#activeProcess\r\n{\r\n  margin-top: 50px;\r\n  margin-bottom: 100px;\r\n}\r\n\r\n#bookmarkPanel\r\n{\r\n  width: 1000px;\r\n  /* margin-bottom: 30px; */\r\n}\r\n\r\n.bookmark\r\n{\r\n  background-color: #3dac4d;\r\n  width: 330px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActive\r\n{\r\n  background-color: #3dac4d;\r\n  width: 313px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n.information\r\n{\r\n  font-weight: 700;\r\n  color: #500215;\r\n  margin-top: 50px;\r\n  height: 100px;\r\n}\r\n\r\n#scan\r\n{\r\n  margin-top: 50px; \r\n  /* background-color: rgb(198, 247, 81); */\r\n}\r\n\r\n#see\r\n{\r\n  width: 105px;\r\n  font-weight: 700; \r\n  background-color: rgb(183, 243, 44);\r\n  color: rgb(4, 73, 13);\r\n  border-color: rgb(4, 73, 13);\r\n  border-style: double;\r\n  border-radius: 5px;\r\n}\r\n\r\n#see:hover\r\n{\r\n  background-color: rgb(247, 203, 7);\r\n  color: rgb(2, 53, 9);\r\n  border-color: rgb(2, 53, 9);\r\n}\r\n\r\n#bookmarkPanelSecond\r\n{\r\n  width: 1000px;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n#bookmarkPanelThird\r\n{\r\n  width: 1000px;\r\n  margin-bottom: 30px;\r\n}\r\n\r\n.bookmarkSecond\r\n{\r\n  background-color: #5df173;\r\n  width: 487px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkThird\r\n{\r\n  background-color: #5df173;\r\n  width: 487px;\r\n  height: 30px;\r\n  color: #ffffff;\r\n  font-weight: 700;\r\n}\r\n\r\n.bookmarkActiveSecond\r\n{\r\n  background-color: #5df173;\r\n  /* width: 313px; */\r\n  width: 487px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n.bookmarkActiveThird\r\n{\r\n  background-color: #5df173;\r\n  /* width: 313px; */\r\n  width: 487px;\r\n  height: 30px;\r\n  font-weight: 700;\r\n  color: #173017;\r\n  border-bottom-width: 4px;\r\n  border-right-width: 4px;\r\n  border-color: #173017;\r\n}\r\n\r\n#backDesign\r\n{\r\n  margin-bottom: 30px;\r\n}\r\n\r\n#backdesignBtn\r\n{\r\n  font-weight: 700; \r\n  background-color: rgb(80, 110, 9);\r\n  color: rgb(209, 245, 8);\r\n  border-color: rgb(209, 245, 8);\r\n  border-style: double;\r\n  border-radius: 5px;\r\n}\r\n\r\n#backdesignBtn:hover\r\n{\r\n  background-color: rgb(209, 245, 8);\r\n  color: rgb(80, 110, 9);\r\n  border-color: rgb(80, 110, 9);\r\n}\r\n\r\n#info\r\n{\r\n  margin-right: 20px;\r\n  margin-left: 10px;\r\n  color: rgb(41, 35, 1);\r\n  font-weight: 700;\r\n}\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdXNiL3VzYi5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBOztFQUVFLHNCQUFzQjtFQUN0QixvQkFBb0I7RUFDcEIsZUFBZTtFQUNmLG1CQUFtQjtDQUNwQjs7QUFFRDs7RUFFRSxzQkFBc0I7RUFDdEIsb0JBQW9CO0VBQ3BCLHFCQUFxQjtDQUN0Qjs7QUFFRDs7RUFFRSx5Q0FBeUM7RUFDekMsb0NBQW9DO0VBQ3BDLGFBQWE7RUFDYixrQkFBa0I7RUFDbEIsb0JBQW9CO0VBQ3BCLGtCQUFrQjtFQUNsQixtQkFBbUI7RUFDbkIsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixxQkFBcUI7RUFDckIsbUJBQW1CO0VBQ25CLG9CQUFvQjtFQUNwQixpQkFBaUI7RUFDakIsZUFBZTtFQUNmLDZCQUE2QjtFQUM3QixvQkFBb0I7Q0FDckI7O0FBRUQ7O0VBRUUsaUJBQWlCO0NBQ2xCOztBQUVEOztFQUVFLGlCQUFpQjtFQUNqQiwwQkFBMEI7Q0FDM0I7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLHFCQUFxQjtDQUN0Qjs7QUFFRDs7RUFFRSxjQUFjO0VBQ2QsMEJBQTBCO0NBQzNCOztBQUVEOztFQUVFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7O0VBRUUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHNCQUFzQjtDQUN2Qjs7QUFFRDs7RUFFRSxpQkFBaUI7RUFDakIsZUFBZTtFQUNmLGlCQUFpQjtFQUNqQixjQUFjO0NBQ2Y7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLDBDQUEwQztDQUMzQzs7QUFFRDs7RUFFRSxhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLG9DQUFvQztFQUNwQyxzQkFBc0I7RUFDdEIsNkJBQTZCO0VBQzdCLHFCQUFxQjtFQUNyQixtQkFBbUI7Q0FDcEI7O0FBRUQ7O0VBRUUsbUNBQW1DO0VBQ25DLHFCQUFxQjtFQUNyQiw0QkFBNEI7Q0FDN0I7O0FBRUQ7O0VBRUUsY0FBYztFQUNkLG9CQUFvQjtDQUNyQjs7QUFFRDs7RUFFRSxjQUFjO0VBQ2Qsb0JBQW9CO0NBQ3JCOztBQUVEOztFQUVFLDBCQUEwQjtFQUMxQixhQUFhO0VBQ2IsYUFBYTtFQUNiLGVBQWU7RUFDZixpQkFBaUI7Q0FDbEI7O0FBRUQ7O0VBRUUsMEJBQTBCO0VBQzFCLGFBQWE7RUFDYixhQUFhO0VBQ2IsZUFBZTtFQUNmLGlCQUFpQjtDQUNsQjs7QUFFRDs7RUFFRSwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHNCQUFzQjtDQUN2Qjs7QUFFRDs7RUFFRSwwQkFBMEI7RUFDMUIsbUJBQW1CO0VBQ25CLGFBQWE7RUFDYixhQUFhO0VBQ2IsaUJBQWlCO0VBQ2pCLGVBQWU7RUFDZix5QkFBeUI7RUFDekIsd0JBQXdCO0VBQ3hCLHNCQUFzQjtDQUN2Qjs7QUFFRDs7RUFFRSxvQkFBb0I7Q0FDckI7O0FBRUQ7O0VBRUUsaUJBQWlCO0VBQ2pCLGtDQUFrQztFQUNsQyx3QkFBd0I7RUFDeEIsK0JBQStCO0VBQy9CLHFCQUFxQjtFQUNyQixtQkFBbUI7Q0FDcEI7O0FBRUQ7O0VBRUUsbUNBQW1DO0VBQ25DLHVCQUF1QjtFQUN2Qiw4QkFBOEI7Q0FDL0I7O0FBRUQ7O0VBRUUsbUJBQW1CO0VBQ25CLGtCQUFrQjtFQUNsQixzQkFBc0I7RUFDdEIsaUJBQWlCO0NBQ2xCIiwiZmlsZSI6InNyYy9hcHAvdXNiL3VzYi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbnNcclxue1xyXG4gIGJvcmRlci1jb2xvcjogIzdjNmEwNTtcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIGNvbG9yOiAjN2M2YTA1O1xyXG4gIG1hcmdpbi1yaWdodDogMjBweDtcclxufVxyXG5cclxuLmJ1dHRvbnM6aG92ZXJcclxue1xyXG4gIGJvcmRlci1jb2xvcjogI2Y3ZDMwYztcclxuICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gIC8qIGNvbG9yOiAjZjdkMzBjOyAqL1xyXG59XHJcblxyXG4jYmxvY2tVU0Jcclxue1xyXG4gIC8qIGJveC1zaGFkb3c6aW5zZXQgMCAwIDEwMHB4ICMyMzUxNjk7ICovXHJcbiAgYm94LXNoYWRvdzppbnNldCAwIDAgMTAwcHggICM2YmU0MWI7XHJcbiAgd2lkdGg6IDk4MHB4O1xyXG4gIC8qIHdpZHRoOiBhdXRvOyAqL1xyXG4gIC8qIGhlaWdodDogNjEwcHg7ICovXHJcbiAgbWFyZ2luLWxlZnQ6IGF1dG87XHJcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzVweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICBwYWRkaW5nLWxlZnQ6IDEwcHg7XHJcbiAgcGFkZGluZy1yaWdodDogMTBweDtcclxuICBwYWRkaW5nLXRvcDogMHB4O1xyXG4gIGNvbG9yOiAjMTEwODMxO1xyXG4gIGZvbnQtc3RyZXRjaDogdWx0cmEtZXhwYW5kZWQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxufVxyXG5cclxuLmxhYmVsXHJcbntcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4ucXVlc3Rpb24xXHJcbntcclxuICBtYXJnaW4tdG9wOiAzMHB4O1xyXG4gIC8qIG1hcmdpbi1ib3R0b206IDUwcHg7ICovXHJcbn1cclxuXHJcbiNhY3RpdmVQcm9jZXNzXHJcbntcclxuICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEwMHB4O1xyXG59XHJcblxyXG4jYm9va21hcmtQYW5lbFxyXG57XHJcbiAgd2lkdGg6IDEwMDBweDtcclxuICAvKiBtYXJnaW4tYm90dG9tOiAzMHB4OyAqL1xyXG59XHJcblxyXG4uYm9va21hcmtcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZGFjNGQ7XHJcbiAgd2lkdGg6IDMzMHB4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4uYm9va21hcmtBY3RpdmVcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMzZGFjNGQ7XHJcbiAgd2lkdGg6IDMxM3B4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTczMDE3O1xyXG4gIGJvcmRlci1ib3R0b20td2lkdGg6IDRweDtcclxuICBib3JkZXItcmlnaHQtd2lkdGg6IDRweDtcclxuICBib3JkZXItY29sb3I6ICMxNzMwMTc7XHJcbn1cclxuXHJcbi5pbmZvcm1hdGlvblxyXG57XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogIzUwMDIxNTtcclxuICBtYXJnaW4tdG9wOiA1MHB4O1xyXG4gIGhlaWdodDogMTAwcHg7XHJcbn1cclxuXHJcbiNzY2FuXHJcbntcclxuICBtYXJnaW4tdG9wOiA1MHB4OyBcclxuICAvKiBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMTk4LCAyNDcsIDgxKTsgKi9cclxufVxyXG5cclxuI3NlZVxyXG57XHJcbiAgd2lkdGg6IDEwNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7IFxyXG4gIGJhY2tncm91bmQtY29sb3I6IHJnYigxODMsIDI0MywgNDQpO1xyXG4gIGNvbG9yOiByZ2IoNCwgNzMsIDEzKTtcclxuICBib3JkZXItY29sb3I6IHJnYig0LCA3MywgMTMpO1xyXG4gIGJvcmRlci1zdHlsZTogZG91YmxlO1xyXG4gIGJvcmRlci1yYWRpdXM6IDVweDtcclxufVxyXG5cclxuI3NlZTpob3ZlclxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI0NywgMjAzLCA3KTtcclxuICBjb2xvcjogcmdiKDIsIDUzLCA5KTtcclxuICBib3JkZXItY29sb3I6IHJnYigyLCA1MywgOSk7XHJcbn1cclxuXHJcbiNib29rbWFya1BhbmVsU2Vjb25kXHJcbntcclxuICB3aWR0aDogMTAwMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbiNib29rbWFya1BhbmVsVGhpcmRcclxue1xyXG4gIHdpZHRoOiAxMDAwcHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxufVxyXG5cclxuLmJvb2ttYXJrU2Vjb25kXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNWRmMTczO1xyXG4gIHdpZHRoOiA0ODdweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgY29sb3I6ICNmZmZmZmY7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxufVxyXG5cclxuLmJvb2ttYXJrVGhpcmRcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM1ZGYxNzM7XHJcbiAgd2lkdGg6IDQ4N3B4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBjb2xvcjogI2ZmZmZmZjtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4uYm9va21hcmtBY3RpdmVTZWNvbmRcclxue1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICM1ZGYxNzM7XHJcbiAgLyogd2lkdGg6IDMxM3B4OyAqL1xyXG4gIHdpZHRoOiA0ODdweDtcclxuICBoZWlnaHQ6IDMwcHg7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBjb2xvcjogIzE3MzAxNztcclxuICBib3JkZXItYm90dG9tLXdpZHRoOiA0cHg7XHJcbiAgYm9yZGVyLXJpZ2h0LXdpZHRoOiA0cHg7XHJcbiAgYm9yZGVyLWNvbG9yOiAjMTczMDE3O1xyXG59XHJcblxyXG4uYm9va21hcmtBY3RpdmVUaGlyZFxyXG57XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzVkZjE3MztcclxuICAvKiB3aWR0aDogMzEzcHg7ICovXHJcbiAgd2lkdGg6IDQ4N3B4O1xyXG4gIGhlaWdodDogMzBweDtcclxuICBmb250LXdlaWdodDogNzAwO1xyXG4gIGNvbG9yOiAjMTczMDE3O1xyXG4gIGJvcmRlci1ib3R0b20td2lkdGg6IDRweDtcclxuICBib3JkZXItcmlnaHQtd2lkdGg6IDRweDtcclxuICBib3JkZXItY29sb3I6ICMxNzMwMTc7XHJcbn1cclxuXHJcbiNiYWNrRGVzaWduXHJcbntcclxuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG59XHJcblxyXG4jYmFja2Rlc2lnbkJ0blxyXG57XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDsgXHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDgwLCAxMTAsIDkpO1xyXG4gIGNvbG9yOiByZ2IoMjA5LCAyNDUsIDgpO1xyXG4gIGJvcmRlci1jb2xvcjogcmdiKDIwOSwgMjQ1LCA4KTtcclxuICBib3JkZXItc3R5bGU6IGRvdWJsZTtcclxuICBib3JkZXItcmFkaXVzOiA1cHg7XHJcbn1cclxuXHJcbiNiYWNrZGVzaWduQnRuOmhvdmVyXHJcbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMjA5LCAyNDUsIDgpO1xyXG4gIGNvbG9yOiByZ2IoODAsIDExMCwgOSk7XHJcbiAgYm9yZGVyLWNvbG9yOiByZ2IoODAsIDExMCwgOSk7XHJcbn1cclxuXHJcbiNpbmZvXHJcbntcclxuICBtYXJnaW4tcmlnaHQ6IDIwcHg7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XHJcbiAgY29sb3I6IHJnYig0MSwgMzUsIDEpO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/usb/usb.component.html":
/*!****************************************!*\
  !*** ./src/app/usb/usb.component.html ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div id = \"begining\">\r\n  <div id = \"tracked\" >\r\n    Obserwujesz użytkownika {{user$.username}} \r\n  </div>  \r\n  <div id = \"tracking\" >\r\n      {{logName}}\r\n  </div>\r\n  <div id = \"line\" >\r\n      |\r\n  </div> \r\n  <div id = \"Logout\" (click)=\"logout()\">\r\n      Wyloguj \r\n  </div> \r\n  <div class=\"middle\" style= \"clear:both\"></div>                      \r\n</div>\r\n<div id=\"container1\">\r\n  <div id=\"sectionTitle\">\r\n    <h1>\r\n      {{ title }}\r\n    </h1>\r\n  </div>\r\n  <div id=\"back\">\r\n    <i (click)=\"goBack()\" class=\"material-icons\">arrow_back</i>\r\n  </div>\r\n  <div id=\"home\">\r\n      <a routerLink=\"/home2\" [class.activated]=\"currentUrl == '/home2'\">\r\n        <i class=\"material-icons\">home</i>\r\n      </a>\r\n  </div>\r\n</div>\r\n<div id=\"middle0\" style= \"clear:both\"></div>\r\n<div id=\"blockUSB\">\r\n  <div  id = \"bookmarkPanel\" >\r\n    <button class = \"bookmarkActive\" *ngIf = \"bookmark === 1\">URZĄDZENIA</button> \r\n    <button class = \"bookmark\" *ngIf = \"bookmark != 1\" (click)=\"changeBookmark(1)\">URZĄDZENIA</button>\r\n    <button class = \"bookmarkActive\" *ngIf = \"bookmark === 2\">DYSKI ZDALNE</button>\r\n    <button class = \"bookmark\" *ngIf = \"bookmark != 2\" (click)=\"changeBookmark(2)\">DYSKI ZDALNE</button> \r\n    <button class = \"bookmarkActive\" *ngIf = \"bookmark === 3\">USTAWIENIA</button> \r\n    <button class = \"bookmark\" *ngIf = \"bookmark != 3\" (click)=\"changeBookmark(3)\">USTAWIENIA</button> \r\n  </div>\r\n\r\n  <div id = \"bookmark1\" *ngIf = \"bookmark === 1\">\r\n    <div  id = \"bookmarkPanelSecond\" >\r\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 1\">BIEŻĄCE</button> \r\n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 1\" (click)=\"changeBookmarkSecond(1)\">BIEŻĄCE</button>\r\n      <button class = \"bookmarkActiveSecond\" *ngIf = \"bookmarkSecond === 2\">HISTORIA</button>\r\n      <button class = \"bookmarkSecond\" *ngIf = \"bookmarkSecond != 2\" (click)=\"changeBookmarkSecond(2)\">HISTORIA</button> \r\n    </div>\r\n    <div *ngIf = \"bookmarkSecond === 1\">\r\n      <style> th { border: 2px solid black; color: green;} </style>\r\n      <style> td { border: 1px solid black; word-break:break-all; word-wrap:break-word;} </style>\r\n      <style> caption { color: black; font-weight: 700;} </style>\r\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\r\n          <caption>URZĄDZENIA</caption>\r\n        <tr>\r\n            <th style = \"width: 105px\">Nazwa</th>\r\n            <th style = \"width: 100px\">Data operacji</th>\r\n            <th style = \"width: 130px\">Rozpoznany jako</th>\r\n            <th style = \"width: 55px\">Status operacji</th>\r\n            <th style = \"width: 200px\">Opis</th>\r\n            <th style = \"width: 100px\">Producent</th>\r\n            <th style = \"width: 75px\">ID</th>\r\n            <th style = \"width: 70px\">GUID</th>\r\n            <th style = \"width: 65px\">Typ</th>\r\n        </tr>\r\n        <tr *ngFor=\"let allCurrentDevice of allCurrentDevices | paginate: { itemsPerPage: 20, currentPage: p }\">\r\n          <td >{{allCurrentDevice.deviceName}}</td>\r\n          <td >{{allCurrentDevice.operationDate}}</td>\r\n          <td >{{allCurrentDevice.recognizedAs}}</td>\r\n          <td >{{allCurrentDevice.operationStatus}}</td>\r\n          <td >{{allCurrentDevice.description}}</td>\r\n          <td >{{allCurrentDevice.producer}}</td>\r\n          <td >{{allCurrentDevice.deviceId}}</td>\r\n          <td >{{allCurrentDevice.deviceGUID}}</td>\r\n          <td >{{allCurrentDevice.deviceType}}</td>\r\n        </tr>\r\n      </table>\r\n      <pagination-controls (pageChange)=\"p = $event\" previousLabel=\"Poprzednia\"\r\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\r\n    </div>\r\n    <div *ngIf = \"bookmarkSecond === 2\">\r\n      <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\r\n          <caption>URZĄDZENIA</caption>\r\n          <tr>\r\n            <th style = \"width: 105px\">Nazwa</th>\r\n            <th style = \"width: 100px\">Data operacji</th>\r\n            <th style = \"width: 130px\">Rozpoznany jako</th>\r\n            <th style = \"width: 55px\">Status operacji</th>\r\n            <th style = \"width: 200px\">Opis</th>\r\n            <th style = \"width: 100px\">Producent</th>\r\n            <th style = \"width: 75px\">ID</th>\r\n            <th style = \"width: 70px\">GUID</th>\r\n            <th style = \"width: 65px\">Typ</th>\r\n        </tr>\r\n        <tr *ngFor=\"let allHistoryDevice of allHistoryDevices | paginate: { itemsPerPage: 20, currentPage: q }\">\r\n          <td>{{allHistoryDevice.deviceName}}</td>\r\n          <td>{{allHistoryDevice.operationDate}}</td>\r\n          <td>{{allHistoryDevice.recognizedAs}}</td>\r\n          <td>{{allHistoryDevice.operationStatus}}</td>\r\n          <td>{{allHistoryDevice.description}}</td>\r\n          <td>{{allHistoryDevice.producer}}</td>\r\n          <td>{{allHistoryDevice.deviceId}}</td>\r\n          <td>{{allHistoryDevice.deviceGUID}}</td>\r\n          <td>{{allHistoryDevice.deviceType}}</td>\r\n        </tr>\r\n      </table>\r\n      <pagination-controls (pageChange)=\"q = $event\" previousLabel=\"Poprzednia\"\r\n      nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\r\n    </div>\r\n  </div>\r\n\r\n  <div id = \"bookmark2\" *ngIf = \"bookmark === 2\">\r\n    <div  id = \"bookmarkPanelThird\" >\r\n      <button class = \"bookmarkActiveThird\" *ngIf = \"bookmarkThird === 1\">BIEŻĄCE</button> \r\n      <button class = \"bookmarkThird\" *ngIf = \"bookmarkThird != 1\" (click)=\"changeBookmarkThird(1)\">BIEŻĄCE</button>\r\n      <button class = \"bookmarkActiveThird\" *ngIf = \"bookmarkThird === 2\">HISTORIA</button>\r\n      <button class = \"bookmarkThird\" *ngIf = \"bookmarkThird != 2\" (click)=\"changeBookmarkThird(2)\">HISTORIA</button> \r\n    </div>\r\n    <div *ngIf = \"bookmarkThird === 1\">\r\n      <style> th { border: 2px solid black; color: green;} </style>\r\n      <style> td { border: 1px solid black; font-size: 12px;} </style>\r\n      <style> caption { color: black; font-weight: 700; } </style>\r\n      <!-- <table style = \"width: 1000px\"> -->\r\n        <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\r\n          <caption>DYSKI ZDALNE</caption>\r\n          <tr>\r\n            <th style = \"width: 100px\">Nazwa</th>\r\n            <th style = \"width: 100px\">Data operacji</th>\r\n            <th style = \"width: 120px\">Rozpoznany jako</th>\r\n            <th style = \"width: 100px\">Status operacji</th>\r\n            <th style = \"width: 180px\">Opis</th>\r\n            <th style = \"width: 100px\">System plików</th>\r\n            <th style = \"width: 50px\">Wolna pamięć</th>\r\n            <th style = \"width: 50px\">Całkowita pamięć</th>\r\n            <th style = \"width: 100px\">Nazwa woluminu</th>\r\n            <th style = \"width: 50px\">ID woluminu</th>\r\n            <th style = \"width: 50px\">Wyniki skanowania</th>\r\n        </tr>\r\n        <tr *ngFor=\"let allCurrentDrive of allCurrentDrives\">\r\n          <td>{{allCurrentDrive.driveName}}</td>\r\n          <td>{{allCurrentDrive.operationDate}}</td>\r\n          <td>{{allCurrentDrive.recognizedAs}}</td>\r\n          <td>{{allCurrentDrive.operationStatus}}</td>\r\n          <td>{{allCurrentDrive.description}}</td>\r\n          <td>{{allCurrentDrive.fileSystem}}</td>\r\n          <td>{{allCurrentDrive.freeSpace}}</td>\r\n          <td>{{allCurrentDrive.totalSpace}}</td>\r\n          <td>{{allCurrentDrive.volumeId}}</td>\r\n          <td>{{allCurrentDrive.volumeName}}</td>\r\n          <!-- <td> <button id = \"see\" (click) = \"show('pierwszy')\">POKAŻ</button></td> -->\r\n          <td> <button id = \"see\" (click) = \"showCurrentFiles(allCurrentDrive.id, allCurrentDrive.driveName)\">POKAŻ</button></td>\r\n        </tr>\r\n        <!-- <tr>\r\n          <td>drugi</td>\r\n          <td>cos2</td>\r\n          <td>cos1</td>\r\n          <td>cos2</td>\r\n          <td>cos1</td>\r\n          <td>cos2</td>\r\n          <td>cos2</td>\r\n          <td>cos1</td>\r\n          <td>cos2</td>\r\n          <td>cos2</td>\r\n          <td> <button id = \"see\"  (click) = \"show('drugi')\">POKAŻ</button></td>\r\n        </tr> -->\r\n      </table>\r\n\r\n      <!-- <style> th { border: 2px solid rgb(12, 221, 158); color: rgb(218, 197, 12);} </style>\r\n      <style> td { border: 1px solid rgb(12, 221, 158);} </style>\r\n      <style> caption { color: rgb(12, 221, 158); font-weight: 700;} </style> -->\r\n      <div id = \"scan\">\r\n        <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\" *ngIf = \"is2\">\r\n            <caption>WYNIKI SKANOWANIA DYSKU {{showName}} </caption>\r\n          <tr>\r\n              <th style = \"width: 160px\">Nazwa pliku</th>\r\n              <th style = \"width: 200px\">Ścieżka pliku</th>\r\n              <th style = \"width: 120px\">Typ pliku</th>\r\n              <th style = \"width: 120px\">Czy binarny</th>\r\n              <th style = \"width: 100px\">Rozszerzenie</th>\r\n              <th style = \"width: 140px\">Rozmiar</th>\r\n              <th style = \"width: 140px\">Data operacji</th>\r\n          </tr>\r\n          <tr *ngFor=\"let driveFile of driveFiles\">\r\n              <td>{{driveFile.fileName}}</td>\r\n              <td>{{driveFile.filePath}}</td>\r\n              <td>{{driveFile.fileType}}</td>\r\n              <td>{{driveFile.isBinary}}</td>\r\n              <td>{{driveFile.extension}}</td>\r\n              <td>{{driveFile.size}}</td>\r\n              <td>{{driveFile.operationDate}}</td>\r\n            </tr>\r\n        </table>\r\n      </div>\r\n    </div>\r\n      <div *ngIf = \"bookmarkThird === 2\">\r\n        <div  id = \"NotFileDesign\" *ngIf = \"!fileDesign\">\r\n          <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\r\n              <caption>DYSKI ZDALNE</caption>\r\n            <tr>\r\n                <th style = \"width: 100px\">Nazwa</th>\r\n                <th style = \"width: 100px\">Data operacji</th>\r\n                <th style = \"width: 120px\">Rozpoznany jako</th>\r\n                <th style = \"width: 100px\">Status operacji</th>\r\n                <th style = \"width: 180px\">Opis</th>\r\n                <th style = \"width: 100px\">System plików</th>\r\n                <th style = \"width: 50px\">Wolna pamięć</th>\r\n                <th style = \"width: 50px\">Całkowita pamięć</th>\r\n                <th style = \"width: 100px\">Nazwa woluminu</th>\r\n                <th style = \"width: 50px\">ID woluminu</th>\r\n                <th style = \"width: 50px\">Wyniki skanowania</th>\r\n            </tr>\r\n            <tr *ngFor=\"let allHistoryDrive of allHistoryDrives  | paginate: { itemsPerPage: 20, currentPage: r }\">\r\n              <td>{{allHistoryDrive.driveName}}</td>\r\n              <td>{{allHistoryDrive.operationDate}}</td>\r\n              <td>{{allHistoryDrive.recognizedAs}}</td>\r\n              <td>{{allHistoryDrive.operationStatus}}</td>\r\n              <td>{{allHistoryDrive.description}}</td>\r\n              <td>{{allHistoryDrive.fileSystem}}</td>\r\n              <td>{{allHistoryDrive.freeSpace}}</td>\r\n              <td>{{allHistoryDrive.totalSpace}}</td>\r\n              <td>{{allHistoryDrive.volumeId}}</td>\r\n              <td>{{allHistoryDrive.volumeName}}</td>\r\n              <td> <button id = \"see\" (click) = \"showHistoryFiles(allHistoryDrive.id, allHistoryDrive.driveName)\">POKAŻ</button></td>\r\n            </tr>\r\n            <!-- <tr>\r\n              <td>drugi</td>\r\n              <td>cos2</td>\r\n              <td>cos1</td>\r\n              <td>cos2</td>\r\n              <td>cos1</td>\r\n              <td>cos2</td>\r\n              <td>cos2</td>\r\n              <td>cos1</td>\r\n              <td>cos2</td>\r\n              <td>cos2</td>\r\n              <td> <button id = \"see\"  (click) = \"turnOnFileDesign(0)\">POKAŻ</button></td>\r\n            </tr> -->\r\n          </table>\r\n          <pagination-controls (pageChange)=\"r = $event\" previousLabel=\"Poprzednia\"\r\n          nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\r\n        </div>\r\n        <div  id = \"FileDesign\" *ngIf = \"fileDesign\">\r\n          <div id = \"backDesign\">\r\n            <label id = \"info\">Jesteś w widoku podglądu zawartości wybranego dysku</label>\r\n            <button id = \"backdesignBtn\" (click) = \"turnOffFileDesign()\"> POWRÓT</button>\r\n          </div>\r\n          <table style = \"border: 1px solid black; border-collapse: collapse; font-weight: 700;\">\r\n              <caption>WYNIKI SKANOWANIA DYSKU {{showNameHistoric}} </caption>\r\n            <tr>\r\n                <th style = \"width: 160px\">Nazwa pliku</th>\r\n                <th style = \"width: 200px\">Ścieżka pliku</th>\r\n                <th style = \"width: 120px\">Typ pliku</th>\r\n                <th style = \"width: 120px\">Czy binarny</th>\r\n                <th style = \"width: 100px\">Rozszerzenie</th>\r\n                <th style = \"width: 140px\">Rozmiar</th>\r\n                <th style = \"width: 140px\">Data operacji</th>\r\n            </tr>\r\n            <tr *ngFor=\"let historyDriveFile of historyDriveFiles   | paginate: { itemsPerPage: 20, currentPage: s }\">\r\n              <td>{{historyDriveFile.fileName}}</td>\r\n              <td>{{historyDriveFile.filePath}}</td>\r\n              <td>{{historyDriveFile.fileType}}</td>\r\n              <td>{{historyDriveFile.isBinary}}</td>\r\n              <td>{{historyDriveFile.extension}}</td>\r\n              <td>{{historyDriveFile.size}}</td>\r\n              <td>{{historyDriveFile.operationDate}}</td>\r\n            </tr>\r\n          </table>\r\n          <pagination-controls (pageChange)=\"s = $event\" previousLabel=\"Poprzednia\"\r\n          nextLabel=\"Następna\" style=\"font-size: 16px; text-align: center\"></pagination-controls>\r\n        </div>\r\n      </div>\r\n    \r\n    <div *ngIf = \"bookmarkThird === 1\">\r\n      <label class = \"information\" *ngIf = \"!is2\">Obecnie nie śledzisz dysków zdalnych, możesz to zmienić w zakładce \"USTAWIENIA\".</label>\r\n    </div>\r\n  </div>\r\n\r\n  <div id = \"bookmark3\" *ngIf = \"bookmark === 3\">\r\n    <!-- <div class = \"question1\" *ngIf = \"!is1\"> -->\r\n        <div class = \"question1\" *ngIf = \"!USBBlockSettings.blockPorts\">\r\n      <p>Czy blokować porty USB: \r\n          <font color = \"red\" >  \r\n        <label  class = \"label\"> NIE   </label> </font> \r\n        <!-- <button class = \"buttons\" (click)=\"change1()\" >ROZPOCZNIJ</button> -->\r\n        <button class = \"buttons\" (click)=\"updateUSBBlockingSettings()\" >ROZPOCZNIJ</button>\r\n      </p>\r\n    </div>\r\n    <div  class = \"question1\" *ngIf = \"USBBlockSettings.blockPorts\">\r\n      <p>Czy blokować porty USB:                                   \r\n        <font color = \"lime\" >  \r\n        <label  class = \"label\" >TAK     </label> </font>\r\n        <button class = \"buttons\"  (click)=\"updateUSBBlockingSettings()\" >ZAKOŃCZ</button>\r\n      </p>\r\n    </div>\r\n\r\n    <div class = \"question1\" *ngIf = \"!remoteMemoryMonitorAndScanSettings.isMonitored\">\r\n      <p>Czy śledzić pamięć zdalną: \r\n          <font color = \"red\" >  \r\n        <label  class = \"label\"> NIE   </label> </font> \r\n        <button class = \"buttons\" (click)=\"updateRemoteMemoryMonitoringSettings()\" >ROZPOCZNIJ</button>\r\n      </p>\r\n    </div>\r\n    <div  class = \"question1\" *ngIf = \"remoteMemoryMonitorAndScanSettings.isMonitored\">\r\n      <p>Czy śledzić pamięć zdalną:                                   \r\n        <font color = \"lime\" >  \r\n        <label  class = \"label\" >TAK     </label> </font>\r\n        <button class = \"buttons\"  (click)=\"updateRemoteMemoryMonitoringSettings()\" >ZAKOŃCZ</button>\r\n      </p>\r\n    </div>\r\n\r\n    <div class = \"question1\" *ngIf = \"!remoteMemoryMonitorAndScanSettings.isScaned\">\r\n      <p>Czy skanować pamięć zdalną: \r\n          <font color = \"red\" >  \r\n        <label  class = \"label\"> NIE   </label> </font> \r\n        <button class = \"buttons\" (click)=\"updateRemoteMemoryScaningSettings()\" >ROZPOCZNIJ</button>\r\n      </p>\r\n    </div>\r\n    <div  class = \"question1\" *ngIf = \"remoteMemoryMonitorAndScanSettings.isScaned\">\r\n      <p>Czy skanować pamięć zdalną:                                   \r\n        <font color = \"lime\" >  \r\n        <label  class = \"label\" >TAK     </label> </font>\r\n        <button class = \"buttons\"  (click)=\"updateRemoteMemoryScaningSettings()\" >ZAKOŃCZ</button>\r\n      </p>\r\n    </div>\r\n\r\n    <div class = \"question1\" *ngIf = \"!discMonitorAndScanSettings.isMonitored\">\r\n      <p>Czy śledzić napęd CD/DVD: \r\n          <font color = \"red\" >  \r\n        <label  class = \"label\"> NIE   </label> </font> \r\n        <button class = \"buttons\" (click)=\"updateDiscMonitoringSettings()\" >ROZPOCZNIJ</button>\r\n      </p>\r\n    </div>\r\n    <div  class = \"question1\" *ngIf = \"discMonitorAndScanSettings.isMonitored\">\r\n      <p>Czy śledzić napęd CD/DVD:                                   \r\n        <font color = \"lime\" >  \r\n        <label  class = \"label\" >TAK     </label> </font>\r\n        <button class = \"buttons\"  (click)=\"updateDiscMonitoringSettings()\" >ZAKOŃCZ</button>\r\n      </p>\r\n    </div>\r\n\r\n    <div class = \"question1\" *ngIf = \"!discMonitorAndScanSettings.isScaned\">\r\n      <p>Czy skanować napęd CD/DVD: \r\n          <font color = \"red\" >  \r\n        <label  class = \"label\"> NIE   </label> </font> \r\n        <button class = \"buttons\" (click)=\"updateDiscScaningSettings()\" >ROZPOCZNIJ</button>\r\n      </p>\r\n    </div>\r\n    <div  class = \"question1\" *ngIf = \"discMonitorAndScanSettings.isScaned\">\r\n      <p>Czy skanować napęd CD/DVD:                                   \r\n        <font color = \"lime\" >  \r\n        <label  class = \"label\" >TAK     </label> </font>\r\n        <button class = \"buttons\"  (click)=\"updateDiscScaningSettings()\" >ZAKOŃCZ</button>\r\n      </p>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./src/app/usb/usb.component.ts":
/*!**************************************!*\
  !*** ./src/app/usb/usb.component.ts ***!
  \**************************************/
/*! exports provided: UsbComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UsbComponent", function() { return UsbComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _data_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../data.service */ "./src/app/data.service.ts");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");
/* harmony import */ var _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../_services/authentication.service */ "./src/app/_services/authentication.service.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var UsbComponent = /** @class */ (function () {
    function UsbComponent(http, route, data, location, authenticationService) {
        var _this = this;
        this.http = http;
        this.route = route;
        this.data = data;
        this.location = location;
        this.authenticationService = authenticationService;
        this.title = "Urządzenia i pamięć";
        this.is1 = true;
        this.is2 = true;
        this.is3 = true;
        this.is4 = true;
        this.is5 = true;
        this.is6 = true;
        this.freq = 1;
        this.bookmark = 1; // 1, 2 or 3
        this.bookmarkSecond = 1; // 1, 2
        this.bookmarkThird = 1;
        this.fileDesign = false;
        this.route.params.subscribe(function (params) { return _this.userId = params.id; });
        this.token = localStorage.getItem('token');
    }
    UsbComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.data.getUser(this.userId).subscribe(function (data) { return _this.user$ = data; });
        this.logName = localStorage.getItem('username');
        this.data.getAllDevices(this.userId).subscribe(function (data) { return _this.allCurrentDevices = data; }
        // data => console.log(data)
        );
        this.data.getAllDevicesHistory(this.userId).subscribe(function (data) { return _this.allHistoryDevices = data; }
        // data => console.log(data)
        );
        this.data.getAllDiscDrives(this.userId).subscribe(function (data) { return _this.allCurrentDrives = data; }
        // data => console.log(data)
        );
        this.data.getAllDiscDrivesHistory(this.userId).subscribe(function (data) { return _this.allHistoryDrives = data; }
        // data => console.log(data)
        );
        // this.data.getUSBBlockingSettings(this.userId).subscribe( 
        //   data => this.USBSettings = data
        //   // data => console.log(data)
        // );
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["timer"])(0, 2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function () { return _this.data.getUSBBlockingSettings(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.USBBlockSettings = data; });
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["timer"])(0, 2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function () { return _this.data.getDiscMonitoringAndScaningSettings(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.discMonitorAndScanSettings = data; });
        this.subscription = Object(rxjs__WEBPACK_IMPORTED_MODULE_7__["timer"])(0, 2000).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_8__["switchMap"])(function () { return _this.data.getRemoteMemoryMonitoringAndScaningSettings(_this.userId); })
        // ).subscribe(data => console.log('poszlo', data));
        ).subscribe(function (data) { return _this.remoteMemoryMonitorAndScanSettings = data; });
        // this.subscription = timer(0, 2000).pipe(
        //   switchMap(() => this.data.getDriveFiles(this.driveId))
        // // ).subscribe(data => console.log('poszlo', data));
        // ).subscribe(data => this.driveFiles = data);
    };
    UsbComponent.prototype.updateUSBBlockingSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/usb-blocking/' + this.userId + '/change-blocking', {
        // "blockPorts": !is 
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateUSBBlockingSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateUSBBlockingSettings", error);
        });
    };
    UsbComponent.prototype.updateDiscMonitoringSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/disc-drives/' + this.userId + '/change-monitoring', {
        // "blockPorts": !is 
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateDiscMonitoringSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateDiscMonitoringSettings", error);
        });
    };
    UsbComponent.prototype.updateDiscScaningSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/disc-drives/' + this.userId + '/change-scanning', {
        // "blockPorts": !is 
        }, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateDiscScaningSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateDiscScaningSettings", error);
        });
    };
    UsbComponent.prototype.updateRemoteMemoryMonitoringSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/usb-monitoring-settings/' + this.userId + '/change-usb-monitoring-status', {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateRemoteMemoryMonitoringSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateRemoteMemoryMonitoringSettings", error);
        });
    };
    UsbComponent.prototype.updateRemoteMemoryScaningSettings = function () {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpHeaders"]()
            .set('X-Auth-Token', this.token);
        this.http.put('http://localhost:8080/api/usb-monitoring-settings/' + this.userId + '/change-usb-scanning-status', {}, { headers: headers })
            .subscribe(function (data) {
            console.log("PUT Request is successful - updateRemoteMemoryScaningSettings", data);
        }, function (error) {
            console.log("Error in put request Iwona :( - updateRemoteMemoryScaningSettings", error);
        });
    };
    UsbComponent.prototype.goBack = function () {
        // window.history.back();
        this.location.back();
    };
    //  ngOnDestroy() {
    //    this.subscription.unsubscribe();
    //  }
    UsbComponent.prototype.logout = function () {
        console.log("wylogowanie");
        this.authenticationService.logout();
        // this.router.navigate(['']);
    };
    UsbComponent.prototype.change1 = function () { this.is1 = !this.is1; };
    UsbComponent.prototype.change2 = function () { this.is2 = !this.is2; };
    UsbComponent.prototype.change3 = function () { this.is3 = !this.is3; };
    UsbComponent.prototype.change4 = function () { this.is4 = !this.is4; };
    UsbComponent.prototype.change5 = function () { this.is5 = !this.is5; };
    UsbComponent.prototype.showCurrentFiles = function (driveId, driveName) {
        var _this = this;
        this.data.getDriveFiles(driveId).subscribe(function (data) { return _this.driveFiles = data; }
        // data => console.log(data)
        );
        this.showName = driveName;
    };
    UsbComponent.prototype.showHistoryFiles = function (driveId, driveName) {
        var _this = this;
        this.fileDesign = true;
        this.data.getHistoryDriveFiles(driveId).subscribe(function (data) { return _this.historyDriveFiles = data; }
        // data => console.log(data)
        );
        this.showNameHistoric = driveName;
    };
    //  show(name)
    //  {
    //   this.showName = name;
    //  }
    UsbComponent.prototype.changeBookmark = function (nr) {
        this.bookmark = nr;
    };
    UsbComponent.prototype.changeBookmarkSecond = function (nr) {
        this.bookmarkSecond = nr;
    };
    UsbComponent.prototype.changeBookmarkThird = function (nr) {
        this.bookmarkThird = nr;
    };
    // turnOnFileDesign(nr)
    //  {
    //     this.fileDesign = true;
    //     // nr wykorzystasz do pokazania plikow z dysku o tym numerze
    //  }
    UsbComponent.prototype.turnOffFileDesign = function () {
        this.fileDesign = false;
    };
    UsbComponent.prototype.freqM = function () { };
    ;
    UsbComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-usb',
            template: __webpack_require__(/*! ./usb.component.html */ "./src/app/usb/usb.component.html"),
            styles: [__webpack_require__(/*! ./usb.component.css */ "./src/app/usb/usb.component.css"), __webpack_require__(/*! ../app.component.css */ "./src/app/app.component.css")],
            animations: [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["trigger"])('listStagger', [
                    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["transition"])('* <=> *', [
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':enter', [
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0, transform: 'translateY(-15px)' }),
                            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["stagger"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('550ms ease-out', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 1, transform: 'translateY(0px)' })))
                        ], { optional: true }),
                        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["query"])(':leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["animate"])('50ms', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_4__["style"])({ opacity: 0 })), {
                            optional: true
                        })
                    ])
                ])
            ]
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_6__["HttpClient"], _angular_router__WEBPACK_IMPORTED_MODULE_3__["ActivatedRoute"], _data_service__WEBPACK_IMPORTED_MODULE_1__["DataService"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["Location"], _services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"]])
    ], UsbComponent);
    return UsbComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! D:\HOME\UMK\III_rok\PZ\projekt_zespolowy\pz_umk_logit\Web_application\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map